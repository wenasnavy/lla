<?php
$tdatacode_list_doc = array();
$tdatacode_list_doc[".searchableFields"] = array();
$tdatacode_list_doc[".ShortName"] = "code_list_doc";
$tdatacode_list_doc[".OwnerID"] = "";
$tdatacode_list_doc[".OriginalTable"] = "m_code_list";


$tdatacode_list_doc[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatacode_list_doc[".originalPagesByType"] = $tdatacode_list_doc[".pagesByType"];
$tdatacode_list_doc[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatacode_list_doc[".originalPages"] = $tdatacode_list_doc[".pages"];
$tdatacode_list_doc[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacode_list_doc[".originalDefaultPages"] = $tdatacode_list_doc[".defaultPages"];

//	field labels
$fieldLabelscode_list_doc = array();
$fieldToolTipscode_list_doc = array();
$pageTitlescode_list_doc = array();
$placeHolderscode_list_doc = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscode_list_doc["English"] = array();
	$fieldToolTipscode_list_doc["English"] = array();
	$placeHolderscode_list_doc["English"] = array();
	$pageTitlescode_list_doc["English"] = array();
	$fieldLabelscode_list_doc["English"]["CatID"] = "Cat ID";
	$fieldToolTipscode_list_doc["English"]["CatID"] = "";
	$placeHolderscode_list_doc["English"]["CatID"] = "";
	$fieldLabelscode_list_doc["English"]["Code"] = "Code";
	$fieldToolTipscode_list_doc["English"]["Code"] = "";
	$placeHolderscode_list_doc["English"]["Code"] = "";
	$fieldLabelscode_list_doc["English"]["Description"] = "Description";
	$fieldToolTipscode_list_doc["English"]["Description"] = "";
	$placeHolderscode_list_doc["English"]["Description"] = "";
	$fieldLabelscode_list_doc["English"]["AssRec"] = "Ass Rec";
	$fieldToolTipscode_list_doc["English"]["AssRec"] = "";
	$placeHolderscode_list_doc["English"]["AssRec"] = "";
	$fieldLabelscode_list_doc["English"]["OrderNo"] = "Order No";
	$fieldToolTipscode_list_doc["English"]["OrderNo"] = "";
	$placeHolderscode_list_doc["English"]["OrderNo"] = "";
	$fieldLabelscode_list_doc["English"]["Active"] = "Active";
	$fieldToolTipscode_list_doc["English"]["Active"] = "";
	$placeHolderscode_list_doc["English"]["Active"] = "";
	$fieldLabelscode_list_doc["English"]["Rate"] = "Rate";
	$fieldToolTipscode_list_doc["English"]["Rate"] = "";
	$placeHolderscode_list_doc["English"]["Rate"] = "";
	$pageTitlescode_list_doc["English"]["edit"] = "Code List Doc, Edit [{%CatID}, {%Code}]";
	$pageTitlescode_list_doc["English"]["view"] = "Code List Doc {%CatID}, {%Code}";
	if (count($fieldToolTipscode_list_doc["English"]))
		$tdatacode_list_doc[".isUseToolTips"] = true;
}


	$tdatacode_list_doc[".NCSearch"] = true;



$tdatacode_list_doc[".shortTableName"] = "code_list_doc";
$tdatacode_list_doc[".nSecOptions"] = 0;

$tdatacode_list_doc[".mainTableOwnerID"] = "";
$tdatacode_list_doc[".entityType"] = 1;
$tdatacode_list_doc[".connId"] = "db_lla_at_localhost";


$tdatacode_list_doc[".strOriginalTableName"] = "m_code_list";

		 



$tdatacode_list_doc[".showAddInPopup"] = false;

$tdatacode_list_doc[".showEditInPopup"] = false;

$tdatacode_list_doc[".showViewInPopup"] = false;

$tdatacode_list_doc[".listAjax"] = false;
//	temporary
//$tdatacode_list_doc[".listAjax"] = false;

	$tdatacode_list_doc[".audit"] = false;

	$tdatacode_list_doc[".locking"] = false;


$pages = $tdatacode_list_doc[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacode_list_doc[".edit"] = true;
	$tdatacode_list_doc[".afterEditAction"] = 1;
	$tdatacode_list_doc[".closePopupAfterEdit"] = 1;
	$tdatacode_list_doc[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacode_list_doc[".add"] = true;
$tdatacode_list_doc[".afterAddAction"] = 1;
$tdatacode_list_doc[".closePopupAfterAdd"] = 1;
$tdatacode_list_doc[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacode_list_doc[".list"] = true;
}



$tdatacode_list_doc[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatacode_list_doc[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacode_list_doc[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacode_list_doc[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacode_list_doc[".printFriendly"] = true;
}



$tdatacode_list_doc[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacode_list_doc[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacode_list_doc[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacode_list_doc[".isUseAjaxSuggest"] = true;

$tdatacode_list_doc[".rowHighlite"] = true;



			

$tdatacode_list_doc[".ajaxCodeSnippetAdded"] = false;

$tdatacode_list_doc[".buttonsAdded"] = false;

$tdatacode_list_doc[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacode_list_doc[".isUseTimeForSearch"] = false;


$tdatacode_list_doc[".badgeColor"] = "778899";


$tdatacode_list_doc[".allSearchFields"] = array();
$tdatacode_list_doc[".filterFields"] = array();
$tdatacode_list_doc[".requiredSearchFields"] = array();

$tdatacode_list_doc[".googleLikeFields"] = array();
$tdatacode_list_doc[".googleLikeFields"][] = "CatID";
$tdatacode_list_doc[".googleLikeFields"][] = "Code";
$tdatacode_list_doc[".googleLikeFields"][] = "Description";
$tdatacode_list_doc[".googleLikeFields"][] = "AssRec";
$tdatacode_list_doc[".googleLikeFields"][] = "OrderNo";
$tdatacode_list_doc[".googleLikeFields"][] = "Active";
$tdatacode_list_doc[".googleLikeFields"][] = "Rate";



$tdatacode_list_doc[".tableType"] = "list";

$tdatacode_list_doc[".printerPageOrientation"] = 0;
$tdatacode_list_doc[".nPrinterPageScale"] = 100;

$tdatacode_list_doc[".nPrinterSplitRecords"] = 40;

$tdatacode_list_doc[".geocodingEnabled"] = false;










$tdatacode_list_doc[".pageSize"] = 20;

$tdatacode_list_doc[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatacode_list_doc[".strOrderBy"] = $tstrOrderBy;

$tdatacode_list_doc[".orderindexes"] = array();


$tdatacode_list_doc[".sqlHead"] = "SELECT CatID, 	Code, 	Description, 	AssRec, 	OrderNo, 	Active, 	Rate";
$tdatacode_list_doc[".sqlFrom"] = "FROM m_code_list";
$tdatacode_list_doc[".sqlWhereExpr"] = "CatID='DOCPL'";
$tdatacode_list_doc[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacode_list_doc[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacode_list_doc[".arrGroupsPerPage"] = $arrGPP;

$tdatacode_list_doc[".highlightSearchResults"] = true;

$tableKeyscode_list_doc = array();
$tableKeyscode_list_doc[] = "CatID";
$tableKeyscode_list_doc[] = "Code";
$tableKeyscode_list_doc[] = "Description";
$tableKeyscode_list_doc[] = "AssRec";
$tdatacode_list_doc[".Keys"] = $tableKeyscode_list_doc;


$tdatacode_list_doc[".hideMobileList"] = array();




//	CatID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "CatID";
	$fdata["GoodName"] = "CatID";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","CatID");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "CatID";

		$fdata["sourceSingle"] = "CatID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CatID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["CatID"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "CatID";
//	Code
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Code";
	$fdata["GoodName"] = "Code";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","Code");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Code";

		$fdata["sourceSingle"] = "Code";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Code";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["Code"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "Code";
//	Description
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Description";
	$fdata["GoodName"] = "Description";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","Description");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Description";

		$fdata["sourceSingle"] = "Description";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Description";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["Description"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "Description";
//	AssRec
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "AssRec";
	$fdata["GoodName"] = "AssRec";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","AssRec");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "AssRec";

		$fdata["sourceSingle"] = "AssRec";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AssRec";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["AssRec"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "AssRec";
//	OrderNo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "OrderNo";
	$fdata["GoodName"] = "OrderNo";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","OrderNo");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "OrderNo";

		$fdata["sourceSingle"] = "OrderNo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OrderNo";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["OrderNo"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "OrderNo";
//	Active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Active";
	$fdata["GoodName"] = "Active";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","Active");
	$fdata["FieldType"] = 16;


	
	
										

		$fdata["strField"] = "Active";

		$fdata["sourceSingle"] = "Active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Active";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["Active"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "Active";
//	Rate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Rate";
	$fdata["GoodName"] = "Rate";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_doc","Rate");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Rate";

		$fdata["sourceSingle"] = "Rate";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Rate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_doc["Rate"] = $fdata;
		$tdatacode_list_doc[".searchableFields"][] = "Rate";


$tables_data["code_list_doc"]=&$tdatacode_list_doc;
$field_labels["code_list_doc"] = &$fieldLabelscode_list_doc;
$fieldToolTips["code_list_doc"] = &$fieldToolTipscode_list_doc;
$placeHolders["code_list_doc"] = &$placeHolderscode_list_doc;
$page_titles["code_list_doc"] = &$pageTitlescode_list_doc;


changeTextControlsToDate( "code_list_doc" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["code_list_doc"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["code_list_doc"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_code_list_doc()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "CatID, 	Code, 	Description, 	AssRec, 	OrderNo, 	Active, 	Rate";
$proto0["m_strFrom"] = "FROM m_code_list";
$proto0["m_strWhere"] = "CatID='DOCPL'";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "CatID='DOCPL'";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "CatID",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "='DOCPL'";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "CatID",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto6["m_sql"] = "CatID";
$proto6["m_srcTableName"] = "code_list_doc";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Code",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto8["m_sql"] = "Code";
$proto8["m_srcTableName"] = "code_list_doc";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Description",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto10["m_sql"] = "Description";
$proto10["m_srcTableName"] = "code_list_doc";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "AssRec",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto12["m_sql"] = "AssRec";
$proto12["m_srcTableName"] = "code_list_doc";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "OrderNo",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto14["m_sql"] = "OrderNo";
$proto14["m_srcTableName"] = "code_list_doc";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Active",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto16["m_sql"] = "Active";
$proto16["m_srcTableName"] = "code_list_doc";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Rate",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_doc"
));

$proto18["m_sql"] = "Rate";
$proto18["m_srcTableName"] = "code_list_doc";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "m_code_list";
$proto21["m_srcTableName"] = "code_list_doc";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "CatID";
$proto21["m_columns"][] = "Code";
$proto21["m_columns"][] = "Description";
$proto21["m_columns"][] = "AssRec";
$proto21["m_columns"][] = "OrderNo";
$proto21["m_columns"][] = "Active";
$proto21["m_columns"][] = "Rate";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "m_code_list";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "code_list_doc";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="code_list_doc";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_code_list_doc = createSqlQuery_code_list_doc();


	
					
;

							

$tdatacode_list_doc[".sqlquery"] = $queryData_code_list_doc;



$tdatacode_list_doc[".hasEvents"] = false;

?>