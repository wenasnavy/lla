<?php
$tdatacode_list_kpl = array();
$tdatacode_list_kpl[".searchableFields"] = array();
$tdatacode_list_kpl[".ShortName"] = "code_list_kpl";
$tdatacode_list_kpl[".OwnerID"] = "";
$tdatacode_list_kpl[".OriginalTable"] = "m_code_list";


$tdatacode_list_kpl[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatacode_list_kpl[".originalPagesByType"] = $tdatacode_list_kpl[".pagesByType"];
$tdatacode_list_kpl[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatacode_list_kpl[".originalPages"] = $tdatacode_list_kpl[".pages"];
$tdatacode_list_kpl[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacode_list_kpl[".originalDefaultPages"] = $tdatacode_list_kpl[".defaultPages"];

//	field labels
$fieldLabelscode_list_kpl = array();
$fieldToolTipscode_list_kpl = array();
$pageTitlescode_list_kpl = array();
$placeHolderscode_list_kpl = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscode_list_kpl["English"] = array();
	$fieldToolTipscode_list_kpl["English"] = array();
	$placeHolderscode_list_kpl["English"] = array();
	$pageTitlescode_list_kpl["English"] = array();
	$fieldLabelscode_list_kpl["English"]["CatID"] = "Cat ID";
	$fieldToolTipscode_list_kpl["English"]["CatID"] = "";
	$placeHolderscode_list_kpl["English"]["CatID"] = "";
	$fieldLabelscode_list_kpl["English"]["Code"] = "Code";
	$fieldToolTipscode_list_kpl["English"]["Code"] = "";
	$placeHolderscode_list_kpl["English"]["Code"] = "";
	$fieldLabelscode_list_kpl["English"]["Description"] = "Description";
	$fieldToolTipscode_list_kpl["English"]["Description"] = "";
	$placeHolderscode_list_kpl["English"]["Description"] = "";
	$fieldLabelscode_list_kpl["English"]["AssRec"] = "Ass Rec";
	$fieldToolTipscode_list_kpl["English"]["AssRec"] = "";
	$placeHolderscode_list_kpl["English"]["AssRec"] = "";
	$fieldLabelscode_list_kpl["English"]["OrderNo"] = "Order No";
	$fieldToolTipscode_list_kpl["English"]["OrderNo"] = "";
	$placeHolderscode_list_kpl["English"]["OrderNo"] = "";
	$fieldLabelscode_list_kpl["English"]["Active"] = "Active";
	$fieldToolTipscode_list_kpl["English"]["Active"] = "";
	$placeHolderscode_list_kpl["English"]["Active"] = "";
	$fieldLabelscode_list_kpl["English"]["Rate"] = "Rate";
	$fieldToolTipscode_list_kpl["English"]["Rate"] = "";
	$placeHolderscode_list_kpl["English"]["Rate"] = "";
	$pageTitlescode_list_kpl["English"]["edit"] = "Code List Kpl, Edit [{%CatID}, {%Code}]";
	$pageTitlescode_list_kpl["English"]["view"] = "Code List Kpl {%CatID}, {%Code}";
	if (count($fieldToolTipscode_list_kpl["English"]))
		$tdatacode_list_kpl[".isUseToolTips"] = true;
}


	$tdatacode_list_kpl[".NCSearch"] = true;



$tdatacode_list_kpl[".shortTableName"] = "code_list_kpl";
$tdatacode_list_kpl[".nSecOptions"] = 0;

$tdatacode_list_kpl[".mainTableOwnerID"] = "";
$tdatacode_list_kpl[".entityType"] = 1;
$tdatacode_list_kpl[".connId"] = "db_lla_at_localhost";


$tdatacode_list_kpl[".strOriginalTableName"] = "m_code_list";

		 



$tdatacode_list_kpl[".showAddInPopup"] = false;

$tdatacode_list_kpl[".showEditInPopup"] = false;

$tdatacode_list_kpl[".showViewInPopup"] = false;

$tdatacode_list_kpl[".listAjax"] = false;
//	temporary
//$tdatacode_list_kpl[".listAjax"] = false;

	$tdatacode_list_kpl[".audit"] = false;

	$tdatacode_list_kpl[".locking"] = false;


$pages = $tdatacode_list_kpl[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacode_list_kpl[".edit"] = true;
	$tdatacode_list_kpl[".afterEditAction"] = 1;
	$tdatacode_list_kpl[".closePopupAfterEdit"] = 1;
	$tdatacode_list_kpl[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacode_list_kpl[".add"] = true;
$tdatacode_list_kpl[".afterAddAction"] = 1;
$tdatacode_list_kpl[".closePopupAfterAdd"] = 1;
$tdatacode_list_kpl[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacode_list_kpl[".list"] = true;
}



$tdatacode_list_kpl[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatacode_list_kpl[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacode_list_kpl[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacode_list_kpl[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacode_list_kpl[".printFriendly"] = true;
}



$tdatacode_list_kpl[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacode_list_kpl[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacode_list_kpl[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacode_list_kpl[".isUseAjaxSuggest"] = true;

$tdatacode_list_kpl[".rowHighlite"] = true;



			

$tdatacode_list_kpl[".ajaxCodeSnippetAdded"] = false;

$tdatacode_list_kpl[".buttonsAdded"] = false;

$tdatacode_list_kpl[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacode_list_kpl[".isUseTimeForSearch"] = false;


$tdatacode_list_kpl[".badgeColor"] = "7B68EE";


$tdatacode_list_kpl[".allSearchFields"] = array();
$tdatacode_list_kpl[".filterFields"] = array();
$tdatacode_list_kpl[".requiredSearchFields"] = array();

$tdatacode_list_kpl[".googleLikeFields"] = array();
$tdatacode_list_kpl[".googleLikeFields"][] = "CatID";
$tdatacode_list_kpl[".googleLikeFields"][] = "Code";
$tdatacode_list_kpl[".googleLikeFields"][] = "Description";
$tdatacode_list_kpl[".googleLikeFields"][] = "AssRec";
$tdatacode_list_kpl[".googleLikeFields"][] = "OrderNo";
$tdatacode_list_kpl[".googleLikeFields"][] = "Active";
$tdatacode_list_kpl[".googleLikeFields"][] = "Rate";



$tdatacode_list_kpl[".tableType"] = "list";

$tdatacode_list_kpl[".printerPageOrientation"] = 0;
$tdatacode_list_kpl[".nPrinterPageScale"] = 100;

$tdatacode_list_kpl[".nPrinterSplitRecords"] = 40;

$tdatacode_list_kpl[".geocodingEnabled"] = false;










$tdatacode_list_kpl[".pageSize"] = 20;

$tdatacode_list_kpl[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatacode_list_kpl[".strOrderBy"] = $tstrOrderBy;

$tdatacode_list_kpl[".orderindexes"] = array();


$tdatacode_list_kpl[".sqlHead"] = "SELECT CatID, 	Code, 	Description, 	AssRec, 	OrderNo, 	Active, 	Rate";
$tdatacode_list_kpl[".sqlFrom"] = "FROM m_code_list";
$tdatacode_list_kpl[".sqlWhereExpr"] = "CatID='KPL'";
$tdatacode_list_kpl[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacode_list_kpl[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacode_list_kpl[".arrGroupsPerPage"] = $arrGPP;

$tdatacode_list_kpl[".highlightSearchResults"] = true;

$tableKeyscode_list_kpl = array();
$tableKeyscode_list_kpl[] = "CatID";
$tableKeyscode_list_kpl[] = "Code";
$tableKeyscode_list_kpl[] = "Description";
$tableKeyscode_list_kpl[] = "AssRec";
$tdatacode_list_kpl[".Keys"] = $tableKeyscode_list_kpl;


$tdatacode_list_kpl[".hideMobileList"] = array();




//	CatID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "CatID";
	$fdata["GoodName"] = "CatID";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","CatID");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "CatID";

		$fdata["sourceSingle"] = "CatID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CatID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["CatID"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "CatID";
//	Code
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Code";
	$fdata["GoodName"] = "Code";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","Code");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Code";

		$fdata["sourceSingle"] = "Code";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Code";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["Code"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "Code";
//	Description
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Description";
	$fdata["GoodName"] = "Description";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","Description");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Description";

		$fdata["sourceSingle"] = "Description";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Description";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["Description"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "Description";
//	AssRec
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "AssRec";
	$fdata["GoodName"] = "AssRec";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","AssRec");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "AssRec";

		$fdata["sourceSingle"] = "AssRec";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AssRec";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["AssRec"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "AssRec";
//	OrderNo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "OrderNo";
	$fdata["GoodName"] = "OrderNo";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","OrderNo");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "OrderNo";

		$fdata["sourceSingle"] = "OrderNo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OrderNo";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["OrderNo"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "OrderNo";
//	Active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Active";
	$fdata["GoodName"] = "Active";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","Active");
	$fdata["FieldType"] = 16;


	
	
										

		$fdata["strField"] = "Active";

		$fdata["sourceSingle"] = "Active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Active";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["Active"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "Active";
//	Rate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Rate";
	$fdata["GoodName"] = "Rate";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("code_list_kpl","Rate");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Rate";

		$fdata["sourceSingle"] = "Rate";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Rate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacode_list_kpl["Rate"] = $fdata;
		$tdatacode_list_kpl[".searchableFields"][] = "Rate";


$tables_data["code_list_kpl"]=&$tdatacode_list_kpl;
$field_labels["code_list_kpl"] = &$fieldLabelscode_list_kpl;
$fieldToolTips["code_list_kpl"] = &$fieldToolTipscode_list_kpl;
$placeHolders["code_list_kpl"] = &$placeHolderscode_list_kpl;
$page_titles["code_list_kpl"] = &$pageTitlescode_list_kpl;


changeTextControlsToDate( "code_list_kpl" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["code_list_kpl"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["code_list_kpl"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_code_list_kpl()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "CatID, 	Code, 	Description, 	AssRec, 	OrderNo, 	Active, 	Rate";
$proto0["m_strFrom"] = "FROM m_code_list";
$proto0["m_strWhere"] = "CatID='KPL'";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "CatID='KPL'";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "CatID",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "='KPL'";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "CatID",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto6["m_sql"] = "CatID";
$proto6["m_srcTableName"] = "code_list_kpl";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Code",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto8["m_sql"] = "Code";
$proto8["m_srcTableName"] = "code_list_kpl";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Description",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto10["m_sql"] = "Description";
$proto10["m_srcTableName"] = "code_list_kpl";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "AssRec",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto12["m_sql"] = "AssRec";
$proto12["m_srcTableName"] = "code_list_kpl";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "OrderNo",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto14["m_sql"] = "OrderNo";
$proto14["m_srcTableName"] = "code_list_kpl";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Active",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto16["m_sql"] = "Active";
$proto16["m_srcTableName"] = "code_list_kpl";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Rate",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "code_list_kpl"
));

$proto18["m_sql"] = "Rate";
$proto18["m_srcTableName"] = "code_list_kpl";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "m_code_list";
$proto21["m_srcTableName"] = "code_list_kpl";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "CatID";
$proto21["m_columns"][] = "Code";
$proto21["m_columns"][] = "Description";
$proto21["m_columns"][] = "AssRec";
$proto21["m_columns"][] = "OrderNo";
$proto21["m_columns"][] = "Active";
$proto21["m_columns"][] = "Rate";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "m_code_list";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "code_list_kpl";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="code_list_kpl";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_code_list_kpl = createSqlQuery_code_list_kpl();


	
					
;

							

$tdatacode_list_kpl[".sqlquery"] = $queryData_code_list_kpl;



$tdatacode_list_kpl[".hasEvents"] = false;

?>