<?php
$dalTablet_deal = array();
$dalTablet_deal["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_deal["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_deal["start_date"] = array("type"=>135,"varname"=>"start_date", "name" => "start_date", "autoInc" => "0");
$dalTablet_deal["due_date"] = array("type"=>135,"varname"=>"due_date", "name" => "due_date", "autoInc" => "0");
$dalTablet_deal["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_deal["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_deal["total_harga"] = array("type"=>14,"varname"=>"total_harga", "name" => "total_harga", "autoInc" => "0");
$dalTablet_deal["harga_tatum"] = array("type"=>14,"varname"=>"harga_tatum", "name" => "harga_tatum", "autoInc" => "0");
$dalTablet_deal["harga_bangunan"] = array("type"=>14,"varname"=>"harga_bangunan", "name" => "harga_bangunan", "autoInc" => "0");
$dalTablet_deal["harga_per_m2"] = array("type"=>14,"varname"=>"harga_per_m2", "name" => "harga_per_m2", "autoInc" => "0");
$dalTablet_deal["total_biaya"] = array("type"=>14,"varname"=>"total_biaya", "name" => "total_biaya", "autoInc" => "0");
$dalTablet_deal["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_deal["close_by"] = array("type"=>200,"varname"=>"close_by", "name" => "close_by", "autoInc" => "0");
$dalTablet_deal["close_date"] = array("type"=>135,"varname"=>"close_date", "name" => "close_date", "autoInc" => "0");
$dalTablet_deal["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_deal["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_deal["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_deal["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_deal["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_deal"] = &$dalTablet_deal;
?>