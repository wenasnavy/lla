<?php
$dalTablet_dokumentasi = array();
$dalTablet_dokumentasi["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_dokumentasi["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_dokumentasi["start_date"] = array("type"=>135,"varname"=>"start_date", "name" => "start_date", "autoInc" => "0");
$dalTablet_dokumentasi["due_date"] = array("type"=>135,"varname"=>"due_date", "name" => "due_date", "autoInc" => "0");
$dalTablet_dokumentasi["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_dokumentasi["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_dokumentasi["status_lahan_pelepasan"] = array("type"=>200,"varname"=>"status_lahan_pelepasan", "name" => "status_lahan_pelepasan", "autoInc" => "0");
$dalTablet_dokumentasi["status_lahan_sertifkat"] = array("type"=>200,"varname"=>"status_lahan_sertifkat", "name" => "status_lahan_sertifkat", "autoInc" => "0");
$dalTablet_dokumentasi["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_dokumentasi["close_by"] = array("type"=>200,"varname"=>"close_by", "name" => "close_by", "autoInc" => "0");
$dalTablet_dokumentasi["close_date"] = array("type"=>135,"varname"=>"close_date", "name" => "close_date", "autoInc" => "0");
$dalTablet_dokumentasi["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_dokumentasi["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_dokumentasi["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_dokumentasi["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_dokumentasi["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_dokumentasi"] = &$dalTablet_dokumentasi;
?>