<?php
$dalTablet_input_awal = array();
$dalTablet_input_awal["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_input_awal["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_input_awal["nama_lahan"] = array("type"=>200,"varname"=>"nama_lahan", "name" => "nama_lahan", "autoInc" => "0");
$dalTablet_input_awal["batas_utara"] = array("type"=>200,"varname"=>"batas_utara", "name" => "batas_utara", "autoInc" => "0");
$dalTablet_input_awal["batas_selatan"] = array("type"=>200,"varname"=>"batas_selatan", "name" => "batas_selatan", "autoInc" => "0");
$dalTablet_input_awal["batas_barat"] = array("type"=>200,"varname"=>"batas_barat", "name" => "batas_barat", "autoInc" => "0");
$dalTablet_input_awal["batas_timur"] = array("type"=>200,"varname"=>"batas_timur", "name" => "batas_timur", "autoInc" => "0");
$dalTablet_input_awal["blok_lokasi"] = array("type"=>200,"varname"=>"blok_lokasi", "name" => "blok_lokasi", "autoInc" => "0");
$dalTablet_input_awal["nama_lokasi"] = array("type"=>200,"varname"=>"nama_lokasi", "name" => "nama_lokasi", "autoInc" => "0");
$dalTablet_input_awal["keperluan_lahan"] = array("type"=>200,"varname"=>"keperluan_lahan", "name" => "keperluan_lahan", "autoInc" => "0");
$dalTablet_input_awal["start_date"] = array("type"=>135,"varname"=>"start_date", "name" => "start_date", "autoInc" => "0");
$dalTablet_input_awal["due_date"] = array("type"=>135,"varname"=>"due_date", "name" => "due_date", "autoInc" => "0");
$dalTablet_input_awal["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_input_awal["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_input_awal["close_by"] = array("type"=>200,"varname"=>"close_by", "name" => "close_by", "autoInc" => "0");
$dalTablet_input_awal["close_date"] = array("type"=>135,"varname"=>"close_date", "name" => "close_date", "autoInc" => "0");
$dalTablet_input_awal["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_input_awal["im_file"] = array("type"=>201,"varname"=>"im_file", "name" => "im_file", "autoInc" => "0");
$dalTablet_input_awal["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_input_awal["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_input_awal["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_input_awal["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_input_awal["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_input_awal"] = &$dalTablet_input_awal;
?>