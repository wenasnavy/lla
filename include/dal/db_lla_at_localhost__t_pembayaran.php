<?php
$dalTablet_pembayaran = array();
$dalTablet_pembayaran["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_pembayaran["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_pembayaran["start_date"] = array("type"=>135,"varname"=>"start_date", "name" => "start_date", "autoInc" => "0");
$dalTablet_pembayaran["due_date"] = array("type"=>135,"varname"=>"due_date", "name" => "due_date", "autoInc" => "0");
$dalTablet_pembayaran["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_pembayaran["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_pembayaran["total_sisa_pembayaran"] = array("type"=>14,"varname"=>"total_sisa_pembayaran", "name" => "total_sisa_pembayaran", "autoInc" => "0");
$dalTablet_pembayaran["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_pembayaran["close_by"] = array("type"=>200,"varname"=>"close_by", "name" => "close_by", "autoInc" => "0");
$dalTablet_pembayaran["close_date"] = array("type"=>135,"varname"=>"close_date", "name" => "close_date", "autoInc" => "0");
$dalTablet_pembayaran["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_pembayaran["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_pembayaran["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_pembayaran["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_pembayaran["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_pembayaran"] = &$dalTablet_pembayaran;
?>