<?php
$dalTablet_pengukuran = array();
$dalTablet_pengukuran["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_pengukuran["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_pengukuran["start_date"] = array("type"=>135,"varname"=>"start_date", "name" => "start_date", "autoInc" => "0");
$dalTablet_pengukuran["due_date"] = array("type"=>135,"varname"=>"due_date", "name" => "due_date", "autoInc" => "0");
$dalTablet_pengukuran["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_pengukuran["ukuran"] = array("type"=>14,"varname"=>"ukuran", "name" => "ukuran", "autoInc" => "0");
$dalTablet_pengukuran["utipe"] = array("type"=>200,"varname"=>"utipe", "name" => "utipe", "autoInc" => "0");
$dalTablet_pengukuran["batas_utara"] = array("type"=>200,"varname"=>"batas_utara", "name" => "batas_utara", "autoInc" => "0");
$dalTablet_pengukuran["batas_utara_text"] = array("type"=>200,"varname"=>"batas_utara_text", "name" => "batas_utara_text", "autoInc" => "0");
$dalTablet_pengukuran["stipe"] = array("type"=>200,"varname"=>"stipe", "name" => "stipe", "autoInc" => "0");
$dalTablet_pengukuran["batas_selatan"] = array("type"=>200,"varname"=>"batas_selatan", "name" => "batas_selatan", "autoInc" => "0");
$dalTablet_pengukuran["batas_selatan_text"] = array("type"=>200,"varname"=>"batas_selatan_text", "name" => "batas_selatan_text", "autoInc" => "0");
$dalTablet_pengukuran["btipe"] = array("type"=>200,"varname"=>"btipe", "name" => "btipe", "autoInc" => "0");
$dalTablet_pengukuran["batas_barat"] = array("type"=>200,"varname"=>"batas_barat", "name" => "batas_barat", "autoInc" => "0");
$dalTablet_pengukuran["batas_barat_text"] = array("type"=>200,"varname"=>"batas_barat_text", "name" => "batas_barat_text", "autoInc" => "0");
$dalTablet_pengukuran["ttipe"] = array("type"=>200,"varname"=>"ttipe", "name" => "ttipe", "autoInc" => "0");
$dalTablet_pengukuran["batas_timur"] = array("type"=>200,"varname"=>"batas_timur", "name" => "batas_timur", "autoInc" => "0");
$dalTablet_pengukuran["batas_timur_text"] = array("type"=>200,"varname"=>"batas_timur_text", "name" => "batas_timur_text", "autoInc" => "0");
$dalTablet_pengukuran["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_pengukuran["maps_file"] = array("type"=>201,"varname"=>"maps_file", "name" => "maps_file", "autoInc" => "0");
$dalTablet_pengukuran["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_pengukuran["close_by"] = array("type"=>200,"varname"=>"close_by", "name" => "close_by", "autoInc" => "0");
$dalTablet_pengukuran["close_date"] = array("type"=>135,"varname"=>"close_date", "name" => "close_date", "autoInc" => "0");
$dalTablet_pengukuran["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_pengukuran["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_pengukuran["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_pengukuran["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_pengukuran["Lat"] = array("type"=>14,"varname"=>"Lat", "name" => "Lat", "autoInc" => "0");
$dalTablet_pengukuran["Lng"] = array("type"=>14,"varname"=>"Lng", "name" => "Lng", "autoInc" => "0");
$dalTablet_pengukuran["Lat2"] = array("type"=>14,"varname"=>"Lat2", "name" => "Lat2", "autoInc" => "0");
$dalTablet_pengukuran["Lng2"] = array("type"=>14,"varname"=>"Lng2", "name" => "Lng2", "autoInc" => "0");
$dalTablet_pengukuran["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_pengukuran"] = &$dalTablet_pengukuran;
?>