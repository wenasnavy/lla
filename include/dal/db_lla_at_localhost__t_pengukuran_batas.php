<?php
$dalTablet_pengukuran_batas = array();
$dalTablet_pengukuran_batas["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_pengukuran_batas["id_ukur"] = array("type"=>200,"varname"=>"id_ukur", "name" => "id_ukur", "autoInc" => "0");
$dalTablet_pengukuran_batas["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_pengukuran_batas["jenis_batas"] = array("type"=>200,"varname"=>"jenis_batas", "name" => "jenis_batas", "autoInc" => "0");
$dalTablet_pengukuran_batas["tipe"] = array("type"=>200,"varname"=>"tipe", "name" => "tipe", "autoInc" => "0");
$dalTablet_pengukuran_batas["batas"] = array("type"=>200,"varname"=>"batas", "name" => "batas", "autoInc" => "0");
$dalTablet_pengukuran_batas["batas_text"] = array("type"=>200,"varname"=>"batas_text", "name" => "batas_text", "autoInc" => "0");
$dalTablet_pengukuran_batas["lat"] = array("type"=>14,"varname"=>"lat", "name" => "lat", "autoInc" => "0");
$dalTablet_pengukuran_batas["lng"] = array("type"=>14,"varname"=>"lng", "name" => "lng", "autoInc" => "0");
$dalTablet_pengukuran_batas["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_pengukuran_batas["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_pengukuran_batas["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_pengukuran_batas["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_pengukuran_batas["updated_by"] = array("type"=>135,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_pengukuran_batas["id"]["key"]=true;
$dalTablet_pengukuran_batas["jenis_batas"]["key"]=true;

$dal_info["db_lla_at_localhost__t_pengukuran_batas"] = &$dalTablet_pengukuran_batas;
?>