<?php
$dalTablet_pengukuran_percil = array();
$dalTablet_pengukuran_percil["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_pengukuran_percil["id_ukur"] = array("type"=>200,"varname"=>"id_ukur", "name" => "id_ukur", "autoInc" => "0");
$dalTablet_pengukuran_percil["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_pengukuran_percil["no_register"] = array("type"=>200,"varname"=>"no_register", "name" => "no_register", "autoInc" => "0");
$dalTablet_pengukuran_percil["percil_name"] = array("type"=>200,"varname"=>"percil_name", "name" => "percil_name", "autoInc" => "0");
$dalTablet_pengukuran_percil["percil_date"] = array("type"=>135,"varname"=>"percil_date", "name" => "percil_date", "autoInc" => "0");
$dalTablet_pengukuran_percil["urutan"] = array("type"=>200,"varname"=>"urutan", "name" => "urutan", "autoInc" => "0");
$dalTablet_pengukuran_percil["ukuran_real"] = array("type"=>200,"varname"=>"ukuran_real", "name" => "ukuran_real", "autoInc" => "0");
$dalTablet_pengukuran_percil["param_dok"] = array("type"=>201,"varname"=>"param_dok", "name" => "param_dok", "autoInc" => "0");
$dalTablet_pengukuran_percil["file_dok"] = array("type"=>201,"varname"=>"file_dok", "name" => "file_dok", "autoInc" => "0");
$dalTablet_pengukuran_percil["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_pengukuran_percil["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_pengukuran_percil["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_pengukuran_percil["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_pengukuran_percil["updated_by"] = array("type"=>135,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_pengukuran_percil["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_pengukuran_percil"] = &$dalTablet_pengukuran_percil;
?>