<?php
$dalTablet_perm_dana = array();
$dalTablet_perm_dana["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_perm_dana["id_lahan"] = array("type"=>200,"varname"=>"id_lahan", "name" => "id_lahan", "autoInc" => "0");
$dalTablet_perm_dana["start_date_dp"] = array("type"=>135,"varname"=>"start_date_dp", "name" => "start_date_dp", "autoInc" => "0");
$dalTablet_perm_dana["due_date_dp"] = array("type"=>135,"varname"=>"due_date_dp", "name" => "due_date_dp", "autoInc" => "0");
$dalTablet_perm_dana["start_date_full"] = array("type"=>135,"varname"=>"start_date_full", "name" => "start_date_full", "autoInc" => "0");
$dalTablet_perm_dana["due_date_full"] = array("type"=>135,"varname"=>"due_date_full", "name" => "due_date_full", "autoInc" => "0");
$dalTablet_perm_dana["pic"] = array("type"=>200,"varname"=>"pic", "name" => "pic", "autoInc" => "0");
$dalTablet_perm_dana["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_perm_dana["keterangan_bayar"] = array("type"=>201,"varname"=>"keterangan_bayar", "name" => "keterangan_bayar", "autoInc" => "0");
$dalTablet_perm_dana["close_full_by"] = array("type"=>200,"varname"=>"close_full_by", "name" => "close_full_by", "autoInc" => "0");
$dalTablet_perm_dana["close_full_date"] = array("type"=>135,"varname"=>"close_full_date", "name" => "close_full_date", "autoInc" => "0");
$dalTablet_perm_dana["tipe"] = array("type"=>200,"varname"=>"tipe", "name" => "tipe", "autoInc" => "0");
$dalTablet_perm_dana["dp"] = array("type"=>14,"varname"=>"dp", "name" => "dp", "autoInc" => "0");
$dalTablet_perm_dana["sisa_pembayaran"] = array("type"=>14,"varname"=>"sisa_pembayaran", "name" => "sisa_pembayaran", "autoInc" => "0");
$dalTablet_perm_dana["biaya_admin"] = array("type"=>14,"varname"=>"biaya_admin", "name" => "biaya_admin", "autoInc" => "0");
$dalTablet_perm_dana["status"] = array("type"=>200,"varname"=>"status", "name" => "status", "autoInc" => "0");
$dalTablet_perm_dana["status_full"] = array("type"=>200,"varname"=>"status_full", "name" => "status_full", "autoInc" => "0");
$dalTablet_perm_dana["status_badmin"] = array("type"=>200,"varname"=>"status_badmin", "name" => "status_badmin", "autoInc" => "0");
$dalTablet_perm_dana["close_badmin_by"] = array("type"=>200,"varname"=>"close_badmin_by", "name" => "close_badmin_by", "autoInc" => "0");
$dalTablet_perm_dana["close_badmin_date"] = array("type"=>135,"varname"=>"close_badmin_date", "name" => "close_badmin_date", "autoInc" => "0");
$dalTablet_perm_dana["close_dp_by"] = array("type"=>200,"varname"=>"close_dp_by", "name" => "close_dp_by", "autoInc" => "0");
$dalTablet_perm_dana["close_dp_date"] = array("type"=>135,"varname"=>"close_dp_date", "name" => "close_dp_date", "autoInc" => "0");
$dalTablet_perm_dana["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_perm_dana["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_perm_dana["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_perm_dana["updated_by"] = array("type"=>200,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_perm_dana["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_perm_dana"] = &$dalTablet_perm_dana;
?>