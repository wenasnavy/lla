<?php
$dalTablet_regis_detail = array();
$dalTablet_regis_detail["id"] = array("type"=>3,"varname"=>"id", "name" => "id", "autoInc" => "1");
$dalTablet_regis_detail["id_regis"] = array("type"=>200,"varname"=>"id_regis", "name" => "id_regis", "autoInc" => "0");
$dalTablet_regis_detail["nama_lahan"] = array("type"=>200,"varname"=>"nama_lahan", "name" => "nama_lahan", "autoInc" => "0");
$dalTablet_regis_detail["no_register"] = array("type"=>200,"varname"=>"no_register", "name" => "no_register", "autoInc" => "0");
$dalTablet_regis_detail["percil_name"] = array("type"=>200,"varname"=>"percil_name", "name" => "percil_name", "autoInc" => "0");
$dalTablet_regis_detail["percil_date"] = array("type"=>7,"varname"=>"percil_date", "name" => "percil_date", "autoInc" => "0");
$dalTablet_regis_detail["urutan"] = array("type"=>200,"varname"=>"urutan", "name" => "urutan", "autoInc" => "0");
$dalTablet_regis_detail["ukuran_real"] = array("type"=>200,"varname"=>"ukuran_real", "name" => "ukuran_real", "autoInc" => "0");
$dalTablet_regis_detail["param_dok"] = array("type"=>201,"varname"=>"param_dok", "name" => "param_dok", "autoInc" => "0");
$dalTablet_regis_detail["file_dok"] = array("type"=>201,"varname"=>"file_dok", "name" => "file_dok", "autoInc" => "0");
$dalTablet_regis_detail["keterangan"] = array("type"=>201,"varname"=>"keterangan", "name" => "keterangan", "autoInc" => "0");
$dalTablet_regis_detail["created_date"] = array("type"=>135,"varname"=>"created_date", "name" => "created_date", "autoInc" => "0");
$dalTablet_regis_detail["created_by"] = array("type"=>200,"varname"=>"created_by", "name" => "created_by", "autoInc" => "0");
$dalTablet_regis_detail["updated_date"] = array("type"=>135,"varname"=>"updated_date", "name" => "updated_date", "autoInc" => "0");
$dalTablet_regis_detail["updated_by"] = array("type"=>135,"varname"=>"updated_by", "name" => "updated_by", "autoInc" => "0");
$dalTablet_regis_detail["id"]["key"]=true;

$dal_info["db_lla_at_localhost__t_regis_detail"] = &$dalTablet_regis_detail;
?>