<?php
$dalTablem_code_list = array();
$dalTablem_code_list["CatID"] = array("type"=>200,"varname"=>"CatID", "name" => "CatID", "autoInc" => "0");
$dalTablem_code_list["Code"] = array("type"=>200,"varname"=>"Code", "name" => "Code", "autoInc" => "0");
$dalTablem_code_list["Description"] = array("type"=>200,"varname"=>"Description", "name" => "Description", "autoInc" => "0");
$dalTablem_code_list["AssRec"] = array("type"=>200,"varname"=>"AssRec", "name" => "AssRec", "autoInc" => "0");
$dalTablem_code_list["OrderNo"] = array("type"=>3,"varname"=>"OrderNo", "name" => "OrderNo", "autoInc" => "0");
$dalTablem_code_list["Active"] = array("type"=>16,"varname"=>"Active", "name" => "Active", "autoInc" => "0");
$dalTablem_code_list["Rate"] = array("type"=>14,"varname"=>"Rate", "name" => "Rate", "autoInc" => "0");
$dalTablem_code_list["CatID"]["key"]=true;
$dalTablem_code_list["Code"]["key"]=true;
$dalTablem_code_list["Description"]["key"]=true;
$dalTablem_code_list["AssRec"]["key"]=true;

$dal_info["db_lla_at_localhost__tblm_code_list"] = &$dalTablem_code_list;
?>