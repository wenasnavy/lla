<?php
require_once(getabspath("classes/cipherer.php"));



$tdatadashboard = array();
$tdatadashboard[".ShortName"] = "dashboard";

$tdatadashboard[".pagesByType"] = my_json_decode( "{\"dashboard\":[\"dashboard\"],\"search\":[\"search\"]}" );
$tdatadashboard[".originalPagesByType"] = $tdatadashboard[".pagesByType"];
$tdatadashboard[".pages"] = types2pages( my_json_decode( "{\"dashboard\":[\"dashboard\"],\"search\":[\"search\"]}" ) );
$tdatadashboard[".originalPages"] = $tdatadashboard[".pages"];
$tdatadashboard[".defaultPages"] = my_json_decode( "{\"dashboard\":\"dashboard\",\"search\":\"search\"}" );
$tdatadashboard[".originalDefaultPages"] = $tdatadashboard[".defaultPages"];


//	field labels
$fieldLabelsdashboard = array();
$pageTitlesdashboard = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsdashboard["English"] = array();
}

//	search fields
$tdatadashboard[".searchFields"] = array();

// all search fields
$tdatadashboard[".allSearchFields"] = array();

// good like search fields
$tdatadashboard[".googleLikeFields"] = array();

$tdatadashboard[".dashElements"] = array();

	$dbelement = array( "elementName" => "dashboard_snippet", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_0_0";

				$dbelement["snippetId"] = "Dashboard_snippet";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet1", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_2_3";

				$dbelement["height"] = 120;
	$dbelement["snippetId"] = "Dashboard_snippet1";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet2", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_0_2";

				$dbelement["height"] = 120;
	$dbelement["snippetId"] = "Dashboard_snippet2";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet3", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_1_1";

				$dbelement["snippetId"] = "Dashboard_snippet4";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet4", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_1_2";

				$dbelement["snippetId"] = "Dashboard_snippet5";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet5", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_1_3";

				$dbelement["snippetId"] = "Dashboard_snippet6";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet6", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_2_0";

				$dbelement["height"] = 120;
	$dbelement["snippetId"] = "Dashboard_snippet7";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet7", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_2_1";

				$dbelement["height"] = 120;
	$dbelement["snippetId"] = "Dashboard_snippet8";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet8", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_2_2";

				$dbelement["height"] = 120;
	$dbelement["snippetId"] = "Dashboard_snippet9";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet9", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_3_1";

				$dbelement["snippetId"] = "Dashboard_snippet10";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "dashboard_snippet10", "table" => "Dashboard",
		 "pageName" => "","type" => 7);
	$dbelement["cellName"] = "cell_1_0";

				$dbelement["snippetId"] = "Dashboard_snippet11";


	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "t_deal_lahan_chart", "table" => "t_deal_lahan",
		 "pageName" => "","type" => 1);
	$dbelement["cellName"] = "cell_3_0";

			

	$tdatadashboard[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "t_pengukuran_percil_chart_chart", "table" => "t_pengukuran_percil_chart",
		 "pageName" => "","type" => 1);
	$dbelement["cellName"] = "cell_3_3";

			

	$tdatadashboard[".dashElements"][] = $dbelement;

$tdatadashboard[".shortTableName"] = "dashboard";
$tdatadashboard[".entityType"] = 4;




include_once(getabspath("include/dashboard_events.php"));
$tdatadashboard[".hasEvents"] = true;


$tdatadashboard[".tableType"] = "dashboard";


	
$tdatadashboard[".addPageEvents"] = false;

$tdatadashboard[".isUseAjaxSuggest"] = true;

$tables_data["Dashboard"]=&$tdatadashboard;
$field_labels["Dashboard"] = &$fieldLabelsdashboard;
$page_titles["Dashboard"] = &$pageTitlesdashboard;

?>