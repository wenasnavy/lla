<?php
class eventsBase
{
	var $events = array();
	var $maps = array();
	function exists($event, $table = "")
	{
		if($table == "")
			return (array_key_exists($event,$this->events)!==FALSE);
		else
			return isset($this->events[$event]) && isset($this->events[$event][$table]);
	}

	function existsMap($page)
	{
		return (array_key_exists($page,$this->maps)!==FALSE);
	}
}

class class_GlobalEvents extends eventsBase
{
	function __construct()
	{
	// fill list of events
		$this->events["AfterSuccessfulLogin"]=true;

		$this->events["BeforeProcessMenu"]=true;


//	onscreen events
		$this->events["_global__snippet"] = true;
		$this->events["Dashboard_snippet3"] = true;
		$this->events["t_pengukuran_map"] = true;
		$this->events["t_pengukuran_map1"] = true;
		$this->events["t_pengukuran_batas_view_map"] = true;
		$this->events["Dashboard_snippet2"] = true;
		$this->events["Dashboard_snippet"] = true;
		$this->events["Dashboard_snippet11"] = true;
		$this->events["Dashboard_snippet5"] = true;
		$this->events["Dashboard_snippet6"] = true;
		$this->events["Dashboard_snippet4"] = true;
		$this->events["Dashboard_snippet7"] = true;
		$this->events["Dashboard_snippet1"] = true;
		$this->events["Dashboard_snippet9"] = true;
		$this->events["Dashboard_snippet8"] = true;
		$this->events["Dashboard_snippet10"] = true;



		}

//	handlers

		
		
		
		
		
		
				// After successful login
function AfterSuccessfulLogin($username, $password, &$data, $pageObject)
{

		date_default_timezone_set('Asia/Jakarta');
$lastlogin= date('Y-m-d H:i:s');

$_SESSION["username"] = $username;
$_SESSION["groupid"] = $data['groupid'];
//$_SESSION["opu"] = $data["opu"];

// Place event code here.
// Use "Add Action" button to add code snippets.
;		
} // function AfterSuccessfulLogin

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
				// Welcome page: Before process
function BeforeProcessMenu($pageObject)
{

		header("Location: Dashboard_dashboard.php");
exit();

// Place event code here.
// Use "Add Action" button to add code snippets.
;		
} // function BeforeProcessMenu

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

//	onscreen events
	function event__global__snippet(&$params)
	{
	echo'<div data-dashtype="7" id="dashelement_accessories_snippet1" data-tablocation="above" data-itemtype="dashboard-item" data-itemid="dashboard-item4" data-pageid="1" data-page="dashboard_dashboard">
	<div class="r-box-right"><div class="r-box-main"><div class="r-box-text"><div class="r-box-header">Progress Data Percil</div><div class="r-box-body"><table width="100%" border="0">
<tbody><tr>
<td width="50%"><font size="2">F.Payment</font></td><td width="50%"><font size="2">B. Admin</font></td>
</tr>
<tr>
<td><font size="2">150%</font></td><td><font size="2">150%</font></td>
</tr>
</tbody></table></div></div><div class="r-box-icon"><span data-icontype="text" class="r-panel-icon"><span class="fa fa-gift"></span></span> </div></div></div>
</div>';
	;
}
	function event_Dashboard_snippet3(&$params)
	{
	


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_red{
  background-color:red !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#f5f5f5;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_red glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_red'>
<b>Total Assets</b><br><br>
<font face='Arial'>150    200</font>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet2(&$params)
	{
	$year=date('Y');
$sql = "select * from t_target_percil where `year`='$year'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 150px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_green{
  background-color:#24963e !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#24963e;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_green glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_green'>
<b>Percil   $data[year]</b>
<table border=0 width=75%>
<tr><td>&nbsp</td></tr>
<tr>
<td><font face='Arial' size='6'>$data[target_percil] Percil</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet(&$params)
	{
	$year=date('Y');
$sql = "select * from t_target_lahan where `year`='$year'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 150px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_red{
  background-color:#dc3545 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#f5f5f5;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_red glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_red'>
<b>Target Bebas   $data[year]</b>
<table border=0 width=75%>
<tr><td>&nbsp</td></tr>
<tr>
<td><font face='Arial' size='6'>$data[target_lahan] Ha</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet11(&$params)
	{
	$year=date('Y');
$sql = "select count(ta.id_lahan) as jml, tl.year, tl.target_lahan from t_input_awal ta
left join t_target_lahan tl on tl.year = date_format(ta.created_date,'%Y') 
where date_format(ta.created_date,'%Y') = date_format(now(),'%Y')";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_magenta{
  background-color:#FF32FF !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#FF32FF;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_magenta glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_magenta'>
<b>Input Awal $data[year]</b>
<table border=0 width=75%>
<tr><td>Jumlah Data</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml]</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet5(&$params)
	{
	$year=date('Y');
$sql = "select tn.id_lahan, sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_negosiasi tn
left join t_pengukuran tp on tp.id_lahan = tn.id_lahan 
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y') 
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and tn.`status`='CLOSED'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);

$sql2 = "select tn.id_lahan, sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_negosiasi tn
left join t_pengukuran tp on tp.id_lahan = tn.id_lahan 
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y') 
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and tn.`status`='ONPROGRESS'";
$rs2 = CustomQuery($sql2);
$data2 = db_fetch_array($rs2);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_cyan{
  background-color:#1fdb96 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#1fdb96;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_cyan glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_cyan'>
<b>Negosiasi $data[year]</b>
<table border=0 width=75%>
<tr><td>Done</td><td>OnProgress</td><td>Persentase</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml_ha] Ha</font></td> <td><font face='Arial' size='4'>$data2[jml_ha] Ha</font></td> <td><font face='Arial' size='4'>$data[persentase] %</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet6(&$params)
	{
	$year=date('Y');
$sql = "select td.id_lahan, sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_deal td
left join t_pengukuran tp on tp.id_lahan = td.id_lahan 
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y') 
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED';";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_magenta{
  background-color:#FF32FF !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#FF32FF;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_magenta glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_magenta'>
<b>Deal </b>
<table border=0 width=75%>
<tr><td>Done</td><td>Sisa</td><td>Persentase</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml_ha] Ha</font></td><td><font face='Arial' size='4'>$data[sisa] Ha</font></td> <td><font face='Arial' size='4'>$data[persentase] %</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet4(&$params)
	{
	$year=date('Y');
$sql = "select sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_pengukuran tp
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and tp.`status`='CLOSED'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_blue{
  background-color:#17a2b8 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#17a2b8;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_blue glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_blue'>
<b>Pengukuran   $data[year]</b>
<table border=0 width=75%>
<tr><td>Done</td><td>Sisa</td><td>Persentase</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml_ha] Ha</font></td><td><font face='Arial' size='4'>$data[sisa] Ha</font></td> <td><font face='Arial' size='4'>$data[persentase] %</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet7(&$params)
	{
	$year=date('Y');
$sql = "select tdk.id_lahan, sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_dokumentasi tdk
left join t_pengukuran tp on tp.id_lahan = tdk.id_lahan 
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y') 
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and tdk.`status`='CLOSED'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);

$sql2 = "select tdk.id_lahan, sum(tp.ukuran) as jml_mp, round(sum(tp.ukuran)/10000,2) as jml_ha, tl.year, tl.target_lahan, tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa, round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase from t_dokumentasi tdk
left join t_pengukuran tp on tp.id_lahan = tdk.id_lahan 
left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y') 
where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and tdk.`status`='ONPROGRESS'";
$rs2 = CustomQuery($sql2);
$data2 = db_fetch_array($rs2);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_blue{
  background-color:#ffc107 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#ffc107;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_yellow glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_yellow'>
<b>Dokumentasi $data[year]</b>
<table border=0 width=75%>
<tr><td>Done</td><td>OnProgress</td><td>Persentase</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml] Ha</font></td><td><font face='Arial' size='4'>$data2[sisa] Ha</font></td> <td><font face='Arial' size='4'>$data[persentase] %</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet1(&$params)
	{
	$year=date('Y');
$sql = "select count(tpp.id) as jml_data, ttp.target_percil, sum(jml_done) as jml_done, sum(jml_prog) as jml_prog, sum(jml_done)/ttp.target_percil*100 as persentase from t_pengukuran_percil tpp
left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y') 
LEFT JOIN (
	SELECT id, count(id) AS jml_done FROM t_pengukuran_percil where DATE_FORMAT(t_pengukuran_percil.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null
)jml_done ON jml_done.id = tpp.id
LEFT JOIN (
	SELECT id, count(id) AS jml_prog FROM t_pengukuran_percil where DATE_FORMAT(t_pengukuran_percil.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is null
)jml_prog ON jml_prog.id = tpp.id

where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y')";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_yellow{
  background-color:#ffc107 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#ffc107;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_yellow glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_yellow'>
<b>Register Lahan $data[year]</b>
<table border=0 width=75%>
<tr><td>Done</td><td>OnProgress</td><td>Persentase</td></tr>
<tr>
<td><font face='Arial' size='4'>$data[jml_done] Percil</font></td><td><font face='Arial' size='4'>$data[jml_prog] Percil</font></td> <td><font face='Arial' size='4'>".round($data['persentase'],2)."%</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet9(&$params)
	{
	$year=date('Y');
$sql = "Select count(tpd.id_lahan) as jml_data, sum(dpayment) as dpayment, sum(badmin) as badmin, sum(spayment) as spayment from t_perm_dana tpd
LEFT JOIN (
	SELECT id_lahan, SUM(dp/1000000) AS dpayment FROM t_perm_dana where status = 'CLOSED'
)dp ON dp.id_lahan = tpd.id_lahan
LEFT JOIN (
	SELECT id_lahan, SUM(biaya_admin/1000000) AS badmin FROM t_perm_dana where status_badmin = 'CLOSED'
)admin ON admin.id_lahan = tpd.id_lahan
LEFT JOIN (
	SELECT id_lahan, SUM(sisa_pembayaran/1000000) AS spayment FROM t_perm_dana where status_full = 'CLOSED'
)sisafull ON sisafull.id_lahan = tpd.id_lahan

where date_format(tpd.created_date,'%Y') = date_format(now(),'%Y')";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_blue{
  background-color:#17a2b8 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#17a2b8;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_blue glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_blue'>
<b>Pembayaran $data[year]</b>
<table border=0 width=75%>
<tr><td>DP</td><td>Sisa/Full</td><td>Biaya Adm</td></tr>
<tr>
<td><font face='Arial' size='3'>".round($data['dpayment'],2)." Jt</font></td> <td><font face='Arial' size='3'>".round($data['spayment'],2)." Jt</font></td> <td><font face='Arial' size='3'>".round($data['badmin'],2)." Jt</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet8(&$params)
	{
	$year=date('Y');
$sql = "select tpd.id_lahan, sum(tpd.dp/1000000) as dp, sum(tpd.sisa_pembayaran/1000000) as sisa_pembayaran, sum(tpd.biaya_admin/1000000) as biaya_admin from t_perm_dana tpd
where date_format(tpd.created_date,'%Y') = date_format(now(),'%Y')";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);


$res = "
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_blue{
  background-color:#ffc107 !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#ffc107;
	color:white;
}
</style>
<div class='info-box'>
<span class='info-img img_bgcolor_red glyphicon glyphicon-folder-open'></span>
<div class='dashtext img_bgcolor_red'>
<b>Pengajuan Dana $data[year]</b>
<table border=0 width=75%>
<tr><td>DP</td><td>Sisa/Full</td><td>Biaya Adm</td></tr>
<tr>
<td><font face='Arial' size='3'>".round($data['dp'],2)." Jt</font></td> <td><font face='Arial' size='3'>".round($data['sisa_pembayaran'],2)." Jt</font></td> <td><font face='Arial' size='3'>".round($data['biaya_admin'],2)." Jt</font></td>
</tr>
</table>
</div>
</div>";
echo $res;
	;
}
	function event_Dashboard_snippet10(&$params)
	{
	$year=date('Y');
$sqlpercil = "select ttp.`year`, target_percil, ifnull(jml_done,0) as jml_done, target_percil-ifnull(jml_done,0) as sisa from t_target_percil ttp
LEFT JOIN (
	SELECT count(id) as jml_done, DATE_FORMAT(percil_date,'%Y') as `year` FROM t_pengukuran_percil where no_register is not null
	group by `year`
)jml_done ON jml_done.`year` = ttp.`year`

where ttp.`year`<> DATE_FORMAT(now(),'%Y')
group by ttp.`year`
order by ttp.`year` desc";
$rspercil = CustomQuery($sqlpercil);


$res ="
<style>
.info-box{
    display: block;
  min-height: 95px;
  width: 100%;
  border-radius: 5px;
}
.info-box a{
text-decoration:none; 
}
.info-img{
	border-radius: 5px; 
  display: block;
  float: right;
  height: 90px;
  width: 90px;
  text-align: center;
  font-size: 28px;
  line-height: 90px;
  color:white;
}
.img_bgcolor_gray{
  background-color:#6d767e !important;
}
.dashtext{
  border-radius: 5px;  
  min-height: 91px;
  padding:7px;
  padding-left:10px;
  line-height: 1.6em;
  background-color:#6d767e;
	color:white;
}
</style>
<div class='info-box'>

<div class='dashtext img_bgcolor_gray' align=center>
<b>Data Percil Back Year</b><br><br>
<table border=0 width=75% align=center>
<tr align=center><td>Tahun</td><td>Jml</td><td>Done</td><td>Sisa</td></tr>";
while($datapercil = db_fetch_array($rspercil))
{
$res.="<tr align=center>
<td><font face='Arial' size='3'>$datapercil[year]</font></td> <td><font face='Arial' size='3'>$datapercil[target_percil]</font></td>
<td><font face='Arial' size='3'>$datapercil[jml_done] Percil</font></td>
<td><font face='Arial' size='3'>$datapercil[sisa] Percil</font></td>
</tr>";
}

$res .="</table>
</div>
</div>";
echo $res;
	;
}

	function event_t_pengukuran_map(&$params)
	{
		$mapSettings['id'] = "t_pengukuran_map";
	// Longitude and latitude or address field should be specified
// name of field in table that used as address for map
//$mapSettings["addressField"] = "Address";

// name of field in table that used as latitude for map
$mapSettings["LatField"] = "-6.168819294634154";

// name of field in table that used as longitude for map
$mapSettings["LngField"] = "106.81867998233238";

// You can specify a custom marker icon.
// Enter either a file name for all markers:
//  $mapSettings["markerIcon"] = "images/Smile.png";
// Or specify a field from your SQL query that contains marker name:
//  $mapSettings["markerField"] = "map_icon";
// The query field should contain a path tho the image file
// Leave both fields blank to use default marker.
$mapSettings["markerIcon"] = "";
$mapSettings["markerField"] = "";

// width of map in px
$mapSettings["width"] = 1200;
// height of map in px
$mapSettings["height"] = 450;
//$mapSettings['description'] = 'Description'; // a field that contains the marker description

// uncomment the next line if you need a fixed zoom for your map. Valid values are 1-20
$mapSettings['zoom'] = 13;

// Google maps only.
// Make the markers join in clusters when zoom is wide.
// This feature works only with markers with coordinates specified.
//$mapSettings['clustering'] = true;


// Google maps only.
// Display markers in a form of a heat map.
// This feature works only with markers with coordinates specified.
//$mapSettings['heatMap'] = true;

// uncomment the next line to make map markers lead to Edit pages.
// By default clicks on markers open View pages.
// $mapSettings["markerAsEditLink"] = true;

// Display Center map link in the address field
// 1 - display center link near the address
// 2 - center map when clicking on the address itself
 $mapSettings['showCenterLink'] = 1;
// Center Link text
 $mapSettings['centerLinkText'] = 'Center';

// uncomment the next line if you want to show user's location on the map
// $mapSettings['showCurrentLocation'] = true;
// optional icon for current location marker
// $mapSettings['currentLocationIcon'] = "images/smile.png";


DisplayMap($mapSettings);
	;
}
	function event_t_pengukuran_map1(&$params)
	{
		$mapSettings['id'] = "t_pengukuran_map1";
	// Longitude and latitude or address field should be specified
// name of field in table that used as address for map
//$mapSettings["addressField"] = "Address";

// name of field in table that used as latitude for map
$mapSettings["latField"] = "Lat";

// name of field in table that used as longitude for map
$mapSettings["lngField"] = "Lng";

// You can specify a custom marker icon.
// Enter either a file name for all markers:
//  $mapSettings["markerIcon"] = "images/Smile.png";
// Or specify a field from your SQL query that contains marker name:
//  $mapSettings["markerField"] = "map_icon";
// The query field should contain a path tho the image file
// Leave both fields blank to use default marker.
$mapSettings["markerIcon"] = "";
$mapSettings["markerField"] = "";

// width of map in px
$mapSettings["width"] = 1200;
// height of map in px
$mapSettings["height"] = 450;
//$mapSettings['description'] = 'Description'; // a field that contains the marker description

// uncomment the next line if you need a fixed zoom for your map. Valid values are 1-20
//$mapSettings['zoom'] = 13;

// Google maps only.
// Make the markers join in clusters when zoom is wide.
// This feature works only with markers with coordinates specified.
//$mapSettings['clustering'] = true;


// Google maps only.
// Display markers in a form of a heat map.
// This feature works only with markers with coordinates specified.
//$mapSettings['heatMap'] = true;

// uncomment the next line to make map markers lead to Edit pages.
// By default clicks on markers open View pages.
// $mapSettings["markerAsEditLink"] = true;

// Display Center map link in the address field
// 1 - display center link near the address
// 2 - center map when clicking on the address itself
// $mapSettings['showCenterLink'] = 1;
// Center Link text
// $mapSettings['centerLinkText'] = 'Center';

// uncomment the next line if you want to show user's location on the map
// $mapSettings['showCurrentLocation'] = true;
// optional icon for current location marker
// $mapSettings['currentLocationIcon'] = "images/smile.png";


DisplayMap($mapSettings);
	;
}
	function event_t_pengukuran_batas_view_map(&$params)
	{
		$mapSettings['id'] = "t_pengukuran_batas_view_map";
	// Longitude and latitude or address field should be specified
// name of field in table that used as address for map
//$mapSettings["addressField"] = "Address";

// name of field in table that used as latitude for map
$mapSettings["latField"] = "lat";

// name of field in table that used as longitude for map
$mapSettings["lngField"] = "lng";

// You can specify a custom marker icon.
// Enter either a file name for all markers:
//  $mapSettings["markerIcon"] = "images/Smile.png";
// Or specify a field from your SQL query that contains marker name:
//  $mapSettings["markerField"] = "map_icon";
// The query field should contain a path tho the image file
// Leave both fields blank to use default marker.
$mapSettings["markerIcon"] = "";
$mapSettings["markerField"] = "";

// width of map in px
$mapSettings["width"] = 1200;
// height of map in px
$mapSettings["height"] = 450;
//$mapSettings['description'] = 'Description'; // a field that contains the marker description

// uncomment the next line if you need a fixed zoom for your map. Valid values are 1-20
//$mapSettings['zoom'] = 13;

// Google maps only.
// Make the markers join in clusters when zoom is wide.
// This feature works only with markers with coordinates specified.
//$mapSettings['clustering'] = true;


// Google maps only.
// Display markers in a form of a heat map.
// This feature works only with markers with coordinates specified.
//$mapSettings['heatMap'] = true;

// uncomment the next line to make map markers lead to Edit pages.
// By default clicks on markers open View pages.
// $mapSettings["markerAsEditLink"] = true;

// Display Center map link in the address field
// 1 - display center link near the address
// 2 - center map when clicking on the address itself
// $mapSettings['showCenterLink'] = 1;
// Center Link text
// $mapSettings['centerLinkText'] = 'Center';

// uncomment the next line if you want to show user's location on the map
// $mapSettings['showCurrentLocation'] = true;
// optional icon for current location marker
// $mapSettings['currentLocationIcon'] = "images/smile.png";


DisplayMap($mapSettings);
	;
}



}
?>
