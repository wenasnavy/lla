<?php
$tdatallauggroups = array();
$tdatallauggroups[".searchableFields"] = array();
$tdatallauggroups[".ShortName"] = "llauggroups";
$tdatallauggroups[".OwnerID"] = "";
$tdatallauggroups[".OriginalTable"] = "llauggroups";


$tdatallauggroups[".pagesByType"] = my_json_decode( "{}" );
$tdatallauggroups[".originalPagesByType"] = $tdatallauggroups[".pagesByType"];
$tdatallauggroups[".pages"] = types2pages( my_json_decode( "{}" ) );
$tdatallauggroups[".originalPages"] = $tdatallauggroups[".pages"];
$tdatallauggroups[".defaultPages"] = my_json_decode( "{}" );
$tdatallauggroups[".originalDefaultPages"] = $tdatallauggroups[".defaultPages"];

//	field labels
$fieldLabelsllauggroups = array();
$fieldToolTipsllauggroups = array();
$pageTitlesllauggroups = array();
$placeHoldersllauggroups = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsllauggroups["English"] = array();
	$fieldToolTipsllauggroups["English"] = array();
	$placeHoldersllauggroups["English"] = array();
	$pageTitlesllauggroups["English"] = array();
	$fieldLabelsllauggroups["English"]["GroupID"] = "Group ID";
	$fieldToolTipsllauggroups["English"]["GroupID"] = "";
	$placeHoldersllauggroups["English"]["GroupID"] = "";
	$fieldLabelsllauggroups["English"]["Label"] = "Label";
	$fieldToolTipsllauggroups["English"]["Label"] = "";
	$placeHoldersllauggroups["English"]["Label"] = "";
	$fieldLabelsllauggroups["English"]["Provider"] = "Provider";
	$fieldToolTipsllauggroups["English"]["Provider"] = "";
	$placeHoldersllauggroups["English"]["Provider"] = "";
	$fieldLabelsllauggroups["English"]["Comment"] = "Comment";
	$fieldToolTipsllauggroups["English"]["Comment"] = "";
	$placeHoldersllauggroups["English"]["Comment"] = "";
	if (count($fieldToolTipsllauggroups["English"]))
		$tdatallauggroups[".isUseToolTips"] = true;
}


	$tdatallauggroups[".NCSearch"] = true;



$tdatallauggroups[".shortTableName"] = "llauggroups";
$tdatallauggroups[".nSecOptions"] = 0;

$tdatallauggroups[".mainTableOwnerID"] = "";
$tdatallauggroups[".entityType"] = 0;
$tdatallauggroups[".connId"] = "db_lla_at_localhost";


$tdatallauggroups[".strOriginalTableName"] = "llauggroups";

		 



$tdatallauggroups[".showAddInPopup"] = false;

$tdatallauggroups[".showEditInPopup"] = false;

$tdatallauggroups[".showViewInPopup"] = false;

$tdatallauggroups[".listAjax"] = false;
//	temporary
//$tdatallauggroups[".listAjax"] = false;

	$tdatallauggroups[".audit"] = false;

	$tdatallauggroups[".locking"] = false;


$pages = $tdatallauggroups[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatallauggroups[".edit"] = true;
	$tdatallauggroups[".afterEditAction"] = 1;
	$tdatallauggroups[".closePopupAfterEdit"] = 1;
	$tdatallauggroups[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatallauggroups[".add"] = true;
$tdatallauggroups[".afterAddAction"] = 1;
$tdatallauggroups[".closePopupAfterAdd"] = 1;
$tdatallauggroups[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatallauggroups[".list"] = true;
}



$tdatallauggroups[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatallauggroups[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatallauggroups[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatallauggroups[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatallauggroups[".printFriendly"] = true;
}



$tdatallauggroups[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatallauggroups[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatallauggroups[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatallauggroups[".isUseAjaxSuggest"] = true;

$tdatallauggroups[".rowHighlite"] = true;



			

$tdatallauggroups[".ajaxCodeSnippetAdded"] = false;

$tdatallauggroups[".buttonsAdded"] = false;

$tdatallauggroups[".addPageEvents"] = false;

// use timepicker for search panel
$tdatallauggroups[".isUseTimeForSearch"] = false;


$tdatallauggroups[".badgeColor"] = "4169E1";


$tdatallauggroups[".allSearchFields"] = array();
$tdatallauggroups[".filterFields"] = array();
$tdatallauggroups[".requiredSearchFields"] = array();

$tdatallauggroups[".googleLikeFields"] = array();
$tdatallauggroups[".googleLikeFields"][] = "GroupID";
$tdatallauggroups[".googleLikeFields"][] = "Label";
$tdatallauggroups[".googleLikeFields"][] = "Provider";
$tdatallauggroups[".googleLikeFields"][] = "Comment";



$tdatallauggroups[".tableType"] = "list";

$tdatallauggroups[".printerPageOrientation"] = 0;
$tdatallauggroups[".nPrinterPageScale"] = 100;

$tdatallauggroups[".nPrinterSplitRecords"] = 40;

$tdatallauggroups[".geocodingEnabled"] = false;










$tdatallauggroups[".pageSize"] = 20;

$tdatallauggroups[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatallauggroups[".strOrderBy"] = $tstrOrderBy;

$tdatallauggroups[".orderindexes"] = array();


$tdatallauggroups[".sqlHead"] = "SELECT GroupID,  	`Label`,  	Provider,  	`Comment`";
$tdatallauggroups[".sqlFrom"] = "FROM llauggroups";
$tdatallauggroups[".sqlWhereExpr"] = "";
$tdatallauggroups[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatallauggroups[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatallauggroups[".arrGroupsPerPage"] = $arrGPP;

$tdatallauggroups[".highlightSearchResults"] = true;

$tableKeysllauggroups = array();
$tableKeysllauggroups[] = "GroupID";
$tdatallauggroups[".Keys"] = $tableKeysllauggroups;


$tdatallauggroups[".hideMobileList"] = array();




//	GroupID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "GroupID";
	$fdata["GoodName"] = "GroupID";
	$fdata["ownerTable"] = "llauggroups";
	$fdata["Label"] = GetFieldLabel("llauggroups","GroupID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "GroupID";

		$fdata["sourceSingle"] = "GroupID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "GroupID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatallauggroups["GroupID"] = $fdata;
		$tdatallauggroups[".searchableFields"][] = "GroupID";
//	Label
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Label";
	$fdata["GoodName"] = "Label";
	$fdata["ownerTable"] = "llauggroups";
	$fdata["Label"] = GetFieldLabel("llauggroups","Label");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Label";

		$fdata["sourceSingle"] = "Label";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`Label`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=300";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatallauggroups["Label"] = $fdata;
		$tdatallauggroups[".searchableFields"][] = "Label";
//	Provider
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Provider";
	$fdata["GoodName"] = "Provider";
	$fdata["ownerTable"] = "llauggroups";
	$fdata["Label"] = GetFieldLabel("llauggroups","Provider");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Provider";

		$fdata["sourceSingle"] = "Provider";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Provider";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatallauggroups["Provider"] = $fdata;
		$tdatallauggroups[".searchableFields"][] = "Provider";
//	Comment
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Comment";
	$fdata["GoodName"] = "Comment";
	$fdata["ownerTable"] = "llauggroups";
	$fdata["Label"] = GetFieldLabel("llauggroups","Comment");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "Comment";

		$fdata["sourceSingle"] = "Comment";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`Comment`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatallauggroups["Comment"] = $fdata;
		$tdatallauggroups[".searchableFields"][] = "Comment";


$tables_data["llauggroups"]=&$tdatallauggroups;
$field_labels["llauggroups"] = &$fieldLabelsllauggroups;
$fieldToolTips["llauggroups"] = &$fieldToolTipsllauggroups;
$placeHolders["llauggroups"] = &$placeHoldersllauggroups;
$page_titles["llauggroups"] = &$pageTitlesllauggroups;


changeTextControlsToDate( "llauggroups" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["llauggroups"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["llauggroups"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_llauggroups()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "GroupID,  	`Label`,  	Provider,  	`Comment`";
$proto0["m_strFrom"] = "FROM llauggroups";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "GroupID",
	"m_strTable" => "llauggroups",
	"m_srcTableName" => "llauggroups"
));

$proto6["m_sql"] = "GroupID";
$proto6["m_srcTableName"] = "llauggroups";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Label",
	"m_strTable" => "llauggroups",
	"m_srcTableName" => "llauggroups"
));

$proto8["m_sql"] = "`Label`";
$proto8["m_srcTableName"] = "llauggroups";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Provider",
	"m_strTable" => "llauggroups",
	"m_srcTableName" => "llauggroups"
));

$proto10["m_sql"] = "Provider";
$proto10["m_srcTableName"] = "llauggroups";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Comment",
	"m_strTable" => "llauggroups",
	"m_srcTableName" => "llauggroups"
));

$proto12["m_sql"] = "`Comment`";
$proto12["m_srcTableName"] = "llauggroups";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "llauggroups";
$proto15["m_srcTableName"] = "llauggroups";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "GroupID";
$proto15["m_columns"][] = "Label";
$proto15["m_columns"][] = "Provider";
$proto15["m_columns"][] = "Comment";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "llauggroups";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "llauggroups";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="llauggroups";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_llauggroups = createSqlQuery_llauggroups();


	
					
;

				

$tdatallauggroups[".sqlquery"] = $queryData_llauggroups;



$tdatallauggroups[".hasEvents"] = false;

?>