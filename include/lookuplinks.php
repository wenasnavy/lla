<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

		if( !isset( $lookupTableLinks["llauggroups"] ) ) {
			$lookupTableLinks["llauggroups"] = array();
		}
		if( !isset( $lookupTableLinks["llauggroups"]["admin_users.groupid"] )) {
			$lookupTableLinks["llauggroups"]["admin_users.groupid"] = array();
		}
		$lookupTableLinks["llauggroups"]["admin_users.groupid"]["edit"] = array("table" => "admin_users", "field" => "groupid", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new"] ) ) {
			$lookupTableLinks["t_input_awal_new"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new"]["t_pengukuran.id_lahan"] )) {
			$lookupTableLinks["t_input_awal_new"]["t_pengukuran.id_lahan"] = array();
		}
		$lookupTableLinks["t_input_awal_new"]["t_pengukuran.id_lahan"]["add"] = array("table" => "t_pengukuran", "field" => "id_lahan", "page" => "add");
		if( !isset( $lookupTableLinks["t_input_awal"] ) ) {
			$lookupTableLinks["t_input_awal"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal"]["t_pengukuran.id_lahan"] )) {
			$lookupTableLinks["t_input_awal"]["t_pengukuran.id_lahan"] = array();
		}
		$lookupTableLinks["t_input_awal"]["t_pengukuran.id_lahan"]["search"] = array("table" => "t_pengukuran", "field" => "id_lahan", "page" => "search");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran.utipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran.utipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran.utipe"]["edit"] = array("table" => "t_pengukuran", "field" => "utipe", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran.stipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran.stipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran.stipe"]["edit"] = array("table" => "t_pengukuran", "field" => "stipe", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new"] ) ) {
			$lookupTableLinks["t_input_awal_new"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_selatan"] )) {
			$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_selatan"] = array();
		}
		$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_selatan"]["edit"] = array("table" => "t_pengukuran", "field" => "batas_selatan", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran.btipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran.btipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran.btipe"]["edit"] = array("table" => "t_pengukuran", "field" => "btipe", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new"] ) ) {
			$lookupTableLinks["t_input_awal_new"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_barat"] )) {
			$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_barat"] = array();
		}
		$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_barat"]["edit"] = array("table" => "t_pengukuran", "field" => "batas_barat", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran.ttipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran.ttipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran.ttipe"]["edit"] = array("table" => "t_pengukuran", "field" => "ttipe", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new"] ) ) {
			$lookupTableLinks["t_input_awal_new"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_timur"] )) {
			$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_timur"] = array();
		}
		$lookupTableLinks["t_input_awal_new"]["t_pengukuran.batas_timur"]["edit"] = array("table" => "t_pengukuran", "field" => "batas_timur", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_pengukuran.status"] )) {
			$lookupTableLinks["code_list_status"]["t_pengukuran.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_pengukuran.status"]["edit"] = array("table" => "t_pengukuran", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["t_pengukuran"] ) ) {
			$lookupTableLinks["t_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"] )) {
			$lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"] = array();
		}
		$lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"]["add"] = array("table" => "t_negosiasi", "field" => "id_lahan", "page" => "add");
		if( !isset( $lookupTableLinks["t_pengukuran"] ) ) {
			$lookupTableLinks["t_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"] )) {
			$lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"] = array();
		}
		$lookupTableLinks["t_pengukuran"]["t_negosiasi.id_lahan"]["search"] = array("table" => "t_negosiasi", "field" => "id_lahan", "page" => "search");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_negosiasi.status"] )) {
			$lookupTableLinks["code_list_status"]["t_negosiasi.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_negosiasi.status"]["edit"] = array("table" => "t_negosiasi", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["t_negosiasi_lookup"] ) ) {
			$lookupTableLinks["t_negosiasi_lookup"] = array();
		}
		if( !isset( $lookupTableLinks["t_negosiasi_lookup"]["t_deal.id_lahan"] )) {
			$lookupTableLinks["t_negosiasi_lookup"]["t_deal.id_lahan"] = array();
		}
		$lookupTableLinks["t_negosiasi_lookup"]["t_deal.id_lahan"]["edit"] = array("table" => "t_deal", "field" => "id_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["t_deal"] ) ) {
			$lookupTableLinks["t_deal"] = array();
		}
		if( !isset( $lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"] )) {
			$lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"] = array();
		}
		$lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"]["add"] = array("table" => "t_dokumentasi", "field" => "id_lahan", "page" => "add");
		if( !isset( $lookupTableLinks["t_deal"] ) ) {
			$lookupTableLinks["t_deal"] = array();
		}
		if( !isset( $lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"] )) {
			$lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"] = array();
		}
		$lookupTableLinks["t_deal"]["t_dokumentasi.id_lahan"]["search"] = array("table" => "t_dokumentasi", "field" => "id_lahan", "page" => "search");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_dokumentasi.status_lahan_pelepasan"] )) {
			$lookupTableLinks["m_code_list"]["t_dokumentasi.status_lahan_pelepasan"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_dokumentasi.status_lahan_pelepasan"]["edit"] = array("table" => "t_dokumentasi", "field" => "status_lahan_pelepasan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_dokumentasi.status"] )) {
			$lookupTableLinks["code_list_status"]["t_dokumentasi.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_dokumentasi.status"]["edit"] = array("table" => "t_dokumentasi", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_dok_detail_attach.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_dok_detail_attach.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_dok_detail_attach.param_dok"]["edit"] = array("table" => "t_dok_detail_attach", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["t_dokumentasi"] ) ) {
			$lookupTableLinks["t_dokumentasi"] = array();
		}
		if( !isset( $lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"] )) {
			$lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"] = array();
		}
		$lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"]["add"] = array("table" => "t_perm_dana", "field" => "id_lahan", "page" => "add");
		if( !isset( $lookupTableLinks["t_dokumentasi"] ) ) {
			$lookupTableLinks["t_dokumentasi"] = array();
		}
		if( !isset( $lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"] )) {
			$lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"] = array();
		}
		$lookupTableLinks["t_dokumentasi"]["t_perm_dana.id_lahan"]["search"] = array("table" => "t_perm_dana", "field" => "id_lahan", "page" => "search");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_perm_dana.tipe"] )) {
			$lookupTableLinks["m_code_list"]["t_perm_dana.tipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_perm_dana.tipe"]["add"] = array("table" => "t_perm_dana", "field" => "tipe", "page" => "add");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana.status"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana.status"]["add"] = array("table" => "t_perm_dana", "field" => "status", "page" => "add");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana.status"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana.status"]["search"] = array("table" => "t_perm_dana", "field" => "status", "page" => "search");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana.status_badmin"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana.status_badmin"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana.status_badmin"]["edit"] = array("table" => "t_perm_dana", "field" => "status_badmin", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_perm_dana_detail.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_perm_dana_detail.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_perm_dana_detail.param_dok"]["edit"] = array("table" => "t_perm_dana_detail", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["t_perm_dana"] ) ) {
			$lookupTableLinks["t_perm_dana"] = array();
		}
		if( !isset( $lookupTableLinks["t_perm_dana"]["t_pembayaran.id_lahan"] )) {
			$lookupTableLinks["t_perm_dana"]["t_pembayaran.id_lahan"] = array();
		}
		$lookupTableLinks["t_perm_dana"]["t_pembayaran.id_lahan"]["edit"] = array("table" => "t_pembayaran", "field" => "id_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_pembayaran.status"] )) {
			$lookupTableLinks["code_list_status"]["t_pembayaran.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_pembayaran.status"]["edit"] = array("table" => "t_pembayaran", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_pembayaran_detail.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_pembayaran_detail.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_pembayaran_detail.param_dok"]["edit"] = array("table" => "t_pembayaran_detail", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["t_perm_dana"] ) ) {
			$lookupTableLinks["t_perm_dana"] = array();
		}
		if( !isset( $lookupTableLinks["t_perm_dana"]["t_regis.id_lahan"] )) {
			$lookupTableLinks["t_perm_dana"]["t_regis.id_lahan"] = array();
		}
		$lookupTableLinks["t_perm_dana"]["t_regis.id_lahan"]["edit"] = array("table" => "t_regis", "field" => "id_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_regis.status"] )) {
			$lookupTableLinks["code_list_status"]["t_regis.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_regis.status"]["edit"] = array("table" => "t_regis", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_regis_detail.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_regis_detail.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_regis_detail.param_dok"]["edit"] = array("table" => "t_regis_detail", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_blok"] ) ) {
			$lookupTableLinks["code_list_blok"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_blok"]["t_input_awal_new.blok_lokasi"] )) {
			$lookupTableLinks["code_list_blok"]["t_input_awal_new.blok_lokasi"] = array();
		}
		$lookupTableLinks["code_list_blok"]["t_input_awal_new.blok_lokasi"]["edit"] = array("table" => "t_input_awal_new", "field" => "blok_lokasi", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_kpl"] ) ) {
			$lookupTableLinks["code_list_kpl"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_kpl"]["t_input_awal_new.keperluan_lahan"] )) {
			$lookupTableLinks["code_list_kpl"]["t_input_awal_new.keperluan_lahan"] = array();
		}
		$lookupTableLinks["code_list_kpl"]["t_input_awal_new.keperluan_lahan"]["edit"] = array("table" => "t_input_awal_new", "field" => "keperluan_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_input_awal_new.status"] )) {
			$lookupTableLinks["code_list_status"]["t_input_awal_new.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_input_awal_new.status"]["edit"] = array("table" => "t_input_awal_new", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["t_pengukuran"] ) ) {
			$lookupTableLinks["t_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"] )) {
			$lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"] = array();
		}
		$lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"]["add"] = array("table" => "t_negosiasi_lookup", "field" => "id_lahan", "page" => "add");
		if( !isset( $lookupTableLinks["t_pengukuran"] ) ) {
			$lookupTableLinks["t_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"] )) {
			$lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"] = array();
		}
		$lookupTableLinks["t_pengukuran"]["t_negosiasi_lookup.id_lahan"]["search"] = array("table" => "t_negosiasi_lookup", "field" => "id_lahan", "page" => "search");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_negosiasi_lookup.status"] )) {
			$lookupTableLinks["code_list_status"]["t_negosiasi_lookup.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_negosiasi_lookup.status"]["edit"] = array("table" => "t_negosiasi_lookup", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_pengukuran_percil_2.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_pengukuran_percil_2.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_pengukuran_percil_2.param_dok"]["edit"] = array("table" => "t_pengukuran_percil_2", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_regis_detail_backdate.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_regis_detail_backdate.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_regis_detail_backdate.param_dok"]["edit"] = array("table" => "t_regis_detail_backdate", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_kpl"] ) ) {
			$lookupTableLinks["code_list_kpl"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_kpl"]["t_input_awal_dok.keperluan_lahan"] )) {
			$lookupTableLinks["code_list_kpl"]["t_input_awal_dok.keperluan_lahan"] = array();
		}
		$lookupTableLinks["code_list_kpl"]["t_input_awal_dok.keperluan_lahan"]["edit"] = array("table" => "t_input_awal_dok", "field" => "keperluan_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_perm_dana_detail_dok.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_perm_dana_detail_dok.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_perm_dana_detail_dok.param_dok"]["edit"] = array("table" => "t_perm_dana_detail_dok", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_pembayaran_detail_dok.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_pembayaran_detail_dok.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_pembayaran_detail_dok.param_dok"]["edit"] = array("table" => "t_pembayaran_detail_dok", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_perm_dana_close.tipe"] )) {
			$lookupTableLinks["m_code_list"]["t_perm_dana_close.tipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_perm_dana_close.tipe"]["add"] = array("table" => "t_perm_dana_close", "field" => "tipe", "page" => "add");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana_close.status"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana_close.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana_close.status"]["edit"] = array("table" => "t_perm_dana_close", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana_close.status_full"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana_close.status_full"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana_close.status_full"]["edit"] = array("table" => "t_perm_dana_close", "field" => "status_full", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_perm_dana_close.status_badmin"] )) {
			$lookupTableLinks["code_list_status"]["t_perm_dana_close.status_badmin"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_perm_dana_close.status_badmin"]["edit"] = array("table" => "t_perm_dana_close", "field" => "status_badmin", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_doc"] ) ) {
			$lookupTableLinks["code_list_doc"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_doc"]["t_perm_dana_detail_bayar.param_dok"] )) {
			$lookupTableLinks["code_list_doc"]["t_perm_dana_detail_bayar.param_dok"] = array();
		}
		$lookupTableLinks["code_list_doc"]["t_perm_dana_detail_bayar.param_dok"]["edit"] = array("table" => "t_perm_dana_detail_bayar", "field" => "param_dok", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran_batas.jenis_batas"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran_batas.jenis_batas"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran_batas.jenis_batas"]["edit"] = array("table" => "t_pengukuran_batas", "field" => "jenis_batas", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran_batas.tipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran_batas.tipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran_batas.tipe"]["edit"] = array("table" => "t_pengukuran_batas", "field" => "tipe", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"]["edit"] = array("table" => "t_pengukuran_batas", "field" => "batas", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"]["add"] = array("table" => "t_pengukuran_batas", "field" => "batas", "page" => "add");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas.batas"]["search"] = array("table" => "t_pengukuran_batas", "field" => "batas", "page" => "search");
		if( !isset( $lookupTableLinks["code_list_blok"] ) ) {
			$lookupTableLinks["code_list_blok"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_blok"]["t_input_awal_new_pengukuran.blok_lokasi"] )) {
			$lookupTableLinks["code_list_blok"]["t_input_awal_new_pengukuran.blok_lokasi"] = array();
		}
		$lookupTableLinks["code_list_blok"]["t_input_awal_new_pengukuran.blok_lokasi"]["edit"] = array("table" => "t_input_awal_new_pengukuran", "field" => "blok_lokasi", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_kpl"] ) ) {
			$lookupTableLinks["code_list_kpl"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_kpl"]["t_input_awal_new_pengukuran.keperluan_lahan"] )) {
			$lookupTableLinks["code_list_kpl"]["t_input_awal_new_pengukuran.keperluan_lahan"] = array();
		}
		$lookupTableLinks["code_list_kpl"]["t_input_awal_new_pengukuran.keperluan_lahan"]["edit"] = array("table" => "t_input_awal_new_pengukuran", "field" => "keperluan_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["t_input_awal_new_pengukuran.status"] )) {
			$lookupTableLinks["code_list_status"]["t_input_awal_new_pengukuran.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["t_input_awal_new_pengukuran.status"]["edit"] = array("table" => "t_input_awal_new_pengukuran", "field" => "status", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.jenis_batas"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.jenis_batas"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.jenis_batas"]["edit"] = array("table" => "t_pengukuran_batas_view", "field" => "jenis_batas", "page" => "edit");
		if( !isset( $lookupTableLinks["m_code_list"] ) ) {
			$lookupTableLinks["m_code_list"] = array();
		}
		if( !isset( $lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.tipe"] )) {
			$lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.tipe"] = array();
		}
		$lookupTableLinks["m_code_list"]["t_pengukuran_batas_view.tipe"]["edit"] = array("table" => "t_pengukuran_batas_view", "field" => "tipe", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"]["edit"] = array("table" => "t_pengukuran_batas_view", "field" => "batas", "page" => "edit");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"]["add"] = array("table" => "t_pengukuran_batas_view", "field" => "batas", "page" => "add");
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"] ) ) {
			$lookupTableLinks["t_input_awal_new_pengukuran"] = array();
		}
		if( !isset( $lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] )) {
			$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"] = array();
		}
		$lookupTableLinks["t_input_awal_new_pengukuran"]["t_pengukuran_batas_view.batas"]["search"] = array("table" => "t_pengukuran_batas_view", "field" => "batas", "page" => "search");
		if( !isset( $lookupTableLinks["t_negosiasi_lookup"] ) ) {
			$lookupTableLinks["t_negosiasi_lookup"] = array();
		}
		if( !isset( $lookupTableLinks["t_negosiasi_lookup"]["t_deal_progress_chart.id_lahan"] )) {
			$lookupTableLinks["t_negosiasi_lookup"]["t_deal_progress_chart.id_lahan"] = array();
		}
		$lookupTableLinks["t_negosiasi_lookup"]["t_deal_progress_chart.id_lahan"]["edit"] = array("table" => "t_deal_progress_chart", "field" => "id_lahan", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_blok"] ) ) {
			$lookupTableLinks["code_list_blok"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_blok"]["test.blok_lokasi"] )) {
			$lookupTableLinks["code_list_blok"]["test.blok_lokasi"] = array();
		}
		$lookupTableLinks["code_list_blok"]["test.blok_lokasi"]["edit"] = array("table" => "test", "field" => "blok_lokasi", "page" => "edit");
		if( !isset( $lookupTableLinks["code_list_status"] ) ) {
			$lookupTableLinks["code_list_status"] = array();
		}
		if( !isset( $lookupTableLinks["code_list_status"]["test.status"] )) {
			$lookupTableLinks["code_list_status"]["test.status"] = array();
		}
		$lookupTableLinks["code_list_status"]["test.status"]["edit"] = array("table" => "test", "field" => "status", "page" => "edit");
}

?>