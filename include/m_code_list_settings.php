<?php
$tdatam_code_list = array();
$tdatam_code_list[".searchableFields"] = array();
$tdatam_code_list[".ShortName"] = "m_code_list";
$tdatam_code_list[".OwnerID"] = "";
$tdatam_code_list[".OriginalTable"] = "m_code_list";


$tdatam_code_list[".pagesByType"] = my_json_decode( "{}" );
$tdatam_code_list[".originalPagesByType"] = $tdatam_code_list[".pagesByType"];
$tdatam_code_list[".pages"] = types2pages( my_json_decode( "{}" ) );
$tdatam_code_list[".originalPages"] = $tdatam_code_list[".pages"];
$tdatam_code_list[".defaultPages"] = my_json_decode( "{}" );
$tdatam_code_list[".originalDefaultPages"] = $tdatam_code_list[".defaultPages"];

//	field labels
$fieldLabelsm_code_list = array();
$fieldToolTipsm_code_list = array();
$pageTitlesm_code_list = array();
$placeHoldersm_code_list = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsm_code_list["English"] = array();
	$fieldToolTipsm_code_list["English"] = array();
	$placeHoldersm_code_list["English"] = array();
	$pageTitlesm_code_list["English"] = array();
	$fieldLabelsm_code_list["English"]["CatID"] = "Cat ID";
	$fieldToolTipsm_code_list["English"]["CatID"] = "";
	$placeHoldersm_code_list["English"]["CatID"] = "";
	$fieldLabelsm_code_list["English"]["Code"] = "Code";
	$fieldToolTipsm_code_list["English"]["Code"] = "";
	$placeHoldersm_code_list["English"]["Code"] = "";
	$fieldLabelsm_code_list["English"]["Description"] = "Description";
	$fieldToolTipsm_code_list["English"]["Description"] = "";
	$placeHoldersm_code_list["English"]["Description"] = "";
	$fieldLabelsm_code_list["English"]["AssRec"] = "Ass Rec";
	$fieldToolTipsm_code_list["English"]["AssRec"] = "";
	$placeHoldersm_code_list["English"]["AssRec"] = "";
	$fieldLabelsm_code_list["English"]["OrderNo"] = "Order No";
	$fieldToolTipsm_code_list["English"]["OrderNo"] = "";
	$placeHoldersm_code_list["English"]["OrderNo"] = "";
	$fieldLabelsm_code_list["English"]["Active"] = "Active";
	$fieldToolTipsm_code_list["English"]["Active"] = "";
	$placeHoldersm_code_list["English"]["Active"] = "";
	$fieldLabelsm_code_list["English"]["Rate"] = "Rate";
	$fieldToolTipsm_code_list["English"]["Rate"] = "";
	$placeHoldersm_code_list["English"]["Rate"] = "";
	if (count($fieldToolTipsm_code_list["English"]))
		$tdatam_code_list[".isUseToolTips"] = true;
}


	$tdatam_code_list[".NCSearch"] = true;



$tdatam_code_list[".shortTableName"] = "m_code_list";
$tdatam_code_list[".nSecOptions"] = 0;

$tdatam_code_list[".mainTableOwnerID"] = "";
$tdatam_code_list[".entityType"] = 0;
$tdatam_code_list[".connId"] = "db_lla_at_localhost";


$tdatam_code_list[".strOriginalTableName"] = "m_code_list";

		 



$tdatam_code_list[".showAddInPopup"] = false;

$tdatam_code_list[".showEditInPopup"] = false;

$tdatam_code_list[".showViewInPopup"] = false;

$tdatam_code_list[".listAjax"] = false;
//	temporary
//$tdatam_code_list[".listAjax"] = false;

	$tdatam_code_list[".audit"] = false;

	$tdatam_code_list[".locking"] = false;


$pages = $tdatam_code_list[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatam_code_list[".edit"] = true;
	$tdatam_code_list[".afterEditAction"] = 1;
	$tdatam_code_list[".closePopupAfterEdit"] = 1;
	$tdatam_code_list[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatam_code_list[".add"] = true;
$tdatam_code_list[".afterAddAction"] = 1;
$tdatam_code_list[".closePopupAfterAdd"] = 1;
$tdatam_code_list[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatam_code_list[".list"] = true;
}



$tdatam_code_list[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatam_code_list[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatam_code_list[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatam_code_list[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatam_code_list[".printFriendly"] = true;
}



$tdatam_code_list[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatam_code_list[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatam_code_list[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatam_code_list[".isUseAjaxSuggest"] = true;

$tdatam_code_list[".rowHighlite"] = true;



			

$tdatam_code_list[".ajaxCodeSnippetAdded"] = false;

$tdatam_code_list[".buttonsAdded"] = false;

$tdatam_code_list[".addPageEvents"] = false;

// use timepicker for search panel
$tdatam_code_list[".isUseTimeForSearch"] = false;


$tdatam_code_list[".badgeColor"] = "E07878";


$tdatam_code_list[".allSearchFields"] = array();
$tdatam_code_list[".filterFields"] = array();
$tdatam_code_list[".requiredSearchFields"] = array();

$tdatam_code_list[".googleLikeFields"] = array();
$tdatam_code_list[".googleLikeFields"][] = "CatID";
$tdatam_code_list[".googleLikeFields"][] = "Code";
$tdatam_code_list[".googleLikeFields"][] = "Description";
$tdatam_code_list[".googleLikeFields"][] = "AssRec";
$tdatam_code_list[".googleLikeFields"][] = "OrderNo";
$tdatam_code_list[".googleLikeFields"][] = "Active";
$tdatam_code_list[".googleLikeFields"][] = "Rate";



$tdatam_code_list[".tableType"] = "list";

$tdatam_code_list[".printerPageOrientation"] = 0;
$tdatam_code_list[".nPrinterPageScale"] = 100;

$tdatam_code_list[".nPrinterSplitRecords"] = 40;

$tdatam_code_list[".geocodingEnabled"] = false;










$tdatam_code_list[".pageSize"] = 20;

$tdatam_code_list[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatam_code_list[".strOrderBy"] = $tstrOrderBy;

$tdatam_code_list[".orderindexes"] = array();


$tdatam_code_list[".sqlHead"] = "SELECT CatID,  	Code,  	Description,  	AssRec,  	OrderNo,  	Active,  	Rate";
$tdatam_code_list[".sqlFrom"] = "FROM m_code_list";
$tdatam_code_list[".sqlWhereExpr"] = "";
$tdatam_code_list[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatam_code_list[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatam_code_list[".arrGroupsPerPage"] = $arrGPP;

$tdatam_code_list[".highlightSearchResults"] = true;

$tableKeysm_code_list = array();
$tableKeysm_code_list[] = "CatID";
$tableKeysm_code_list[] = "Code";
$tableKeysm_code_list[] = "Description";
$tableKeysm_code_list[] = "AssRec";
$tdatam_code_list[".Keys"] = $tableKeysm_code_list;


$tdatam_code_list[".hideMobileList"] = array();




//	CatID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "CatID";
	$fdata["GoodName"] = "CatID";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","CatID");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "CatID";

		$fdata["sourceSingle"] = "CatID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CatID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["CatID"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "CatID";
//	Code
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Code";
	$fdata["GoodName"] = "Code";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","Code");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Code";

		$fdata["sourceSingle"] = "Code";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Code";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["Code"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "Code";
//	Description
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Description";
	$fdata["GoodName"] = "Description";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","Description");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "Description";

		$fdata["sourceSingle"] = "Description";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Description";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["Description"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "Description";
//	AssRec
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "AssRec";
	$fdata["GoodName"] = "AssRec";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","AssRec");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "AssRec";

		$fdata["sourceSingle"] = "AssRec";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AssRec";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["AssRec"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "AssRec";
//	OrderNo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "OrderNo";
	$fdata["GoodName"] = "OrderNo";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","OrderNo");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "OrderNo";

		$fdata["sourceSingle"] = "OrderNo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OrderNo";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["OrderNo"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "OrderNo";
//	Active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Active";
	$fdata["GoodName"] = "Active";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","Active");
	$fdata["FieldType"] = 16;


	
	
										

		$fdata["strField"] = "Active";

		$fdata["sourceSingle"] = "Active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Active";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["Active"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "Active";
//	Rate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Rate";
	$fdata["GoodName"] = "Rate";
	$fdata["ownerTable"] = "m_code_list";
	$fdata["Label"] = GetFieldLabel("m_code_list","Rate");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Rate";

		$fdata["sourceSingle"] = "Rate";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Rate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatam_code_list["Rate"] = $fdata;
		$tdatam_code_list[".searchableFields"][] = "Rate";


$tables_data["m_code_list"]=&$tdatam_code_list;
$field_labels["m_code_list"] = &$fieldLabelsm_code_list;
$fieldToolTips["m_code_list"] = &$fieldToolTipsm_code_list;
$placeHolders["m_code_list"] = &$placeHoldersm_code_list;
$page_titles["m_code_list"] = &$pageTitlesm_code_list;


changeTextControlsToDate( "m_code_list" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["m_code_list"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["m_code_list"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_m_code_list()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "CatID,  	Code,  	Description,  	AssRec,  	OrderNo,  	Active,  	Rate";
$proto0["m_strFrom"] = "FROM m_code_list";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "CatID",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto6["m_sql"] = "CatID";
$proto6["m_srcTableName"] = "m_code_list";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Code",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto8["m_sql"] = "Code";
$proto8["m_srcTableName"] = "m_code_list";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Description",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto10["m_sql"] = "Description";
$proto10["m_srcTableName"] = "m_code_list";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "AssRec",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto12["m_sql"] = "AssRec";
$proto12["m_srcTableName"] = "m_code_list";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "OrderNo",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto14["m_sql"] = "OrderNo";
$proto14["m_srcTableName"] = "m_code_list";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Active",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto16["m_sql"] = "Active";
$proto16["m_srcTableName"] = "m_code_list";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Rate",
	"m_strTable" => "m_code_list",
	"m_srcTableName" => "m_code_list"
));

$proto18["m_sql"] = "Rate";
$proto18["m_srcTableName"] = "m_code_list";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "m_code_list";
$proto21["m_srcTableName"] = "m_code_list";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "CatID";
$proto21["m_columns"][] = "Code";
$proto21["m_columns"][] = "Description";
$proto21["m_columns"][] = "AssRec";
$proto21["m_columns"][] = "OrderNo";
$proto21["m_columns"][] = "Active";
$proto21["m_columns"][] = "Rate";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "m_code_list";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "m_code_list";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="m_code_list";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_m_code_list = createSqlQuery_m_code_list();


	
					
;

							

$tdatam_code_list[".sqlquery"] = $queryData_m_code_list;



$tdatam_code_list[".hasEvents"] = false;

?>