<?php
			$optionsArray = array( 'totals' => array( 'id' => array( 'totalsType' => '' ),
'id_lahan' => array( 'totalsType' => '' ),
'nama_lahan' => array( 'totalsType' => '' ),
'ukuran' => array( 'totalsType' => '' ),
'start_date' => array( 'totalsType' => '' ),
'due_date' => array( 'totalsType' => '' ),
'pic' => array( 'totalsType' => '' ),
'harga_bangunan' => array( 'totalsType' => '' ),
'harga_per_m2' => array( 'totalsType' => '' ),
'total_biaya' => array( 'totalsType' => '' ),
'keterangan' => array( 'totalsType' => '' ),
'status_lahan_pelepasan' => array( 'totalsType' => '' ),
'status_lahan_sertifkat' => array( 'totalsType' => '' ),
'status' => array( 'totalsType' => '' ),
'close_by' => array( 'totalsType' => '' ),
'close_date' => array( 'totalsType' => '' ),
'created_date' => array( 'totalsType' => '' ),
'created_by' => array( 'totalsType' => '' ),
'updated_date' => array( 'totalsType' => '' ),
'updated_by' => array( 'totalsType' => '' ),
'blok_lokasi' => array( 'totalsType' => '' ),
'nama_lokasi' => array( 'totalsType' => '' ),
'keperluan_lahan' => array( 'totalsType' => '' ),
'batas_utara' => array( 'totalsType' => '' ),
'batas_selatan' => array( 'totalsType' => '' ),
'batas_barat' => array( 'totalsType' => '' ),
'batas_timur' => array( 'totalsType' => '' ),
'keterangan_pengukuran' => array( 'totalsType' => '' ),
'keterangan_input_awal' => array( 'totalsType' => '' ),
'keterangan_negosiasi' => array( 'totalsType' => '' ),
'keterangan_deal' => array( 'totalsType' => '' ),
'dp' => array( 'totalsType' => '' ),
'sisa_pembayaran' => array( 'totalsType' => '' ),
'tipe' => array( 'totalsType' => '' ),
'biaya_admin' => array( 'totalsType' => '' ),
'keterangan_perm_dana' => array( 'totalsType' => '' ) ),
'fields' => array( 'gridFields' => array( 'nama_lahan',
'id_lahan',
'start_date',
'due_date',
'pic',
'keterangan',
'status_lahan_pelepasan',
'status',
'close_by',
'close_date',
'created_date',
'created_by',
'updated_date',
'updated_by',
'ukuran',
'nama_lokasi',
'keperluan_lahan',
'harga_bangunan',
'harga_per_m2',
'total_biaya',
'blok_lokasi',
'batas_utara',
'batas_selatan',
'batas_barat',
'batas_timur',
'keterangan_pengukuran',
'keterangan_input_awal',
'keterangan_negosiasi',
'keterangan_deal',
'dp',
'sisa_pembayaran',
'tipe',
'biaya_admin',
'keterangan_perm_dana' ),
'exportFields' => array( 'id_lahan',
'start_date',
'due_date',
'pic',
'keterangan',
'status_lahan_pelepasan',
'status',
'close_by',
'close_date',
'created_date',
'created_by',
'updated_date',
'updated_by',
'nama_lahan',
'ukuran',
'harga_bangunan',
'harga_per_m2',
'total_biaya',
'blok_lokasi',
'nama_lokasi',
'keperluan_lahan',
'batas_utara',
'batas_selatan',
'batas_barat',
'batas_timur',
'keterangan_pengukuran',
'keterangan_input_awal',
'keterangan_negosiasi',
'keterangan_deal',
'dp',
'sisa_pembayaran',
'tipe',
'biaya_admin',
'keterangan_perm_dana' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'nama_lahan' => array( 'export_field' ),
'id_lahan' => array( 'export_field1' ),
'start_date' => array( 'export_field2' ),
'due_date' => array( 'export_field3' ),
'pic' => array( 'export_field4' ),
'keterangan' => array( 'export_field5' ),
'status_lahan_pelepasan' => array( 'export_field6' ),
'status' => array( 'export_field8' ),
'close_by' => array( 'export_field9' ),
'close_date' => array( 'export_field10' ),
'created_date' => array( 'export_field11' ),
'created_by' => array( 'export_field12' ),
'updated_date' => array( 'export_field13' ),
'updated_by' => array( 'export_field14' ),
'ukuran' => array( 'export_field15' ),
'nama_lokasi' => array( 'export_field16' ),
'keperluan_lahan' => array( 'export_field17' ),
'harga_bangunan' => array( 'export_field18' ),
'harga_per_m2' => array( 'export_field19' ),
'total_biaya' => array( 'export_field20' ),
'blok_lokasi' => array( 'export_field7' ),
'batas_utara' => array( 'export_field21' ),
'batas_selatan' => array( 'export_field22' ),
'batas_barat' => array( 'export_field23' ),
'batas_timur' => array( 'export_field24' ),
'keterangan_pengukuran' => array( 'export_field25' ),
'keterangan_input_awal' => array( 'export_field26' ),
'keterangan_negosiasi' => array( 'export_field27' ),
'keterangan_deal' => array( 'export_field28' ),
'dp' => array( 'export_field29' ),
'sisa_pembayaran' => array( 'export_field30' ),
'tipe' => array( 'export_field31' ),
'biaya_admin' => array( 'export_field32' ),
'keterangan_perm_dana' => array( 'export_field33' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'export_header' ),
'grid' => array( 'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field',
'export_field15',
'export_field18',
'export_field19',
'export_field20',
'export_field7',
'export_field16',
'export_field17',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27',
'export_field28',
'export_field29',
'export_field30',
'export_field31',
'export_field32',
'export_field33' ),
'footer' => array( 'export_export',
'export_cancel' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'export_header' => 'top',
'export_field1' => 'grid',
'export_field2' => 'grid',
'export_field3' => 'grid',
'export_field4' => 'grid',
'export_field5' => 'grid',
'export_field6' => 'grid',
'export_field8' => 'grid',
'export_field9' => 'grid',
'export_field10' => 'grid',
'export_field11' => 'grid',
'export_field12' => 'grid',
'export_field13' => 'grid',
'export_field14' => 'grid',
'export_field' => 'grid',
'export_field15' => 'grid',
'export_field18' => 'grid',
'export_field19' => 'grid',
'export_field20' => 'grid',
'export_field7' => 'grid',
'export_field16' => 'grid',
'export_field17' => 'grid',
'export_field21' => 'grid',
'export_field22' => 'grid',
'export_field23' => 'grid',
'export_field24' => 'grid',
'export_field25' => 'grid',
'export_field26' => 'grid',
'export_field27' => 'grid',
'export_field28' => 'grid',
'export_field29' => 'grid',
'export_field30' => 'grid',
'export_field31' => 'grid',
'export_field32' => 'grid',
'export_field33' => 'grid',
'export_export' => 'footer',
'export_cancel' => 'footer' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'export_header' => array( 'export_header' ),
'export_export' => array( 'export_export' ),
'export_cancel' => array( 'export_cancel' ),
'export_field' => array( 'export_field',
'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19',
'export_field20',
'export_field7',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27',
'export_field28',
'export_field29',
'export_field30',
'export_field31',
'export_field32',
'export_field33' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'verticalBar' => false,
'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ),
'hasNotifications' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'export' => array( 'format' => 2,
'selectFields' => false,
'delimiter' => ',',
'selectDelimiter' => false,
'exportFileTypes' => array( 'excel' => true,
'word' => true,
'csv' => true,
'xml' => false ) ) );
			$pageArray = array( 'id' => 'export',
'type' => 'export',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'export-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_header' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'export-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field',
'export_field15',
'export_field18',
'export_field19',
'export_field20',
'export_field7',
'export_field16',
'export_field17',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27',
'export_field28',
'export_field29',
'export_field30',
'export_field31',
'export_field32',
'export_field33' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'footer' => array( 'modelId' => 'export-footer',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ),
'c2' => array( 'model' => 'c2',
'items' => array( 'export_export',
'export_cancel' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'export_header' => array( 'type' => 'export_header' ),
'export_export' => array( 'type' => 'export_export' ),
'export_cancel' => array( 'type' => 'export_cancel' ),
'export_field' => array( 'field' => 'nama_lahan',
'type' => 'export_field' ),
'export_field1' => array( 'field' => 'id_lahan',
'type' => 'export_field' ),
'export_field2' => array( 'field' => 'start_date',
'type' => 'export_field' ),
'export_field3' => array( 'field' => 'due_date',
'type' => 'export_field' ),
'export_field4' => array( 'field' => 'pic',
'type' => 'export_field' ),
'export_field5' => array( 'field' => 'keterangan',
'type' => 'export_field' ),
'export_field6' => array( 'field' => 'status_lahan_pelepasan',
'type' => 'export_field' ),
'export_field8' => array( 'field' => 'status',
'type' => 'export_field' ),
'export_field9' => array( 'field' => 'close_by',
'type' => 'export_field' ),
'export_field10' => array( 'field' => 'close_date',
'type' => 'export_field' ),
'export_field11' => array( 'field' => 'created_date',
'type' => 'export_field' ),
'export_field12' => array( 'field' => 'created_by',
'type' => 'export_field' ),
'export_field13' => array( 'field' => 'updated_date',
'type' => 'export_field' ),
'export_field14' => array( 'field' => 'updated_by',
'type' => 'export_field' ),
'export_field15' => array( 'field' => 'ukuran',
'type' => 'export_field' ),
'export_field16' => array( 'field' => 'nama_lokasi',
'type' => 'export_field' ),
'export_field17' => array( 'field' => 'keperluan_lahan',
'type' => 'export_field' ),
'export_field18' => array( 'field' => 'harga_bangunan',
'type' => 'export_field' ),
'export_field19' => array( 'field' => 'harga_per_m2',
'type' => 'export_field' ),
'export_field20' => array( 'field' => 'total_biaya',
'type' => 'export_field' ),
'export_field7' => array( 'field' => 'blok_lokasi',
'type' => 'export_field' ),
'export_field21' => array( 'field' => 'batas_utara',
'type' => 'export_field' ),
'export_field22' => array( 'field' => 'batas_selatan',
'type' => 'export_field' ),
'export_field23' => array( 'field' => 'batas_barat',
'type' => 'export_field' ),
'export_field24' => array( 'field' => 'batas_timur',
'type' => 'export_field' ),
'export_field25' => array( 'field' => 'keterangan_pengukuran',
'type' => 'export_field' ),
'export_field26' => array( 'field' => 'keterangan_input_awal',
'type' => 'export_field' ),
'export_field27' => array( 'field' => 'keterangan_negosiasi',
'type' => 'export_field' ),
'export_field28' => array( 'field' => 'keterangan_deal',
'type' => 'export_field' ),
'export_field29' => array( 'field' => 'dp',
'type' => 'export_field' ),
'export_field30' => array( 'field' => 'sisa_pembayaran',
'type' => 'export_field' ),
'export_field31' => array( 'field' => 'tipe',
'type' => 'export_field' ),
'export_field32' => array( 'field' => 'biaya_admin',
'type' => 'export_field' ),
'export_field33' => array( 'field' => 'keterangan_perm_dana',
'type' => 'export_field' ) ),
'dbProps' => array(  ),
'version' => 11,
'imageItem' => array( 'type' => 'page_image' ),
'imageBgColor' => '#f2f2f2',
'controlsBgColor' => 'white',
'imagePosition' => 'right',
'exportFormat' => 2,
'exportDelimiter' => ',',
'exportSelectDelimiter' => false,
'exportSelectFields' => false );
		?>