<?php
			$optionsArray = array( 'totals' => array( 'id' => array( 'totalsType' => '' ),
'id_lahan' => array( 'totalsType' => '' ),
'nama_lahan' => array( 'totalsType' => '' ),
'nama_lokasi' => array( 'totalsType' => '' ),
'ukuran' => array( 'totalsType' => '' ),
'harga_per_m2' => array( 'totalsType' => '' ),
'total_biaya' => array( 'totalsType' => '' ),
'status_input_awal' => array( 'totalsType' => '' ),
'close_date_input' => array( 'totalsType' => '' ),
'status_pengukuran' => array( 'totalsType' => '' ),
'close_date_pengukuran' => array( 'totalsType' => '' ),
'status_negosiasi' => array( 'totalsType' => '' ),
'close_date_negosiasi' => array( 'totalsType' => '' ),
'status_deal' => array( 'totalsType' => '' ),
'close_date_deal' => array( 'totalsType' => '' ),
'status_dokumentasi' => array( 'totalsType' => '' ),
'close_date_dokumentasi' => array( 'totalsType' => '' ),
'status_dp' => array( 'totalsType' => '' ),
'close_dp_date' => array( 'totalsType' => '' ),
'status_full' => array( 'totalsType' => '' ),
'close_full_date' => array( 'totalsType' => '' ),
'status_badmin' => array( 'totalsType' => '' ),
'close_badmin_date' => array( 'totalsType' => '' ),
'status_pemb_admin' => array( 'totalsType' => '' ),
'status_pemb_dp' => array( 'totalsType' => '' ),
'status_pemb_full' => array( 'totalsType' => '' ),
'status_regis' => array( 'totalsType' => '' ),
'close_regis_date' => array( 'totalsType' => '' ),
'no_register' => array( 'totalsType' => '' ) ),
'fields' => array( 'gridFields' => array( 'id_lahan',
'nama_lahan',
'status_pengukuran',
'nama_lokasi',
'close_date_pengukuran',
'status_negosiasi',
'close_date_negosiasi',
'status_deal',
'close_date_deal',
'status_dokumentasi',
'close_date_dokumentasi',
'status_dp',
'close_dp_date',
'status_full',
'close_full_date',
'status_badmin',
'ukuran',
'harga_per_m2',
'total_biaya',
'status_input_awal',
'close_date_input',
'close_badmin_date',
'status_pemb_admin',
'status_pemb_dp',
'status_pemb_full',
'status_regis',
'close_regis_date',
'no_register' ),
'exportFields' => array( 'id_lahan',
'nama_lahan',
'nama_lokasi',
'ukuran',
'harga_per_m2',
'total_biaya',
'status_input_awal',
'close_date_input',
'status_pengukuran',
'close_date_pengukuran',
'status_negosiasi',
'close_date_negosiasi',
'status_deal',
'close_date_deal',
'status_dokumentasi',
'close_date_dokumentasi',
'status_dp',
'close_dp_date',
'status_full',
'close_full_date',
'status_badmin',
'close_badmin_date',
'status_pemb_admin',
'status_pemb_dp',
'status_pemb_full',
'status_regis',
'close_regis_date',
'no_register' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'id_lahan' => array( 'export_field1' ),
'nama_lahan' => array( 'export_field2' ),
'status_pengukuran' => array( 'export_field7' ),
'nama_lokasi' => array( 'export_field8' ),
'close_date_pengukuran' => array( 'export_field9' ),
'status_negosiasi' => array( 'export_field10' ),
'close_date_negosiasi' => array( 'export_field11' ),
'status_deal' => array( 'export_field12' ),
'close_date_deal' => array( 'export_field13' ),
'status_dokumentasi' => array( 'export_field14' ),
'close_date_dokumentasi' => array( 'export_field15' ),
'status_dp' => array( 'export_field16' ),
'close_dp_date' => array( 'export_field17' ),
'status_full' => array( 'export_field18' ),
'close_full_date' => array( 'export_field19' ),
'status_badmin' => array( 'export_field20' ),
'ukuran' => array( 'export_field' ),
'harga_per_m2' => array( 'export_field3' ),
'total_biaya' => array( 'export_field4' ),
'status_input_awal' => array( 'export_field5' ),
'close_date_input' => array( 'export_field6' ),
'close_badmin_date' => array( 'export_field21' ),
'status_pemb_admin' => array( 'export_field22' ),
'status_pemb_dp' => array( 'export_field23' ),
'status_pemb_full' => array( 'export_field24' ),
'status_regis' => array( 'export_field25' ),
'close_regis_date' => array( 'export_field26' ),
'no_register' => array( 'export_field27' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'export_header' ),
'grid' => array( 'export_field1',
'export_field2',
'export_field8',
'export_field',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19',
'export_field20',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27' ),
'footer' => array( 'export_export',
'export_cancel' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'export_header' => 'top',
'export_field1' => 'grid',
'export_field2' => 'grid',
'export_field8' => 'grid',
'export_field' => 'grid',
'export_field3' => 'grid',
'export_field4' => 'grid',
'export_field5' => 'grid',
'export_field6' => 'grid',
'export_field7' => 'grid',
'export_field9' => 'grid',
'export_field10' => 'grid',
'export_field11' => 'grid',
'export_field12' => 'grid',
'export_field13' => 'grid',
'export_field14' => 'grid',
'export_field15' => 'grid',
'export_field16' => 'grid',
'export_field17' => 'grid',
'export_field18' => 'grid',
'export_field19' => 'grid',
'export_field20' => 'grid',
'export_field21' => 'grid',
'export_field22' => 'grid',
'export_field23' => 'grid',
'export_field24' => 'grid',
'export_field25' => 'grid',
'export_field26' => 'grid',
'export_field27' => 'grid',
'export_export' => 'footer',
'export_cancel' => 'footer' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'export_header' => array( 'export_header' ),
'export_export' => array( 'export_export' ),
'export_cancel' => array( 'export_cancel' ),
'export_field' => array( 'export_field1',
'export_field2',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19',
'export_field20',
'export_field',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'verticalBar' => false,
'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ),
'hasNotifications' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'export' => array( 'format' => 2,
'selectFields' => false,
'delimiter' => ',',
'selectDelimiter' => false,
'exportFileTypes' => array( 'excel' => true,
'word' => true,
'csv' => true,
'xml' => false ) ) );
			$pageArray = array( 'id' => 'export',
'type' => 'export',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'export-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_header' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'export-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_field1',
'export_field2',
'export_field8',
'export_field',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19',
'export_field20',
'export_field21',
'export_field22',
'export_field23',
'export_field24',
'export_field25',
'export_field26',
'export_field27' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'footer' => array( 'modelId' => 'export-footer',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ),
'c2' => array( 'model' => 'c2',
'items' => array( 'export_export',
'export_cancel' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'export_header' => array( 'type' => 'export_header' ),
'export_export' => array( 'type' => 'export_export' ),
'export_cancel' => array( 'type' => 'export_cancel' ),
'export_field1' => array( 'field' => 'id_lahan',
'type' => 'export_field' ),
'export_field2' => array( 'field' => 'nama_lahan',
'type' => 'export_field' ),
'export_field7' => array( 'field' => 'status_pengukuran',
'type' => 'export_field' ),
'export_field8' => array( 'field' => 'nama_lokasi',
'type' => 'export_field' ),
'export_field9' => array( 'field' => 'close_date_pengukuran',
'type' => 'export_field' ),
'export_field10' => array( 'field' => 'status_negosiasi',
'type' => 'export_field' ),
'export_field11' => array( 'field' => 'close_date_negosiasi',
'type' => 'export_field' ),
'export_field12' => array( 'field' => 'status_deal',
'type' => 'export_field' ),
'export_field13' => array( 'field' => 'close_date_deal',
'type' => 'export_field' ),
'export_field14' => array( 'field' => 'status_dokumentasi',
'type' => 'export_field' ),
'export_field15' => array( 'field' => 'close_date_dokumentasi',
'type' => 'export_field' ),
'export_field16' => array( 'field' => 'status_dp',
'type' => 'export_field' ),
'export_field17' => array( 'field' => 'close_dp_date',
'type' => 'export_field' ),
'export_field18' => array( 'field' => 'status_full',
'type' => 'export_field' ),
'export_field19' => array( 'field' => 'close_full_date',
'type' => 'export_field' ),
'export_field20' => array( 'field' => 'status_badmin',
'type' => 'export_field' ),
'export_field' => array( 'field' => 'ukuran',
'type' => 'export_field' ),
'export_field3' => array( 'field' => 'harga_per_m2',
'type' => 'export_field' ),
'export_field4' => array( 'field' => 'total_biaya',
'type' => 'export_field' ),
'export_field5' => array( 'field' => 'status_input_awal',
'type' => 'export_field' ),
'export_field6' => array( 'field' => 'close_date_input',
'type' => 'export_field' ),
'export_field21' => array( 'field' => 'close_badmin_date',
'type' => 'export_field' ),
'export_field22' => array( 'field' => 'status_pemb_admin',
'type' => 'export_field' ),
'export_field23' => array( 'field' => 'status_pemb_dp',
'type' => 'export_field' ),
'export_field24' => array( 'field' => 'status_pemb_full',
'type' => 'export_field' ),
'export_field25' => array( 'field' => 'status_regis',
'type' => 'export_field' ),
'export_field26' => array( 'field' => 'close_regis_date',
'type' => 'export_field' ),
'export_field27' => array( 'field' => 'no_register',
'type' => 'export_field' ) ),
'dbProps' => array(  ),
'version' => 11,
'imageItem' => array( 'type' => 'page_image' ),
'imageBgColor' => '#f2f2f2',
'controlsBgColor' => 'white',
'imagePosition' => 'right',
'exportFormat' => 2,
'exportDelimiter' => ',',
'exportSelectDelimiter' => false,
'exportSelectFields' => false );
		?>