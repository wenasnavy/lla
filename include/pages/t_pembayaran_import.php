<?php
			$optionsArray = array( 'fields' => array( 'gridFields' => array( 'id_lahan',
'nama_lahan',
'pic',
'keterangan',
'status',
'created_date',
'created_by',
'updated_date',
'updated_by',
'tipe',
'dp',
'sisa_pembayaran',
'biaya_admin',
'start_date_dp',
'due_date_dp',
'start_date_full',
'due_date_full',
'close_full_by',
'close_full_date',
'close_dp_by',
'close_dp_date' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'nama_lahan' => array( 'import_field' ),
'id_lahan' => array( 'import_field1' ),
'tipe' => array( 'import_field2' ),
'dp' => array( 'import_field3' ),
'pic' => array( 'import_field4' ),
'keterangan' => array( 'import_field5' ),
'sisa_pembayaran' => array( 'import_field6' ),
'status' => array( 'import_field7' ),
'biaya_admin' => array( 'import_field8' ),
'start_date_dp' => array( 'import_field9' ),
'created_date' => array( 'import_field10' ),
'created_by' => array( 'import_field11' ),
'updated_date' => array( 'import_field12' ),
'updated_by' => array( 'import_field13' ),
'due_date_dp' => array( 'import_field14' ),
'start_date_full' => array( 'import_field15' ),
'due_date_full' => array( 'import_field16' ),
'close_full_by' => array( 'import_field17' ),
'close_full_date' => array( 'import_field18' ),
'close_dp_by' => array( 'import_field19' ),
'close_dp_date' => array( 'import_field20' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'import_header' ),
'grid' => array( 'import_field1',
'import_field',
'import_field4',
'import_field5',
'import_field7',
'import_field10',
'import_field11',
'import_field12',
'import_field13',
'import_field2',
'import_field3',
'import_field6',
'import_field8',
'import_field9',
'import_field14',
'import_field15',
'import_field16',
'import_field17',
'import_field18',
'import_field19',
'import_field20' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'import_header' => 'top',
'import_field1' => 'grid',
'import_field' => 'grid',
'import_field4' => 'grid',
'import_field5' => 'grid',
'import_field7' => 'grid',
'import_field10' => 'grid',
'import_field11' => 'grid',
'import_field12' => 'grid',
'import_field13' => 'grid',
'import_field2' => 'grid',
'import_field3' => 'grid',
'import_field6' => 'grid',
'import_field8' => 'grid',
'import_field9' => 'grid',
'import_field14' => 'grid',
'import_field15' => 'grid',
'import_field16' => 'grid',
'import_field17' => 'grid',
'import_field18' => 'grid',
'import_field19' => 'grid',
'import_field20' => 'grid' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'import_header' => array( 'import_header' ),
'import_field' => array( 'import_field',
'import_field1',
'import_field2',
'import_field3',
'import_field4',
'import_field5',
'import_field6',
'import_field7',
'import_field8',
'import_field9',
'import_field10',
'import_field11',
'import_field12',
'import_field13',
'import_field14',
'import_field15',
'import_field16',
'import_field17',
'import_field18',
'import_field19',
'import_field20' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'verticalBar' => false,
'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ),
'hasNotifications' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'import',
'type' => 'import',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'import-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'import_header' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'import-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'import_field1',
'import_field',
'import_field4',
'import_field5',
'import_field7',
'import_field10',
'import_field11',
'import_field12',
'import_field13',
'import_field2',
'import_field3',
'import_field6',
'import_field8',
'import_field9',
'import_field14',
'import_field15',
'import_field16',
'import_field17',
'import_field18',
'import_field19',
'import_field20' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'import_header' => array( 'type' => 'import_header' ),
'import_field' => array( 'field' => 'nama_lahan',
'type' => 'import_field' ),
'import_field1' => array( 'field' => 'id_lahan',
'type' => 'import_field' ),
'import_field2' => array( 'field' => 'tipe',
'type' => 'import_field' ),
'import_field3' => array( 'field' => 'dp',
'type' => 'import_field' ),
'import_field4' => array( 'field' => 'pic',
'type' => 'import_field' ),
'import_field5' => array( 'field' => 'keterangan',
'type' => 'import_field' ),
'import_field6' => array( 'field' => 'sisa_pembayaran',
'type' => 'import_field' ),
'import_field7' => array( 'field' => 'status',
'type' => 'import_field' ),
'import_field8' => array( 'field' => 'biaya_admin',
'type' => 'import_field' ),
'import_field9' => array( 'field' => 'start_date_dp',
'type' => 'import_field' ),
'import_field10' => array( 'field' => 'created_date',
'type' => 'import_field' ),
'import_field11' => array( 'field' => 'created_by',
'type' => 'import_field' ),
'import_field12' => array( 'field' => 'updated_date',
'type' => 'import_field' ),
'import_field13' => array( 'field' => 'updated_by',
'type' => 'import_field' ),
'import_field14' => array( 'field' => 'due_date_dp',
'type' => 'import_field' ),
'import_field15' => array( 'field' => 'start_date_full',
'type' => 'import_field' ),
'import_field16' => array( 'field' => 'due_date_full',
'type' => 'import_field' ),
'import_field17' => array( 'field' => 'close_full_by',
'type' => 'import_field' ),
'import_field18' => array( 'field' => 'close_full_date',
'type' => 'import_field' ),
'import_field19' => array( 'field' => 'close_dp_by',
'type' => 'import_field' ),
'import_field20' => array( 'field' => 'close_dp_date',
'type' => 'import_field' ) ),
'dbProps' => array(  ),
'version' => 11,
'imageItem' => array( 'type' => 'page_image' ),
'imageBgColor' => '#f2f2f2',
'controlsBgColor' => 'white',
'imagePosition' => 'right' );
		?>