
Runner.pages.PageSettings.addPageEvent('t_pengukuran_batas',Runner.pages.constants.PAGE_ADD,"afterPageReady",function(pageObj,proxy,pageid,inlineRow,inlineObject,row){var tipe=Runner.getControl(pageid,"tipe");var batas=Runner.getControl(pageid,"batas");var batas_text=Runner.getControl(pageid,"batas_text");var lat=Runner.getControl(pageid,"lat");var lng=Runner.getControl(pageid,"lng");if(tipe.getValue()!="WID"||tipe.getValue()!="WTID"){pageObj.hideField("batas");pageObj.hideField("batas_text");}
tipe.on('change',function(){if(this.getValue()=="WID"){pageObj.showField("batas");pageObj.hideField("batas_text");batas.clear();batas_text.clear();lat.clear();lng.clear();}
else if(this.getValue()=="WTID"){pageObj.hideField("batas");pageObj.showField("batas_text");batas.clear();batas_text.clear();lat.clear();lng.clear();}
else
{pageObj.hideField("batas");pageObj.hideField("batas_text");batas.clear();batas_text.clear();lat.clear();lng.clear();}});});Runner.pages.PageSettings.addPageEvent('t_pengukuran_batas',Runner.pages.constants.PAGE_EDIT,"afterPageReady",function(pageObj,proxy,pageid,inlineRow,inlineObject,row){var tipe=Runner.getControl(pageid,"tipe");var batas=Runner.getControl(pageid,"batas");var batas_text=Runner.getControl(pageid,"batas_text");if(tipe.getValue()!="WID"||tipe.getValue()!="WTID"){pageObj.hideField("batas_utara");pageObj.hideField("batas_utara_text");}
tipe.on('change',function(){if(this.getValue()=="WID"){pageObj.showField("batas");pageObj.hideField("batas_text");}
else if(this.getValue()=="WTID"){pageObj.hideField("batas");pageObj.showField("batas_text");}
else
{pageObj.hideField("batas");pageObj.hideField("batas_text");}});});