<?php
$tdatat_deal_lahan = array();
$tdatat_deal_lahan[".searchableFields"] = array();
$tdatat_deal_lahan[".ShortName"] = "t_deal_lahan";
$tdatat_deal_lahan[".OwnerID"] = "";
$tdatat_deal_lahan[".OriginalTable"] = "t_deal";


$tdatat_deal_lahan[".pagesByType"] = my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" );
$tdatat_deal_lahan[".originalPagesByType"] = $tdatat_deal_lahan[".pagesByType"];
$tdatat_deal_lahan[".pages"] = types2pages( my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" ) );
$tdatat_deal_lahan[".originalPages"] = $tdatat_deal_lahan[".pages"];
$tdatat_deal_lahan[".defaultPages"] = my_json_decode( "{\"chart\":\"chart\",\"search\":\"search\"}" );
$tdatat_deal_lahan[".originalDefaultPages"] = $tdatat_deal_lahan[".defaultPages"];

//	field labels
$fieldLabelst_deal_lahan = array();
$fieldToolTipst_deal_lahan = array();
$pageTitlest_deal_lahan = array();
$placeHolderst_deal_lahan = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_deal_lahan["English"] = array();
	$fieldToolTipst_deal_lahan["English"] = array();
	$placeHolderst_deal_lahan["English"] = array();
	$pageTitlest_deal_lahan["English"] = array();
	$fieldLabelst_deal_lahan["English"]["status_lahan"] = "Status Lahan";
	$fieldToolTipst_deal_lahan["English"]["status_lahan"] = "";
	$placeHolderst_deal_lahan["English"]["status_lahan"] = "";
	$fieldLabelst_deal_lahan["English"]["Jml"] = "Jml";
	$fieldToolTipst_deal_lahan["English"]["Jml"] = "";
	$placeHolderst_deal_lahan["English"]["Jml"] = "";
	if (count($fieldToolTipst_deal_lahan["English"]))
		$tdatat_deal_lahan[".isUseToolTips"] = true;
}


	$tdatat_deal_lahan[".NCSearch"] = true;

	$tdatat_deal_lahan[".ChartRefreshTime"] = 0;


$tdatat_deal_lahan[".shortTableName"] = "t_deal_lahan";
$tdatat_deal_lahan[".nSecOptions"] = 0;

$tdatat_deal_lahan[".mainTableOwnerID"] = "";
$tdatat_deal_lahan[".entityType"] = 3;
$tdatat_deal_lahan[".connId"] = "db_lla_at_localhost";


$tdatat_deal_lahan[".strOriginalTableName"] = "t_deal";

		 



$tdatat_deal_lahan[".showAddInPopup"] = false;

$tdatat_deal_lahan[".showEditInPopup"] = false;

$tdatat_deal_lahan[".showViewInPopup"] = false;

$tdatat_deal_lahan[".listAjax"] = false;
//	temporary
//$tdatat_deal_lahan[".listAjax"] = false;

	$tdatat_deal_lahan[".audit"] = false;

	$tdatat_deal_lahan[".locking"] = false;


$pages = $tdatat_deal_lahan[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_deal_lahan[".edit"] = true;
	$tdatat_deal_lahan[".afterEditAction"] = 1;
	$tdatat_deal_lahan[".closePopupAfterEdit"] = 1;
	$tdatat_deal_lahan[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_deal_lahan[".add"] = true;
$tdatat_deal_lahan[".afterAddAction"] = 1;
$tdatat_deal_lahan[".closePopupAfterAdd"] = 1;
$tdatat_deal_lahan[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_deal_lahan[".list"] = true;
}



$tdatat_deal_lahan[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_deal_lahan[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_deal_lahan[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_deal_lahan[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_deal_lahan[".printFriendly"] = true;
}



$tdatat_deal_lahan[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_deal_lahan[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_deal_lahan[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_deal_lahan[".isUseAjaxSuggest"] = true;




			

$tdatat_deal_lahan[".ajaxCodeSnippetAdded"] = false;

$tdatat_deal_lahan[".buttonsAdded"] = false;

$tdatat_deal_lahan[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_deal_lahan[".isUseTimeForSearch"] = false;


$tdatat_deal_lahan[".badgeColor"] = "4682B4";


$tdatat_deal_lahan[".allSearchFields"] = array();
$tdatat_deal_lahan[".filterFields"] = array();
$tdatat_deal_lahan[".requiredSearchFields"] = array();

$tdatat_deal_lahan[".googleLikeFields"] = array();
$tdatat_deal_lahan[".googleLikeFields"][] = "status_lahan";
$tdatat_deal_lahan[".googleLikeFields"][] = "Jml";



$tdatat_deal_lahan[".tableType"] = "chart";

$tdatat_deal_lahan[".printerPageOrientation"] = 0;
$tdatat_deal_lahan[".nPrinterPageScale"] = 100;

$tdatat_deal_lahan[".nPrinterSplitRecords"] = 40;

$tdatat_deal_lahan[".geocodingEnabled"] = false;



// chart settings
$tdatat_deal_lahan[".chartType"] = "2DPie";
// end of chart settings








$tstrOrderBy = "";
$tdatat_deal_lahan[".strOrderBy"] = $tstrOrderBy;

$tdatat_deal_lahan[".orderindexes"] = array();


$tdatat_deal_lahan[".sqlHead"] = "select *";
$tdatat_deal_lahan[".sqlFrom"] = "from  (  select  'Done' as status_lahan,  round(round(sum(tp.ukuran)/10000,2),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  Union  select   'Sisa' as status_lahan,  round((target_lahan-sum(tp.ukuran)/10000),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  )a";
$tdatat_deal_lahan[".sqlWhereExpr"] = "";
$tdatat_deal_lahan[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_deal_lahan[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_deal_lahan[".arrGroupsPerPage"] = $arrGPP;

$tdatat_deal_lahan[".highlightSearchResults"] = true;

$tableKeyst_deal_lahan = array();
$tdatat_deal_lahan[".Keys"] = $tableKeyst_deal_lahan;


$tdatat_deal_lahan[".hideMobileList"] = array();




//	status_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "status_lahan";
	$fdata["GoodName"] = "status_lahan";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_lahan","status_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_lahan";

	
		$fdata["FullName"] = "status_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_lahan["status_lahan"] = $fdata;
		$tdatat_deal_lahan[".searchableFields"][] = "status_lahan";
//	Jml
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Jml";
	$fdata["GoodName"] = "Jml";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_lahan","Jml");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Jml";

	
		$fdata["FullName"] = "Jml";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_lahan["Jml"] = $fdata;
		$tdatat_deal_lahan[".searchableFields"][] = "Jml";

$tdatat_deal_lahan[".chartLabelField"] = "status_lahan";
$tdatat_deal_lahan[".chartSeries"] = array();
$tdatat_deal_lahan[".chartSeries"][] = array(
	"field" => "Jml",
	"total" => ""
);
	$tdatat_deal_lahan[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">t_deal_lahan</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_pie</attr>
		</attr>

		<attr value="parameters">';
	$tdatat_deal_lahan[".chartXml"] .= '<attr value="0">
			<attr value="name">Jml</attr>';
	$tdatat_deal_lahan[".chartXml"] .= '</attr>';
	$tdatat_deal_lahan[".chartXml"] .= '<attr value="1">
		<attr value="name">status_lahan</attr>
	</attr>';
	$tdatat_deal_lahan[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatat_deal_lahan[".chartXml"] .= '<attr value="head">'.xmlencode("Progress Lahan").'</attr>
<attr value="foot">'.xmlencode("Lahan (Ha)").'</attr>
<attr value="y_axis_label">'.xmlencode("").'</attr>


<attr value="slegend">true</attr>
<attr value="sgrid">false</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatat_deal_lahan[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatat_deal_lahan[".chartXml"] .= '<attr value="0">
		<attr value="name">status_lahan</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("t_deal_lahan","status_lahan")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatat_deal_lahan[".chartXml"] .= '<attr value="1">
		<attr value="name">Jml</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("t_deal_lahan","Jml")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatat_deal_lahan[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">t_deal_lahan</attr>
<attr value="short_table_name">t_deal_lahan</attr>
</attr>

</chart>';

$tables_data["t_deal_lahan"]=&$tdatat_deal_lahan;
$field_labels["t_deal_lahan"] = &$fieldLabelst_deal_lahan;
$fieldToolTips["t_deal_lahan"] = &$fieldToolTipst_deal_lahan;
$placeHolders["t_deal_lahan"] = &$placeHolderst_deal_lahan;
$page_titles["t_deal_lahan"] = &$pageTitlest_deal_lahan;


changeTextControlsToDate( "t_deal_lahan" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_deal_lahan"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_deal_lahan"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_deal_lahan()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "*";
$proto0["m_strFrom"] = "from  (  select  'Done' as status_lahan,  round(round(sum(tp.ukuran)/10000,2),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  Union  select   'Sisa' as status_lahan,  round((target_lahan-sum(tp.ukuran)/10000),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  )a";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "*"
));

$proto6["m_sql"] = "*";
$proto6["m_srcTableName"] = "t_deal_lahan";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto8=array();
$proto8["m_link"] = "SQLL_MAIN";
			$proto9=array();
$proto9["m_strHead"] = "  select";
$proto9["m_strFieldList"] = "'Done' as status_lahan,  round(round(sum(tp.ukuran)/10000,2),2) as Jml";
$proto9["m_strFrom"] = "from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')";
$proto9["m_strWhere"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'";
$proto9["m_strOrderBy"] = "";
	
					
;
						$proto9["cipherer"] = null;
$proto11=array();
$proto11["m_sql"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'";
$proto11["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'"
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
						$proto13=array();
$proto13["m_sql"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y')";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
						$proto14=array();
$proto14["m_functiontype"] = "SQLF_CUSTOM";
$proto14["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "tp.created_date"
));

$proto14["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'%Y'"
));

$proto14["m_arguments"][]=$obj;
$proto14["m_strFunctionName"] = "date_format";
$obj = new SQLFunctionCall($proto14);

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "= date_format(now(),'%Y')";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

			$proto11["m_contained"][]=$obj;
						$proto17=array();
$proto17["m_sql"] = "td.`status`='CLOSED'";
$proto17["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "td",
	"m_srcTableName" => "t_deal_lahan"
));

$proto17["m_column"]=$obj;
$proto17["m_contained"] = array();
$proto17["m_strCase"] = "='CLOSED'";
$proto17["m_havingmode"] = false;
$proto17["m_inBrackets"] = false;
$proto17["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto17);

			$proto11["m_contained"][]=$obj;
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_where"] = $obj;
$proto19=array();
$proto19["m_sql"] = "";
$proto19["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto19["m_column"]=$obj;
$proto19["m_contained"] = array();
$proto19["m_strCase"] = "";
$proto19["m_havingmode"] = false;
$proto19["m_inBrackets"] = false;
$proto19["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto19);

$proto9["m_having"] = $obj;
$proto9["m_fieldlist"] = array();
						$proto21=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "'Done'"
));

$proto21["m_sql"] = "'Done'";
$proto21["m_srcTableName"] = "t_deal_lahan";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "status_lahan";
$obj = new SQLFieldListItem($proto21);

$proto9["m_fieldlist"][]=$obj;
						$proto23=array();
			$proto24=array();
$proto24["m_functiontype"] = "SQLF_CUSTOM";
$proto24["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "round(sum(tp.ukuran)/10000,2)"
));

$proto24["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "2"
));

$proto24["m_arguments"][]=$obj;
$proto24["m_strFunctionName"] = "round";
$obj = new SQLFunctionCall($proto24);

$proto23["m_sql"] = "round(round(sum(tp.ukuran)/10000,2),2)";
$proto23["m_srcTableName"] = "t_deal_lahan";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "Jml";
$obj = new SQLFieldListItem($proto23);

$proto9["m_fieldlist"][]=$obj;
$proto9["m_fromlist"] = array();
												$proto27=array();
$proto27["m_link"] = "SQLL_MAIN";
			$proto28=array();
$proto28["m_strName"] = "t_deal";
$proto28["m_srcTableName"] = "t_deal_lahan";
$proto28["m_columns"] = array();
$proto28["m_columns"][] = "id";
$proto28["m_columns"][] = "id_lahan";
$proto28["m_columns"][] = "start_date";
$proto28["m_columns"][] = "due_date";
$proto28["m_columns"][] = "pic";
$proto28["m_columns"][] = "keterangan";
$proto28["m_columns"][] = "total_harga";
$proto28["m_columns"][] = "harga_tatum";
$proto28["m_columns"][] = "harga_bangunan";
$proto28["m_columns"][] = "harga_per_m2";
$proto28["m_columns"][] = "total_biaya";
$proto28["m_columns"][] = "status";
$proto28["m_columns"][] = "close_by";
$proto28["m_columns"][] = "close_date";
$proto28["m_columns"][] = "created_date";
$proto28["m_columns"][] = "created_by";
$proto28["m_columns"][] = "updated_date";
$proto28["m_columns"][] = "updated_by";
$obj = new SQLTable($proto28);

$proto27["m_table"] = $obj;
$proto27["m_sql"] = "t_deal td";
$proto27["m_alias"] = "td";
$proto27["m_srcTableName"] = "t_deal_lahan";
$proto29=array();
$proto29["m_sql"] = "";
$proto29["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto29["m_column"]=$obj;
$proto29["m_contained"] = array();
$proto29["m_strCase"] = "";
$proto29["m_havingmode"] = false;
$proto29["m_inBrackets"] = false;
$proto29["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto29);

$proto27["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto27);

$proto9["m_fromlist"][]=$obj;
												$proto31=array();
$proto31["m_link"] = "SQLL_LEFTJOIN";
			$proto32=array();
$proto32["m_strName"] = "t_pengukuran";
$proto32["m_srcTableName"] = "t_deal_lahan";
$proto32["m_columns"] = array();
$proto32["m_columns"][] = "id";
$proto32["m_columns"][] = "id_lahan";
$proto32["m_columns"][] = "start_date";
$proto32["m_columns"][] = "due_date";
$proto32["m_columns"][] = "pic";
$proto32["m_columns"][] = "ukuran";
$proto32["m_columns"][] = "utipe";
$proto32["m_columns"][] = "batas_utara";
$proto32["m_columns"][] = "batas_utara_text";
$proto32["m_columns"][] = "stipe";
$proto32["m_columns"][] = "batas_selatan";
$proto32["m_columns"][] = "batas_selatan_text";
$proto32["m_columns"][] = "btipe";
$proto32["m_columns"][] = "batas_barat";
$proto32["m_columns"][] = "batas_barat_text";
$proto32["m_columns"][] = "ttipe";
$proto32["m_columns"][] = "batas_timur";
$proto32["m_columns"][] = "batas_timur_text";
$proto32["m_columns"][] = "keterangan";
$proto32["m_columns"][] = "maps_file";
$proto32["m_columns"][] = "status";
$proto32["m_columns"][] = "close_by";
$proto32["m_columns"][] = "close_date";
$proto32["m_columns"][] = "created_date";
$proto32["m_columns"][] = "created_by";
$proto32["m_columns"][] = "updated_date";
$proto32["m_columns"][] = "updated_by";
$proto32["m_columns"][] = "Lat";
$proto32["m_columns"][] = "Lng";
$proto32["m_columns"][] = "Lat2";
$proto32["m_columns"][] = "Lng2";
$obj = new SQLTable($proto32);

$proto31["m_table"] = $obj;
$proto31["m_sql"] = "left join t_pengukuran tp on tp.id_lahan = td.id_lahan";
$proto31["m_alias"] = "tp";
$proto31["m_srcTableName"] = "t_deal_lahan";
$proto33=array();
$proto33["m_sql"] = "tp.id_lahan = td.id_lahan";
$proto33["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "tp",
	"m_srcTableName" => "t_deal_lahan"
));

$proto33["m_column"]=$obj;
$proto33["m_contained"] = array();
$proto33["m_strCase"] = "= td.id_lahan";
$proto33["m_havingmode"] = false;
$proto33["m_inBrackets"] = false;
$proto33["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto33);

$proto31["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto31);

$proto9["m_fromlist"][]=$obj;
												$proto35=array();
$proto35["m_link"] = "SQLL_LEFTJOIN";
			$proto36=array();
$proto36["m_strName"] = "t_target_lahan";
$proto36["m_srcTableName"] = "t_deal_lahan";
$proto36["m_columns"] = array();
$proto36["m_columns"][] = "id";
$proto36["m_columns"][] = "year";
$proto36["m_columns"][] = "target_lahan";
$proto36["m_columns"][] = "created_date";
$proto36["m_columns"][] = "created_by";
$proto36["m_columns"][] = "updated_date";
$proto36["m_columns"][] = "updated_by";
$obj = new SQLTable($proto36);

$proto35["m_table"] = $obj;
$proto35["m_sql"] = "left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')";
$proto35["m_alias"] = "tl";
$proto35["m_srcTableName"] = "t_deal_lahan";
$proto37=array();
$proto37["m_sql"] = "tl.year = date_format(tp.created_date,'%Y')";
$proto37["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "year",
	"m_strTable" => "tl",
	"m_srcTableName" => "t_deal_lahan"
));

$proto37["m_column"]=$obj;
$proto37["m_contained"] = array();
$proto37["m_strCase"] = "= date_format(tp.created_date,'%Y')";
$proto37["m_havingmode"] = false;
$proto37["m_inBrackets"] = false;
$proto37["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto37);

$proto35["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto35);

$proto9["m_fromlist"][]=$obj;
$proto9["m_groupby"] = array();
$proto9["m_orderby"] = array();
$proto9["m_srcTableName"]="t_deal_lahan";		
$obj = new SQLQuery($proto9);

$proto8["m_table"] = $obj;
$proto8["m_sql"] = "(  select  'Done' as status_lahan,  round(round(sum(tp.ukuran)/10000,2),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  Union  select   'Sisa' as status_lahan,  round((target_lahan-sum(tp.ukuran)/10000),2) as Jml  from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')   where date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'  )a";
$proto8["m_alias"] = "a";
$proto8["m_srcTableName"] = "t_deal_lahan";
$proto39=array();
$proto39["m_sql"] = "";
$proto39["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto39["m_column"]=$obj;
$proto39["m_contained"] = array();
$proto39["m_strCase"] = "";
$proto39["m_havingmode"] = false;
$proto39["m_inBrackets"] = false;
$proto39["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto39);

$proto8["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto8);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_deal_lahan";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_deal_lahan = createSqlQuery_t_deal_lahan();


	
					
;

		

$tdatat_deal_lahan[".sqlquery"] = $queryData_t_deal_lahan;



include_once(getabspath("include/t_deal_lahan_events.php"));
$tdatat_deal_lahan[".hasEvents"] = true;

?>