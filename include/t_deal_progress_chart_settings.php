<?php
$tdatat_deal_progress_chart = array();
$tdatat_deal_progress_chart[".searchableFields"] = array();
$tdatat_deal_progress_chart[".ShortName"] = "t_deal_progress_chart";
$tdatat_deal_progress_chart[".OwnerID"] = "";
$tdatat_deal_progress_chart[".OriginalTable"] = "t_deal";


$tdatat_deal_progress_chart[".pagesByType"] = my_json_decode( "{\"search\":[\"search\"]}" );
$tdatat_deal_progress_chart[".originalPagesByType"] = $tdatat_deal_progress_chart[".pagesByType"];
$tdatat_deal_progress_chart[".pages"] = types2pages( my_json_decode( "{\"search\":[\"search\"]}" ) );
$tdatat_deal_progress_chart[".originalPages"] = $tdatat_deal_progress_chart[".pages"];
$tdatat_deal_progress_chart[".defaultPages"] = my_json_decode( "{\"search\":\"search\"}" );
$tdatat_deal_progress_chart[".originalDefaultPages"] = $tdatat_deal_progress_chart[".defaultPages"];

//	field labels
$fieldLabelst_deal_progress_chart = array();
$fieldToolTipst_deal_progress_chart = array();
$pageTitlest_deal_progress_chart = array();
$placeHolderst_deal_progress_chart = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_deal_progress_chart["English"] = array();
	$fieldToolTipst_deal_progress_chart["English"] = array();
	$placeHolderst_deal_progress_chart["English"] = array();
	$pageTitlest_deal_progress_chart["English"] = array();
	$fieldLabelst_deal_progress_chart["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_deal_progress_chart["English"]["id_lahan"] = "";
	$placeHolderst_deal_progress_chart["English"]["id_lahan"] = "";
	$fieldLabelst_deal_progress_chart["English"]["jml_mp"] = "Jml Mp";
	$fieldToolTipst_deal_progress_chart["English"]["jml_mp"] = "";
	$placeHolderst_deal_progress_chart["English"]["jml_mp"] = "";
	$fieldLabelst_deal_progress_chart["English"]["jml_ha"] = "Jml Ha";
	$fieldToolTipst_deal_progress_chart["English"]["jml_ha"] = "";
	$placeHolderst_deal_progress_chart["English"]["jml_ha"] = "";
	$fieldLabelst_deal_progress_chart["English"]["year"] = "Year";
	$fieldToolTipst_deal_progress_chart["English"]["year"] = "";
	$placeHolderst_deal_progress_chart["English"]["year"] = "";
	$fieldLabelst_deal_progress_chart["English"]["target_lahan"] = "Target Lahan";
	$fieldToolTipst_deal_progress_chart["English"]["target_lahan"] = "";
	$placeHolderst_deal_progress_chart["English"]["target_lahan"] = "";
	$fieldLabelst_deal_progress_chart["English"]["sisa"] = "Sisa";
	$fieldToolTipst_deal_progress_chart["English"]["sisa"] = "";
	$placeHolderst_deal_progress_chart["English"]["sisa"] = "";
	$fieldLabelst_deal_progress_chart["English"]["persentase_done"] = "Persentase Done";
	$fieldToolTipst_deal_progress_chart["English"]["persentase_done"] = "";
	$placeHolderst_deal_progress_chart["English"]["persentase_done"] = "";
	$fieldLabelst_deal_progress_chart["English"]["persentase_progress"] = "Persentase Progress";
	$fieldToolTipst_deal_progress_chart["English"]["persentase_progress"] = "";
	$placeHolderst_deal_progress_chart["English"]["persentase_progress"] = "";
	if (count($fieldToolTipst_deal_progress_chart["English"]))
		$tdatat_deal_progress_chart[".isUseToolTips"] = true;
}


	$tdatat_deal_progress_chart[".NCSearch"] = true;



$tdatat_deal_progress_chart[".shortTableName"] = "t_deal_progress_chart";
$tdatat_deal_progress_chart[".nSecOptions"] = 0;

$tdatat_deal_progress_chart[".mainTableOwnerID"] = "";
$tdatat_deal_progress_chart[".entityType"] = 1;
$tdatat_deal_progress_chart[".connId"] = "db_lla_at_localhost";


$tdatat_deal_progress_chart[".strOriginalTableName"] = "t_deal";

		 



$tdatat_deal_progress_chart[".showAddInPopup"] = false;

$tdatat_deal_progress_chart[".showEditInPopup"] = false;

$tdatat_deal_progress_chart[".showViewInPopup"] = false;

$tdatat_deal_progress_chart[".listAjax"] = false;
//	temporary
//$tdatat_deal_progress_chart[".listAjax"] = false;

	$tdatat_deal_progress_chart[".audit"] = false;

	$tdatat_deal_progress_chart[".locking"] = false;


$pages = $tdatat_deal_progress_chart[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_deal_progress_chart[".edit"] = true;
	$tdatat_deal_progress_chart[".afterEditAction"] = 1;
	$tdatat_deal_progress_chart[".closePopupAfterEdit"] = 1;
	$tdatat_deal_progress_chart[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_deal_progress_chart[".add"] = true;
$tdatat_deal_progress_chart[".afterAddAction"] = 1;
$tdatat_deal_progress_chart[".closePopupAfterAdd"] = 1;
$tdatat_deal_progress_chart[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_deal_progress_chart[".list"] = true;
}



$tdatat_deal_progress_chart[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_deal_progress_chart[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_deal_progress_chart[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_deal_progress_chart[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_deal_progress_chart[".printFriendly"] = true;
}



$tdatat_deal_progress_chart[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_deal_progress_chart[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_deal_progress_chart[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_deal_progress_chart[".isUseAjaxSuggest"] = true;

$tdatat_deal_progress_chart[".rowHighlite"] = true;



			

$tdatat_deal_progress_chart[".ajaxCodeSnippetAdded"] = false;

$tdatat_deal_progress_chart[".buttonsAdded"] = false;

$tdatat_deal_progress_chart[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_deal_progress_chart[".isUseTimeForSearch"] = false;


$tdatat_deal_progress_chart[".badgeColor"] = "E8926F";


$tdatat_deal_progress_chart[".allSearchFields"] = array();
$tdatat_deal_progress_chart[".filterFields"] = array();
$tdatat_deal_progress_chart[".requiredSearchFields"] = array();

$tdatat_deal_progress_chart[".googleLikeFields"] = array();
$tdatat_deal_progress_chart[".googleLikeFields"][] = "id_lahan";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "jml_mp";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "jml_ha";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "year";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "target_lahan";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "sisa";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "persentase_done";
$tdatat_deal_progress_chart[".googleLikeFields"][] = "persentase_progress";



$tdatat_deal_progress_chart[".tableType"] = "list";

$tdatat_deal_progress_chart[".printerPageOrientation"] = 0;
$tdatat_deal_progress_chart[".nPrinterPageScale"] = 100;

$tdatat_deal_progress_chart[".nPrinterSplitRecords"] = 40;

$tdatat_deal_progress_chart[".geocodingEnabled"] = false;










$tdatat_deal_progress_chart[".pageSize"] = 20;

$tdatat_deal_progress_chart[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_deal_progress_chart[".strOrderBy"] = $tstrOrderBy;

$tdatat_deal_progress_chart[".orderindexes"] = array();


$tdatat_deal_progress_chart[".sqlHead"] = "select td.id_lahan,   sum(tp.ukuran) as jml_mp,   round(sum(tp.ukuran)/10000,2) as jml_ha,   tl.year,   tl.target_lahan,   tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa,   round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase_done,   round((target_lahan-sum(tp.ukuran)/10000)/tl.target_lahan*100,2) as persentase_progress";
$tdatat_deal_progress_chart[".sqlFrom"] = "from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')";
$tdatat_deal_progress_chart[".sqlWhereExpr"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'";
$tdatat_deal_progress_chart[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_deal_progress_chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_deal_progress_chart[".arrGroupsPerPage"] = $arrGPP;

$tdatat_deal_progress_chart[".highlightSearchResults"] = true;

$tableKeyst_deal_progress_chart = array();
$tdatat_deal_progress_chart[".Keys"] = $tableKeyst_deal_progress_chart;


$tdatat_deal_progress_chart[".hideMobileList"] = array();




//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "td.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_negosiasi_lookup";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["autoCompleteFields"][] = array('masterF'=>"ukuran", 'lookupF'=>"ukuran");
	$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_negosiasi.status ='CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["id_lahan"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "id_lahan";
//	jml_mp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "jml_mp";
	$fdata["GoodName"] = "jml_mp";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","jml_mp");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "jml_mp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sum(tp.ukuran)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["jml_mp"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "jml_mp";
//	jml_ha
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "jml_ha";
	$fdata["GoodName"] = "jml_ha";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","jml_ha");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "jml_ha";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "round(sum(tp.ukuran)/10000,2)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["jml_ha"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "jml_ha";
//	year
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "year";
	$fdata["GoodName"] = "year";
	$fdata["ownerTable"] = "t_target_lahan";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","year");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "year";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tl.year";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["year"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "year";
//	target_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "target_lahan";
	$fdata["GoodName"] = "target_lahan";
	$fdata["ownerTable"] = "t_target_lahan";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","target_lahan");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "target_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tl.target_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["target_lahan"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "target_lahan";
//	sisa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "sisa";
	$fdata["GoodName"] = "sisa";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","sisa");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "sisa";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tl.target_lahan-round(sum(tp.ukuran)/10000,2)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["sisa"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "sisa";
//	persentase_done
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "persentase_done";
	$fdata["GoodName"] = "persentase_done";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","persentase_done");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "persentase_done";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["persentase_done"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "persentase_done";
//	persentase_progress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "persentase_progress";
	$fdata["GoodName"] = "persentase_progress";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_deal_progress_chart","persentase_progress");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "persentase_progress";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "round((target_lahan-sum(tp.ukuran)/10000)/tl.target_lahan*100,2)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_deal_progress_chart["persentase_progress"] = $fdata;
		$tdatat_deal_progress_chart[".searchableFields"][] = "persentase_progress";


$tables_data["t_deal_progress_chart"]=&$tdatat_deal_progress_chart;
$field_labels["t_deal_progress_chart"] = &$fieldLabelst_deal_progress_chart;
$fieldToolTips["t_deal_progress_chart"] = &$fieldToolTipst_deal_progress_chart;
$placeHolders["t_deal_progress_chart"] = &$placeHolderst_deal_progress_chart;
$page_titles["t_deal_progress_chart"] = &$pageTitlest_deal_progress_chart;


changeTextControlsToDate( "t_deal_progress_chart" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_deal_progress_chart"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_deal_progress_chart"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_deal_progress_chart()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "td.id_lahan,   sum(tp.ukuran) as jml_mp,   round(sum(tp.ukuran)/10000,2) as jml_ha,   tl.year,   tl.target_lahan,   tl.target_lahan-round(sum(tp.ukuran)/10000,2) as sisa,   round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2) as persentase_done,   round((target_lahan-sum(tp.ukuran)/10000)/tl.target_lahan*100,2) as persentase_progress";
$proto0["m_strFrom"] = "from t_deal td  left join t_pengukuran tp on tp.id_lahan = td.id_lahan   left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')";
$proto0["m_strWhere"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'";
$proto2["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "date_format(tp.created_date,'%Y') = date_format(now(),'%Y') and td.`status`='CLOSED'"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
						$proto4=array();
$proto4["m_sql"] = "date_format(tp.created_date,'%Y') = date_format(now(),'%Y')";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
						$proto5=array();
$proto5["m_functiontype"] = "SQLF_CUSTOM";
$proto5["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "tp.created_date"
));

$proto5["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'%Y'"
));

$proto5["m_arguments"][]=$obj;
$proto5["m_strFunctionName"] = "date_format";
$obj = new SQLFunctionCall($proto5);

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "= date_format(now(),'%Y')";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

			$proto2["m_contained"][]=$obj;
						$proto8=array();
$proto8["m_sql"] = "td.`status`='CLOSED'";
$proto8["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "td",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto8["m_column"]=$obj;
$proto8["m_contained"] = array();
$proto8["m_strCase"] = "='CLOSED'";
$proto8["m_havingmode"] = false;
$proto8["m_inBrackets"] = false;
$proto8["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto8);

			$proto2["m_contained"][]=$obj;
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto10=array();
$proto10["m_sql"] = "";
$proto10["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto10["m_column"]=$obj;
$proto10["m_contained"] = array();
$proto10["m_strCase"] = "";
$proto10["m_havingmode"] = false;
$proto10["m_inBrackets"] = false;
$proto10["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto10);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "td",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto12["m_sql"] = "td.id_lahan";
$proto12["m_srcTableName"] = "t_deal_progress_chart";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$proto15=array();
$proto15["m_functiontype"] = "SQLF_SUM";
$proto15["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "ukuran",
	"m_strTable" => "tp",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto15["m_arguments"][]=$obj;
$proto15["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto15);

$proto14["m_sql"] = "sum(tp.ukuran)";
$proto14["m_srcTableName"] = "t_deal_progress_chart";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "jml_mp";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$proto18=array();
$proto18["m_functiontype"] = "SQLF_CUSTOM";
$proto18["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "sum(tp.ukuran)/10000"
));

$proto18["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "2"
));

$proto18["m_arguments"][]=$obj;
$proto18["m_strFunctionName"] = "round";
$obj = new SQLFunctionCall($proto18);

$proto17["m_sql"] = "round(sum(tp.ukuran)/10000,2)";
$proto17["m_srcTableName"] = "t_deal_progress_chart";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "jml_ha";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "year",
	"m_strTable" => "tl",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto21["m_sql"] = "tl.year";
$proto21["m_srcTableName"] = "t_deal_progress_chart";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "target_lahan",
	"m_strTable" => "tl",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto23["m_sql"] = "tl.target_lahan";
$proto23["m_srcTableName"] = "t_deal_progress_chart";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "tl.target_lahan-round(sum(tp.ukuran)/10000,2)"
));

$proto25["m_sql"] = "tl.target_lahan-round(sum(tp.ukuran)/10000,2)";
$proto25["m_srcTableName"] = "t_deal_progress_chart";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "sisa";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$proto28=array();
$proto28["m_functiontype"] = "SQLF_CUSTOM";
$proto28["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100"
));

$proto28["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "2"
));

$proto28["m_arguments"][]=$obj;
$proto28["m_strFunctionName"] = "round";
$obj = new SQLFunctionCall($proto28);

$proto27["m_sql"] = "round(round(sum(tp.ukuran)/10000,2)/tl.target_lahan*100,2)";
$proto27["m_srcTableName"] = "t_deal_progress_chart";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "persentase_done";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
						$proto31=array();
			$proto32=array();
$proto32["m_functiontype"] = "SQLF_CUSTOM";
$proto32["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "(target_lahan-sum(tp.ukuran)/10000)/tl.target_lahan*100"
));

$proto32["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "2"
));

$proto32["m_arguments"][]=$obj;
$proto32["m_strFunctionName"] = "round";
$obj = new SQLFunctionCall($proto32);

$proto31["m_sql"] = "round((target_lahan-sum(tp.ukuran)/10000)/tl.target_lahan*100,2)";
$proto31["m_srcTableName"] = "t_deal_progress_chart";
$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "persentase_progress";
$obj = new SQLFieldListItem($proto31);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto35=array();
$proto35["m_link"] = "SQLL_MAIN";
			$proto36=array();
$proto36["m_strName"] = "t_deal";
$proto36["m_srcTableName"] = "t_deal_progress_chart";
$proto36["m_columns"] = array();
$proto36["m_columns"][] = "id";
$proto36["m_columns"][] = "id_lahan";
$proto36["m_columns"][] = "start_date";
$proto36["m_columns"][] = "due_date";
$proto36["m_columns"][] = "pic";
$proto36["m_columns"][] = "keterangan";
$proto36["m_columns"][] = "total_harga";
$proto36["m_columns"][] = "harga_tatum";
$proto36["m_columns"][] = "harga_bangunan";
$proto36["m_columns"][] = "harga_per_m2";
$proto36["m_columns"][] = "total_biaya";
$proto36["m_columns"][] = "status";
$proto36["m_columns"][] = "close_by";
$proto36["m_columns"][] = "close_date";
$proto36["m_columns"][] = "created_date";
$proto36["m_columns"][] = "created_by";
$proto36["m_columns"][] = "updated_date";
$proto36["m_columns"][] = "updated_by";
$obj = new SQLTable($proto36);

$proto35["m_table"] = $obj;
$proto35["m_sql"] = "t_deal td";
$proto35["m_alias"] = "td";
$proto35["m_srcTableName"] = "t_deal_progress_chart";
$proto37=array();
$proto37["m_sql"] = "";
$proto37["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto37["m_column"]=$obj;
$proto37["m_contained"] = array();
$proto37["m_strCase"] = "";
$proto37["m_havingmode"] = false;
$proto37["m_inBrackets"] = false;
$proto37["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto37);

$proto35["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto35);

$proto0["m_fromlist"][]=$obj;
												$proto39=array();
$proto39["m_link"] = "SQLL_LEFTJOIN";
			$proto40=array();
$proto40["m_strName"] = "t_pengukuran";
$proto40["m_srcTableName"] = "t_deal_progress_chart";
$proto40["m_columns"] = array();
$proto40["m_columns"][] = "id";
$proto40["m_columns"][] = "id_lahan";
$proto40["m_columns"][] = "start_date";
$proto40["m_columns"][] = "due_date";
$proto40["m_columns"][] = "pic";
$proto40["m_columns"][] = "ukuran";
$proto40["m_columns"][] = "utipe";
$proto40["m_columns"][] = "batas_utara";
$proto40["m_columns"][] = "batas_utara_text";
$proto40["m_columns"][] = "stipe";
$proto40["m_columns"][] = "batas_selatan";
$proto40["m_columns"][] = "batas_selatan_text";
$proto40["m_columns"][] = "btipe";
$proto40["m_columns"][] = "batas_barat";
$proto40["m_columns"][] = "batas_barat_text";
$proto40["m_columns"][] = "ttipe";
$proto40["m_columns"][] = "batas_timur";
$proto40["m_columns"][] = "batas_timur_text";
$proto40["m_columns"][] = "keterangan";
$proto40["m_columns"][] = "maps_file";
$proto40["m_columns"][] = "status";
$proto40["m_columns"][] = "close_by";
$proto40["m_columns"][] = "close_date";
$proto40["m_columns"][] = "created_date";
$proto40["m_columns"][] = "created_by";
$proto40["m_columns"][] = "updated_date";
$proto40["m_columns"][] = "updated_by";
$proto40["m_columns"][] = "Lat";
$proto40["m_columns"][] = "Lng";
$proto40["m_columns"][] = "Lat2";
$proto40["m_columns"][] = "Lng2";
$obj = new SQLTable($proto40);

$proto39["m_table"] = $obj;
$proto39["m_sql"] = "left join t_pengukuran tp on tp.id_lahan = td.id_lahan";
$proto39["m_alias"] = "tp";
$proto39["m_srcTableName"] = "t_deal_progress_chart";
$proto41=array();
$proto41["m_sql"] = "tp.id_lahan = td.id_lahan";
$proto41["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "tp",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto41["m_column"]=$obj;
$proto41["m_contained"] = array();
$proto41["m_strCase"] = "= td.id_lahan";
$proto41["m_havingmode"] = false;
$proto41["m_inBrackets"] = false;
$proto41["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto41);

$proto39["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto39);

$proto0["m_fromlist"][]=$obj;
												$proto43=array();
$proto43["m_link"] = "SQLL_LEFTJOIN";
			$proto44=array();
$proto44["m_strName"] = "t_target_lahan";
$proto44["m_srcTableName"] = "t_deal_progress_chart";
$proto44["m_columns"] = array();
$proto44["m_columns"][] = "id";
$proto44["m_columns"][] = "year";
$proto44["m_columns"][] = "target_lahan";
$proto44["m_columns"][] = "created_date";
$proto44["m_columns"][] = "created_by";
$proto44["m_columns"][] = "updated_date";
$proto44["m_columns"][] = "updated_by";
$obj = new SQLTable($proto44);

$proto43["m_table"] = $obj;
$proto43["m_sql"] = "left join t_target_lahan tl on tl.year = date_format(tp.created_date,'%Y')";
$proto43["m_alias"] = "tl";
$proto43["m_srcTableName"] = "t_deal_progress_chart";
$proto45=array();
$proto45["m_sql"] = "tl.year = date_format(tp.created_date,'%Y')";
$proto45["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "year",
	"m_strTable" => "tl",
	"m_srcTableName" => "t_deal_progress_chart"
));

$proto45["m_column"]=$obj;
$proto45["m_contained"] = array();
$proto45["m_strCase"] = "= date_format(tp.created_date,'%Y')";
$proto45["m_havingmode"] = false;
$proto45["m_inBrackets"] = false;
$proto45["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto45);

$proto43["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto43);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_deal_progress_chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_deal_progress_chart = createSqlQuery_t_deal_progress_chart();


	
					
;

								

$tdatat_deal_progress_chart[".sqlquery"] = $queryData_t_deal_progress_chart;



include_once(getabspath("include/t_deal_progress_chart_events.php"));
$tdatat_deal_progress_chart[".hasEvents"] = true;

?>