<?php
$tdatat_dok_detail_attach = array();
$tdatat_dok_detail_attach[".searchableFields"] = array();
$tdatat_dok_detail_attach[".ShortName"] = "t_dok_detail_attach";
$tdatat_dok_detail_attach[".OwnerID"] = "";
$tdatat_dok_detail_attach[".OriginalTable"] = "t_dok_detail_attach";


$tdatat_dok_detail_attach[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_dok_detail_attach[".originalPagesByType"] = $tdatat_dok_detail_attach[".pagesByType"];
$tdatat_dok_detail_attach[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_dok_detail_attach[".originalPages"] = $tdatat_dok_detail_attach[".pages"];
$tdatat_dok_detail_attach[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_dok_detail_attach[".originalDefaultPages"] = $tdatat_dok_detail_attach[".defaultPages"];

//	field labels
$fieldLabelst_dok_detail_attach = array();
$fieldToolTipst_dok_detail_attach = array();
$pageTitlest_dok_detail_attach = array();
$placeHolderst_dok_detail_attach = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_dok_detail_attach["English"] = array();
	$fieldToolTipst_dok_detail_attach["English"] = array();
	$placeHolderst_dok_detail_attach["English"] = array();
	$pageTitlest_dok_detail_attach["English"] = array();
	$fieldLabelst_dok_detail_attach["English"]["id"] = "Id";
	$fieldToolTipst_dok_detail_attach["English"]["id"] = "";
	$placeHolderst_dok_detail_attach["English"]["id"] = "";
	$fieldLabelst_dok_detail_attach["English"]["id_dok"] = "Id Dok";
	$fieldToolTipst_dok_detail_attach["English"]["id_dok"] = "";
	$placeHolderst_dok_detail_attach["English"]["id_dok"] = "";
	$fieldLabelst_dok_detail_attach["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_dok_detail_attach["English"]["param_dok"] = "";
	$placeHolderst_dok_detail_attach["English"]["param_dok"] = "";
	$fieldLabelst_dok_detail_attach["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_dok_detail_attach["English"]["file_dok"] = "";
	$placeHolderst_dok_detail_attach["English"]["file_dok"] = "";
	$fieldLabelst_dok_detail_attach["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_dok_detail_attach["English"]["keterangan"] = "";
	$placeHolderst_dok_detail_attach["English"]["keterangan"] = "";
	$fieldLabelst_dok_detail_attach["English"]["created_date"] = "Created Date";
	$fieldToolTipst_dok_detail_attach["English"]["created_date"] = "";
	$placeHolderst_dok_detail_attach["English"]["created_date"] = "";
	$fieldLabelst_dok_detail_attach["English"]["created_by"] = "Created By";
	$fieldToolTipst_dok_detail_attach["English"]["created_by"] = "";
	$placeHolderst_dok_detail_attach["English"]["created_by"] = "";
	$fieldLabelst_dok_detail_attach["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_dok_detail_attach["English"]["updated_date"] = "";
	$placeHolderst_dok_detail_attach["English"]["updated_date"] = "";
	$fieldLabelst_dok_detail_attach["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_dok_detail_attach["English"]["updated_by"] = "";
	$placeHolderst_dok_detail_attach["English"]["updated_by"] = "";
	if (count($fieldToolTipst_dok_detail_attach["English"]))
		$tdatat_dok_detail_attach[".isUseToolTips"] = true;
}


	$tdatat_dok_detail_attach[".NCSearch"] = true;



$tdatat_dok_detail_attach[".shortTableName"] = "t_dok_detail_attach";
$tdatat_dok_detail_attach[".nSecOptions"] = 0;

$tdatat_dok_detail_attach[".mainTableOwnerID"] = "";
$tdatat_dok_detail_attach[".entityType"] = 0;
$tdatat_dok_detail_attach[".connId"] = "db_lla_at_localhost";


$tdatat_dok_detail_attach[".strOriginalTableName"] = "t_dok_detail_attach";

		 



$tdatat_dok_detail_attach[".showAddInPopup"] = false;

$tdatat_dok_detail_attach[".showEditInPopup"] = false;

$tdatat_dok_detail_attach[".showViewInPopup"] = false;

$tdatat_dok_detail_attach[".listAjax"] = false;
//	temporary
//$tdatat_dok_detail_attach[".listAjax"] = false;

	$tdatat_dok_detail_attach[".audit"] = false;

	$tdatat_dok_detail_attach[".locking"] = false;


$pages = $tdatat_dok_detail_attach[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_dok_detail_attach[".edit"] = true;
	$tdatat_dok_detail_attach[".afterEditAction"] = 1;
	$tdatat_dok_detail_attach[".closePopupAfterEdit"] = 1;
	$tdatat_dok_detail_attach[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_dok_detail_attach[".add"] = true;
$tdatat_dok_detail_attach[".afterAddAction"] = 1;
$tdatat_dok_detail_attach[".closePopupAfterAdd"] = 1;
$tdatat_dok_detail_attach[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_dok_detail_attach[".list"] = true;
}



$tdatat_dok_detail_attach[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_dok_detail_attach[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_dok_detail_attach[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_dok_detail_attach[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_dok_detail_attach[".printFriendly"] = true;
}



$tdatat_dok_detail_attach[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_dok_detail_attach[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_dok_detail_attach[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_dok_detail_attach[".isUseAjaxSuggest"] = true;

$tdatat_dok_detail_attach[".rowHighlite"] = true;



						

$tdatat_dok_detail_attach[".ajaxCodeSnippetAdded"] = false;

$tdatat_dok_detail_attach[".buttonsAdded"] = false;

$tdatat_dok_detail_attach[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_dok_detail_attach[".isUseTimeForSearch"] = false;


$tdatat_dok_detail_attach[".badgeColor"] = "d2691e";


$tdatat_dok_detail_attach[".allSearchFields"] = array();
$tdatat_dok_detail_attach[".filterFields"] = array();
$tdatat_dok_detail_attach[".requiredSearchFields"] = array();

$tdatat_dok_detail_attach[".googleLikeFields"] = array();
$tdatat_dok_detail_attach[".googleLikeFields"][] = "id";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "id_dok";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "param_dok";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "file_dok";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "keterangan";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "created_date";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "created_by";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "updated_date";
$tdatat_dok_detail_attach[".googleLikeFields"][] = "updated_by";



$tdatat_dok_detail_attach[".tableType"] = "list";

$tdatat_dok_detail_attach[".printerPageOrientation"] = 0;
$tdatat_dok_detail_attach[".nPrinterPageScale"] = 100;

$tdatat_dok_detail_attach[".nPrinterSplitRecords"] = 40;

$tdatat_dok_detail_attach[".geocodingEnabled"] = false;










$tdatat_dok_detail_attach[".pageSize"] = 20;

$tdatat_dok_detail_attach[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_dok_detail_attach[".strOrderBy"] = $tstrOrderBy;

$tdatat_dok_detail_attach[".orderindexes"] = array();


$tdatat_dok_detail_attach[".sqlHead"] = "SELECT id,  	id_dok,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_dok_detail_attach[".sqlFrom"] = "FROM t_dok_detail_attach";
$tdatat_dok_detail_attach[".sqlWhereExpr"] = "";
$tdatat_dok_detail_attach[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_dok_detail_attach[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_dok_detail_attach[".arrGroupsPerPage"] = $arrGPP;

$tdatat_dok_detail_attach[".highlightSearchResults"] = true;

$tableKeyst_dok_detail_attach = array();
$tableKeyst_dok_detail_attach[] = "id";
$tdatat_dok_detail_attach[".Keys"] = $tableKeyst_dok_detail_attach;


$tdatat_dok_detail_attach[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["id"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "id";
//	id_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_dok";
	$fdata["GoodName"] = "id_dok";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","id_dok");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_dok";

		$fdata["sourceSingle"] = "id_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["id_dok"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "id_dok";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

		$edata["HorizontalLookup"] = true;

		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["param_dok"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["file_dok"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["keterangan"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["created_date"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["created_by"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["updated_date"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_dok_detail_attach";
	$fdata["Label"] = GetFieldLabel("t_dok_detail_attach","updated_by");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dok_detail_attach["updated_by"] = $fdata;
		$tdatat_dok_detail_attach[".searchableFields"][] = "updated_by";


$tables_data["t_dok_detail_attach"]=&$tdatat_dok_detail_attach;
$field_labels["t_dok_detail_attach"] = &$fieldLabelst_dok_detail_attach;
$fieldToolTips["t_dok_detail_attach"] = &$fieldToolTipst_dok_detail_attach;
$placeHolders["t_dok_detail_attach"] = &$placeHolderst_dok_detail_attach;
$page_titles["t_dok_detail_attach"] = &$pageTitlest_dok_detail_attach;


changeTextControlsToDate( "t_dok_detail_attach" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_dok_detail_attach"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_dok_detail_attach"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_dokumentasi";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_dokumentasi";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_dokumentasi";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_dok_detail_attach"][0] = $masterParams;
				$masterTablesData["t_dok_detail_attach"][0]["masterKeys"] = array();
	$masterTablesData["t_dok_detail_attach"][0]["masterKeys"][]="id";
				$masterTablesData["t_dok_detail_attach"][0]["detailKeys"] = array();
	$masterTablesData["t_dok_detail_attach"][0]["detailKeys"][]="id_dok";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_dok_detail_attach()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_dok,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_dok_detail_attach";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_dok_detail_attach";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_dok",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto8["m_sql"] = "id_dok";
$proto8["m_srcTableName"] = "t_dok_detail_attach";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto10["m_sql"] = "param_dok";
$proto10["m_srcTableName"] = "t_dok_detail_attach";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto12["m_sql"] = "file_dok";
$proto12["m_srcTableName"] = "t_dok_detail_attach";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto14["m_sql"] = "keterangan";
$proto14["m_srcTableName"] = "t_dok_detail_attach";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto16["m_sql"] = "created_date";
$proto16["m_srcTableName"] = "t_dok_detail_attach";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto18["m_sql"] = "created_by";
$proto18["m_srcTableName"] = "t_dok_detail_attach";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto20["m_sql"] = "updated_date";
$proto20["m_srcTableName"] = "t_dok_detail_attach";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_dok_detail_attach",
	"m_srcTableName" => "t_dok_detail_attach"
));

$proto22["m_sql"] = "updated_by";
$proto22["m_srcTableName"] = "t_dok_detail_attach";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "t_dok_detail_attach";
$proto25["m_srcTableName"] = "t_dok_detail_attach";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "id";
$proto25["m_columns"][] = "id_dok";
$proto25["m_columns"][] = "param_dok";
$proto25["m_columns"][] = "file_dok";
$proto25["m_columns"][] = "keterangan";
$proto25["m_columns"][] = "created_date";
$proto25["m_columns"][] = "created_by";
$proto25["m_columns"][] = "updated_date";
$proto25["m_columns"][] = "updated_by";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "t_dok_detail_attach";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "t_dok_detail_attach";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_dok_detail_attach";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_dok_detail_attach = createSqlQuery_t_dok_detail_attach();


	
					
;

									

$tdatat_dok_detail_attach[".sqlquery"] = $queryData_t_dok_detail_attach;



include_once(getabspath("include/t_dok_detail_attach_events.php"));
$tdatat_dok_detail_attach[".hasEvents"] = true;

?>