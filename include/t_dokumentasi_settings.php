<?php
$tdatat_dokumentasi = array();
$tdatat_dokumentasi[".searchableFields"] = array();
$tdatat_dokumentasi[".ShortName"] = "t_dokumentasi";
$tdatat_dokumentasi[".OwnerID"] = "";
$tdatat_dokumentasi[".OriginalTable"] = "t_dokumentasi";


$tdatat_dokumentasi[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_dokumentasi[".originalPagesByType"] = $tdatat_dokumentasi[".pagesByType"];
$tdatat_dokumentasi[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_dokumentasi[".originalPages"] = $tdatat_dokumentasi[".pages"];
$tdatat_dokumentasi[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_dokumentasi[".originalDefaultPages"] = $tdatat_dokumentasi[".defaultPages"];

//	field labels
$fieldLabelst_dokumentasi = array();
$fieldToolTipst_dokumentasi = array();
$pageTitlest_dokumentasi = array();
$placeHolderst_dokumentasi = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_dokumentasi["English"] = array();
	$fieldToolTipst_dokumentasi["English"] = array();
	$placeHolderst_dokumentasi["English"] = array();
	$pageTitlest_dokumentasi["English"] = array();
	$fieldLabelst_dokumentasi["English"]["id"] = "Id";
	$fieldToolTipst_dokumentasi["English"]["id"] = "";
	$placeHolderst_dokumentasi["English"]["id"] = "";
	$fieldLabelst_dokumentasi["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_dokumentasi["English"]["id_lahan"] = "";
	$placeHolderst_dokumentasi["English"]["id_lahan"] = "";
	$fieldLabelst_dokumentasi["English"]["start_date"] = "Start Date";
	$fieldToolTipst_dokumentasi["English"]["start_date"] = "";
	$placeHolderst_dokumentasi["English"]["start_date"] = "";
	$fieldLabelst_dokumentasi["English"]["due_date"] = "Due Date";
	$fieldToolTipst_dokumentasi["English"]["due_date"] = "";
	$placeHolderst_dokumentasi["English"]["due_date"] = "";
	$fieldLabelst_dokumentasi["English"]["pic"] = "PIC";
	$fieldToolTipst_dokumentasi["English"]["pic"] = "";
	$placeHolderst_dokumentasi["English"]["pic"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_dokumentasi["English"]["keterangan"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan"] = "";
	$fieldLabelst_dokumentasi["English"]["status_lahan_pelepasan"] = "Status Lahan";
	$fieldToolTipst_dokumentasi["English"]["status_lahan_pelepasan"] = "";
	$placeHolderst_dokumentasi["English"]["status_lahan_pelepasan"] = "";
	$fieldLabelst_dokumentasi["English"]["status_lahan_sertifkat"] = "Status Lahan Sertifkat";
	$fieldToolTipst_dokumentasi["English"]["status_lahan_sertifkat"] = "";
	$placeHolderst_dokumentasi["English"]["status_lahan_sertifkat"] = "";
	$fieldLabelst_dokumentasi["English"]["status"] = "Status";
	$fieldToolTipst_dokumentasi["English"]["status"] = "";
	$placeHolderst_dokumentasi["English"]["status"] = "";
	$fieldLabelst_dokumentasi["English"]["close_by"] = "Close By";
	$fieldToolTipst_dokumentasi["English"]["close_by"] = "";
	$placeHolderst_dokumentasi["English"]["close_by"] = "";
	$fieldLabelst_dokumentasi["English"]["close_date"] = "Close Date";
	$fieldToolTipst_dokumentasi["English"]["close_date"] = "";
	$placeHolderst_dokumentasi["English"]["close_date"] = "";
	$fieldLabelst_dokumentasi["English"]["created_date"] = "Created Date";
	$fieldToolTipst_dokumentasi["English"]["created_date"] = "";
	$placeHolderst_dokumentasi["English"]["created_date"] = "";
	$fieldLabelst_dokumentasi["English"]["created_by"] = "Created By";
	$fieldToolTipst_dokumentasi["English"]["created_by"] = "";
	$placeHolderst_dokumentasi["English"]["created_by"] = "";
	$fieldLabelst_dokumentasi["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_dokumentasi["English"]["updated_date"] = "";
	$placeHolderst_dokumentasi["English"]["updated_date"] = "";
	$fieldLabelst_dokumentasi["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_dokumentasi["English"]["updated_by"] = "";
	$placeHolderst_dokumentasi["English"]["updated_by"] = "";
	$fieldLabelst_dokumentasi["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_dokumentasi["English"]["nama_lahan"] = "";
	$placeHolderst_dokumentasi["English"]["nama_lahan"] = "";
	$fieldLabelst_dokumentasi["English"]["ukuran"] = "Ukuran";
	$fieldToolTipst_dokumentasi["English"]["ukuran"] = "";
	$placeHolderst_dokumentasi["English"]["ukuran"] = "";
	$fieldLabelst_dokumentasi["English"]["harga_bangunan"] = "Harga Bangunan";
	$fieldToolTipst_dokumentasi["English"]["harga_bangunan"] = "";
	$placeHolderst_dokumentasi["English"]["harga_bangunan"] = "";
	$fieldLabelst_dokumentasi["English"]["harga_per_m2"] = "Harga Per M2";
	$fieldToolTipst_dokumentasi["English"]["harga_per_m2"] = "";
	$placeHolderst_dokumentasi["English"]["harga_per_m2"] = "";
	$fieldLabelst_dokumentasi["English"]["total_biaya"] = "Total Biaya";
	$fieldToolTipst_dokumentasi["English"]["total_biaya"] = "";
	$placeHolderst_dokumentasi["English"]["total_biaya"] = "";
	$fieldLabelst_dokumentasi["English"]["blok_lokasi"] = "Blok Lokasi";
	$fieldToolTipst_dokumentasi["English"]["blok_lokasi"] = "";
	$placeHolderst_dokumentasi["English"]["blok_lokasi"] = "";
	$fieldLabelst_dokumentasi["English"]["nama_lokasi"] = "Nama Lokasi";
	$fieldToolTipst_dokumentasi["English"]["nama_lokasi"] = "";
	$placeHolderst_dokumentasi["English"]["nama_lokasi"] = "";
	$fieldLabelst_dokumentasi["English"]["keperluan_lahan"] = "Keperluan Lahan";
	$fieldToolTipst_dokumentasi["English"]["keperluan_lahan"] = "";
	$placeHolderst_dokumentasi["English"]["keperluan_lahan"] = "";
	$fieldLabelst_dokumentasi["English"]["batas_utara"] = "Batas Utara";
	$fieldToolTipst_dokumentasi["English"]["batas_utara"] = "";
	$placeHolderst_dokumentasi["English"]["batas_utara"] = "";
	$fieldLabelst_dokumentasi["English"]["batas_selatan"] = "Batas Selatan";
	$fieldToolTipst_dokumentasi["English"]["batas_selatan"] = "";
	$placeHolderst_dokumentasi["English"]["batas_selatan"] = "";
	$fieldLabelst_dokumentasi["English"]["batas_barat"] = "Batas Barat";
	$fieldToolTipst_dokumentasi["English"]["batas_barat"] = "";
	$placeHolderst_dokumentasi["English"]["batas_barat"] = "";
	$fieldLabelst_dokumentasi["English"]["batas_timur"] = "Batas Timur";
	$fieldToolTipst_dokumentasi["English"]["batas_timur"] = "";
	$placeHolderst_dokumentasi["English"]["batas_timur"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan_pengukuran"] = "Keterangan Pengukuran";
	$fieldToolTipst_dokumentasi["English"]["keterangan_pengukuran"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan_pengukuran"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan_input_awal"] = "Keterangan Input Awal";
	$fieldToolTipst_dokumentasi["English"]["keterangan_input_awal"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan_input_awal"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan_negosiasi"] = "Keterangan Negosiasi";
	$fieldToolTipst_dokumentasi["English"]["keterangan_negosiasi"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan_negosiasi"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan_deal"] = "Keterangan Deal";
	$fieldToolTipst_dokumentasi["English"]["keterangan_deal"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan_deal"] = "";
	$fieldLabelst_dokumentasi["English"]["dp"] = "DP";
	$fieldToolTipst_dokumentasi["English"]["dp"] = "";
	$placeHolderst_dokumentasi["English"]["dp"] = "";
	$fieldLabelst_dokumentasi["English"]["sisa_pembayaran"] = "Sisa Pembayaran";
	$fieldToolTipst_dokumentasi["English"]["sisa_pembayaran"] = "";
	$placeHolderst_dokumentasi["English"]["sisa_pembayaran"] = "";
	$fieldLabelst_dokumentasi["English"]["tipe"] = "Tipe";
	$fieldToolTipst_dokumentasi["English"]["tipe"] = "";
	$placeHolderst_dokumentasi["English"]["tipe"] = "";
	$fieldLabelst_dokumentasi["English"]["biaya_admin"] = "Biaya Admin";
	$fieldToolTipst_dokumentasi["English"]["biaya_admin"] = "";
	$placeHolderst_dokumentasi["English"]["biaya_admin"] = "";
	$fieldLabelst_dokumentasi["English"]["keterangan_perm_dana"] = "Keterangan Permintaan Dana";
	$fieldToolTipst_dokumentasi["English"]["keterangan_perm_dana"] = "";
	$placeHolderst_dokumentasi["English"]["keterangan_perm_dana"] = "";
	$pageTitlest_dokumentasi["English"]["masterlist"] = "Dokumentasi {%nama_lahan}";
	$pageTitlest_dokumentasi["English"]["view"] = "Dokumentasi {%id_lahan}, {%nama_lahan}";
	if (count($fieldToolTipst_dokumentasi["English"]))
		$tdatat_dokumentasi[".isUseToolTips"] = true;
}


	$tdatat_dokumentasi[".NCSearch"] = true;



$tdatat_dokumentasi[".shortTableName"] = "t_dokumentasi";
$tdatat_dokumentasi[".nSecOptions"] = 0;

$tdatat_dokumentasi[".mainTableOwnerID"] = "";
$tdatat_dokumentasi[".entityType"] = 0;
$tdatat_dokumentasi[".connId"] = "db_lla_at_localhost";


$tdatat_dokumentasi[".strOriginalTableName"] = "t_dokumentasi";

		 



$tdatat_dokumentasi[".showAddInPopup"] = false;

$tdatat_dokumentasi[".showEditInPopup"] = false;

$tdatat_dokumentasi[".showViewInPopup"] = false;

$tdatat_dokumentasi[".listAjax"] = false;
//	temporary
//$tdatat_dokumentasi[".listAjax"] = false;

	$tdatat_dokumentasi[".audit"] = false;

	$tdatat_dokumentasi[".locking"] = false;


$pages = $tdatat_dokumentasi[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_dokumentasi[".edit"] = true;
	$tdatat_dokumentasi[".afterEditAction"] = 1;
	$tdatat_dokumentasi[".closePopupAfterEdit"] = 1;
	$tdatat_dokumentasi[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_dokumentasi[".add"] = true;
$tdatat_dokumentasi[".afterAddAction"] = 1;
$tdatat_dokumentasi[".closePopupAfterAdd"] = 1;
$tdatat_dokumentasi[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_dokumentasi[".list"] = true;
}



$tdatat_dokumentasi[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_dokumentasi[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_dokumentasi[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_dokumentasi[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_dokumentasi[".printFriendly"] = true;
}



$tdatat_dokumentasi[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_dokumentasi[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_dokumentasi[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_dokumentasi[".isUseAjaxSuggest"] = true;

$tdatat_dokumentasi[".rowHighlite"] = true;



			

$tdatat_dokumentasi[".ajaxCodeSnippetAdded"] = false;

$tdatat_dokumentasi[".buttonsAdded"] = false;

$tdatat_dokumentasi[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_dokumentasi[".isUseTimeForSearch"] = false;


$tdatat_dokumentasi[".badgeColor"] = "6b8e23";


$tdatat_dokumentasi[".allSearchFields"] = array();
$tdatat_dokumentasi[".filterFields"] = array();
$tdatat_dokumentasi[".requiredSearchFields"] = array();

$tdatat_dokumentasi[".googleLikeFields"] = array();
$tdatat_dokumentasi[".googleLikeFields"][] = "id";
$tdatat_dokumentasi[".googleLikeFields"][] = "id_lahan";
$tdatat_dokumentasi[".googleLikeFields"][] = "nama_lahan";
$tdatat_dokumentasi[".googleLikeFields"][] = "ukuran";
$tdatat_dokumentasi[".googleLikeFields"][] = "start_date";
$tdatat_dokumentasi[".googleLikeFields"][] = "due_date";
$tdatat_dokumentasi[".googleLikeFields"][] = "pic";
$tdatat_dokumentasi[".googleLikeFields"][] = "harga_bangunan";
$tdatat_dokumentasi[".googleLikeFields"][] = "harga_per_m2";
$tdatat_dokumentasi[".googleLikeFields"][] = "total_biaya";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan";
$tdatat_dokumentasi[".googleLikeFields"][] = "status_lahan_pelepasan";
$tdatat_dokumentasi[".googleLikeFields"][] = "status_lahan_sertifkat";
$tdatat_dokumentasi[".googleLikeFields"][] = "status";
$tdatat_dokumentasi[".googleLikeFields"][] = "close_by";
$tdatat_dokumentasi[".googleLikeFields"][] = "close_date";
$tdatat_dokumentasi[".googleLikeFields"][] = "created_date";
$tdatat_dokumentasi[".googleLikeFields"][] = "created_by";
$tdatat_dokumentasi[".googleLikeFields"][] = "updated_date";
$tdatat_dokumentasi[".googleLikeFields"][] = "updated_by";
$tdatat_dokumentasi[".googleLikeFields"][] = "blok_lokasi";
$tdatat_dokumentasi[".googleLikeFields"][] = "nama_lokasi";
$tdatat_dokumentasi[".googleLikeFields"][] = "keperluan_lahan";
$tdatat_dokumentasi[".googleLikeFields"][] = "batas_utara";
$tdatat_dokumentasi[".googleLikeFields"][] = "batas_selatan";
$tdatat_dokumentasi[".googleLikeFields"][] = "batas_barat";
$tdatat_dokumentasi[".googleLikeFields"][] = "batas_timur";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan_pengukuran";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan_input_awal";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan_negosiasi";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan_deal";
$tdatat_dokumentasi[".googleLikeFields"][] = "dp";
$tdatat_dokumentasi[".googleLikeFields"][] = "sisa_pembayaran";
$tdatat_dokumentasi[".googleLikeFields"][] = "tipe";
$tdatat_dokumentasi[".googleLikeFields"][] = "biaya_admin";
$tdatat_dokumentasi[".googleLikeFields"][] = "keterangan_perm_dana";



$tdatat_dokumentasi[".tableType"] = "list";

$tdatat_dokumentasi[".printerPageOrientation"] = 0;
$tdatat_dokumentasi[".nPrinterPageScale"] = 100;

$tdatat_dokumentasi[".nPrinterSplitRecords"] = 40;

$tdatat_dokumentasi[".geocodingEnabled"] = false;










$tdatat_dokumentasi[".pageSize"] = 20;

$tdatat_dokumentasi[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_dokumentasi[".strOrderBy"] = $tstrOrderBy;

$tdatat_dokumentasi[".orderindexes"] = array();


$tdatat_dokumentasi[".sqlHead"] = "SELECT t_dokumentasi.id,  t_dokumentasi.id_lahan,  t_input_awal.nama_lahan,  t_pengukuran.ukuran,  t_dokumentasi.start_date,  t_dokumentasi.due_date,  t_dokumentasi.pic,  t_deal.harga_bangunan,  t_deal.harga_per_m2,  t_deal.total_biaya,  t_dokumentasi.keterangan,  t_dokumentasi.status_lahan_pelepasan,  t_dokumentasi.status_lahan_sertifkat,  t_dokumentasi.status,  t_dokumentasi.close_by,  t_dokumentasi.close_date,  t_dokumentasi.created_date,  t_dokumentasi.created_by,  t_dokumentasi.updated_date,  t_dokumentasi.updated_by,  t_input_awal.blok_lokasi,  t_input_awal.nama_lokasi,  t_input_awal.keperluan_lahan,  t_pengukuran.batas_utara,  t_pengukuran.batas_selatan,  t_pengukuran.batas_barat,  t_pengukuran.batas_timur,  t_pengukuran.keterangan AS keterangan_pengukuran,  t_input_awal.keterangan AS keterangan_input_awal,  t_negosiasi.keterangan AS keterangan_negosiasi,  t_deal.keterangan AS keterangan_deal,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.tipe,  t_perm_dana.biaya_admin,  t_perm_dana.keterangan AS keterangan_perm_dana";
$tdatat_dokumentasi[".sqlFrom"] = "FROM t_dokumentasi  LEFT JOIN t_input_awal ON t_dokumentasi.id_lahan = t_input_awal.id_lahan  LEFT JOIN t_pengukuran ON t_dokumentasi.id_lahan = t_pengukuran.id_lahan  LEFT JOIN t_deal ON t_dokumentasi.id_lahan = t_deal.id_lahan  LEFT JOIN t_negosiasi ON t_dokumentasi.id_lahan = t_negosiasi.id_lahan  LEFT JOIN t_perm_dana ON t_dokumentasi.id_lahan = t_perm_dana.id_lahan";
$tdatat_dokumentasi[".sqlWhereExpr"] = "";
$tdatat_dokumentasi[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_dokumentasi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_dokumentasi[".arrGroupsPerPage"] = $arrGPP;

$tdatat_dokumentasi[".highlightSearchResults"] = true;

$tableKeyst_dokumentasi = array();
$tableKeyst_dokumentasi[] = "id";
$tdatat_dokumentasi[".Keys"] = $tableKeyst_dokumentasi;


$tdatat_dokumentasi[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["id"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_deal";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

	

	
	$edata["LookupOrderBy"] = "created_date";

		$edata["LookupDesc"] = true;

	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_deal";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

	

	
	$edata["LookupOrderBy"] = "created_date";

		$edata["LookupDesc"] = true;

	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["id_lahan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["nama_lahan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "nama_lahan";
//	ukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ukuran";
	$fdata["GoodName"] = "ukuran";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","ukuran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "ukuran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.ukuran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["ukuran"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "ukuran";
//	start_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "start_date";
	$fdata["GoodName"] = "start_date";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","start_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date";

		$fdata["sourceSingle"] = "start_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.start_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["start_date"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "start_date";
//	due_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "due_date";
	$fdata["GoodName"] = "due_date";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","due_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date";

		$fdata["sourceSingle"] = "due_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.due_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["due_date"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "due_date";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["pic"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "pic";
//	harga_bangunan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "harga_bangunan";
	$fdata["GoodName"] = "harga_bangunan";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","harga_bangunan");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "harga_bangunan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.harga_bangunan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["harga_bangunan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "harga_bangunan";
//	harga_per_m2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "harga_per_m2";
	$fdata["GoodName"] = "harga_per_m2";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","harga_per_m2");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "harga_per_m2";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.harga_per_m2";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["harga_per_m2"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "harga_per_m2";
//	total_biaya
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "total_biaya";
	$fdata["GoodName"] = "total_biaya";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","total_biaya");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "total_biaya";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.total_biaya";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["total_biaya"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "total_biaya";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan";
//	status_lahan_pelepasan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "status_lahan_pelepasan";
	$fdata["GoodName"] = "status_lahan_pelepasan";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","status_lahan_pelepasan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_lahan_pelepasan";

		$fdata["sourceSingle"] = "status_lahan_pelepasan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.status_lahan_pelepasan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "m_code_list";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 200;
	$edata["DisplayField"] = "Description";

				$edata["LookupWhere"] = "CatID='STST'";


	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["status_lahan_pelepasan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "status_lahan_pelepasan";
//	status_lahan_sertifkat
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "status_lahan_sertifkat";
	$fdata["GoodName"] = "status_lahan_sertifkat";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","status_lahan_sertifkat");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_lahan_sertifkat";

		$fdata["sourceSingle"] = "status_lahan_sertifkat";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.status_lahan_sertifkat";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["status_lahan_sertifkat"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "status_lahan_sertifkat";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["status"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "status";
//	close_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "close_by";
	$fdata["GoodName"] = "close_by";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","close_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_by";

		$fdata["sourceSingle"] = "close_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.close_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["close_by"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "close_by";
//	close_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "close_date";
	$fdata["GoodName"] = "close_date";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","close_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_date";

		$fdata["sourceSingle"] = "close_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.close_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["close_date"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "close_date";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["created_date"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["created_by"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["updated_date"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_dokumentasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_dokumentasi.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["updated_by"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "updated_by";
//	blok_lokasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "blok_lokasi";
	$fdata["GoodName"] = "blok_lokasi";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","blok_lokasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "blok_lokasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.blok_lokasi";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["blok_lokasi"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "blok_lokasi";
//	nama_lokasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "nama_lokasi";
	$fdata["GoodName"] = "nama_lokasi";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","nama_lokasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lokasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lokasi";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["nama_lokasi"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "nama_lokasi";
//	keperluan_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "keperluan_lahan";
	$fdata["GoodName"] = "keperluan_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keperluan_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "keperluan_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.keperluan_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keperluan_lahan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keperluan_lahan";
//	batas_utara
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "batas_utara";
	$fdata["GoodName"] = "batas_utara";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","batas_utara");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas_utara";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.batas_utara";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["batas_utara"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "batas_utara";
//	batas_selatan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "batas_selatan";
	$fdata["GoodName"] = "batas_selatan";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","batas_selatan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas_selatan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.batas_selatan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["batas_selatan"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "batas_selatan";
//	batas_barat
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "batas_barat";
	$fdata["GoodName"] = "batas_barat";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","batas_barat");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas_barat";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.batas_barat";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["batas_barat"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "batas_barat";
//	batas_timur
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "batas_timur";
	$fdata["GoodName"] = "batas_timur";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","batas_timur");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas_timur";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.batas_timur";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["batas_timur"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "batas_timur";
//	keterangan_pengukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "keterangan_pengukuran";
	$fdata["GoodName"] = "keterangan_pengukuran";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan_pengukuran");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan_pengukuran"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan_pengukuran";
//	keterangan_input_awal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "keterangan_input_awal";
	$fdata["GoodName"] = "keterangan_input_awal";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan_input_awal");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan_input_awal"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan_input_awal";
//	keterangan_negosiasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "keterangan_negosiasi";
	$fdata["GoodName"] = "keterangan_negosiasi";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan_negosiasi");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan_negosiasi"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan_negosiasi";
//	keterangan_deal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 31;
	$fdata["strName"] = "keterangan_deal";
	$fdata["GoodName"] = "keterangan_deal";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan_deal");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan_deal"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan_deal";
//	dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 32;
	$fdata["strName"] = "dp";
	$fdata["GoodName"] = "dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","dp");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["dp"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "dp";
//	sisa_pembayaran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 33;
	$fdata["strName"] = "sisa_pembayaran";
	$fdata["GoodName"] = "sisa_pembayaran";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","sisa_pembayaran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "sisa_pembayaran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.sisa_pembayaran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["sisa_pembayaran"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "sisa_pembayaran";
//	tipe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 34;
	$fdata["strName"] = "tipe";
	$fdata["GoodName"] = "tipe";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","tipe");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "tipe";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.tipe";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["tipe"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "tipe";
//	biaya_admin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 35;
	$fdata["strName"] = "biaya_admin";
	$fdata["GoodName"] = "biaya_admin";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","biaya_admin");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "biaya_admin";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.biaya_admin";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["biaya_admin"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "biaya_admin";
//	keterangan_perm_dana
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 36;
	$fdata["strName"] = "keterangan_perm_dana";
	$fdata["GoodName"] = "keterangan_perm_dana";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_dokumentasi","keterangan_perm_dana");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_dokumentasi["keterangan_perm_dana"] = $fdata;
		$tdatat_dokumentasi[".searchableFields"][] = "keterangan_perm_dana";


$tables_data["t_dokumentasi"]=&$tdatat_dokumentasi;
$field_labels["t_dokumentasi"] = &$fieldLabelst_dokumentasi;
$fieldToolTips["t_dokumentasi"] = &$fieldToolTipst_dokumentasi;
$placeHolders["t_dokumentasi"] = &$placeHolderst_dokumentasi;
$page_titles["t_dokumentasi"] = &$pageTitlest_dokumentasi;


changeTextControlsToDate( "t_dokumentasi" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_dokumentasi"] = array();
//	t_dok_detail_attach
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_dok_detail_attach";
		$detailsParam["dOriginalTable"] = "t_dok_detail_attach";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_dok_detail_attach";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_dok_detail_attach");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_dok";
//	t_input_awal_dok
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_input_awal_dok";
		$detailsParam["dOriginalTable"] = "t_input_awal";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_input_awal_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_input_awal_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//	t_pengukuran_percil_dok
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_pengukuran_percil_dok";
		$detailsParam["dOriginalTable"] = "t_pengukuran_percil";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_pengukuran_percil_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_pengukuran_percil_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//	t_nego_detail_dok
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_nego_detail_dok";
		$detailsParam["dOriginalTable"] = "t_nego_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_nego_detail_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_nego_detail_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//	t_perm_dana_detail_dok
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_perm_dana_detail_dok";
		$detailsParam["dOriginalTable"] = "t_perm_dana_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_perm_dana_detail_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_perm_dana_detail_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//	t_pembayaran_detail_dok
	
	

		$dIndex = 5;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_pembayaran_detail_dok";
		$detailsParam["dOriginalTable"] = "t_pembayaran_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_pembayaran_detail_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_pembayaran_detail_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//	t_registrasi_percil_dok
	
	

		$dIndex = 6;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_registrasi_percil_dok";
		$detailsParam["dOriginalTable"] = "t_pengukuran_percil";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_registrasi_percil_dok";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_registrasi_percil_dok");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_dokumentasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["masterKeys"][]="id_lahan";

				$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_dokumentasi"][$dIndex]["detailKeys"][]="id_lahan";
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_dokumentasi"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_dokumentasi()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_dokumentasi.id,  t_dokumentasi.id_lahan,  t_input_awal.nama_lahan,  t_pengukuran.ukuran,  t_dokumentasi.start_date,  t_dokumentasi.due_date,  t_dokumentasi.pic,  t_deal.harga_bangunan,  t_deal.harga_per_m2,  t_deal.total_biaya,  t_dokumentasi.keterangan,  t_dokumentasi.status_lahan_pelepasan,  t_dokumentasi.status_lahan_sertifkat,  t_dokumentasi.status,  t_dokumentasi.close_by,  t_dokumentasi.close_date,  t_dokumentasi.created_date,  t_dokumentasi.created_by,  t_dokumentasi.updated_date,  t_dokumentasi.updated_by,  t_input_awal.blok_lokasi,  t_input_awal.nama_lokasi,  t_input_awal.keperluan_lahan,  t_pengukuran.batas_utara,  t_pengukuran.batas_selatan,  t_pengukuran.batas_barat,  t_pengukuran.batas_timur,  t_pengukuran.keterangan AS keterangan_pengukuran,  t_input_awal.keterangan AS keterangan_input_awal,  t_negosiasi.keterangan AS keterangan_negosiasi,  t_deal.keterangan AS keterangan_deal,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.tipe,  t_perm_dana.biaya_admin,  t_perm_dana.keterangan AS keterangan_perm_dana";
$proto0["m_strFrom"] = "FROM t_dokumentasi  LEFT JOIN t_input_awal ON t_dokumentasi.id_lahan = t_input_awal.id_lahan  LEFT JOIN t_pengukuran ON t_dokumentasi.id_lahan = t_pengukuran.id_lahan  LEFT JOIN t_deal ON t_dokumentasi.id_lahan = t_deal.id_lahan  LEFT JOIN t_negosiasi ON t_dokumentasi.id_lahan = t_negosiasi.id_lahan  LEFT JOIN t_perm_dana ON t_dokumentasi.id_lahan = t_perm_dana.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto6["m_sql"] = "t_dokumentasi.id";
$proto6["m_srcTableName"] = "t_dokumentasi";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto8["m_sql"] = "t_dokumentasi.id_lahan";
$proto8["m_srcTableName"] = "t_dokumentasi";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto10["m_sql"] = "t_input_awal.nama_lahan";
$proto10["m_srcTableName"] = "t_dokumentasi";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ukuran",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto12["m_sql"] = "t_pengukuran.ukuran";
$proto12["m_srcTableName"] = "t_dokumentasi";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto14["m_sql"] = "t_dokumentasi.start_date";
$proto14["m_srcTableName"] = "t_dokumentasi";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto16["m_sql"] = "t_dokumentasi.due_date";
$proto16["m_srcTableName"] = "t_dokumentasi";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto18["m_sql"] = "t_dokumentasi.pic";
$proto18["m_srcTableName"] = "t_dokumentasi";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "harga_bangunan",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto20["m_sql"] = "t_deal.harga_bangunan";
$proto20["m_srcTableName"] = "t_dokumentasi";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "harga_per_m2",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto22["m_sql"] = "t_deal.harga_per_m2";
$proto22["m_srcTableName"] = "t_dokumentasi";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "total_biaya",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto24["m_sql"] = "t_deal.total_biaya";
$proto24["m_srcTableName"] = "t_dokumentasi";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto26["m_sql"] = "t_dokumentasi.keterangan";
$proto26["m_srcTableName"] = "t_dokumentasi";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "status_lahan_pelepasan",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto28["m_sql"] = "t_dokumentasi.status_lahan_pelepasan";
$proto28["m_srcTableName"] = "t_dokumentasi";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "status_lahan_sertifkat",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto30["m_sql"] = "t_dokumentasi.status_lahan_sertifkat";
$proto30["m_srcTableName"] = "t_dokumentasi";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto32["m_sql"] = "t_dokumentasi.status";
$proto32["m_srcTableName"] = "t_dokumentasi";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "close_by",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto34["m_sql"] = "t_dokumentasi.close_by";
$proto34["m_srcTableName"] = "t_dokumentasi";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "close_date",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto36["m_sql"] = "t_dokumentasi.close_date";
$proto36["m_srcTableName"] = "t_dokumentasi";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto38["m_sql"] = "t_dokumentasi.created_date";
$proto38["m_srcTableName"] = "t_dokumentasi";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto40["m_sql"] = "t_dokumentasi.created_by";
$proto40["m_srcTableName"] = "t_dokumentasi";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto42["m_sql"] = "t_dokumentasi.updated_date";
$proto42["m_srcTableName"] = "t_dokumentasi";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto44["m_sql"] = "t_dokumentasi.updated_by";
$proto44["m_srcTableName"] = "t_dokumentasi";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "blok_lokasi",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto46["m_sql"] = "t_input_awal.blok_lokasi";
$proto46["m_srcTableName"] = "t_dokumentasi";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lokasi",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto48["m_sql"] = "t_input_awal.nama_lokasi";
$proto48["m_srcTableName"] = "t_dokumentasi";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "keperluan_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto50["m_sql"] = "t_input_awal.keperluan_lahan";
$proto50["m_srcTableName"] = "t_dokumentasi";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "batas_utara",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto52["m_sql"] = "t_pengukuran.batas_utara";
$proto52["m_srcTableName"] = "t_dokumentasi";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "batas_selatan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto54["m_sql"] = "t_pengukuran.batas_selatan";
$proto54["m_srcTableName"] = "t_dokumentasi";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "batas_barat",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto56["m_sql"] = "t_pengukuran.batas_barat";
$proto56["m_srcTableName"] = "t_dokumentasi";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "batas_timur",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto58["m_sql"] = "t_pengukuran.batas_timur";
$proto58["m_srcTableName"] = "t_dokumentasi";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto60["m_sql"] = "t_pengukuran.keterangan";
$proto60["m_srcTableName"] = "t_dokumentasi";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "keterangan_pengukuran";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto62["m_sql"] = "t_input_awal.keterangan";
$proto62["m_srcTableName"] = "t_dokumentasi";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "keterangan_input_awal";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto64["m_sql"] = "t_negosiasi.keterangan";
$proto64["m_srcTableName"] = "t_dokumentasi";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "keterangan_negosiasi";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto66["m_sql"] = "t_deal.keterangan";
$proto66["m_srcTableName"] = "t_dokumentasi";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "keterangan_deal";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto68=array();
			$obj = new SQLField(array(
	"m_strName" => "dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto68["m_sql"] = "t_perm_dana.dp";
$proto68["m_srcTableName"] = "t_dokumentasi";
$proto68["m_expr"]=$obj;
$proto68["m_alias"] = "";
$obj = new SQLFieldListItem($proto68);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$obj = new SQLField(array(
	"m_strName" => "sisa_pembayaran",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto70["m_sql"] = "t_perm_dana.sisa_pembayaran";
$proto70["m_srcTableName"] = "t_dokumentasi";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto72=array();
			$obj = new SQLField(array(
	"m_strName" => "tipe",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto72["m_sql"] = "t_perm_dana.tipe";
$proto72["m_srcTableName"] = "t_dokumentasi";
$proto72["m_expr"]=$obj;
$proto72["m_alias"] = "";
$obj = new SQLFieldListItem($proto72);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$obj = new SQLField(array(
	"m_strName" => "biaya_admin",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto74["m_sql"] = "t_perm_dana.biaya_admin";
$proto74["m_srcTableName"] = "t_dokumentasi";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto76=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto76["m_sql"] = "t_perm_dana.keterangan";
$proto76["m_srcTableName"] = "t_dokumentasi";
$proto76["m_expr"]=$obj;
$proto76["m_alias"] = "keterangan_perm_dana";
$obj = new SQLFieldListItem($proto76);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto78=array();
$proto78["m_link"] = "SQLL_MAIN";
			$proto79=array();
$proto79["m_strName"] = "t_dokumentasi";
$proto79["m_srcTableName"] = "t_dokumentasi";
$proto79["m_columns"] = array();
$proto79["m_columns"][] = "id";
$proto79["m_columns"][] = "id_lahan";
$proto79["m_columns"][] = "start_date";
$proto79["m_columns"][] = "due_date";
$proto79["m_columns"][] = "pic";
$proto79["m_columns"][] = "keterangan";
$proto79["m_columns"][] = "status_lahan_pelepasan";
$proto79["m_columns"][] = "status_lahan_sertifkat";
$proto79["m_columns"][] = "status";
$proto79["m_columns"][] = "close_by";
$proto79["m_columns"][] = "close_date";
$proto79["m_columns"][] = "created_date";
$proto79["m_columns"][] = "created_by";
$proto79["m_columns"][] = "updated_date";
$proto79["m_columns"][] = "updated_by";
$obj = new SQLTable($proto79);

$proto78["m_table"] = $obj;
$proto78["m_sql"] = "t_dokumentasi";
$proto78["m_alias"] = "";
$proto78["m_srcTableName"] = "t_dokumentasi";
$proto80=array();
$proto80["m_sql"] = "";
$proto80["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto80["m_column"]=$obj;
$proto80["m_contained"] = array();
$proto80["m_strCase"] = "";
$proto80["m_havingmode"] = false;
$proto80["m_inBrackets"] = false;
$proto80["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto80);

$proto78["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto78);

$proto0["m_fromlist"][]=$obj;
												$proto82=array();
$proto82["m_link"] = "SQLL_LEFTJOIN";
			$proto83=array();
$proto83["m_strName"] = "t_input_awal";
$proto83["m_srcTableName"] = "t_dokumentasi";
$proto83["m_columns"] = array();
$proto83["m_columns"][] = "id";
$proto83["m_columns"][] = "id_lahan";
$proto83["m_columns"][] = "nama_lahan";
$proto83["m_columns"][] = "batas_utara";
$proto83["m_columns"][] = "batas_selatan";
$proto83["m_columns"][] = "batas_barat";
$proto83["m_columns"][] = "batas_timur";
$proto83["m_columns"][] = "blok_lokasi";
$proto83["m_columns"][] = "nama_lokasi";
$proto83["m_columns"][] = "keperluan_lahan";
$proto83["m_columns"][] = "start_date";
$proto83["m_columns"][] = "due_date";
$proto83["m_columns"][] = "pic";
$proto83["m_columns"][] = "status";
$proto83["m_columns"][] = "close_by";
$proto83["m_columns"][] = "close_date";
$proto83["m_columns"][] = "keterangan";
$proto83["m_columns"][] = "im_file";
$proto83["m_columns"][] = "created_date";
$proto83["m_columns"][] = "created_by";
$proto83["m_columns"][] = "updated_date";
$proto83["m_columns"][] = "updated_by";
$obj = new SQLTable($proto83);

$proto82["m_table"] = $obj;
$proto82["m_sql"] = "LEFT JOIN t_input_awal ON t_dokumentasi.id_lahan = t_input_awal.id_lahan";
$proto82["m_alias"] = "";
$proto82["m_srcTableName"] = "t_dokumentasi";
$proto84=array();
$proto84["m_sql"] = "t_input_awal.id_lahan = t_dokumentasi.id_lahan";
$proto84["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto84["m_column"]=$obj;
$proto84["m_contained"] = array();
$proto84["m_strCase"] = "= t_dokumentasi.id_lahan";
$proto84["m_havingmode"] = false;
$proto84["m_inBrackets"] = false;
$proto84["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto84);

$proto82["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto82);

$proto0["m_fromlist"][]=$obj;
												$proto86=array();
$proto86["m_link"] = "SQLL_LEFTJOIN";
			$proto87=array();
$proto87["m_strName"] = "t_pengukuran";
$proto87["m_srcTableName"] = "t_dokumentasi";
$proto87["m_columns"] = array();
$proto87["m_columns"][] = "id";
$proto87["m_columns"][] = "id_lahan";
$proto87["m_columns"][] = "start_date";
$proto87["m_columns"][] = "due_date";
$proto87["m_columns"][] = "pic";
$proto87["m_columns"][] = "ukuran";
$proto87["m_columns"][] = "utipe";
$proto87["m_columns"][] = "batas_utara";
$proto87["m_columns"][] = "batas_utara_text";
$proto87["m_columns"][] = "stipe";
$proto87["m_columns"][] = "batas_selatan";
$proto87["m_columns"][] = "batas_selatan_text";
$proto87["m_columns"][] = "btipe";
$proto87["m_columns"][] = "batas_barat";
$proto87["m_columns"][] = "batas_barat_text";
$proto87["m_columns"][] = "ttipe";
$proto87["m_columns"][] = "batas_timur";
$proto87["m_columns"][] = "batas_timur_text";
$proto87["m_columns"][] = "keterangan";
$proto87["m_columns"][] = "maps_file";
$proto87["m_columns"][] = "status";
$proto87["m_columns"][] = "close_by";
$proto87["m_columns"][] = "close_date";
$proto87["m_columns"][] = "created_date";
$proto87["m_columns"][] = "created_by";
$proto87["m_columns"][] = "updated_date";
$proto87["m_columns"][] = "updated_by";
$proto87["m_columns"][] = "Lat";
$proto87["m_columns"][] = "Lng";
$proto87["m_columns"][] = "Lat2";
$proto87["m_columns"][] = "Lng2";
$obj = new SQLTable($proto87);

$proto86["m_table"] = $obj;
$proto86["m_sql"] = "LEFT JOIN t_pengukuran ON t_dokumentasi.id_lahan = t_pengukuran.id_lahan";
$proto86["m_alias"] = "";
$proto86["m_srcTableName"] = "t_dokumentasi";
$proto88=array();
$proto88["m_sql"] = "t_pengukuran.id_lahan = t_dokumentasi.id_lahan";
$proto88["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_dokumentasi"
));

$proto88["m_column"]=$obj;
$proto88["m_contained"] = array();
$proto88["m_strCase"] = "= t_dokumentasi.id_lahan";
$proto88["m_havingmode"] = false;
$proto88["m_inBrackets"] = false;
$proto88["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto88);

$proto86["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto86);

$proto0["m_fromlist"][]=$obj;
												$proto90=array();
$proto90["m_link"] = "SQLL_LEFTJOIN";
			$proto91=array();
$proto91["m_strName"] = "t_deal";
$proto91["m_srcTableName"] = "t_dokumentasi";
$proto91["m_columns"] = array();
$proto91["m_columns"][] = "id";
$proto91["m_columns"][] = "id_lahan";
$proto91["m_columns"][] = "start_date";
$proto91["m_columns"][] = "due_date";
$proto91["m_columns"][] = "pic";
$proto91["m_columns"][] = "keterangan";
$proto91["m_columns"][] = "total_harga";
$proto91["m_columns"][] = "harga_tatum";
$proto91["m_columns"][] = "harga_bangunan";
$proto91["m_columns"][] = "harga_per_m2";
$proto91["m_columns"][] = "total_biaya";
$proto91["m_columns"][] = "status";
$proto91["m_columns"][] = "close_by";
$proto91["m_columns"][] = "close_date";
$proto91["m_columns"][] = "created_date";
$proto91["m_columns"][] = "created_by";
$proto91["m_columns"][] = "updated_date";
$proto91["m_columns"][] = "updated_by";
$obj = new SQLTable($proto91);

$proto90["m_table"] = $obj;
$proto90["m_sql"] = "LEFT JOIN t_deal ON t_dokumentasi.id_lahan = t_deal.id_lahan";
$proto90["m_alias"] = "";
$proto90["m_srcTableName"] = "t_dokumentasi";
$proto92=array();
$proto92["m_sql"] = "t_deal.id_lahan = t_dokumentasi.id_lahan";
$proto92["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_dokumentasi"
));

$proto92["m_column"]=$obj;
$proto92["m_contained"] = array();
$proto92["m_strCase"] = "= t_dokumentasi.id_lahan";
$proto92["m_havingmode"] = false;
$proto92["m_inBrackets"] = false;
$proto92["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto92);

$proto90["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto90);

$proto0["m_fromlist"][]=$obj;
												$proto94=array();
$proto94["m_link"] = "SQLL_LEFTJOIN";
			$proto95=array();
$proto95["m_strName"] = "t_negosiasi";
$proto95["m_srcTableName"] = "t_dokumentasi";
$proto95["m_columns"] = array();
$proto95["m_columns"][] = "id";
$proto95["m_columns"][] = "id_lahan";
$proto95["m_columns"][] = "start_date";
$proto95["m_columns"][] = "due_date";
$proto95["m_columns"][] = "pic";
$proto95["m_columns"][] = "keterangan";
$proto95["m_columns"][] = "status";
$proto95["m_columns"][] = "close_by";
$proto95["m_columns"][] = "close_date";
$proto95["m_columns"][] = "created_date";
$proto95["m_columns"][] = "created_by";
$proto95["m_columns"][] = "updated_date";
$proto95["m_columns"][] = "updated_by";
$obj = new SQLTable($proto95);

$proto94["m_table"] = $obj;
$proto94["m_sql"] = "LEFT JOIN t_negosiasi ON t_dokumentasi.id_lahan = t_negosiasi.id_lahan";
$proto94["m_alias"] = "";
$proto94["m_srcTableName"] = "t_dokumentasi";
$proto96=array();
$proto96["m_sql"] = "t_negosiasi.id_lahan = t_dokumentasi.id_lahan";
$proto96["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_dokumentasi"
));

$proto96["m_column"]=$obj;
$proto96["m_contained"] = array();
$proto96["m_strCase"] = "= t_dokumentasi.id_lahan";
$proto96["m_havingmode"] = false;
$proto96["m_inBrackets"] = false;
$proto96["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto96);

$proto94["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto94);

$proto0["m_fromlist"][]=$obj;
												$proto98=array();
$proto98["m_link"] = "SQLL_LEFTJOIN";
			$proto99=array();
$proto99["m_strName"] = "t_perm_dana";
$proto99["m_srcTableName"] = "t_dokumentasi";
$proto99["m_columns"] = array();
$proto99["m_columns"][] = "id";
$proto99["m_columns"][] = "id_lahan";
$proto99["m_columns"][] = "start_date_dp";
$proto99["m_columns"][] = "due_date_dp";
$proto99["m_columns"][] = "start_date_full";
$proto99["m_columns"][] = "due_date_full";
$proto99["m_columns"][] = "pic";
$proto99["m_columns"][] = "keterangan";
$proto99["m_columns"][] = "keterangan_bayar";
$proto99["m_columns"][] = "close_full_by";
$proto99["m_columns"][] = "close_full_date";
$proto99["m_columns"][] = "tipe";
$proto99["m_columns"][] = "dp";
$proto99["m_columns"][] = "sisa_pembayaran";
$proto99["m_columns"][] = "biaya_admin";
$proto99["m_columns"][] = "status";
$proto99["m_columns"][] = "status_full";
$proto99["m_columns"][] = "status_badmin";
$proto99["m_columns"][] = "close_badmin_by";
$proto99["m_columns"][] = "close_badmin_date";
$proto99["m_columns"][] = "close_dp_by";
$proto99["m_columns"][] = "close_dp_date";
$proto99["m_columns"][] = "created_date";
$proto99["m_columns"][] = "created_by";
$proto99["m_columns"][] = "updated_date";
$proto99["m_columns"][] = "updated_by";
$obj = new SQLTable($proto99);

$proto98["m_table"] = $obj;
$proto98["m_sql"] = "LEFT JOIN t_perm_dana ON t_dokumentasi.id_lahan = t_perm_dana.id_lahan";
$proto98["m_alias"] = "";
$proto98["m_srcTableName"] = "t_dokumentasi";
$proto100=array();
$proto100["m_sql"] = "t_perm_dana.id_lahan = t_dokumentasi.id_lahan";
$proto100["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_dokumentasi"
));

$proto100["m_column"]=$obj;
$proto100["m_contained"] = array();
$proto100["m_strCase"] = "= t_dokumentasi.id_lahan";
$proto100["m_havingmode"] = false;
$proto100["m_inBrackets"] = false;
$proto100["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto100);

$proto98["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto98);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_dokumentasi";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_dokumentasi = createSqlQuery_t_dokumentasi();


	
					
;

																																				

$tdatat_dokumentasi[".sqlquery"] = $queryData_t_dokumentasi;



include_once(getabspath("include/t_dokumentasi_events.php"));
$tdatat_dokumentasi[".hasEvents"] = true;

?>