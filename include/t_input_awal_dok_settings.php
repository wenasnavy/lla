<?php
$tdatat_input_awal_dok = array();
$tdatat_input_awal_dok[".searchableFields"] = array();
$tdatat_input_awal_dok[".ShortName"] = "t_input_awal_dok";
$tdatat_input_awal_dok[".OwnerID"] = "";
$tdatat_input_awal_dok[".OriginalTable"] = "t_input_awal";


$tdatat_input_awal_dok[".pagesByType"] = my_json_decode( "{\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"search\":[\"search\"]}" );
$tdatat_input_awal_dok[".originalPagesByType"] = $tdatat_input_awal_dok[".pagesByType"];
$tdatat_input_awal_dok[".pages"] = types2pages( my_json_decode( "{\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"search\":[\"search\"]}" ) );
$tdatat_input_awal_dok[".originalPages"] = $tdatat_input_awal_dok[".pages"];
$tdatat_input_awal_dok[".defaultPages"] = my_json_decode( "{\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"search\":\"search\"}" );
$tdatat_input_awal_dok[".originalDefaultPages"] = $tdatat_input_awal_dok[".defaultPages"];

//	field labels
$fieldLabelst_input_awal_dok = array();
$fieldToolTipst_input_awal_dok = array();
$pageTitlest_input_awal_dok = array();
$placeHolderst_input_awal_dok = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_input_awal_dok["English"] = array();
	$fieldToolTipst_input_awal_dok["English"] = array();
	$placeHolderst_input_awal_dok["English"] = array();
	$pageTitlest_input_awal_dok["English"] = array();
	$fieldLabelst_input_awal_dok["English"]["id"] = "Id";
	$fieldToolTipst_input_awal_dok["English"]["id"] = "";
	$placeHolderst_input_awal_dok["English"]["id"] = "";
	$fieldLabelst_input_awal_dok["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_input_awal_dok["English"]["id_lahan"] = "";
	$placeHolderst_input_awal_dok["English"]["id_lahan"] = "";
	$fieldLabelst_input_awal_dok["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_input_awal_dok["English"]["nama_lahan"] = "";
	$placeHolderst_input_awal_dok["English"]["nama_lahan"] = "";
	$fieldLabelst_input_awal_dok["English"]["keperluan_lahan"] = "Keperluan Lahan";
	$fieldToolTipst_input_awal_dok["English"]["keperluan_lahan"] = "";
	$placeHolderst_input_awal_dok["English"]["keperluan_lahan"] = "";
	$fieldLabelst_input_awal_dok["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_input_awal_dok["English"]["keterangan"] = "";
	$placeHolderst_input_awal_dok["English"]["keterangan"] = "";
	$fieldLabelst_input_awal_dok["English"]["created_date"] = "Created Date";
	$fieldToolTipst_input_awal_dok["English"]["created_date"] = "";
	$placeHolderst_input_awal_dok["English"]["created_date"] = "";
	$fieldLabelst_input_awal_dok["English"]["created_by"] = "Created By";
	$fieldToolTipst_input_awal_dok["English"]["created_by"] = "";
	$placeHolderst_input_awal_dok["English"]["created_by"] = "";
	$fieldLabelst_input_awal_dok["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_input_awal_dok["English"]["updated_date"] = "";
	$placeHolderst_input_awal_dok["English"]["updated_date"] = "";
	$fieldLabelst_input_awal_dok["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_input_awal_dok["English"]["updated_by"] = "";
	$placeHolderst_input_awal_dok["English"]["updated_by"] = "";
	$fieldLabelst_input_awal_dok["English"]["im_file"] = "IM File";
	$fieldToolTipst_input_awal_dok["English"]["im_file"] = "";
	$placeHolderst_input_awal_dok["English"]["im_file"] = "";
	$pageTitlest_input_awal_dok["English"]["list"] = "Input Awal Dokumen";
	if (count($fieldToolTipst_input_awal_dok["English"]))
		$tdatat_input_awal_dok[".isUseToolTips"] = true;
}


	$tdatat_input_awal_dok[".NCSearch"] = true;



$tdatat_input_awal_dok[".shortTableName"] = "t_input_awal_dok";
$tdatat_input_awal_dok[".nSecOptions"] = 0;

$tdatat_input_awal_dok[".mainTableOwnerID"] = "";
$tdatat_input_awal_dok[".entityType"] = 1;
$tdatat_input_awal_dok[".connId"] = "db_lla_at_localhost";


$tdatat_input_awal_dok[".strOriginalTableName"] = "t_input_awal";

		 



$tdatat_input_awal_dok[".showAddInPopup"] = false;

$tdatat_input_awal_dok[".showEditInPopup"] = false;

$tdatat_input_awal_dok[".showViewInPopup"] = false;

$tdatat_input_awal_dok[".listAjax"] = false;
//	temporary
//$tdatat_input_awal_dok[".listAjax"] = false;

	$tdatat_input_awal_dok[".audit"] = false;

	$tdatat_input_awal_dok[".locking"] = false;


$pages = $tdatat_input_awal_dok[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_input_awal_dok[".edit"] = true;
	$tdatat_input_awal_dok[".afterEditAction"] = 1;
	$tdatat_input_awal_dok[".closePopupAfterEdit"] = 1;
	$tdatat_input_awal_dok[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_input_awal_dok[".add"] = true;
$tdatat_input_awal_dok[".afterAddAction"] = 1;
$tdatat_input_awal_dok[".closePopupAfterAdd"] = 1;
$tdatat_input_awal_dok[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_input_awal_dok[".list"] = true;
}



$tdatat_input_awal_dok[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_input_awal_dok[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_input_awal_dok[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_input_awal_dok[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_input_awal_dok[".printFriendly"] = true;
}



$tdatat_input_awal_dok[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_input_awal_dok[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_input_awal_dok[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_input_awal_dok[".isUseAjaxSuggest"] = true;

$tdatat_input_awal_dok[".rowHighlite"] = true;



						

$tdatat_input_awal_dok[".ajaxCodeSnippetAdded"] = false;

$tdatat_input_awal_dok[".buttonsAdded"] = false;

$tdatat_input_awal_dok[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_input_awal_dok[".isUseTimeForSearch"] = false;


$tdatat_input_awal_dok[".badgeColor"] = "1e90ff";


$tdatat_input_awal_dok[".allSearchFields"] = array();
$tdatat_input_awal_dok[".filterFields"] = array();
$tdatat_input_awal_dok[".requiredSearchFields"] = array();

$tdatat_input_awal_dok[".googleLikeFields"] = array();
$tdatat_input_awal_dok[".googleLikeFields"][] = "id";
$tdatat_input_awal_dok[".googleLikeFields"][] = "id_lahan";
$tdatat_input_awal_dok[".googleLikeFields"][] = "nama_lahan";
$tdatat_input_awal_dok[".googleLikeFields"][] = "keperluan_lahan";
$tdatat_input_awal_dok[".googleLikeFields"][] = "keterangan";
$tdatat_input_awal_dok[".googleLikeFields"][] = "im_file";
$tdatat_input_awal_dok[".googleLikeFields"][] = "created_date";
$tdatat_input_awal_dok[".googleLikeFields"][] = "created_by";
$tdatat_input_awal_dok[".googleLikeFields"][] = "updated_date";
$tdatat_input_awal_dok[".googleLikeFields"][] = "updated_by";



$tdatat_input_awal_dok[".tableType"] = "list";

$tdatat_input_awal_dok[".printerPageOrientation"] = 0;
$tdatat_input_awal_dok[".nPrinterPageScale"] = 100;

$tdatat_input_awal_dok[".nPrinterSplitRecords"] = 40;

$tdatat_input_awal_dok[".geocodingEnabled"] = false;










$tdatat_input_awal_dok[".pageSize"] = 20;

$tdatat_input_awal_dok[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_input_awal_dok[".strOrderBy"] = $tstrOrderBy;

$tdatat_input_awal_dok[".orderindexes"] = array();


$tdatat_input_awal_dok[".sqlHead"] = "SELECT id, 	id_lahan, 	nama_lahan, 	keperluan_lahan, 	keterangan,  	im_file, 	created_date, 	created_by, 	updated_date, 	updated_by";
$tdatat_input_awal_dok[".sqlFrom"] = "FROM `t_input_awal`";
$tdatat_input_awal_dok[".sqlWhereExpr"] = "";
$tdatat_input_awal_dok[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_input_awal_dok[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_input_awal_dok[".arrGroupsPerPage"] = $arrGPP;

$tdatat_input_awal_dok[".highlightSearchResults"] = true;

$tableKeyst_input_awal_dok = array();
$tableKeyst_input_awal_dok[] = "id";
$tableKeyst_input_awal_dok[] = "id_lahan";
$tdatat_input_awal_dok[".Keys"] = $tableKeyst_input_awal_dok;


$tdatat_input_awal_dok[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["id"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["id_lahan"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

		$fdata["sourceSingle"] = "nama_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["nama_lahan"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "nama_lahan";
//	keperluan_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "keperluan_lahan";
	$fdata["GoodName"] = "keperluan_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","keperluan_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "keperluan_lahan";

		$fdata["sourceSingle"] = "keperluan_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keperluan_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_kpl";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "Description";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["keperluan_lahan"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "keperluan_lahan";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["keterangan"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "keterangan";
//	im_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "im_file";
	$fdata["GoodName"] = "im_file";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","im_file");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "im_file";

		$fdata["sourceSingle"] = "im_file";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "im_file";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["im_file"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "im_file";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["created_date"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["created_by"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["updated_date"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_dok","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_dok["updated_by"] = $fdata;
		$tdatat_input_awal_dok[".searchableFields"][] = "updated_by";


$tables_data["t_input_awal_dok"]=&$tdatat_input_awal_dok;
$field_labels["t_input_awal_dok"] = &$fieldLabelst_input_awal_dok;
$fieldToolTips["t_input_awal_dok"] = &$fieldToolTipst_input_awal_dok;
$placeHolders["t_input_awal_dok"] = &$placeHolderst_input_awal_dok;
$page_titles["t_input_awal_dok"] = &$pageTitlest_input_awal_dok;


changeTextControlsToDate( "t_input_awal_dok" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_input_awal_dok"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_input_awal_dok"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_dokumentasi";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_dokumentasi";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_dokumentasi";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_input_awal_dok"][0] = $masterParams;
				$masterTablesData["t_input_awal_dok"][0]["masterKeys"] = array();
	$masterTablesData["t_input_awal_dok"][0]["masterKeys"][]="id_lahan";
				$masterTablesData["t_input_awal_dok"][0]["detailKeys"] = array();
	$masterTablesData["t_input_awal_dok"][0]["detailKeys"][]="id_lahan";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_input_awal_dok()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id, 	id_lahan, 	nama_lahan, 	keperluan_lahan, 	keterangan,  	im_file, 	created_date, 	created_by, 	updated_date, 	updated_by";
$proto0["m_strFrom"] = "FROM `t_input_awal`";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_input_awal_dok";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto8["m_sql"] = "id_lahan";
$proto8["m_srcTableName"] = "t_input_awal_dok";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto10["m_sql"] = "nama_lahan";
$proto10["m_srcTableName"] = "t_input_awal_dok";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "keperluan_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto12["m_sql"] = "keperluan_lahan";
$proto12["m_srcTableName"] = "t_input_awal_dok";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto14["m_sql"] = "keterangan";
$proto14["m_srcTableName"] = "t_input_awal_dok";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "im_file",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto16["m_sql"] = "im_file";
$proto16["m_srcTableName"] = "t_input_awal_dok";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto18["m_sql"] = "created_date";
$proto18["m_srcTableName"] = "t_input_awal_dok";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto20["m_sql"] = "created_by";
$proto20["m_srcTableName"] = "t_input_awal_dok";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto22["m_sql"] = "updated_date";
$proto22["m_srcTableName"] = "t_input_awal_dok";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_dok"
));

$proto24["m_sql"] = "updated_by";
$proto24["m_srcTableName"] = "t_input_awal_dok";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto26=array();
$proto26["m_link"] = "SQLL_MAIN";
			$proto27=array();
$proto27["m_strName"] = "t_input_awal";
$proto27["m_srcTableName"] = "t_input_awal_dok";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "id";
$proto27["m_columns"][] = "id_lahan";
$proto27["m_columns"][] = "nama_lahan";
$proto27["m_columns"][] = "batas_utara";
$proto27["m_columns"][] = "batas_selatan";
$proto27["m_columns"][] = "batas_barat";
$proto27["m_columns"][] = "batas_timur";
$proto27["m_columns"][] = "blok_lokasi";
$proto27["m_columns"][] = "nama_lokasi";
$proto27["m_columns"][] = "keperluan_lahan";
$proto27["m_columns"][] = "start_date";
$proto27["m_columns"][] = "due_date";
$proto27["m_columns"][] = "pic";
$proto27["m_columns"][] = "status";
$proto27["m_columns"][] = "close_by";
$proto27["m_columns"][] = "close_date";
$proto27["m_columns"][] = "keterangan";
$proto27["m_columns"][] = "im_file";
$proto27["m_columns"][] = "created_date";
$proto27["m_columns"][] = "created_by";
$proto27["m_columns"][] = "updated_date";
$proto27["m_columns"][] = "updated_by";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "`t_input_awal`";
$proto26["m_alias"] = "";
$proto26["m_srcTableName"] = "t_input_awal_dok";
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_input_awal_dok";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_input_awal_dok = createSqlQuery_t_input_awal_dok();


	
					
;

										

$tdatat_input_awal_dok[".sqlquery"] = $queryData_t_input_awal_dok;



include_once(getabspath("include/t_input_awal_dok_events.php"));
$tdatat_input_awal_dok[".hasEvents"] = true;

?>