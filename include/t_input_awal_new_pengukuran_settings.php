<?php
$tdatat_input_awal_new_pengukuran = array();
$tdatat_input_awal_new_pengukuran[".searchableFields"] = array();
$tdatat_input_awal_new_pengukuran[".ShortName"] = "t_input_awal_new_pengukuran";
$tdatat_input_awal_new_pengukuran[".OwnerID"] = "";
$tdatat_input_awal_new_pengukuran[".OriginalTable"] = "t_input_awal";


$tdatat_input_awal_new_pengukuran[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_input_awal_new_pengukuran[".originalPagesByType"] = $tdatat_input_awal_new_pengukuran[".pagesByType"];
$tdatat_input_awal_new_pengukuran[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_input_awal_new_pengukuran[".originalPages"] = $tdatat_input_awal_new_pengukuran[".pages"];
$tdatat_input_awal_new_pengukuran[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_input_awal_new_pengukuran[".originalDefaultPages"] = $tdatat_input_awal_new_pengukuran[".defaultPages"];

//	field labels
$fieldLabelst_input_awal_new_pengukuran = array();
$fieldToolTipst_input_awal_new_pengukuran = array();
$pageTitlest_input_awal_new_pengukuran = array();
$placeHolderst_input_awal_new_pengukuran = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_input_awal_new_pengukuran["English"] = array();
	$fieldToolTipst_input_awal_new_pengukuran["English"] = array();
	$placeHolderst_input_awal_new_pengukuran["English"] = array();
	$pageTitlest_input_awal_new_pengukuran["English"] = array();
	$fieldLabelst_input_awal_new_pengukuran["English"]["id"] = "Id";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["id"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["id"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["id_lahan"] = "Id Lahan";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["id_lahan"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["id_lahan"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["nama_lahan"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["nama_lahan"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["blok_lokasi"] = "Blok Lokasi";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["blok_lokasi"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["blok_lokasi"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["nama_lokasi"] = "Nama Lokasi";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["nama_lokasi"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["nama_lokasi"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["keperluan_lahan"] = "Keperluan Lahan";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["keperluan_lahan"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["keperluan_lahan"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["start_date"] = "Start Date";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["start_date"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["start_date"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["due_date"] = "Due Date";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["due_date"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["due_date"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["pic"] = "Pic";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["pic"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["pic"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["status"] = "Status";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["status"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["status"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["close_by"] = "Close By";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["close_by"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["close_by"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["close_date"] = "Close Date";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["close_date"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["close_date"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["keterangan"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["keterangan"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["im_file"] = "Im File";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["im_file"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["im_file"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["created_date"] = "Created Date";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["created_date"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["created_date"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["created_by"] = "Created By";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["created_by"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["created_by"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["updated_date"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["updated_date"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["updated_by"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["updated_by"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["Lat"] = "Lat";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["Lat"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["Lat"] = "";
	$fieldLabelst_input_awal_new_pengukuran["English"]["Lng"] = "Lng";
	$fieldToolTipst_input_awal_new_pengukuran["English"]["Lng"] = "";
	$placeHolderst_input_awal_new_pengukuran["English"]["Lng"] = "";
	if (count($fieldToolTipst_input_awal_new_pengukuran["English"]))
		$tdatat_input_awal_new_pengukuran[".isUseToolTips"] = true;
}


	$tdatat_input_awal_new_pengukuran[".NCSearch"] = true;



$tdatat_input_awal_new_pengukuran[".shortTableName"] = "t_input_awal_new_pengukuran";
$tdatat_input_awal_new_pengukuran[".nSecOptions"] = 0;

$tdatat_input_awal_new_pengukuran[".mainTableOwnerID"] = "";
$tdatat_input_awal_new_pengukuran[".entityType"] = 1;
$tdatat_input_awal_new_pengukuran[".connId"] = "db_lla_at_localhost";


$tdatat_input_awal_new_pengukuran[".strOriginalTableName"] = "t_input_awal";

		 



$tdatat_input_awal_new_pengukuran[".showAddInPopup"] = false;

$tdatat_input_awal_new_pengukuran[".showEditInPopup"] = false;

$tdatat_input_awal_new_pengukuran[".showViewInPopup"] = false;

$tdatat_input_awal_new_pengukuran[".listAjax"] = false;
//	temporary
//$tdatat_input_awal_new_pengukuran[".listAjax"] = false;

	$tdatat_input_awal_new_pengukuran[".audit"] = false;

	$tdatat_input_awal_new_pengukuran[".locking"] = false;


$pages = $tdatat_input_awal_new_pengukuran[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_input_awal_new_pengukuran[".edit"] = true;
	$tdatat_input_awal_new_pengukuran[".afterEditAction"] = 1;
	$tdatat_input_awal_new_pengukuran[".closePopupAfterEdit"] = 1;
	$tdatat_input_awal_new_pengukuran[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_input_awal_new_pengukuran[".add"] = true;
$tdatat_input_awal_new_pengukuran[".afterAddAction"] = 1;
$tdatat_input_awal_new_pengukuran[".closePopupAfterAdd"] = 1;
$tdatat_input_awal_new_pengukuran[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_input_awal_new_pengukuran[".list"] = true;
}



$tdatat_input_awal_new_pengukuran[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_input_awal_new_pengukuran[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_input_awal_new_pengukuran[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_input_awal_new_pengukuran[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_input_awal_new_pengukuran[".printFriendly"] = true;
}



$tdatat_input_awal_new_pengukuran[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_input_awal_new_pengukuran[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_input_awal_new_pengukuran[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_input_awal_new_pengukuran[".isUseAjaxSuggest"] = true;

$tdatat_input_awal_new_pengukuran[".rowHighlite"] = true;



			

$tdatat_input_awal_new_pengukuran[".ajaxCodeSnippetAdded"] = false;

$tdatat_input_awal_new_pengukuran[".buttonsAdded"] = false;

$tdatat_input_awal_new_pengukuran[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_input_awal_new_pengukuran[".isUseTimeForSearch"] = false;


$tdatat_input_awal_new_pengukuran[".badgeColor"] = "00C2C5";


$tdatat_input_awal_new_pengukuran[".allSearchFields"] = array();
$tdatat_input_awal_new_pengukuran[".filterFields"] = array();
$tdatat_input_awal_new_pengukuran[".requiredSearchFields"] = array();

$tdatat_input_awal_new_pengukuran[".googleLikeFields"] = array();
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "id";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "id_lahan";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "nama_lahan";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "blok_lokasi";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "nama_lokasi";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "keperluan_lahan";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "start_date";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "due_date";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "pic";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "status";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "close_by";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "close_date";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "keterangan";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "im_file";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "created_date";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "created_by";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "updated_date";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "updated_by";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "Lat";
$tdatat_input_awal_new_pengukuran[".googleLikeFields"][] = "Lng";



$tdatat_input_awal_new_pengukuran[".tableType"] = "list";

$tdatat_input_awal_new_pengukuran[".printerPageOrientation"] = 0;
$tdatat_input_awal_new_pengukuran[".nPrinterPageScale"] = 100;

$tdatat_input_awal_new_pengukuran[".nPrinterSplitRecords"] = 40;

$tdatat_input_awal_new_pengukuran[".geocodingEnabled"] = false;










$tdatat_input_awal_new_pengukuran[".pageSize"] = 20;

$tdatat_input_awal_new_pengukuran[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_input_awal_new_pengukuran[".strOrderBy"] = $tstrOrderBy;

$tdatat_input_awal_new_pengukuran[".orderindexes"] = array();


$tdatat_input_awal_new_pengukuran[".sqlHead"] = "SELECT t_input_awal.id,  t_input_awal.id_lahan,  t_input_awal.nama_lahan,  t_input_awal.blok_lokasi,  t_input_awal.nama_lokasi,  t_input_awal.keperluan_lahan,  t_input_awal.start_date,  t_input_awal.due_date,  t_input_awal.pic,  t_input_awal.status,  t_input_awal.close_by,  t_input_awal.close_date,  t_input_awal.keterangan,  t_input_awal.im_file,  t_input_awal.created_date,  t_input_awal.created_by,  t_input_awal.updated_date,  t_input_awal.updated_by,  t_pengukuran.Lat,  t_pengukuran.Lng";
$tdatat_input_awal_new_pengukuran[".sqlFrom"] = "FROM t_input_awal  INNER JOIN t_pengukuran ON t_input_awal.id_lahan = t_pengukuran.id_lahan";
$tdatat_input_awal_new_pengukuran[".sqlWhereExpr"] = "";
$tdatat_input_awal_new_pengukuran[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_input_awal_new_pengukuran[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_input_awal_new_pengukuran[".arrGroupsPerPage"] = $arrGPP;

$tdatat_input_awal_new_pengukuran[".highlightSearchResults"] = true;

$tableKeyst_input_awal_new_pengukuran = array();
$tableKeyst_input_awal_new_pengukuran[] = "id";
$tableKeyst_input_awal_new_pengukuran[] = "id_lahan";
$tdatat_input_awal_new_pengukuran[".Keys"] = $tableKeyst_input_awal_new_pengukuran;


$tdatat_input_awal_new_pengukuran[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["id"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["id_lahan"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

		$fdata["sourceSingle"] = "nama_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["nama_lahan"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "nama_lahan";
//	blok_lokasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "blok_lokasi";
	$fdata["GoodName"] = "blok_lokasi";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","blok_lokasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "blok_lokasi";

		$fdata["sourceSingle"] = "blok_lokasi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.blok_lokasi";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_blok";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "Description";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["blok_lokasi"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "blok_lokasi";
//	nama_lokasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "nama_lokasi";
	$fdata["GoodName"] = "nama_lokasi";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","nama_lokasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lokasi";

		$fdata["sourceSingle"] = "nama_lokasi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lokasi";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["nama_lokasi"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "nama_lokasi";
//	keperluan_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "keperluan_lahan";
	$fdata["GoodName"] = "keperluan_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","keperluan_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "keperluan_lahan";

		$fdata["sourceSingle"] = "keperluan_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.keperluan_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_kpl";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "Description";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["keperluan_lahan"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "keperluan_lahan";
//	start_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "start_date";
	$fdata["GoodName"] = "start_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","start_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date";

		$fdata["sourceSingle"] = "start_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.start_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["start_date"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "start_date";
//	due_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "due_date";
	$fdata["GoodName"] = "due_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","due_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date";

		$fdata["sourceSingle"] = "due_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.due_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["due_date"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "due_date";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["pic"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "pic";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["status"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "status";
//	close_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "close_by";
	$fdata["GoodName"] = "close_by";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","close_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_by";

		$fdata["sourceSingle"] = "close_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.close_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["close_by"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "close_by";
//	close_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "close_date";
	$fdata["GoodName"] = "close_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","close_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_date";

		$fdata["sourceSingle"] = "close_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.close_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["close_date"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "close_date";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["keterangan"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "keterangan";
//	im_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "im_file";
	$fdata["GoodName"] = "im_file";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","im_file");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "im_file";

		$fdata["sourceSingle"] = "im_file";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.im_file";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["im_file"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "im_file";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["created_date"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["created_by"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["updated_date"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["updated_by"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "updated_by";
//	Lat
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "Lat";
	$fdata["GoodName"] = "Lat";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","Lat");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Lat";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.Lat";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["Lat"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "Lat";
//	Lng
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "Lng";
	$fdata["GoodName"] = "Lng";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_input_awal_new_pengukuran","Lng");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "Lng";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.Lng";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_new_pengukuran["Lng"] = $fdata;
		$tdatat_input_awal_new_pengukuran[".searchableFields"][] = "Lng";


$tables_data["t_input_awal_new_pengukuran"]=&$tdatat_input_awal_new_pengukuran;
$field_labels["t_input_awal_new_pengukuran"] = &$fieldLabelst_input_awal_new_pengukuran;
$fieldToolTips["t_input_awal_new_pengukuran"] = &$fieldToolTipst_input_awal_new_pengukuran;
$placeHolders["t_input_awal_new_pengukuran"] = &$placeHolderst_input_awal_new_pengukuran;
$page_titles["t_input_awal_new_pengukuran"] = &$pageTitlest_input_awal_new_pengukuran;


changeTextControlsToDate( "t_input_awal_new_pengukuran" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_input_awal_new_pengukuran"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_input_awal_new_pengukuran"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_input_awal_new_pengukuran()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_input_awal.id,  t_input_awal.id_lahan,  t_input_awal.nama_lahan,  t_input_awal.blok_lokasi,  t_input_awal.nama_lokasi,  t_input_awal.keperluan_lahan,  t_input_awal.start_date,  t_input_awal.due_date,  t_input_awal.pic,  t_input_awal.status,  t_input_awal.close_by,  t_input_awal.close_date,  t_input_awal.keterangan,  t_input_awal.im_file,  t_input_awal.created_date,  t_input_awal.created_by,  t_input_awal.updated_date,  t_input_awal.updated_by,  t_pengukuran.Lat,  t_pengukuran.Lng";
$proto0["m_strFrom"] = "FROM t_input_awal  INNER JOIN t_pengukuran ON t_input_awal.id_lahan = t_pengukuran.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto6["m_sql"] = "t_input_awal.id";
$proto6["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto8["m_sql"] = "t_input_awal.id_lahan";
$proto8["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto10["m_sql"] = "t_input_awal.nama_lahan";
$proto10["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "blok_lokasi",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto12["m_sql"] = "t_input_awal.blok_lokasi";
$proto12["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lokasi",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto14["m_sql"] = "t_input_awal.nama_lokasi";
$proto14["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "keperluan_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto16["m_sql"] = "t_input_awal.keperluan_lahan";
$proto16["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto18["m_sql"] = "t_input_awal.start_date";
$proto18["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto20["m_sql"] = "t_input_awal.due_date";
$proto20["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto22["m_sql"] = "t_input_awal.pic";
$proto22["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto24["m_sql"] = "t_input_awal.status";
$proto24["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "close_by",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto26["m_sql"] = "t_input_awal.close_by";
$proto26["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "close_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto28["m_sql"] = "t_input_awal.close_date";
$proto28["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto30["m_sql"] = "t_input_awal.keterangan";
$proto30["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "im_file",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto32["m_sql"] = "t_input_awal.im_file";
$proto32["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto34["m_sql"] = "t_input_awal.created_date";
$proto34["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto36["m_sql"] = "t_input_awal.created_by";
$proto36["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto38["m_sql"] = "t_input_awal.updated_date";
$proto38["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto40["m_sql"] = "t_input_awal.updated_by";
$proto40["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "Lat",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto42["m_sql"] = "t_pengukuran.Lat";
$proto42["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "Lng",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto44["m_sql"] = "t_pengukuran.Lng";
$proto44["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto46=array();
$proto46["m_link"] = "SQLL_MAIN";
			$proto47=array();
$proto47["m_strName"] = "t_input_awal";
$proto47["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto47["m_columns"] = array();
$proto47["m_columns"][] = "id";
$proto47["m_columns"][] = "id_lahan";
$proto47["m_columns"][] = "nama_lahan";
$proto47["m_columns"][] = "batas_utara";
$proto47["m_columns"][] = "batas_selatan";
$proto47["m_columns"][] = "batas_barat";
$proto47["m_columns"][] = "batas_timur";
$proto47["m_columns"][] = "blok_lokasi";
$proto47["m_columns"][] = "nama_lokasi";
$proto47["m_columns"][] = "keperluan_lahan";
$proto47["m_columns"][] = "start_date";
$proto47["m_columns"][] = "due_date";
$proto47["m_columns"][] = "pic";
$proto47["m_columns"][] = "status";
$proto47["m_columns"][] = "close_by";
$proto47["m_columns"][] = "close_date";
$proto47["m_columns"][] = "keterangan";
$proto47["m_columns"][] = "im_file";
$proto47["m_columns"][] = "created_date";
$proto47["m_columns"][] = "created_by";
$proto47["m_columns"][] = "updated_date";
$proto47["m_columns"][] = "updated_by";
$obj = new SQLTable($proto47);

$proto46["m_table"] = $obj;
$proto46["m_sql"] = "t_input_awal";
$proto46["m_alias"] = "";
$proto46["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto48=array();
$proto48["m_sql"] = "";
$proto48["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto48["m_column"]=$obj;
$proto48["m_contained"] = array();
$proto48["m_strCase"] = "";
$proto48["m_havingmode"] = false;
$proto48["m_inBrackets"] = false;
$proto48["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto48);

$proto46["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto46);

$proto0["m_fromlist"][]=$obj;
												$proto50=array();
$proto50["m_link"] = "SQLL_INNERJOIN";
			$proto51=array();
$proto51["m_strName"] = "t_pengukuran";
$proto51["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto51["m_columns"] = array();
$proto51["m_columns"][] = "id";
$proto51["m_columns"][] = "id_lahan";
$proto51["m_columns"][] = "start_date";
$proto51["m_columns"][] = "due_date";
$proto51["m_columns"][] = "pic";
$proto51["m_columns"][] = "ukuran";
$proto51["m_columns"][] = "utipe";
$proto51["m_columns"][] = "batas_utara";
$proto51["m_columns"][] = "batas_utara_text";
$proto51["m_columns"][] = "stipe";
$proto51["m_columns"][] = "batas_selatan";
$proto51["m_columns"][] = "batas_selatan_text";
$proto51["m_columns"][] = "btipe";
$proto51["m_columns"][] = "batas_barat";
$proto51["m_columns"][] = "batas_barat_text";
$proto51["m_columns"][] = "ttipe";
$proto51["m_columns"][] = "batas_timur";
$proto51["m_columns"][] = "batas_timur_text";
$proto51["m_columns"][] = "keterangan";
$proto51["m_columns"][] = "maps_file";
$proto51["m_columns"][] = "status";
$proto51["m_columns"][] = "close_by";
$proto51["m_columns"][] = "close_date";
$proto51["m_columns"][] = "created_date";
$proto51["m_columns"][] = "created_by";
$proto51["m_columns"][] = "updated_date";
$proto51["m_columns"][] = "updated_by";
$proto51["m_columns"][] = "Lat";
$proto51["m_columns"][] = "Lng";
$proto51["m_columns"][] = "Lat2";
$proto51["m_columns"][] = "Lng2";
$obj = new SQLTable($proto51);

$proto50["m_table"] = $obj;
$proto50["m_sql"] = "INNER JOIN t_pengukuran ON t_input_awal.id_lahan = t_pengukuran.id_lahan";
$proto50["m_alias"] = "";
$proto50["m_srcTableName"] = "t_input_awal_new_pengukuran";
$proto52=array();
$proto52["m_sql"] = "t_pengukuran.id_lahan = t_input_awal.id_lahan";
$proto52["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_input_awal_new_pengukuran"
));

$proto52["m_column"]=$obj;
$proto52["m_contained"] = array();
$proto52["m_strCase"] = "= t_input_awal.id_lahan";
$proto52["m_havingmode"] = false;
$proto52["m_inBrackets"] = false;
$proto52["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto52);

$proto50["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto50);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_input_awal_new_pengukuran";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_input_awal_new_pengukuran = createSqlQuery_t_input_awal_new_pengukuran();


	
					
;

																				

$tdatat_input_awal_new_pengukuran[".sqlquery"] = $queryData_t_input_awal_new_pengukuran;



include_once(getabspath("include/t_input_awal_new_pengukuran_events.php"));
$tdatat_input_awal_new_pengukuran[".hasEvents"] = true;

?>