<?php
$tdatat_input_awal_report1 = array();
$tdatat_input_awal_report1[".searchableFields"] = array();
$tdatat_input_awal_report1[".ShortName"] = "t_input_awal_report1";
$tdatat_input_awal_report1[".OwnerID"] = "";
$tdatat_input_awal_report1[".OriginalTable"] = "t_input_awal";


$tdatat_input_awal_report1[".pagesByType"] = my_json_decode( "{\"export\":[\"export\"],\"list\":[\"list\"],\"search\":[\"search\"]}" );
$tdatat_input_awal_report1[".originalPagesByType"] = $tdatat_input_awal_report1[".pagesByType"];
$tdatat_input_awal_report1[".pages"] = types2pages( my_json_decode( "{\"export\":[\"export\"],\"list\":[\"list\"],\"search\":[\"search\"]}" ) );
$tdatat_input_awal_report1[".originalPages"] = $tdatat_input_awal_report1[".pages"];
$tdatat_input_awal_report1[".defaultPages"] = my_json_decode( "{\"export\":\"export\",\"list\":\"list\",\"search\":\"search\"}" );
$tdatat_input_awal_report1[".originalDefaultPages"] = $tdatat_input_awal_report1[".defaultPages"];

//	field labels
$fieldLabelst_input_awal_report1 = array();
$fieldToolTipst_input_awal_report1 = array();
$pageTitlest_input_awal_report1 = array();
$placeHolderst_input_awal_report1 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_input_awal_report1["English"] = array();
	$fieldToolTipst_input_awal_report1["English"] = array();
	$placeHolderst_input_awal_report1["English"] = array();
	$pageTitlest_input_awal_report1["English"] = array();
	$fieldLabelst_input_awal_report1["English"]["id"] = "Id";
	$fieldToolTipst_input_awal_report1["English"]["id"] = "";
	$placeHolderst_input_awal_report1["English"]["id"] = "";
	$fieldLabelst_input_awal_report1["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_input_awal_report1["English"]["id_lahan"] = "";
	$placeHolderst_input_awal_report1["English"]["id_lahan"] = "";
	$fieldLabelst_input_awal_report1["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_input_awal_report1["English"]["nama_lahan"] = "";
	$placeHolderst_input_awal_report1["English"]["nama_lahan"] = "";
	$fieldLabelst_input_awal_report1["English"]["nama_lokasi"] = "Nama Lokasi";
	$fieldToolTipst_input_awal_report1["English"]["nama_lokasi"] = "";
	$placeHolderst_input_awal_report1["English"]["nama_lokasi"] = "";
	$fieldLabelst_input_awal_report1["English"]["ukuran"] = "Ukuran";
	$fieldToolTipst_input_awal_report1["English"]["ukuran"] = "";
	$placeHolderst_input_awal_report1["English"]["ukuran"] = "";
	$fieldLabelst_input_awal_report1["English"]["harga_per_m2"] = "Harga Per M2";
	$fieldToolTipst_input_awal_report1["English"]["harga_per_m2"] = "";
	$placeHolderst_input_awal_report1["English"]["harga_per_m2"] = "";
	$fieldLabelst_input_awal_report1["English"]["total_biaya"] = "Total Biaya";
	$fieldToolTipst_input_awal_report1["English"]["total_biaya"] = "";
	$placeHolderst_input_awal_report1["English"]["total_biaya"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_input_awal"] = "Status<br> Input Awal";
	$fieldToolTipst_input_awal_report1["English"]["status_input_awal"] = "";
	$placeHolderst_input_awal_report1["English"]["status_input_awal"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_date_input"] = "Close Date <br>Input Awal";
	$fieldToolTipst_input_awal_report1["English"]["close_date_input"] = "";
	$placeHolderst_input_awal_report1["English"]["close_date_input"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_pengukuran"] = "Status <br>Pengukuran";
	$fieldToolTipst_input_awal_report1["English"]["status_pengukuran"] = "";
	$placeHolderst_input_awal_report1["English"]["status_pengukuran"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_date_pengukuran"] = "Close Date <br>Pengukuran";
	$fieldToolTipst_input_awal_report1["English"]["close_date_pengukuran"] = "";
	$placeHolderst_input_awal_report1["English"]["close_date_pengukuran"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_negosiasi"] = "Status <br>Negosiasi";
	$fieldToolTipst_input_awal_report1["English"]["status_negosiasi"] = "";
	$placeHolderst_input_awal_report1["English"]["status_negosiasi"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_date_negosiasi"] = "Close Date <br>Negosiasi";
	$fieldToolTipst_input_awal_report1["English"]["close_date_negosiasi"] = "";
	$placeHolderst_input_awal_report1["English"]["close_date_negosiasi"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_deal"] = "Status <br>Deal";
	$fieldToolTipst_input_awal_report1["English"]["status_deal"] = "";
	$placeHolderst_input_awal_report1["English"]["status_deal"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_date_deal"] = "Close Date <br>Deal";
	$fieldToolTipst_input_awal_report1["English"]["close_date_deal"] = "";
	$placeHolderst_input_awal_report1["English"]["close_date_deal"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_dokumentasi"] = "Status <br>Dokumentasi";
	$fieldToolTipst_input_awal_report1["English"]["status_dokumentasi"] = "";
	$placeHolderst_input_awal_report1["English"]["status_dokumentasi"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_date_dokumentasi"] = "Close Date <br>Dokumentasi";
	$fieldToolTipst_input_awal_report1["English"]["close_date_dokumentasi"] = "";
	$placeHolderst_input_awal_report1["English"]["close_date_dokumentasi"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_dp"] = "Status <br>Down Payment";
	$fieldToolTipst_input_awal_report1["English"]["status_dp"] = "";
	$placeHolderst_input_awal_report1["English"]["status_dp"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_dp_date"] = "Close Date <br>Down Payment";
	$fieldToolTipst_input_awal_report1["English"]["close_dp_date"] = "";
	$placeHolderst_input_awal_report1["English"]["close_dp_date"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_full"] = "Status <br>Full Payment";
	$fieldToolTipst_input_awal_report1["English"]["status_full"] = "";
	$placeHolderst_input_awal_report1["English"]["status_full"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_full_date"] = "Close Date <br>Full Payment";
	$fieldToolTipst_input_awal_report1["English"]["close_full_date"] = "";
	$placeHolderst_input_awal_report1["English"]["close_full_date"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_badmin"] = "Status <br>Biaya admin";
	$fieldToolTipst_input_awal_report1["English"]["status_badmin"] = "";
	$placeHolderst_input_awal_report1["English"]["status_badmin"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_badmin_date"] = "Close Date <br>Biaya Admin";
	$fieldToolTipst_input_awal_report1["English"]["close_badmin_date"] = "";
	$placeHolderst_input_awal_report1["English"]["close_badmin_date"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_pemb_admin"] = "Status Pembayaran <br>Biaya Admin";
	$fieldToolTipst_input_awal_report1["English"]["status_pemb_admin"] = "";
	$placeHolderst_input_awal_report1["English"]["status_pemb_admin"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_pemb_dp"] = "Status Pembayaran <br>Down Payment";
	$fieldToolTipst_input_awal_report1["English"]["status_pemb_dp"] = "";
	$placeHolderst_input_awal_report1["English"]["status_pemb_dp"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_pemb_full"] = "Status Pembayaran <br>Full Payment";
	$fieldToolTipst_input_awal_report1["English"]["status_pemb_full"] = "";
	$placeHolderst_input_awal_report1["English"]["status_pemb_full"] = "";
	$fieldLabelst_input_awal_report1["English"]["status_regis"] = "Status <br>Registrasi Lahan";
	$fieldToolTipst_input_awal_report1["English"]["status_regis"] = "";
	$placeHolderst_input_awal_report1["English"]["status_regis"] = "";
	$fieldLabelst_input_awal_report1["English"]["close_regis_date"] = "Close Date <br>Registrasi Lahan";
	$fieldToolTipst_input_awal_report1["English"]["close_regis_date"] = "";
	$placeHolderst_input_awal_report1["English"]["close_regis_date"] = "";
	$fieldLabelst_input_awal_report1["English"]["no_register"] = "No Register";
	$fieldToolTipst_input_awal_report1["English"]["no_register"] = "";
	$placeHolderst_input_awal_report1["English"]["no_register"] = "";
	if (count($fieldToolTipst_input_awal_report1["English"]))
		$tdatat_input_awal_report1[".isUseToolTips"] = true;
}


	$tdatat_input_awal_report1[".NCSearch"] = true;



$tdatat_input_awal_report1[".shortTableName"] = "t_input_awal_report1";
$tdatat_input_awal_report1[".nSecOptions"] = 0;

$tdatat_input_awal_report1[".mainTableOwnerID"] = "";
$tdatat_input_awal_report1[".entityType"] = 1;
$tdatat_input_awal_report1[".connId"] = "db_lla_at_localhost";


$tdatat_input_awal_report1[".strOriginalTableName"] = "t_input_awal";

		 



$tdatat_input_awal_report1[".showAddInPopup"] = false;

$tdatat_input_awal_report1[".showEditInPopup"] = false;

$tdatat_input_awal_report1[".showViewInPopup"] = false;

$tdatat_input_awal_report1[".listAjax"] = false;
//	temporary
//$tdatat_input_awal_report1[".listAjax"] = false;

	$tdatat_input_awal_report1[".audit"] = false;

	$tdatat_input_awal_report1[".locking"] = false;


$pages = $tdatat_input_awal_report1[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_input_awal_report1[".edit"] = true;
	$tdatat_input_awal_report1[".afterEditAction"] = 1;
	$tdatat_input_awal_report1[".closePopupAfterEdit"] = 1;
	$tdatat_input_awal_report1[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_input_awal_report1[".add"] = true;
$tdatat_input_awal_report1[".afterAddAction"] = 1;
$tdatat_input_awal_report1[".closePopupAfterAdd"] = 1;
$tdatat_input_awal_report1[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_input_awal_report1[".list"] = true;
}



$tdatat_input_awal_report1[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_input_awal_report1[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_input_awal_report1[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_input_awal_report1[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_input_awal_report1[".printFriendly"] = true;
}



$tdatat_input_awal_report1[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_input_awal_report1[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_input_awal_report1[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_input_awal_report1[".isUseAjaxSuggest"] = true;

$tdatat_input_awal_report1[".rowHighlite"] = true;



			

$tdatat_input_awal_report1[".ajaxCodeSnippetAdded"] = false;

$tdatat_input_awal_report1[".buttonsAdded"] = false;

$tdatat_input_awal_report1[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_input_awal_report1[".isUseTimeForSearch"] = false;


$tdatat_input_awal_report1[".badgeColor"] = "CD5C5C";


$tdatat_input_awal_report1[".allSearchFields"] = array();
$tdatat_input_awal_report1[".filterFields"] = array();
$tdatat_input_awal_report1[".requiredSearchFields"] = array();

$tdatat_input_awal_report1[".googleLikeFields"] = array();
$tdatat_input_awal_report1[".googleLikeFields"][] = "id";
$tdatat_input_awal_report1[".googleLikeFields"][] = "id_lahan";
$tdatat_input_awal_report1[".googleLikeFields"][] = "nama_lahan";
$tdatat_input_awal_report1[".googleLikeFields"][] = "nama_lokasi";
$tdatat_input_awal_report1[".googleLikeFields"][] = "ukuran";
$tdatat_input_awal_report1[".googleLikeFields"][] = "harga_per_m2";
$tdatat_input_awal_report1[".googleLikeFields"][] = "total_biaya";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_input_awal";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_date_input";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_pengukuran";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_date_pengukuran";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_negosiasi";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_date_negosiasi";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_deal";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_date_deal";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_dokumentasi";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_date_dokumentasi";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_dp";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_dp_date";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_full";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_full_date";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_badmin";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_badmin_date";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_pemb_admin";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_pemb_dp";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_pemb_full";
$tdatat_input_awal_report1[".googleLikeFields"][] = "status_regis";
$tdatat_input_awal_report1[".googleLikeFields"][] = "close_regis_date";
$tdatat_input_awal_report1[".googleLikeFields"][] = "no_register";



$tdatat_input_awal_report1[".tableType"] = "list";

$tdatat_input_awal_report1[".printerPageOrientation"] = 0;
$tdatat_input_awal_report1[".nPrinterPageScale"] = 100;

$tdatat_input_awal_report1[".nPrinterSplitRecords"] = 40;

$tdatat_input_awal_report1[".geocodingEnabled"] = false;










$tdatat_input_awal_report1[".pageSize"] = 20;

$tdatat_input_awal_report1[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_input_awal_report1[".strOrderBy"] = $tstrOrderBy;

$tdatat_input_awal_report1[".orderindexes"] = array();


$tdatat_input_awal_report1[".sqlHead"] = "SELECT t_input_awal.id,   t_input_awal.id_lahan,   nama_lahan,   nama_lokasi,  t_pengukuran.ukuran,  t_deal.harga_per_m2,  t_deal.total_biaya,  t_input_awal.status as status_input_awal,   ifnull(t_input_awal.close_date,'-') as close_date_input,   ifnull(t_pengukuran.status,'-') as status_pengukuran,   ifnull(t_pengukuran.close_date,'-') as close_date_pengukuran,   ifnull(t_negosiasi.status,'-') as status_negosiasi,   ifnull(t_negosiasi.close_date,'-') as close_date_negosiasi,   ifnull(t_deal.status,'-') as status_deal,   ifnull(t_deal.close_date,'-') as close_date_deal,  ifnull(t_dokumentasi.status,'-') as status_dokumentasi,   ifnull(t_dokumentasi.close_date,'-') as close_date_dokumentasi,   ifnull(t_perm_dana.status,'-') as status_dp,   ifnull(t_perm_dana.close_dp_date,'-') as close_dp_date,  ifnull(t_perm_dana.status_full,'-') as status_full,   ifnull(t_perm_dana.close_full_date,'-') as close_full_date,  ifnull(t_perm_dana.status_badmin,'-') as status_badmin,   ifnull(t_perm_dana.close_badmin_date,'-') as close_badmin_date,  ifnull(t_perm_dana.status_badmin,'-') as status_pemb_admin,  ifnull(t_perm_dana.status,'-') as status_pemb_dp,   ifnull(t_perm_dana.status_full,'-') as status_pemb_full,  ifnull(t_regis.status,'-') as status_regis,   ifnull(t_regis.close_date,'-') as close_regis_date,  ifnull(register.no_register,'-') as no_register";
$tdatat_input_awal_report1[".sqlFrom"] = "FROM t_input_awal  left join t_pengukuran on t_pengukuran.id_lahan = t_input_awal.id_lahan  left join t_negosiasi on t_negosiasi.id_lahan = t_input_awal.id_lahan  left join t_deal on t_deal.id_lahan = t_input_awal.id_lahan  left join t_dokumentasi on t_dokumentasi.id_lahan = t_input_awal.id_lahan  left join t_perm_dana on t_perm_dana.id_lahan = t_input_awal.id_lahan  left join t_regis on t_regis.id_lahan = t_input_awal.id_lahan  LEFT JOIN (  	SELECT id_ukur, GROUP_CONCAT(DISTINCT(no_register)) AS no_register FROM t_pengukuran_percil   	GROUP BY id_ukur  )register ON register.id_ukur = t_pengukuran.id";
$tdatat_input_awal_report1[".sqlWhereExpr"] = "";
$tdatat_input_awal_report1[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_input_awal_report1[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_input_awal_report1[".arrGroupsPerPage"] = $arrGPP;

$tdatat_input_awal_report1[".highlightSearchResults"] = true;

$tableKeyst_input_awal_report1 = array();
$tableKeyst_input_awal_report1[] = "id";
$tableKeyst_input_awal_report1[] = "id_lahan";
$tdatat_input_awal_report1[".Keys"] = $tableKeyst_input_awal_report1;


$tdatat_input_awal_report1[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["id"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["id_lahan"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

		$fdata["sourceSingle"] = "nama_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["nama_lahan"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "nama_lahan";
//	nama_lokasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "nama_lokasi";
	$fdata["GoodName"] = "nama_lokasi";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","nama_lokasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lokasi";

		$fdata["sourceSingle"] = "nama_lokasi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nama_lokasi";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["nama_lokasi"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "nama_lokasi";
//	ukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ukuran";
	$fdata["GoodName"] = "ukuran";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","ukuran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "ukuran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.ukuran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["ukuran"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "ukuran";
//	harga_per_m2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "harga_per_m2";
	$fdata["GoodName"] = "harga_per_m2";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","harga_per_m2");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "harga_per_m2";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.harga_per_m2";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["harga_per_m2"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "harga_per_m2";
//	total_biaya
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "total_biaya";
	$fdata["GoodName"] = "total_biaya";
	$fdata["ownerTable"] = "t_deal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","total_biaya");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "total_biaya";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_deal.total_biaya";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["total_biaya"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "total_biaya";
//	status_input_awal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "status_input_awal";
	$fdata["GoodName"] = "status_input_awal";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_input_awal");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_input_awal"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_input_awal";
//	close_date_input
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "close_date_input";
	$fdata["GoodName"] = "close_date_input";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_date_input");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_date_input";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_input_awal.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_date_input"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_date_input";
//	status_pengukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "status_pengukuran";
	$fdata["GoodName"] = "status_pengukuran";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_pengukuran");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_pengukuran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_pengukuran.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_pengukuran"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_pengukuran";
//	close_date_pengukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "close_date_pengukuran";
	$fdata["GoodName"] = "close_date_pengukuran";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_date_pengukuran");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_date_pengukuran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_pengukuran.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_date_pengukuran"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_date_pengukuran";
//	status_negosiasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "status_negosiasi";
	$fdata["GoodName"] = "status_negosiasi";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_negosiasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_negosiasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_negosiasi.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_negosiasi"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_negosiasi";
//	close_date_negosiasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "close_date_negosiasi";
	$fdata["GoodName"] = "close_date_negosiasi";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_date_negosiasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_date_negosiasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_negosiasi.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_date_negosiasi"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_date_negosiasi";
//	status_deal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "status_deal";
	$fdata["GoodName"] = "status_deal";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_deal");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_deal";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_deal.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_deal"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_deal";
//	close_date_deal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "close_date_deal";
	$fdata["GoodName"] = "close_date_deal";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_date_deal");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_date_deal";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_deal.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_date_deal"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_date_deal";
//	status_dokumentasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "status_dokumentasi";
	$fdata["GoodName"] = "status_dokumentasi";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_dokumentasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_dokumentasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_dokumentasi.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_dokumentasi"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_dokumentasi";
//	close_date_dokumentasi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "close_date_dokumentasi";
	$fdata["GoodName"] = "close_date_dokumentasi";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_date_dokumentasi");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_date_dokumentasi";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_dokumentasi.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_date_dokumentasi"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_date_dokumentasi";
//	status_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "status_dp";
	$fdata["GoodName"] = "status_dp";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_dp");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_dp"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_dp";
//	close_dp_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "close_dp_date";
	$fdata["GoodName"] = "close_dp_date";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_dp_date");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_dp_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.close_dp_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_dp_date"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_dp_date";
//	status_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "status_full";
	$fdata["GoodName"] = "status_full";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_full");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_full";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status_full,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_full"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_full";
//	close_full_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "close_full_date";
	$fdata["GoodName"] = "close_full_date";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_full_date");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_full_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.close_full_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_full_date"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_full_date";
//	status_badmin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "status_badmin";
	$fdata["GoodName"] = "status_badmin";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_badmin");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_badmin";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status_badmin,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_badmin"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_badmin";
//	close_badmin_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "close_badmin_date";
	$fdata["GoodName"] = "close_badmin_date";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_badmin_date");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_badmin_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.close_badmin_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_badmin_date"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_badmin_date";
//	status_pemb_admin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "status_pemb_admin";
	$fdata["GoodName"] = "status_pemb_admin";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_pemb_admin");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_pemb_admin";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status_badmin,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_pemb_admin"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_pemb_admin";
//	status_pemb_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "status_pemb_dp";
	$fdata["GoodName"] = "status_pemb_dp";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_pemb_dp");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_pemb_dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_pemb_dp"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_pemb_dp";
//	status_pemb_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "status_pemb_full";
	$fdata["GoodName"] = "status_pemb_full";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_pemb_full");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_pemb_full";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_perm_dana.status_full,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_pemb_full"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_pemb_full";
//	status_regis
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "status_regis";
	$fdata["GoodName"] = "status_regis";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","status_regis");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_regis";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_regis.status,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["status_regis"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "status_regis";
//	close_regis_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "close_regis_date";
	$fdata["GoodName"] = "close_regis_date";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","close_regis_date");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_regis_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(t_regis.close_date,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["close_regis_date"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "close_regis_date";
//	no_register
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "no_register";
	$fdata["GoodName"] = "no_register";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_input_awal_report1","no_register");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "no_register";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ifnull(register.no_register,'-')";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_input_awal_report1["no_register"] = $fdata;
		$tdatat_input_awal_report1[".searchableFields"][] = "no_register";


$tables_data["t_input_awal_report1"]=&$tdatat_input_awal_report1;
$field_labels["t_input_awal_report1"] = &$fieldLabelst_input_awal_report1;
$fieldToolTips["t_input_awal_report1"] = &$fieldToolTipst_input_awal_report1;
$placeHolders["t_input_awal_report1"] = &$placeHolderst_input_awal_report1;
$page_titles["t_input_awal_report1"] = &$pageTitlest_input_awal_report1;


changeTextControlsToDate( "t_input_awal_report1" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_input_awal_report1"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_input_awal_report1"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_input_awal_report1()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_input_awal.id,   t_input_awal.id_lahan,   nama_lahan,   nama_lokasi,  t_pengukuran.ukuran,  t_deal.harga_per_m2,  t_deal.total_biaya,  t_input_awal.status as status_input_awal,   ifnull(t_input_awal.close_date,'-') as close_date_input,   ifnull(t_pengukuran.status,'-') as status_pengukuran,   ifnull(t_pengukuran.close_date,'-') as close_date_pengukuran,   ifnull(t_negosiasi.status,'-') as status_negosiasi,   ifnull(t_negosiasi.close_date,'-') as close_date_negosiasi,   ifnull(t_deal.status,'-') as status_deal,   ifnull(t_deal.close_date,'-') as close_date_deal,  ifnull(t_dokumentasi.status,'-') as status_dokumentasi,   ifnull(t_dokumentasi.close_date,'-') as close_date_dokumentasi,   ifnull(t_perm_dana.status,'-') as status_dp,   ifnull(t_perm_dana.close_dp_date,'-') as close_dp_date,  ifnull(t_perm_dana.status_full,'-') as status_full,   ifnull(t_perm_dana.close_full_date,'-') as close_full_date,  ifnull(t_perm_dana.status_badmin,'-') as status_badmin,   ifnull(t_perm_dana.close_badmin_date,'-') as close_badmin_date,  ifnull(t_perm_dana.status_badmin,'-') as status_pemb_admin,  ifnull(t_perm_dana.status,'-') as status_pemb_dp,   ifnull(t_perm_dana.status_full,'-') as status_pemb_full,  ifnull(t_regis.status,'-') as status_regis,   ifnull(t_regis.close_date,'-') as close_regis_date,  ifnull(register.no_register,'-') as no_register";
$proto0["m_strFrom"] = "FROM t_input_awal  left join t_pengukuran on t_pengukuran.id_lahan = t_input_awal.id_lahan  left join t_negosiasi on t_negosiasi.id_lahan = t_input_awal.id_lahan  left join t_deal on t_deal.id_lahan = t_input_awal.id_lahan  left join t_dokumentasi on t_dokumentasi.id_lahan = t_input_awal.id_lahan  left join t_perm_dana on t_perm_dana.id_lahan = t_input_awal.id_lahan  left join t_regis on t_regis.id_lahan = t_input_awal.id_lahan  LEFT JOIN (  	SELECT id_ukur, GROUP_CONCAT(DISTINCT(no_register)) AS no_register FROM t_pengukuran_percil   	GROUP BY id_ukur  )register ON register.id_ukur = t_pengukuran.id";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto6["m_sql"] = "t_input_awal.id";
$proto6["m_srcTableName"] = "t_input_awal_report1";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto8["m_sql"] = "t_input_awal.id_lahan";
$proto8["m_srcTableName"] = "t_input_awal_report1";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto10["m_sql"] = "nama_lahan";
$proto10["m_srcTableName"] = "t_input_awal_report1";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lokasi",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto12["m_sql"] = "nama_lokasi";
$proto12["m_srcTableName"] = "t_input_awal_report1";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ukuran",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto14["m_sql"] = "t_pengukuran.ukuran";
$proto14["m_srcTableName"] = "t_input_awal_report1";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "harga_per_m2",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto16["m_sql"] = "t_deal.harga_per_m2";
$proto16["m_srcTableName"] = "t_input_awal_report1";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "total_biaya",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto18["m_sql"] = "t_deal.total_biaya";
$proto18["m_srcTableName"] = "t_input_awal_report1";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto20["m_sql"] = "t_input_awal.status";
$proto20["m_srcTableName"] = "t_input_awal_report1";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "status_input_awal";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$proto23=array();
$proto23["m_functiontype"] = "SQLF_CUSTOM";
$proto23["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_input_awal.close_date"
));

$proto23["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto23["m_arguments"][]=$obj;
$proto23["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto23);

$proto22["m_sql"] = "ifnull(t_input_awal.close_date,'-')";
$proto22["m_srcTableName"] = "t_input_awal_report1";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "close_date_input";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$proto27=array();
$proto27["m_functiontype"] = "SQLF_CUSTOM";
$proto27["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_pengukuran.status"
));

$proto27["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto27["m_arguments"][]=$obj;
$proto27["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto27);

$proto26["m_sql"] = "ifnull(t_pengukuran.status,'-')";
$proto26["m_srcTableName"] = "t_input_awal_report1";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "status_pengukuran";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$proto31=array();
$proto31["m_functiontype"] = "SQLF_CUSTOM";
$proto31["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_pengukuran.close_date"
));

$proto31["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto31["m_arguments"][]=$obj;
$proto31["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto31);

$proto30["m_sql"] = "ifnull(t_pengukuran.close_date,'-')";
$proto30["m_srcTableName"] = "t_input_awal_report1";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "close_date_pengukuran";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$proto35=array();
$proto35["m_functiontype"] = "SQLF_CUSTOM";
$proto35["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_negosiasi.status"
));

$proto35["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto35["m_arguments"][]=$obj;
$proto35["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto35);

$proto34["m_sql"] = "ifnull(t_negosiasi.status,'-')";
$proto34["m_srcTableName"] = "t_input_awal_report1";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "status_negosiasi";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$proto39=array();
$proto39["m_functiontype"] = "SQLF_CUSTOM";
$proto39["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_negosiasi.close_date"
));

$proto39["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto39["m_arguments"][]=$obj;
$proto39["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto39);

$proto38["m_sql"] = "ifnull(t_negosiasi.close_date,'-')";
$proto38["m_srcTableName"] = "t_input_awal_report1";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "close_date_negosiasi";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$proto43=array();
$proto43["m_functiontype"] = "SQLF_CUSTOM";
$proto43["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_deal.status"
));

$proto43["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto43["m_arguments"][]=$obj;
$proto43["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto43);

$proto42["m_sql"] = "ifnull(t_deal.status,'-')";
$proto42["m_srcTableName"] = "t_input_awal_report1";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "status_deal";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$proto47=array();
$proto47["m_functiontype"] = "SQLF_CUSTOM";
$proto47["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_deal.close_date"
));

$proto47["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto47["m_arguments"][]=$obj;
$proto47["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto47);

$proto46["m_sql"] = "ifnull(t_deal.close_date,'-')";
$proto46["m_srcTableName"] = "t_input_awal_report1";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "close_date_deal";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$proto51=array();
$proto51["m_functiontype"] = "SQLF_CUSTOM";
$proto51["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_dokumentasi.status"
));

$proto51["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto51["m_arguments"][]=$obj;
$proto51["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto51);

$proto50["m_sql"] = "ifnull(t_dokumentasi.status,'-')";
$proto50["m_srcTableName"] = "t_input_awal_report1";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "status_dokumentasi";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$proto55=array();
$proto55["m_functiontype"] = "SQLF_CUSTOM";
$proto55["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_dokumentasi.close_date"
));

$proto55["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto55["m_arguments"][]=$obj;
$proto55["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto55);

$proto54["m_sql"] = "ifnull(t_dokumentasi.close_date,'-')";
$proto54["m_srcTableName"] = "t_input_awal_report1";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "close_date_dokumentasi";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$proto59=array();
$proto59["m_functiontype"] = "SQLF_CUSTOM";
$proto59["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status"
));

$proto59["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto59["m_arguments"][]=$obj;
$proto59["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto59);

$proto58["m_sql"] = "ifnull(t_perm_dana.status,'-')";
$proto58["m_srcTableName"] = "t_input_awal_report1";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "status_dp";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$proto63=array();
$proto63["m_functiontype"] = "SQLF_CUSTOM";
$proto63["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.close_dp_date"
));

$proto63["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto63["m_arguments"][]=$obj;
$proto63["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto63);

$proto62["m_sql"] = "ifnull(t_perm_dana.close_dp_date,'-')";
$proto62["m_srcTableName"] = "t_input_awal_report1";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "close_dp_date";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$proto67=array();
$proto67["m_functiontype"] = "SQLF_CUSTOM";
$proto67["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status_full"
));

$proto67["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto67["m_arguments"][]=$obj;
$proto67["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto67);

$proto66["m_sql"] = "ifnull(t_perm_dana.status_full,'-')";
$proto66["m_srcTableName"] = "t_input_awal_report1";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "status_full";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$proto71=array();
$proto71["m_functiontype"] = "SQLF_CUSTOM";
$proto71["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.close_full_date"
));

$proto71["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto71["m_arguments"][]=$obj;
$proto71["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto71);

$proto70["m_sql"] = "ifnull(t_perm_dana.close_full_date,'-')";
$proto70["m_srcTableName"] = "t_input_awal_report1";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "close_full_date";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$proto75=array();
$proto75["m_functiontype"] = "SQLF_CUSTOM";
$proto75["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status_badmin"
));

$proto75["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto75["m_arguments"][]=$obj;
$proto75["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto75);

$proto74["m_sql"] = "ifnull(t_perm_dana.status_badmin,'-')";
$proto74["m_srcTableName"] = "t_input_awal_report1";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "status_badmin";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto78=array();
			$proto79=array();
$proto79["m_functiontype"] = "SQLF_CUSTOM";
$proto79["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.close_badmin_date"
));

$proto79["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto79["m_arguments"][]=$obj;
$proto79["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto79);

$proto78["m_sql"] = "ifnull(t_perm_dana.close_badmin_date,'-')";
$proto78["m_srcTableName"] = "t_input_awal_report1";
$proto78["m_expr"]=$obj;
$proto78["m_alias"] = "close_badmin_date";
$obj = new SQLFieldListItem($proto78);

$proto0["m_fieldlist"][]=$obj;
						$proto82=array();
			$proto83=array();
$proto83["m_functiontype"] = "SQLF_CUSTOM";
$proto83["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status_badmin"
));

$proto83["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto83["m_arguments"][]=$obj;
$proto83["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto83);

$proto82["m_sql"] = "ifnull(t_perm_dana.status_badmin,'-')";
$proto82["m_srcTableName"] = "t_input_awal_report1";
$proto82["m_expr"]=$obj;
$proto82["m_alias"] = "status_pemb_admin";
$obj = new SQLFieldListItem($proto82);

$proto0["m_fieldlist"][]=$obj;
						$proto86=array();
			$proto87=array();
$proto87["m_functiontype"] = "SQLF_CUSTOM";
$proto87["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status"
));

$proto87["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto87["m_arguments"][]=$obj;
$proto87["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto87);

$proto86["m_sql"] = "ifnull(t_perm_dana.status,'-')";
$proto86["m_srcTableName"] = "t_input_awal_report1";
$proto86["m_expr"]=$obj;
$proto86["m_alias"] = "status_pemb_dp";
$obj = new SQLFieldListItem($proto86);

$proto0["m_fieldlist"][]=$obj;
						$proto90=array();
			$proto91=array();
$proto91["m_functiontype"] = "SQLF_CUSTOM";
$proto91["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_perm_dana.status_full"
));

$proto91["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto91["m_arguments"][]=$obj;
$proto91["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto91);

$proto90["m_sql"] = "ifnull(t_perm_dana.status_full,'-')";
$proto90["m_srcTableName"] = "t_input_awal_report1";
$proto90["m_expr"]=$obj;
$proto90["m_alias"] = "status_pemb_full";
$obj = new SQLFieldListItem($proto90);

$proto0["m_fieldlist"][]=$obj;
						$proto94=array();
			$proto95=array();
$proto95["m_functiontype"] = "SQLF_CUSTOM";
$proto95["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_regis.status"
));

$proto95["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto95["m_arguments"][]=$obj;
$proto95["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto95);

$proto94["m_sql"] = "ifnull(t_regis.status,'-')";
$proto94["m_srcTableName"] = "t_input_awal_report1";
$proto94["m_expr"]=$obj;
$proto94["m_alias"] = "status_regis";
$obj = new SQLFieldListItem($proto94);

$proto0["m_fieldlist"][]=$obj;
						$proto98=array();
			$proto99=array();
$proto99["m_functiontype"] = "SQLF_CUSTOM";
$proto99["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "t_regis.close_date"
));

$proto99["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto99["m_arguments"][]=$obj;
$proto99["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto99);

$proto98["m_sql"] = "ifnull(t_regis.close_date,'-')";
$proto98["m_srcTableName"] = "t_input_awal_report1";
$proto98["m_expr"]=$obj;
$proto98["m_alias"] = "close_regis_date";
$obj = new SQLFieldListItem($proto98);

$proto0["m_fieldlist"][]=$obj;
						$proto102=array();
			$proto103=array();
$proto103["m_functiontype"] = "SQLF_CUSTOM";
$proto103["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "register.no_register"
));

$proto103["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'-'"
));

$proto103["m_arguments"][]=$obj;
$proto103["m_strFunctionName"] = "ifnull";
$obj = new SQLFunctionCall($proto103);

$proto102["m_sql"] = "ifnull(register.no_register,'-')";
$proto102["m_srcTableName"] = "t_input_awal_report1";
$proto102["m_expr"]=$obj;
$proto102["m_alias"] = "no_register";
$obj = new SQLFieldListItem($proto102);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto106=array();
$proto106["m_link"] = "SQLL_MAIN";
			$proto107=array();
$proto107["m_strName"] = "t_input_awal";
$proto107["m_srcTableName"] = "t_input_awal_report1";
$proto107["m_columns"] = array();
$proto107["m_columns"][] = "id";
$proto107["m_columns"][] = "id_lahan";
$proto107["m_columns"][] = "nama_lahan";
$proto107["m_columns"][] = "batas_utara";
$proto107["m_columns"][] = "batas_selatan";
$proto107["m_columns"][] = "batas_barat";
$proto107["m_columns"][] = "batas_timur";
$proto107["m_columns"][] = "blok_lokasi";
$proto107["m_columns"][] = "nama_lokasi";
$proto107["m_columns"][] = "keperluan_lahan";
$proto107["m_columns"][] = "start_date";
$proto107["m_columns"][] = "due_date";
$proto107["m_columns"][] = "pic";
$proto107["m_columns"][] = "status";
$proto107["m_columns"][] = "close_by";
$proto107["m_columns"][] = "close_date";
$proto107["m_columns"][] = "keterangan";
$proto107["m_columns"][] = "im_file";
$proto107["m_columns"][] = "created_date";
$proto107["m_columns"][] = "created_by";
$proto107["m_columns"][] = "updated_date";
$proto107["m_columns"][] = "updated_by";
$obj = new SQLTable($proto107);

$proto106["m_table"] = $obj;
$proto106["m_sql"] = "t_input_awal";
$proto106["m_alias"] = "";
$proto106["m_srcTableName"] = "t_input_awal_report1";
$proto108=array();
$proto108["m_sql"] = "";
$proto108["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto108["m_column"]=$obj;
$proto108["m_contained"] = array();
$proto108["m_strCase"] = "";
$proto108["m_havingmode"] = false;
$proto108["m_inBrackets"] = false;
$proto108["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto108);

$proto106["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto106);

$proto0["m_fromlist"][]=$obj;
												$proto110=array();
$proto110["m_link"] = "SQLL_LEFTJOIN";
			$proto111=array();
$proto111["m_strName"] = "t_pengukuran";
$proto111["m_srcTableName"] = "t_input_awal_report1";
$proto111["m_columns"] = array();
$proto111["m_columns"][] = "id";
$proto111["m_columns"][] = "id_lahan";
$proto111["m_columns"][] = "start_date";
$proto111["m_columns"][] = "due_date";
$proto111["m_columns"][] = "pic";
$proto111["m_columns"][] = "ukuran";
$proto111["m_columns"][] = "utipe";
$proto111["m_columns"][] = "batas_utara";
$proto111["m_columns"][] = "batas_utara_text";
$proto111["m_columns"][] = "stipe";
$proto111["m_columns"][] = "batas_selatan";
$proto111["m_columns"][] = "batas_selatan_text";
$proto111["m_columns"][] = "btipe";
$proto111["m_columns"][] = "batas_barat";
$proto111["m_columns"][] = "batas_barat_text";
$proto111["m_columns"][] = "ttipe";
$proto111["m_columns"][] = "batas_timur";
$proto111["m_columns"][] = "batas_timur_text";
$proto111["m_columns"][] = "keterangan";
$proto111["m_columns"][] = "maps_file";
$proto111["m_columns"][] = "status";
$proto111["m_columns"][] = "close_by";
$proto111["m_columns"][] = "close_date";
$proto111["m_columns"][] = "created_date";
$proto111["m_columns"][] = "created_by";
$proto111["m_columns"][] = "updated_date";
$proto111["m_columns"][] = "updated_by";
$proto111["m_columns"][] = "Lat";
$proto111["m_columns"][] = "Lng";
$proto111["m_columns"][] = "Lat2";
$proto111["m_columns"][] = "Lng2";
$obj = new SQLTable($proto111);

$proto110["m_table"] = $obj;
$proto110["m_sql"] = "left join t_pengukuran on t_pengukuran.id_lahan = t_input_awal.id_lahan";
$proto110["m_alias"] = "";
$proto110["m_srcTableName"] = "t_input_awal_report1";
$proto112=array();
$proto112["m_sql"] = "t_pengukuran.id_lahan = t_input_awal.id_lahan";
$proto112["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto112["m_column"]=$obj;
$proto112["m_contained"] = array();
$proto112["m_strCase"] = "= t_input_awal.id_lahan";
$proto112["m_havingmode"] = false;
$proto112["m_inBrackets"] = false;
$proto112["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto112);

$proto110["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto110);

$proto0["m_fromlist"][]=$obj;
												$proto114=array();
$proto114["m_link"] = "SQLL_LEFTJOIN";
			$proto115=array();
$proto115["m_strName"] = "t_negosiasi";
$proto115["m_srcTableName"] = "t_input_awal_report1";
$proto115["m_columns"] = array();
$proto115["m_columns"][] = "id";
$proto115["m_columns"][] = "id_lahan";
$proto115["m_columns"][] = "start_date";
$proto115["m_columns"][] = "due_date";
$proto115["m_columns"][] = "pic";
$proto115["m_columns"][] = "keterangan";
$proto115["m_columns"][] = "status";
$proto115["m_columns"][] = "close_by";
$proto115["m_columns"][] = "close_date";
$proto115["m_columns"][] = "created_date";
$proto115["m_columns"][] = "created_by";
$proto115["m_columns"][] = "updated_date";
$proto115["m_columns"][] = "updated_by";
$obj = new SQLTable($proto115);

$proto114["m_table"] = $obj;
$proto114["m_sql"] = "left join t_negosiasi on t_negosiasi.id_lahan = t_input_awal.id_lahan";
$proto114["m_alias"] = "";
$proto114["m_srcTableName"] = "t_input_awal_report1";
$proto116=array();
$proto116["m_sql"] = "t_negosiasi.id_lahan = t_input_awal.id_lahan";
$proto116["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto116["m_column"]=$obj;
$proto116["m_contained"] = array();
$proto116["m_strCase"] = "= t_input_awal.id_lahan";
$proto116["m_havingmode"] = false;
$proto116["m_inBrackets"] = false;
$proto116["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto116);

$proto114["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto114);

$proto0["m_fromlist"][]=$obj;
												$proto118=array();
$proto118["m_link"] = "SQLL_LEFTJOIN";
			$proto119=array();
$proto119["m_strName"] = "t_deal";
$proto119["m_srcTableName"] = "t_input_awal_report1";
$proto119["m_columns"] = array();
$proto119["m_columns"][] = "id";
$proto119["m_columns"][] = "id_lahan";
$proto119["m_columns"][] = "start_date";
$proto119["m_columns"][] = "due_date";
$proto119["m_columns"][] = "pic";
$proto119["m_columns"][] = "keterangan";
$proto119["m_columns"][] = "total_harga";
$proto119["m_columns"][] = "harga_tatum";
$proto119["m_columns"][] = "harga_bangunan";
$proto119["m_columns"][] = "harga_per_m2";
$proto119["m_columns"][] = "total_biaya";
$proto119["m_columns"][] = "status";
$proto119["m_columns"][] = "close_by";
$proto119["m_columns"][] = "close_date";
$proto119["m_columns"][] = "created_date";
$proto119["m_columns"][] = "created_by";
$proto119["m_columns"][] = "updated_date";
$proto119["m_columns"][] = "updated_by";
$obj = new SQLTable($proto119);

$proto118["m_table"] = $obj;
$proto118["m_sql"] = "left join t_deal on t_deal.id_lahan = t_input_awal.id_lahan";
$proto118["m_alias"] = "";
$proto118["m_srcTableName"] = "t_input_awal_report1";
$proto120=array();
$proto120["m_sql"] = "t_deal.id_lahan = t_input_awal.id_lahan";
$proto120["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_deal",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto120["m_column"]=$obj;
$proto120["m_contained"] = array();
$proto120["m_strCase"] = "= t_input_awal.id_lahan";
$proto120["m_havingmode"] = false;
$proto120["m_inBrackets"] = false;
$proto120["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto120);

$proto118["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto118);

$proto0["m_fromlist"][]=$obj;
												$proto122=array();
$proto122["m_link"] = "SQLL_LEFTJOIN";
			$proto123=array();
$proto123["m_strName"] = "t_dokumentasi";
$proto123["m_srcTableName"] = "t_input_awal_report1";
$proto123["m_columns"] = array();
$proto123["m_columns"][] = "id";
$proto123["m_columns"][] = "id_lahan";
$proto123["m_columns"][] = "start_date";
$proto123["m_columns"][] = "due_date";
$proto123["m_columns"][] = "pic";
$proto123["m_columns"][] = "keterangan";
$proto123["m_columns"][] = "status_lahan_pelepasan";
$proto123["m_columns"][] = "status_lahan_sertifkat";
$proto123["m_columns"][] = "status";
$proto123["m_columns"][] = "close_by";
$proto123["m_columns"][] = "close_date";
$proto123["m_columns"][] = "created_date";
$proto123["m_columns"][] = "created_by";
$proto123["m_columns"][] = "updated_date";
$proto123["m_columns"][] = "updated_by";
$obj = new SQLTable($proto123);

$proto122["m_table"] = $obj;
$proto122["m_sql"] = "left join t_dokumentasi on t_dokumentasi.id_lahan = t_input_awal.id_lahan";
$proto122["m_alias"] = "";
$proto122["m_srcTableName"] = "t_input_awal_report1";
$proto124=array();
$proto124["m_sql"] = "t_dokumentasi.id_lahan = t_input_awal.id_lahan";
$proto124["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_dokumentasi",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto124["m_column"]=$obj;
$proto124["m_contained"] = array();
$proto124["m_strCase"] = "= t_input_awal.id_lahan";
$proto124["m_havingmode"] = false;
$proto124["m_inBrackets"] = false;
$proto124["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto124);

$proto122["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto122);

$proto0["m_fromlist"][]=$obj;
												$proto126=array();
$proto126["m_link"] = "SQLL_LEFTJOIN";
			$proto127=array();
$proto127["m_strName"] = "t_perm_dana";
$proto127["m_srcTableName"] = "t_input_awal_report1";
$proto127["m_columns"] = array();
$proto127["m_columns"][] = "id";
$proto127["m_columns"][] = "id_lahan";
$proto127["m_columns"][] = "start_date_dp";
$proto127["m_columns"][] = "due_date_dp";
$proto127["m_columns"][] = "start_date_full";
$proto127["m_columns"][] = "due_date_full";
$proto127["m_columns"][] = "pic";
$proto127["m_columns"][] = "keterangan";
$proto127["m_columns"][] = "keterangan_bayar";
$proto127["m_columns"][] = "close_full_by";
$proto127["m_columns"][] = "close_full_date";
$proto127["m_columns"][] = "tipe";
$proto127["m_columns"][] = "dp";
$proto127["m_columns"][] = "sisa_pembayaran";
$proto127["m_columns"][] = "biaya_admin";
$proto127["m_columns"][] = "status";
$proto127["m_columns"][] = "status_full";
$proto127["m_columns"][] = "status_badmin";
$proto127["m_columns"][] = "close_badmin_by";
$proto127["m_columns"][] = "close_badmin_date";
$proto127["m_columns"][] = "close_dp_by";
$proto127["m_columns"][] = "close_dp_date";
$proto127["m_columns"][] = "created_date";
$proto127["m_columns"][] = "created_by";
$proto127["m_columns"][] = "updated_date";
$proto127["m_columns"][] = "updated_by";
$obj = new SQLTable($proto127);

$proto126["m_table"] = $obj;
$proto126["m_sql"] = "left join t_perm_dana on t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto126["m_alias"] = "";
$proto126["m_srcTableName"] = "t_input_awal_report1";
$proto128=array();
$proto128["m_sql"] = "t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto128["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto128["m_column"]=$obj;
$proto128["m_contained"] = array();
$proto128["m_strCase"] = "= t_input_awal.id_lahan";
$proto128["m_havingmode"] = false;
$proto128["m_inBrackets"] = false;
$proto128["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto128);

$proto126["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto126);

$proto0["m_fromlist"][]=$obj;
												$proto130=array();
$proto130["m_link"] = "SQLL_LEFTJOIN";
			$proto131=array();
$proto131["m_strName"] = "t_regis";
$proto131["m_srcTableName"] = "t_input_awal_report1";
$proto131["m_columns"] = array();
$proto131["m_columns"][] = "id";
$proto131["m_columns"][] = "id_lahan";
$proto131["m_columns"][] = "no_register";
$proto131["m_columns"][] = "start_date";
$proto131["m_columns"][] = "due_date";
$proto131["m_columns"][] = "pic";
$proto131["m_columns"][] = "keterangan";
$proto131["m_columns"][] = "file";
$proto131["m_columns"][] = "status";
$proto131["m_columns"][] = "close_by";
$proto131["m_columns"][] = "close_date";
$proto131["m_columns"][] = "created_date";
$proto131["m_columns"][] = "created_by";
$proto131["m_columns"][] = "updated_date";
$proto131["m_columns"][] = "updated_by";
$obj = new SQLTable($proto131);

$proto130["m_table"] = $obj;
$proto130["m_sql"] = "left join t_regis on t_regis.id_lahan = t_input_awal.id_lahan";
$proto130["m_alias"] = "";
$proto130["m_srcTableName"] = "t_input_awal_report1";
$proto132=array();
$proto132["m_sql"] = "t_regis.id_lahan = t_input_awal.id_lahan";
$proto132["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_regis",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto132["m_column"]=$obj;
$proto132["m_contained"] = array();
$proto132["m_strCase"] = "= t_input_awal.id_lahan";
$proto132["m_havingmode"] = false;
$proto132["m_inBrackets"] = false;
$proto132["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto132);

$proto130["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto130);

$proto0["m_fromlist"][]=$obj;
												$proto134=array();
$proto134["m_link"] = "SQLL_LEFTJOIN";
			$proto135=array();
$proto135["m_strHead"] = "  	SELECT";
$proto135["m_strFieldList"] = "id_ukur, GROUP_CONCAT(DISTINCT(no_register)) AS no_register";
$proto135["m_strFrom"] = "FROM t_pengukuran_percil";
$proto135["m_strWhere"] = "";
$proto135["m_strOrderBy"] = "";
	
					
;
						$proto135["cipherer"] = null;
$proto137=array();
$proto137["m_sql"] = "";
$proto137["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto137["m_column"]=$obj;
$proto137["m_contained"] = array();
$proto137["m_strCase"] = "";
$proto137["m_havingmode"] = false;
$proto137["m_inBrackets"] = false;
$proto137["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto137);

$proto135["m_where"] = $obj;
$proto139=array();
$proto139["m_sql"] = "";
$proto139["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto139["m_column"]=$obj;
$proto139["m_contained"] = array();
$proto139["m_strCase"] = "";
$proto139["m_havingmode"] = false;
$proto139["m_inBrackets"] = false;
$proto139["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto139);

$proto135["m_having"] = $obj;
$proto135["m_fieldlist"] = array();
						$proto141=array();
			$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto141["m_sql"] = "id_ukur";
$proto141["m_srcTableName"] = "t_input_awal_report1";
$proto141["m_expr"]=$obj;
$proto141["m_alias"] = "";
$obj = new SQLFieldListItem($proto141);

$proto135["m_fieldlist"][]=$obj;
						$proto143=array();
			$proto144=array();
$proto144["m_functiontype"] = "SQLF_CUSTOM";
$proto144["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "DISTINCT(no_register)"
));

$proto144["m_arguments"][]=$obj;
$proto144["m_strFunctionName"] = "GROUP_CONCAT";
$obj = new SQLFunctionCall($proto144);

$proto143["m_sql"] = "GROUP_CONCAT(DISTINCT(no_register))";
$proto143["m_srcTableName"] = "t_input_awal_report1";
$proto143["m_expr"]=$obj;
$proto143["m_alias"] = "no_register";
$obj = new SQLFieldListItem($proto143);

$proto135["m_fieldlist"][]=$obj;
$proto135["m_fromlist"] = array();
												$proto146=array();
$proto146["m_link"] = "SQLL_MAIN";
			$proto147=array();
$proto147["m_strName"] = "t_pengukuran_percil";
$proto147["m_srcTableName"] = "t_input_awal_report1";
$proto147["m_columns"] = array();
$proto147["m_columns"][] = "id";
$proto147["m_columns"][] = "id_ukur";
$proto147["m_columns"][] = "id_lahan";
$proto147["m_columns"][] = "no_register";
$proto147["m_columns"][] = "percil_name";
$proto147["m_columns"][] = "percil_date";
$proto147["m_columns"][] = "urutan";
$proto147["m_columns"][] = "ukuran_real";
$proto147["m_columns"][] = "param_dok";
$proto147["m_columns"][] = "file_dok";
$proto147["m_columns"][] = "keterangan";
$proto147["m_columns"][] = "created_date";
$proto147["m_columns"][] = "created_by";
$proto147["m_columns"][] = "updated_date";
$proto147["m_columns"][] = "updated_by";
$obj = new SQLTable($proto147);

$proto146["m_table"] = $obj;
$proto146["m_sql"] = "t_pengukuran_percil";
$proto146["m_alias"] = "";
$proto146["m_srcTableName"] = "t_input_awal_report1";
$proto148=array();
$proto148["m_sql"] = "";
$proto148["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto148["m_column"]=$obj;
$proto148["m_contained"] = array();
$proto148["m_strCase"] = "";
$proto148["m_havingmode"] = false;
$proto148["m_inBrackets"] = false;
$proto148["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto148);

$proto146["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto146);

$proto135["m_fromlist"][]=$obj;
$proto135["m_groupby"] = array();
												$proto150=array();
						$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto150["m_column"]=$obj;
$obj = new SQLGroupByItem($proto150);

$proto135["m_groupby"][]=$obj;
$proto135["m_orderby"] = array();
$proto135["m_srcTableName"]="t_input_awal_report1";		
$obj = new SQLQuery($proto135);

$proto134["m_table"] = $obj;
$proto134["m_sql"] = "LEFT JOIN (  	SELECT id_ukur, GROUP_CONCAT(DISTINCT(no_register)) AS no_register FROM t_pengukuran_percil   	GROUP BY id_ukur  )register ON register.id_ukur = t_pengukuran.id";
$proto134["m_alias"] = "register";
$proto134["m_srcTableName"] = "t_input_awal_report1";
$proto152=array();
$proto152["m_sql"] = "register.id_ukur = t_pengukuran.id";
$proto152["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "register",
	"m_srcTableName" => "t_input_awal_report1"
));

$proto152["m_column"]=$obj;
$proto152["m_contained"] = array();
$proto152["m_strCase"] = "= t_pengukuran.id";
$proto152["m_havingmode"] = false;
$proto152["m_inBrackets"] = false;
$proto152["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto152);

$proto134["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto134);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_input_awal_report1";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_input_awal_report1 = createSqlQuery_t_input_awal_report1();


	
					
;

																													

$tdatat_input_awal_report1[".sqlquery"] = $queryData_t_input_awal_report1;



include_once(getabspath("include/t_input_awal_report1_events.php"));
$tdatat_input_awal_report1[".hasEvents"] = true;

?>