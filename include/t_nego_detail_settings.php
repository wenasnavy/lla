<?php
$tdatat_nego_detail = array();
$tdatat_nego_detail[".searchableFields"] = array();
$tdatat_nego_detail[".ShortName"] = "t_nego_detail";
$tdatat_nego_detail[".OwnerID"] = "";
$tdatat_nego_detail[".OriginalTable"] = "t_nego_detail";


$tdatat_nego_detail[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_nego_detail[".originalPagesByType"] = $tdatat_nego_detail[".pagesByType"];
$tdatat_nego_detail[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_nego_detail[".originalPages"] = $tdatat_nego_detail[".pages"];
$tdatat_nego_detail[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_nego_detail[".originalDefaultPages"] = $tdatat_nego_detail[".defaultPages"];

//	field labels
$fieldLabelst_nego_detail = array();
$fieldToolTipst_nego_detail = array();
$pageTitlest_nego_detail = array();
$placeHolderst_nego_detail = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_nego_detail["English"] = array();
	$fieldToolTipst_nego_detail["English"] = array();
	$placeHolderst_nego_detail["English"] = array();
	$pageTitlest_nego_detail["English"] = array();
	$fieldLabelst_nego_detail["English"]["id"] = "Id";
	$fieldToolTipst_nego_detail["English"]["id"] = "";
	$placeHolderst_nego_detail["English"]["id"] = "";
	$fieldLabelst_nego_detail["English"]["id_nego"] = "Id Nego";
	$fieldToolTipst_nego_detail["English"]["id_nego"] = "";
	$placeHolderst_nego_detail["English"]["id_nego"] = "";
	$fieldLabelst_nego_detail["English"]["nego_date"] = "Nego Date";
	$fieldToolTipst_nego_detail["English"]["nego_date"] = "";
	$placeHolderst_nego_detail["English"]["nego_date"] = "";
	$fieldLabelst_nego_detail["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_nego_detail["English"]["keterangan"] = "";
	$placeHolderst_nego_detail["English"]["keterangan"] = "";
	$fieldLabelst_nego_detail["English"]["created_date"] = "Created Date";
	$fieldToolTipst_nego_detail["English"]["created_date"] = "";
	$placeHolderst_nego_detail["English"]["created_date"] = "";
	$fieldLabelst_nego_detail["English"]["created_by"] = "Created By";
	$fieldToolTipst_nego_detail["English"]["created_by"] = "";
	$placeHolderst_nego_detail["English"]["created_by"] = "";
	$fieldLabelst_nego_detail["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_nego_detail["English"]["updated_date"] = "";
	$placeHolderst_nego_detail["English"]["updated_date"] = "";
	$fieldLabelst_nego_detail["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_nego_detail["English"]["updated_by"] = "";
	$placeHolderst_nego_detail["English"]["updated_by"] = "";
	$fieldLabelst_nego_detail["English"]["nego_step"] = "Nego Step";
	$fieldToolTipst_nego_detail["English"]["nego_step"] = "";
	$placeHolderst_nego_detail["English"]["nego_step"] = "";
	$fieldLabelst_nego_detail["English"]["harga_perusahaan"] = "Harga Perusahaan";
	$fieldToolTipst_nego_detail["English"]["harga_perusahaan"] = "";
	$placeHolderst_nego_detail["English"]["harga_perusahaan"] = "";
	$fieldLabelst_nego_detail["English"]["harga"] = "Harga Penjual";
	$fieldToolTipst_nego_detail["English"]["harga"] = "";
	$placeHolderst_nego_detail["English"]["harga"] = "";
	$fieldLabelst_nego_detail["English"]["upload_file"] = "Upload File";
	$fieldToolTipst_nego_detail["English"]["upload_file"] = "";
	$placeHolderst_nego_detail["English"]["upload_file"] = "";
	if (count($fieldToolTipst_nego_detail["English"]))
		$tdatat_nego_detail[".isUseToolTips"] = true;
}


	$tdatat_nego_detail[".NCSearch"] = true;



$tdatat_nego_detail[".shortTableName"] = "t_nego_detail";
$tdatat_nego_detail[".nSecOptions"] = 0;

$tdatat_nego_detail[".mainTableOwnerID"] = "";
$tdatat_nego_detail[".entityType"] = 0;
$tdatat_nego_detail[".connId"] = "db_lla_at_localhost";


$tdatat_nego_detail[".strOriginalTableName"] = "t_nego_detail";

		 



$tdatat_nego_detail[".showAddInPopup"] = false;

$tdatat_nego_detail[".showEditInPopup"] = false;

$tdatat_nego_detail[".showViewInPopup"] = false;

$tdatat_nego_detail[".listAjax"] = false;
//	temporary
//$tdatat_nego_detail[".listAjax"] = false;

	$tdatat_nego_detail[".audit"] = false;

	$tdatat_nego_detail[".locking"] = false;


$pages = $tdatat_nego_detail[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_nego_detail[".edit"] = true;
	$tdatat_nego_detail[".afterEditAction"] = 1;
	$tdatat_nego_detail[".closePopupAfterEdit"] = 1;
	$tdatat_nego_detail[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_nego_detail[".add"] = true;
$tdatat_nego_detail[".afterAddAction"] = 1;
$tdatat_nego_detail[".closePopupAfterAdd"] = 1;
$tdatat_nego_detail[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_nego_detail[".list"] = true;
}



$tdatat_nego_detail[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_nego_detail[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_nego_detail[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_nego_detail[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_nego_detail[".printFriendly"] = true;
}



$tdatat_nego_detail[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_nego_detail[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_nego_detail[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_nego_detail[".isUseAjaxSuggest"] = true;

$tdatat_nego_detail[".rowHighlite"] = true;



						

$tdatat_nego_detail[".ajaxCodeSnippetAdded"] = false;

$tdatat_nego_detail[".buttonsAdded"] = false;

$tdatat_nego_detail[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_nego_detail[".isUseTimeForSearch"] = false;


$tdatat_nego_detail[".badgeColor"] = "db7093";


$tdatat_nego_detail[".allSearchFields"] = array();
$tdatat_nego_detail[".filterFields"] = array();
$tdatat_nego_detail[".requiredSearchFields"] = array();

$tdatat_nego_detail[".googleLikeFields"] = array();
$tdatat_nego_detail[".googleLikeFields"][] = "id";
$tdatat_nego_detail[".googleLikeFields"][] = "id_nego";
$tdatat_nego_detail[".googleLikeFields"][] = "nego_step";
$tdatat_nego_detail[".googleLikeFields"][] = "nego_date";
$tdatat_nego_detail[".googleLikeFields"][] = "harga";
$tdatat_nego_detail[".googleLikeFields"][] = "harga_perusahaan";
$tdatat_nego_detail[".googleLikeFields"][] = "keterangan";
$tdatat_nego_detail[".googleLikeFields"][] = "upload_file";
$tdatat_nego_detail[".googleLikeFields"][] = "created_date";
$tdatat_nego_detail[".googleLikeFields"][] = "created_by";
$tdatat_nego_detail[".googleLikeFields"][] = "updated_date";
$tdatat_nego_detail[".googleLikeFields"][] = "updated_by";



$tdatat_nego_detail[".tableType"] = "list";

$tdatat_nego_detail[".printerPageOrientation"] = 0;
$tdatat_nego_detail[".nPrinterPageScale"] = 100;

$tdatat_nego_detail[".nPrinterSplitRecords"] = 40;

$tdatat_nego_detail[".geocodingEnabled"] = false;










$tdatat_nego_detail[".pageSize"] = 20;

$tdatat_nego_detail[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_nego_detail[".strOrderBy"] = $tstrOrderBy;

$tdatat_nego_detail[".orderindexes"] = array();


$tdatat_nego_detail[".sqlHead"] = "SELECT id,  	id_nego,  	nego_step,  	nego_date,  	harga,  	harga_perusahaan,  	keterangan,  	upload_file,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_nego_detail[".sqlFrom"] = "FROM t_nego_detail";
$tdatat_nego_detail[".sqlWhereExpr"] = "";
$tdatat_nego_detail[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_nego_detail[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_nego_detail[".arrGroupsPerPage"] = $arrGPP;

$tdatat_nego_detail[".highlightSearchResults"] = true;

$tableKeyst_nego_detail = array();
$tableKeyst_nego_detail[] = "id";
$tdatat_nego_detail[".Keys"] = $tableKeyst_nego_detail;


$tdatat_nego_detail[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["id"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "id";
//	id_nego
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_nego";
	$fdata["GoodName"] = "id_nego";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","id_nego");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_nego";

		$fdata["sourceSingle"] = "id_nego";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_nego";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["id_nego"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "id_nego";
//	nego_step
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nego_step";
	$fdata["GoodName"] = "nego_step";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","nego_step");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nego_step";

		$fdata["sourceSingle"] = "nego_step";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nego_step";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["nego_step"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "nego_step";
//	nego_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "nego_date";
	$fdata["GoodName"] = "nego_date";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","nego_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "nego_date";

		$fdata["sourceSingle"] = "nego_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nego_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["nego_date"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "nego_date";
//	harga
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "harga";
	$fdata["GoodName"] = "harga";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","harga");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "harga";

		$fdata["sourceSingle"] = "harga";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "harga";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["harga"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "harga";
//	harga_perusahaan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "harga_perusahaan";
	$fdata["GoodName"] = "harga_perusahaan";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","harga_perusahaan");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "harga_perusahaan";

		$fdata["sourceSingle"] = "harga_perusahaan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "harga_perusahaan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["harga_perusahaan"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "harga_perusahaan";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["keterangan"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "keterangan";
//	upload_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "upload_file";
	$fdata["GoodName"] = "upload_file";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","upload_file");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "upload_file";

		$fdata["sourceSingle"] = "upload_file";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "upload_file";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "File-based Image");

	
	
				$vdata["ShowThumbnail"] = true;
	$vdata["ThumbWidth"] = 200;
	$vdata["ThumbHeight"] = 150;
	$vdata["ImageWidth"] = 600;
	$vdata["ImageHeight"] = 400;

			$vdata["multipleImgMode"] = 1;
	$vdata["maxImages"] = 0;

			$vdata["showGallery"] = true;
	$vdata["galleryMode"] = 2;
	$vdata["captionMode"] = 2;
	$vdata["captionField"] = "";

	$vdata["imageBorder"] = 1;
	$vdata["imageFullWidth"] = 1;


		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = array();
			$edata["acceptFileTypes"][] = strtoupper("bmp");
						$edata["acceptFileTypesHtml"] = ".bmp";
			$edata["acceptFileTypes"][] = strtoupper("doc");
						$edata["acceptFileTypesHtml"] .= ",.doc";
			$edata["acceptFileTypes"][] = strtoupper("docx");
						$edata["acceptFileTypesHtml"] .= ",.docx";
			$edata["acceptFileTypes"][] = strtoupper("gif");
						$edata["acceptFileTypesHtml"] .= ",.gif";
			$edata["acceptFileTypes"][] = strtoupper("jpeg");
						$edata["acceptFileTypesHtml"] .= ",.jpeg";
			$edata["acceptFileTypes"][] = strtoupper("jpg");
						$edata["acceptFileTypesHtml"] .= ",.jpg";
			$edata["acceptFileTypes"][] = strtoupper("pdf");
						$edata["acceptFileTypesHtml"] .= ",.pdf";
			$edata["acceptFileTypes"][] = strtoupper("png");
						$edata["acceptFileTypesHtml"] .= ",.png";
			$edata["acceptFileTypes"][] = strtoupper("xls");
						$edata["acceptFileTypesHtml"] .= ",.xls";
			$edata["acceptFileTypes"][] = strtoupper("xlsx");
						$edata["acceptFileTypesHtml"] .= ",.xlsx";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["upload_file"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "upload_file";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["created_date"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["created_by"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["updated_date"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_nego_detail";
	$fdata["Label"] = GetFieldLabel("t_nego_detail","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_nego_detail["updated_by"] = $fdata;
		$tdatat_nego_detail[".searchableFields"][] = "updated_by";


$tables_data["t_nego_detail"]=&$tdatat_nego_detail;
$field_labels["t_nego_detail"] = &$fieldLabelst_nego_detail;
$fieldToolTips["t_nego_detail"] = &$fieldToolTipst_nego_detail;
$placeHolders["t_nego_detail"] = &$placeHolderst_nego_detail;
$page_titles["t_nego_detail"] = &$pageTitlest_nego_detail;


changeTextControlsToDate( "t_nego_detail" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_nego_detail"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_nego_detail"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_negosiasi";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_negosiasi";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_negosiasi";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_nego_detail"][0] = $masterParams;
				$masterTablesData["t_nego_detail"][0]["masterKeys"] = array();
	$masterTablesData["t_nego_detail"][0]["masterKeys"][]="id";
				$masterTablesData["t_nego_detail"][0]["detailKeys"] = array();
	$masterTablesData["t_nego_detail"][0]["detailKeys"][]="id_nego";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_nego_detail()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_nego,  	nego_step,  	nego_date,  	harga,  	harga_perusahaan,  	keterangan,  	upload_file,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_nego_detail";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_nego_detail";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_nego",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto8["m_sql"] = "id_nego";
$proto8["m_srcTableName"] = "t_nego_detail";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nego_step",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto10["m_sql"] = "nego_step";
$proto10["m_srcTableName"] = "t_nego_detail";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "nego_date",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto12["m_sql"] = "nego_date";
$proto12["m_srcTableName"] = "t_nego_detail";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "harga",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto14["m_sql"] = "harga";
$proto14["m_srcTableName"] = "t_nego_detail";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "harga_perusahaan",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto16["m_sql"] = "harga_perusahaan";
$proto16["m_srcTableName"] = "t_nego_detail";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto18["m_sql"] = "keterangan";
$proto18["m_srcTableName"] = "t_nego_detail";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "upload_file",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto20["m_sql"] = "upload_file";
$proto20["m_srcTableName"] = "t_nego_detail";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto22["m_sql"] = "created_date";
$proto22["m_srcTableName"] = "t_nego_detail";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto24["m_sql"] = "created_by";
$proto24["m_srcTableName"] = "t_nego_detail";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto26["m_sql"] = "updated_date";
$proto26["m_srcTableName"] = "t_nego_detail";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_nego_detail",
	"m_srcTableName" => "t_nego_detail"
));

$proto28["m_sql"] = "updated_by";
$proto28["m_srcTableName"] = "t_nego_detail";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto30=array();
$proto30["m_link"] = "SQLL_MAIN";
			$proto31=array();
$proto31["m_strName"] = "t_nego_detail";
$proto31["m_srcTableName"] = "t_nego_detail";
$proto31["m_columns"] = array();
$proto31["m_columns"][] = "id";
$proto31["m_columns"][] = "id_nego";
$proto31["m_columns"][] = "nego_step";
$proto31["m_columns"][] = "nego_date";
$proto31["m_columns"][] = "harga";
$proto31["m_columns"][] = "harga_perusahaan";
$proto31["m_columns"][] = "keterangan";
$proto31["m_columns"][] = "upload_file";
$proto31["m_columns"][] = "created_date";
$proto31["m_columns"][] = "created_by";
$proto31["m_columns"][] = "updated_date";
$proto31["m_columns"][] = "updated_by";
$obj = new SQLTable($proto31);

$proto30["m_table"] = $obj;
$proto30["m_sql"] = "t_nego_detail";
$proto30["m_alias"] = "";
$proto30["m_srcTableName"] = "t_nego_detail";
$proto32=array();
$proto32["m_sql"] = "";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto30["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto30);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_nego_detail";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_nego_detail = createSqlQuery_t_nego_detail();


	
					
;

												

$tdatat_nego_detail[".sqlquery"] = $queryData_t_nego_detail;



include_once(getabspath("include/t_nego_detail_events.php"));
$tdatat_nego_detail[".hasEvents"] = true;

?>