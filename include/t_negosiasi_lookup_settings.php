<?php
$tdatat_negosiasi_lookup = array();
$tdatat_negosiasi_lookup[".searchableFields"] = array();
$tdatat_negosiasi_lookup[".ShortName"] = "t_negosiasi_lookup";
$tdatat_negosiasi_lookup[".OwnerID"] = "";
$tdatat_negosiasi_lookup[".OriginalTable"] = "t_negosiasi";


$tdatat_negosiasi_lookup[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_negosiasi_lookup[".originalPagesByType"] = $tdatat_negosiasi_lookup[".pagesByType"];
$tdatat_negosiasi_lookup[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_negosiasi_lookup[".originalPages"] = $tdatat_negosiasi_lookup[".pages"];
$tdatat_negosiasi_lookup[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_negosiasi_lookup[".originalDefaultPages"] = $tdatat_negosiasi_lookup[".defaultPages"];

//	field labels
$fieldLabelst_negosiasi_lookup = array();
$fieldToolTipst_negosiasi_lookup = array();
$pageTitlest_negosiasi_lookup = array();
$placeHolderst_negosiasi_lookup = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_negosiasi_lookup["English"] = array();
	$fieldToolTipst_negosiasi_lookup["English"] = array();
	$placeHolderst_negosiasi_lookup["English"] = array();
	$pageTitlest_negosiasi_lookup["English"] = array();
	$fieldLabelst_negosiasi_lookup["English"]["id"] = "Id";
	$fieldToolTipst_negosiasi_lookup["English"]["id"] = "";
	$placeHolderst_negosiasi_lookup["English"]["id"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["id_lahan"] = "Id Lahan";
	$fieldToolTipst_negosiasi_lookup["English"]["id_lahan"] = "";
	$placeHolderst_negosiasi_lookup["English"]["id_lahan"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_negosiasi_lookup["English"]["nama_lahan"] = "";
	$placeHolderst_negosiasi_lookup["English"]["nama_lahan"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["start_date"] = "Start Date";
	$fieldToolTipst_negosiasi_lookup["English"]["start_date"] = "";
	$placeHolderst_negosiasi_lookup["English"]["start_date"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["due_date"] = "Due Date";
	$fieldToolTipst_negosiasi_lookup["English"]["due_date"] = "";
	$placeHolderst_negosiasi_lookup["English"]["due_date"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["pic"] = "Pic";
	$fieldToolTipst_negosiasi_lookup["English"]["pic"] = "";
	$placeHolderst_negosiasi_lookup["English"]["pic"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_negosiasi_lookup["English"]["keterangan"] = "";
	$placeHolderst_negosiasi_lookup["English"]["keterangan"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["status"] = "Status";
	$fieldToolTipst_negosiasi_lookup["English"]["status"] = "";
	$placeHolderst_negosiasi_lookup["English"]["status"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["close_by"] = "Close By";
	$fieldToolTipst_negosiasi_lookup["English"]["close_by"] = "";
	$placeHolderst_negosiasi_lookup["English"]["close_by"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["close_date"] = "Close Date";
	$fieldToolTipst_negosiasi_lookup["English"]["close_date"] = "";
	$placeHolderst_negosiasi_lookup["English"]["close_date"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["created_date"] = "Created Date";
	$fieldToolTipst_negosiasi_lookup["English"]["created_date"] = "";
	$placeHolderst_negosiasi_lookup["English"]["created_date"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["created_by"] = "Created By";
	$fieldToolTipst_negosiasi_lookup["English"]["created_by"] = "";
	$placeHolderst_negosiasi_lookup["English"]["created_by"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_negosiasi_lookup["English"]["updated_date"] = "";
	$placeHolderst_negosiasi_lookup["English"]["updated_date"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_negosiasi_lookup["English"]["updated_by"] = "";
	$placeHolderst_negosiasi_lookup["English"]["updated_by"] = "";
	$fieldLabelst_negosiasi_lookup["English"]["ukuran"] = "Ukuran";
	$fieldToolTipst_negosiasi_lookup["English"]["ukuran"] = "";
	$placeHolderst_negosiasi_lookup["English"]["ukuran"] = "";
	if (count($fieldToolTipst_negosiasi_lookup["English"]))
		$tdatat_negosiasi_lookup[".isUseToolTips"] = true;
}


	$tdatat_negosiasi_lookup[".NCSearch"] = true;



$tdatat_negosiasi_lookup[".shortTableName"] = "t_negosiasi_lookup";
$tdatat_negosiasi_lookup[".nSecOptions"] = 0;

$tdatat_negosiasi_lookup[".mainTableOwnerID"] = "";
$tdatat_negosiasi_lookup[".entityType"] = 1;
$tdatat_negosiasi_lookup[".connId"] = "db_lla_at_localhost";


$tdatat_negosiasi_lookup[".strOriginalTableName"] = "t_negosiasi";

		 



$tdatat_negosiasi_lookup[".showAddInPopup"] = false;

$tdatat_negosiasi_lookup[".showEditInPopup"] = false;

$tdatat_negosiasi_lookup[".showViewInPopup"] = false;

$tdatat_negosiasi_lookup[".listAjax"] = false;
//	temporary
//$tdatat_negosiasi_lookup[".listAjax"] = false;

	$tdatat_negosiasi_lookup[".audit"] = false;

	$tdatat_negosiasi_lookup[".locking"] = false;


$pages = $tdatat_negosiasi_lookup[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_negosiasi_lookup[".edit"] = true;
	$tdatat_negosiasi_lookup[".afterEditAction"] = 1;
	$tdatat_negosiasi_lookup[".closePopupAfterEdit"] = 1;
	$tdatat_negosiasi_lookup[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_negosiasi_lookup[".add"] = true;
$tdatat_negosiasi_lookup[".afterAddAction"] = 1;
$tdatat_negosiasi_lookup[".closePopupAfterAdd"] = 1;
$tdatat_negosiasi_lookup[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_negosiasi_lookup[".list"] = true;
}



$tdatat_negosiasi_lookup[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_negosiasi_lookup[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_negosiasi_lookup[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_negosiasi_lookup[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_negosiasi_lookup[".printFriendly"] = true;
}



$tdatat_negosiasi_lookup[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_negosiasi_lookup[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_negosiasi_lookup[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_negosiasi_lookup[".isUseAjaxSuggest"] = true;

$tdatat_negosiasi_lookup[".rowHighlite"] = true;



			

$tdatat_negosiasi_lookup[".ajaxCodeSnippetAdded"] = false;

$tdatat_negosiasi_lookup[".buttonsAdded"] = false;

$tdatat_negosiasi_lookup[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_negosiasi_lookup[".isUseTimeForSearch"] = false;


$tdatat_negosiasi_lookup[".badgeColor"] = "4682B4";


$tdatat_negosiasi_lookup[".allSearchFields"] = array();
$tdatat_negosiasi_lookup[".filterFields"] = array();
$tdatat_negosiasi_lookup[".requiredSearchFields"] = array();

$tdatat_negosiasi_lookup[".googleLikeFields"] = array();
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "id";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "id_lahan";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "nama_lahan";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "start_date";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "due_date";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "pic";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "keterangan";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "status";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "close_by";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "close_date";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "created_date";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "created_by";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "updated_date";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "updated_by";
$tdatat_negosiasi_lookup[".googleLikeFields"][] = "ukuran";



$tdatat_negosiasi_lookup[".tableType"] = "list";

$tdatat_negosiasi_lookup[".printerPageOrientation"] = 0;
$tdatat_negosiasi_lookup[".nPrinterPageScale"] = 100;

$tdatat_negosiasi_lookup[".nPrinterSplitRecords"] = 40;

$tdatat_negosiasi_lookup[".geocodingEnabled"] = false;










$tdatat_negosiasi_lookup[".pageSize"] = 20;

$tdatat_negosiasi_lookup[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_negosiasi_lookup[".strOrderBy"] = $tstrOrderBy;

$tdatat_negosiasi_lookup[".orderindexes"] = array();


$tdatat_negosiasi_lookup[".sqlHead"] = "SELECT t_negosiasi.id,  t_negosiasi.id_lahan,  t_input_awal.nama_lahan,  t_negosiasi.start_date,  t_negosiasi.due_date,  t_negosiasi.pic,  t_negosiasi.keterangan,  t_negosiasi.status,  t_negosiasi.close_by,  t_negosiasi.close_date,  t_negosiasi.created_date,  t_negosiasi.created_by,  t_negosiasi.updated_date,  t_negosiasi.updated_by,  t_pengukuran.ukuran";
$tdatat_negosiasi_lookup[".sqlFrom"] = "FROM t_negosiasi  INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan  INNER JOIN t_pengukuran ON t_negosiasi.id_lahan = t_pengukuran.id_lahan";
$tdatat_negosiasi_lookup[".sqlWhereExpr"] = "";
$tdatat_negosiasi_lookup[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_negosiasi_lookup[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_negosiasi_lookup[".arrGroupsPerPage"] = $arrGPP;

$tdatat_negosiasi_lookup[".highlightSearchResults"] = true;

$tableKeyst_negosiasi_lookup = array();
$tableKeyst_negosiasi_lookup[] = "id";
$tdatat_negosiasi_lookup[".Keys"] = $tableKeyst_negosiasi_lookup;


$tdatat_negosiasi_lookup[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["id"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_pengukuran.status = 'CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_pengukuran.status = 'CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["id_lahan"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["nama_lahan"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "nama_lahan";
//	start_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "start_date";
	$fdata["GoodName"] = "start_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","start_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date";

		$fdata["sourceSingle"] = "start_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.start_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["start_date"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "start_date";
//	due_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "due_date";
	$fdata["GoodName"] = "due_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","due_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date";

		$fdata["sourceSingle"] = "due_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.due_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["due_date"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "due_date";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["pic"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "pic";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["keterangan"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "keterangan";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["status"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "status";
//	close_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "close_by";
	$fdata["GoodName"] = "close_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","close_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_by";

		$fdata["sourceSingle"] = "close_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.close_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["close_by"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "close_by";
//	close_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "close_date";
	$fdata["GoodName"] = "close_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","close_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_date";

		$fdata["sourceSingle"] = "close_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.close_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["close_date"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "close_date";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["created_date"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["created_by"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["updated_date"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["updated_by"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "updated_by";
//	ukuran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "ukuran";
	$fdata["GoodName"] = "ukuran";
	$fdata["ownerTable"] = "t_pengukuran";
	$fdata["Label"] = GetFieldLabel("t_negosiasi_lookup","ukuran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "ukuran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_pengukuran.ukuran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi_lookup["ukuran"] = $fdata;
		$tdatat_negosiasi_lookup[".searchableFields"][] = "ukuran";


$tables_data["t_negosiasi_lookup"]=&$tdatat_negosiasi_lookup;
$field_labels["t_negosiasi_lookup"] = &$fieldLabelst_negosiasi_lookup;
$fieldToolTips["t_negosiasi_lookup"] = &$fieldToolTipst_negosiasi_lookup;
$placeHolders["t_negosiasi_lookup"] = &$placeHolderst_negosiasi_lookup;
$page_titles["t_negosiasi_lookup"] = &$pageTitlest_negosiasi_lookup;


changeTextControlsToDate( "t_negosiasi_lookup" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_negosiasi_lookup"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_negosiasi_lookup"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_negosiasi_lookup()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_negosiasi.id,  t_negosiasi.id_lahan,  t_input_awal.nama_lahan,  t_negosiasi.start_date,  t_negosiasi.due_date,  t_negosiasi.pic,  t_negosiasi.keterangan,  t_negosiasi.status,  t_negosiasi.close_by,  t_negosiasi.close_date,  t_negosiasi.created_date,  t_negosiasi.created_by,  t_negosiasi.updated_date,  t_negosiasi.updated_by,  t_pengukuran.ukuran";
$proto0["m_strFrom"] = "FROM t_negosiasi  INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan  INNER JOIN t_pengukuran ON t_negosiasi.id_lahan = t_pengukuran.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto6["m_sql"] = "t_negosiasi.id";
$proto6["m_srcTableName"] = "t_negosiasi_lookup";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto8["m_sql"] = "t_negosiasi.id_lahan";
$proto8["m_srcTableName"] = "t_negosiasi_lookup";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto10["m_sql"] = "t_input_awal.nama_lahan";
$proto10["m_srcTableName"] = "t_negosiasi_lookup";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto12["m_sql"] = "t_negosiasi.start_date";
$proto12["m_srcTableName"] = "t_negosiasi_lookup";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto14["m_sql"] = "t_negosiasi.due_date";
$proto14["m_srcTableName"] = "t_negosiasi_lookup";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto16["m_sql"] = "t_negosiasi.pic";
$proto16["m_srcTableName"] = "t_negosiasi_lookup";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto18["m_sql"] = "t_negosiasi.keterangan";
$proto18["m_srcTableName"] = "t_negosiasi_lookup";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto20["m_sql"] = "t_negosiasi.status";
$proto20["m_srcTableName"] = "t_negosiasi_lookup";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "close_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto22["m_sql"] = "t_negosiasi.close_by";
$proto22["m_srcTableName"] = "t_negosiasi_lookup";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "close_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto24["m_sql"] = "t_negosiasi.close_date";
$proto24["m_srcTableName"] = "t_negosiasi_lookup";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto26["m_sql"] = "t_negosiasi.created_date";
$proto26["m_srcTableName"] = "t_negosiasi_lookup";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto28["m_sql"] = "t_negosiasi.created_by";
$proto28["m_srcTableName"] = "t_negosiasi_lookup";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto30["m_sql"] = "t_negosiasi.updated_date";
$proto30["m_srcTableName"] = "t_negosiasi_lookup";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto32["m_sql"] = "t_negosiasi.updated_by";
$proto32["m_srcTableName"] = "t_negosiasi_lookup";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "ukuran",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto34["m_sql"] = "t_pengukuran.ukuran";
$proto34["m_srcTableName"] = "t_negosiasi_lookup";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto36=array();
$proto36["m_link"] = "SQLL_MAIN";
			$proto37=array();
$proto37["m_strName"] = "t_negosiasi";
$proto37["m_srcTableName"] = "t_negosiasi_lookup";
$proto37["m_columns"] = array();
$proto37["m_columns"][] = "id";
$proto37["m_columns"][] = "id_lahan";
$proto37["m_columns"][] = "start_date";
$proto37["m_columns"][] = "due_date";
$proto37["m_columns"][] = "pic";
$proto37["m_columns"][] = "keterangan";
$proto37["m_columns"][] = "status";
$proto37["m_columns"][] = "close_by";
$proto37["m_columns"][] = "close_date";
$proto37["m_columns"][] = "created_date";
$proto37["m_columns"][] = "created_by";
$proto37["m_columns"][] = "updated_date";
$proto37["m_columns"][] = "updated_by";
$obj = new SQLTable($proto37);

$proto36["m_table"] = $obj;
$proto36["m_sql"] = "t_negosiasi";
$proto36["m_alias"] = "";
$proto36["m_srcTableName"] = "t_negosiasi_lookup";
$proto38=array();
$proto38["m_sql"] = "";
$proto38["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto38["m_column"]=$obj;
$proto38["m_contained"] = array();
$proto38["m_strCase"] = "";
$proto38["m_havingmode"] = false;
$proto38["m_inBrackets"] = false;
$proto38["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto38);

$proto36["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto36);

$proto0["m_fromlist"][]=$obj;
												$proto40=array();
$proto40["m_link"] = "SQLL_INNERJOIN";
			$proto41=array();
$proto41["m_strName"] = "t_input_awal";
$proto41["m_srcTableName"] = "t_negosiasi_lookup";
$proto41["m_columns"] = array();
$proto41["m_columns"][] = "id";
$proto41["m_columns"][] = "id_lahan";
$proto41["m_columns"][] = "nama_lahan";
$proto41["m_columns"][] = "batas_utara";
$proto41["m_columns"][] = "batas_selatan";
$proto41["m_columns"][] = "batas_barat";
$proto41["m_columns"][] = "batas_timur";
$proto41["m_columns"][] = "blok_lokasi";
$proto41["m_columns"][] = "nama_lokasi";
$proto41["m_columns"][] = "keperluan_lahan";
$proto41["m_columns"][] = "start_date";
$proto41["m_columns"][] = "due_date";
$proto41["m_columns"][] = "pic";
$proto41["m_columns"][] = "status";
$proto41["m_columns"][] = "close_by";
$proto41["m_columns"][] = "close_date";
$proto41["m_columns"][] = "keterangan";
$proto41["m_columns"][] = "im_file";
$proto41["m_columns"][] = "created_date";
$proto41["m_columns"][] = "created_by";
$proto41["m_columns"][] = "updated_date";
$proto41["m_columns"][] = "updated_by";
$obj = new SQLTable($proto41);

$proto40["m_table"] = $obj;
$proto40["m_sql"] = "INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan";
$proto40["m_alias"] = "";
$proto40["m_srcTableName"] = "t_negosiasi_lookup";
$proto42=array();
$proto42["m_sql"] = "t_input_awal.id_lahan = t_negosiasi.id_lahan";
$proto42["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto42["m_column"]=$obj;
$proto42["m_contained"] = array();
$proto42["m_strCase"] = "= t_negosiasi.id_lahan";
$proto42["m_havingmode"] = false;
$proto42["m_inBrackets"] = false;
$proto42["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto42);

$proto40["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto40);

$proto0["m_fromlist"][]=$obj;
												$proto44=array();
$proto44["m_link"] = "SQLL_INNERJOIN";
			$proto45=array();
$proto45["m_strName"] = "t_pengukuran";
$proto45["m_srcTableName"] = "t_negosiasi_lookup";
$proto45["m_columns"] = array();
$proto45["m_columns"][] = "id";
$proto45["m_columns"][] = "id_lahan";
$proto45["m_columns"][] = "start_date";
$proto45["m_columns"][] = "due_date";
$proto45["m_columns"][] = "pic";
$proto45["m_columns"][] = "ukuran";
$proto45["m_columns"][] = "utipe";
$proto45["m_columns"][] = "batas_utara";
$proto45["m_columns"][] = "batas_utara_text";
$proto45["m_columns"][] = "stipe";
$proto45["m_columns"][] = "batas_selatan";
$proto45["m_columns"][] = "batas_selatan_text";
$proto45["m_columns"][] = "btipe";
$proto45["m_columns"][] = "batas_barat";
$proto45["m_columns"][] = "batas_barat_text";
$proto45["m_columns"][] = "ttipe";
$proto45["m_columns"][] = "batas_timur";
$proto45["m_columns"][] = "batas_timur_text";
$proto45["m_columns"][] = "keterangan";
$proto45["m_columns"][] = "maps_file";
$proto45["m_columns"][] = "status";
$proto45["m_columns"][] = "close_by";
$proto45["m_columns"][] = "close_date";
$proto45["m_columns"][] = "created_date";
$proto45["m_columns"][] = "created_by";
$proto45["m_columns"][] = "updated_date";
$proto45["m_columns"][] = "updated_by";
$proto45["m_columns"][] = "Lat";
$proto45["m_columns"][] = "Lng";
$proto45["m_columns"][] = "Lat2";
$proto45["m_columns"][] = "Lng2";
$obj = new SQLTable($proto45);

$proto44["m_table"] = $obj;
$proto44["m_sql"] = "INNER JOIN t_pengukuran ON t_negosiasi.id_lahan = t_pengukuran.id_lahan";
$proto44["m_alias"] = "";
$proto44["m_srcTableName"] = "t_negosiasi_lookup";
$proto46=array();
$proto46["m_sql"] = "t_pengukuran.id_lahan = t_negosiasi.id_lahan";
$proto46["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran",
	"m_srcTableName" => "t_negosiasi_lookup"
));

$proto46["m_column"]=$obj;
$proto46["m_contained"] = array();
$proto46["m_strCase"] = "= t_negosiasi.id_lahan";
$proto46["m_havingmode"] = false;
$proto46["m_inBrackets"] = false;
$proto46["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto46);

$proto44["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto44);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_negosiasi_lookup";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_negosiasi_lookup = createSqlQuery_t_negosiasi_lookup();


	
					
;

															

$tdatat_negosiasi_lookup[".sqlquery"] = $queryData_t_negosiasi_lookup;



include_once(getabspath("include/t_negosiasi_lookup_events.php"));
$tdatat_negosiasi_lookup[".hasEvents"] = true;

?>