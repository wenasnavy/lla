<?php
$tdatat_negosiasi = array();
$tdatat_negosiasi[".searchableFields"] = array();
$tdatat_negosiasi[".ShortName"] = "t_negosiasi";
$tdatat_negosiasi[".OwnerID"] = "";
$tdatat_negosiasi[".OriginalTable"] = "t_negosiasi";


$tdatat_negosiasi[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_negosiasi[".originalPagesByType"] = $tdatat_negosiasi[".pagesByType"];
$tdatat_negosiasi[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_negosiasi[".originalPages"] = $tdatat_negosiasi[".pages"];
$tdatat_negosiasi[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_negosiasi[".originalDefaultPages"] = $tdatat_negosiasi[".defaultPages"];

//	field labels
$fieldLabelst_negosiasi = array();
$fieldToolTipst_negosiasi = array();
$pageTitlest_negosiasi = array();
$placeHolderst_negosiasi = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_negosiasi["English"] = array();
	$fieldToolTipst_negosiasi["English"] = array();
	$placeHolderst_negosiasi["English"] = array();
	$pageTitlest_negosiasi["English"] = array();
	$fieldLabelst_negosiasi["English"]["id"] = "Id";
	$fieldToolTipst_negosiasi["English"]["id"] = "";
	$placeHolderst_negosiasi["English"]["id"] = "";
	$fieldLabelst_negosiasi["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_negosiasi["English"]["id_lahan"] = "";
	$placeHolderst_negosiasi["English"]["id_lahan"] = "";
	$fieldLabelst_negosiasi["English"]["start_date"] = "Start Date";
	$fieldToolTipst_negosiasi["English"]["start_date"] = "";
	$placeHolderst_negosiasi["English"]["start_date"] = "";
	$fieldLabelst_negosiasi["English"]["due_date"] = "Due Date";
	$fieldToolTipst_negosiasi["English"]["due_date"] = "";
	$placeHolderst_negosiasi["English"]["due_date"] = "";
	$fieldLabelst_negosiasi["English"]["pic"] = "PIC";
	$fieldToolTipst_negosiasi["English"]["pic"] = "";
	$placeHolderst_negosiasi["English"]["pic"] = "";
	$fieldLabelst_negosiasi["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_negosiasi["English"]["keterangan"] = "";
	$placeHolderst_negosiasi["English"]["keterangan"] = "";
	$fieldLabelst_negosiasi["English"]["status"] = "Status";
	$fieldToolTipst_negosiasi["English"]["status"] = "";
	$placeHolderst_negosiasi["English"]["status"] = "";
	$fieldLabelst_negosiasi["English"]["close_by"] = "Close By";
	$fieldToolTipst_negosiasi["English"]["close_by"] = "";
	$placeHolderst_negosiasi["English"]["close_by"] = "";
	$fieldLabelst_negosiasi["English"]["close_date"] = "Close Date";
	$fieldToolTipst_negosiasi["English"]["close_date"] = "";
	$placeHolderst_negosiasi["English"]["close_date"] = "";
	$fieldLabelst_negosiasi["English"]["created_date"] = "Created Date";
	$fieldToolTipst_negosiasi["English"]["created_date"] = "";
	$placeHolderst_negosiasi["English"]["created_date"] = "";
	$fieldLabelst_negosiasi["English"]["created_by"] = "Created By";
	$fieldToolTipst_negosiasi["English"]["created_by"] = "";
	$placeHolderst_negosiasi["English"]["created_by"] = "";
	$fieldLabelst_negosiasi["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_negosiasi["English"]["updated_date"] = "";
	$placeHolderst_negosiasi["English"]["updated_date"] = "";
	$fieldLabelst_negosiasi["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_negosiasi["English"]["updated_by"] = "";
	$placeHolderst_negosiasi["English"]["updated_by"] = "";
	$fieldLabelst_negosiasi["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_negosiasi["English"]["nama_lahan"] = "";
	$placeHolderst_negosiasi["English"]["nama_lahan"] = "";
	$pageTitlest_negosiasi["English"]["view"] = "Negosiasi {%id_lahan}, {%nama_lahan}";
	$pageTitlest_negosiasi["English"]["search"] = "Negosiasi, Advanced search";
	$pageTitlest_negosiasi["English"]["masterlist"] = "Negosiasi {%id_lahan}, {%nama_lahan}";
	$pageTitlest_negosiasi["English"]["masterprint"] = "Negosiasi {%id_lahan}, {%nama_lahan}";
	$pageTitlest_negosiasi["English"]["edit"] = "Negosiasi, Edit [{%id_lahan}, {%nama_lahan}]";
	if (count($fieldToolTipst_negosiasi["English"]))
		$tdatat_negosiasi[".isUseToolTips"] = true;
}


	$tdatat_negosiasi[".NCSearch"] = true;



$tdatat_negosiasi[".shortTableName"] = "t_negosiasi";
$tdatat_negosiasi[".nSecOptions"] = 0;

$tdatat_negosiasi[".mainTableOwnerID"] = "";
$tdatat_negosiasi[".entityType"] = 0;
$tdatat_negosiasi[".connId"] = "db_lla_at_localhost";


$tdatat_negosiasi[".strOriginalTableName"] = "t_negosiasi";

		 



$tdatat_negosiasi[".showAddInPopup"] = false;

$tdatat_negosiasi[".showEditInPopup"] = false;

$tdatat_negosiasi[".showViewInPopup"] = false;

$tdatat_negosiasi[".listAjax"] = false;
//	temporary
//$tdatat_negosiasi[".listAjax"] = false;

	$tdatat_negosiasi[".audit"] = false;

	$tdatat_negosiasi[".locking"] = false;


$pages = $tdatat_negosiasi[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_negosiasi[".edit"] = true;
	$tdatat_negosiasi[".afterEditAction"] = 1;
	$tdatat_negosiasi[".closePopupAfterEdit"] = 1;
	$tdatat_negosiasi[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_negosiasi[".add"] = true;
$tdatat_negosiasi[".afterAddAction"] = 3;
$tdatat_negosiasi[".closePopupAfterAdd"] = 1;
$tdatat_negosiasi[".afterAddActionDetTable"] = "t_nego_detail";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_negosiasi[".list"] = true;
}



$tdatat_negosiasi[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_negosiasi[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_negosiasi[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_negosiasi[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_negosiasi[".printFriendly"] = true;
}



$tdatat_negosiasi[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_negosiasi[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_negosiasi[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_negosiasi[".isUseAjaxSuggest"] = true;

$tdatat_negosiasi[".rowHighlite"] = true;



			

$tdatat_negosiasi[".ajaxCodeSnippetAdded"] = false;

$tdatat_negosiasi[".buttonsAdded"] = false;

$tdatat_negosiasi[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_negosiasi[".isUseTimeForSearch"] = false;


$tdatat_negosiasi[".badgeColor"] = "CD853F";


$tdatat_negosiasi[".allSearchFields"] = array();
$tdatat_negosiasi[".filterFields"] = array();
$tdatat_negosiasi[".requiredSearchFields"] = array();

$tdatat_negosiasi[".googleLikeFields"] = array();
$tdatat_negosiasi[".googleLikeFields"][] = "id";
$tdatat_negosiasi[".googleLikeFields"][] = "id_lahan";
$tdatat_negosiasi[".googleLikeFields"][] = "nama_lahan";
$tdatat_negosiasi[".googleLikeFields"][] = "start_date";
$tdatat_negosiasi[".googleLikeFields"][] = "due_date";
$tdatat_negosiasi[".googleLikeFields"][] = "pic";
$tdatat_negosiasi[".googleLikeFields"][] = "keterangan";
$tdatat_negosiasi[".googleLikeFields"][] = "status";
$tdatat_negosiasi[".googleLikeFields"][] = "close_by";
$tdatat_negosiasi[".googleLikeFields"][] = "close_date";
$tdatat_negosiasi[".googleLikeFields"][] = "created_date";
$tdatat_negosiasi[".googleLikeFields"][] = "created_by";
$tdatat_negosiasi[".googleLikeFields"][] = "updated_date";
$tdatat_negosiasi[".googleLikeFields"][] = "updated_by";



$tdatat_negosiasi[".tableType"] = "list";

$tdatat_negosiasi[".printerPageOrientation"] = 0;
$tdatat_negosiasi[".nPrinterPageScale"] = 100;

$tdatat_negosiasi[".nPrinterSplitRecords"] = 40;

$tdatat_negosiasi[".geocodingEnabled"] = false;










$tdatat_negosiasi[".pageSize"] = 20;

$tdatat_negosiasi[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_negosiasi[".strOrderBy"] = $tstrOrderBy;

$tdatat_negosiasi[".orderindexes"] = array();


$tdatat_negosiasi[".sqlHead"] = "SELECT t_negosiasi.id,  t_negosiasi.id_lahan,  t_input_awal.nama_lahan,  t_negosiasi.start_date,  t_negosiasi.due_date,  t_negosiasi.pic,  t_negosiasi.keterangan,  t_negosiasi.status,  t_negosiasi.close_by,  t_negosiasi.close_date,  t_negosiasi.created_date,  t_negosiasi.created_by,  t_negosiasi.updated_date,  t_negosiasi.updated_by";
$tdatat_negosiasi[".sqlFrom"] = "FROM t_negosiasi  INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan";
$tdatat_negosiasi[".sqlWhereExpr"] = "";
$tdatat_negosiasi[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_negosiasi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_negosiasi[".arrGroupsPerPage"] = $arrGPP;

$tdatat_negosiasi[".highlightSearchResults"] = true;

$tableKeyst_negosiasi = array();
$tableKeyst_negosiasi[] = "id";
$tdatat_negosiasi[".Keys"] = $tableKeyst_negosiasi;


$tdatat_negosiasi[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["id"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_pengukuran.status = 'CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_pengukuran.status = 'CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["id_lahan"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "id_lahan";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["nama_lahan"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "nama_lahan";
//	start_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "start_date";
	$fdata["GoodName"] = "start_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","start_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date";

		$fdata["sourceSingle"] = "start_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.start_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["start_date"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "start_date";
//	due_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "due_date";
	$fdata["GoodName"] = "due_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","due_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date";

		$fdata["sourceSingle"] = "due_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.due_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["due_date"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "due_date";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["pic"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "pic";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["keterangan"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "keterangan";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["status"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "status";
//	close_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "close_by";
	$fdata["GoodName"] = "close_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","close_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_by";

		$fdata["sourceSingle"] = "close_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.close_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["close_by"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "close_by";
//	close_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "close_date";
	$fdata["GoodName"] = "close_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","close_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_date";

		$fdata["sourceSingle"] = "close_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.close_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["close_date"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "close_date";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["created_date"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["created_by"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["updated_date"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_negosiasi";
	$fdata["Label"] = GetFieldLabel("t_negosiasi","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_negosiasi.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_negosiasi["updated_by"] = $fdata;
		$tdatat_negosiasi[".searchableFields"][] = "updated_by";


$tables_data["t_negosiasi"]=&$tdatat_negosiasi;
$field_labels["t_negosiasi"] = &$fieldLabelst_negosiasi;
$fieldToolTips["t_negosiasi"] = &$fieldToolTipst_negosiasi;
$placeHolders["t_negosiasi"] = &$placeHolderst_negosiasi;
$page_titles["t_negosiasi"] = &$pageTitlest_negosiasi;


changeTextControlsToDate( "t_negosiasi" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_negosiasi"] = array();
//	t_nego_detail
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_nego_detail";
		$detailsParam["dOriginalTable"] = "t_nego_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_nego_detail";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_nego_detail");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_negosiasi"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_negosiasi"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_negosiasi"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["t_negosiasi"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_negosiasi"][$dIndex]["detailKeys"][]="id_nego";
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_negosiasi"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_negosiasi()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_negosiasi.id,  t_negosiasi.id_lahan,  t_input_awal.nama_lahan,  t_negosiasi.start_date,  t_negosiasi.due_date,  t_negosiasi.pic,  t_negosiasi.keterangan,  t_negosiasi.status,  t_negosiasi.close_by,  t_negosiasi.close_date,  t_negosiasi.created_date,  t_negosiasi.created_by,  t_negosiasi.updated_date,  t_negosiasi.updated_by";
$proto0["m_strFrom"] = "FROM t_negosiasi  INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto6["m_sql"] = "t_negosiasi.id";
$proto6["m_srcTableName"] = "t_negosiasi";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto8["m_sql"] = "t_negosiasi.id_lahan";
$proto8["m_srcTableName"] = "t_negosiasi";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_negosiasi"
));

$proto10["m_sql"] = "t_input_awal.nama_lahan";
$proto10["m_srcTableName"] = "t_negosiasi";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto12["m_sql"] = "t_negosiasi.start_date";
$proto12["m_srcTableName"] = "t_negosiasi";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto14["m_sql"] = "t_negosiasi.due_date";
$proto14["m_srcTableName"] = "t_negosiasi";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto16["m_sql"] = "t_negosiasi.pic";
$proto16["m_srcTableName"] = "t_negosiasi";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto18["m_sql"] = "t_negosiasi.keterangan";
$proto18["m_srcTableName"] = "t_negosiasi";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto20["m_sql"] = "t_negosiasi.status";
$proto20["m_srcTableName"] = "t_negosiasi";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "close_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto22["m_sql"] = "t_negosiasi.close_by";
$proto22["m_srcTableName"] = "t_negosiasi";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "close_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto24["m_sql"] = "t_negosiasi.close_date";
$proto24["m_srcTableName"] = "t_negosiasi";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto26["m_sql"] = "t_negosiasi.created_date";
$proto26["m_srcTableName"] = "t_negosiasi";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto28["m_sql"] = "t_negosiasi.created_by";
$proto28["m_srcTableName"] = "t_negosiasi";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto30["m_sql"] = "t_negosiasi.updated_date";
$proto30["m_srcTableName"] = "t_negosiasi";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_negosiasi",
	"m_srcTableName" => "t_negosiasi"
));

$proto32["m_sql"] = "t_negosiasi.updated_by";
$proto32["m_srcTableName"] = "t_negosiasi";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "t_negosiasi";
$proto35["m_srcTableName"] = "t_negosiasi";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "id";
$proto35["m_columns"][] = "id_lahan";
$proto35["m_columns"][] = "start_date";
$proto35["m_columns"][] = "due_date";
$proto35["m_columns"][] = "pic";
$proto35["m_columns"][] = "keterangan";
$proto35["m_columns"][] = "status";
$proto35["m_columns"][] = "close_by";
$proto35["m_columns"][] = "close_date";
$proto35["m_columns"][] = "created_date";
$proto35["m_columns"][] = "created_by";
$proto35["m_columns"][] = "updated_date";
$proto35["m_columns"][] = "updated_by";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "t_negosiasi";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "t_negosiasi";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
												$proto38=array();
$proto38["m_link"] = "SQLL_INNERJOIN";
			$proto39=array();
$proto39["m_strName"] = "t_input_awal";
$proto39["m_srcTableName"] = "t_negosiasi";
$proto39["m_columns"] = array();
$proto39["m_columns"][] = "id";
$proto39["m_columns"][] = "id_lahan";
$proto39["m_columns"][] = "nama_lahan";
$proto39["m_columns"][] = "batas_utara";
$proto39["m_columns"][] = "batas_selatan";
$proto39["m_columns"][] = "batas_barat";
$proto39["m_columns"][] = "batas_timur";
$proto39["m_columns"][] = "blok_lokasi";
$proto39["m_columns"][] = "nama_lokasi";
$proto39["m_columns"][] = "keperluan_lahan";
$proto39["m_columns"][] = "start_date";
$proto39["m_columns"][] = "due_date";
$proto39["m_columns"][] = "pic";
$proto39["m_columns"][] = "status";
$proto39["m_columns"][] = "close_by";
$proto39["m_columns"][] = "close_date";
$proto39["m_columns"][] = "keterangan";
$proto39["m_columns"][] = "im_file";
$proto39["m_columns"][] = "created_date";
$proto39["m_columns"][] = "created_by";
$proto39["m_columns"][] = "updated_date";
$proto39["m_columns"][] = "updated_by";
$obj = new SQLTable($proto39);

$proto38["m_table"] = $obj;
$proto38["m_sql"] = "INNER JOIN t_input_awal ON t_negosiasi.id_lahan = t_input_awal.id_lahan";
$proto38["m_alias"] = "";
$proto38["m_srcTableName"] = "t_negosiasi";
$proto40=array();
$proto40["m_sql"] = "t_input_awal.id_lahan = t_negosiasi.id_lahan";
$proto40["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_negosiasi"
));

$proto40["m_column"]=$obj;
$proto40["m_contained"] = array();
$proto40["m_strCase"] = "= t_negosiasi.id_lahan";
$proto40["m_havingmode"] = false;
$proto40["m_inBrackets"] = false;
$proto40["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto40);

$proto38["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto38);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_negosiasi";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_negosiasi = createSqlQuery_t_negosiasi();


	
					
;

														

$tdatat_negosiasi[".sqlquery"] = $queryData_t_negosiasi;



include_once(getabspath("include/t_negosiasi_events.php"));
$tdatat_negosiasi[".hasEvents"] = true;

?>