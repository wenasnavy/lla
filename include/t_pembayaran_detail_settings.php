<?php
$tdatat_pembayaran_detail = array();
$tdatat_pembayaran_detail[".searchableFields"] = array();
$tdatat_pembayaran_detail[".ShortName"] = "t_pembayaran_detail";
$tdatat_pembayaran_detail[".OwnerID"] = "";
$tdatat_pembayaran_detail[".OriginalTable"] = "t_pembayaran_detail";


$tdatat_pembayaran_detail[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_pembayaran_detail[".originalPagesByType"] = $tdatat_pembayaran_detail[".pagesByType"];
$tdatat_pembayaran_detail[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_pembayaran_detail[".originalPages"] = $tdatat_pembayaran_detail[".pages"];
$tdatat_pembayaran_detail[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_pembayaran_detail[".originalDefaultPages"] = $tdatat_pembayaran_detail[".defaultPages"];

//	field labels
$fieldLabelst_pembayaran_detail = array();
$fieldToolTipst_pembayaran_detail = array();
$pageTitlest_pembayaran_detail = array();
$placeHolderst_pembayaran_detail = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_pembayaran_detail["English"] = array();
	$fieldToolTipst_pembayaran_detail["English"] = array();
	$placeHolderst_pembayaran_detail["English"] = array();
	$pageTitlest_pembayaran_detail["English"] = array();
	$fieldLabelst_pembayaran_detail["English"]["id"] = "Id";
	$fieldToolTipst_pembayaran_detail["English"]["id"] = "";
	$placeHolderst_pembayaran_detail["English"]["id"] = "";
	$fieldLabelst_pembayaran_detail["English"]["id_p_dana"] = "Id P Dana";
	$fieldToolTipst_pembayaran_detail["English"]["id_p_dana"] = "";
	$placeHolderst_pembayaran_detail["English"]["id_p_dana"] = "";
	$fieldLabelst_pembayaran_detail["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_pembayaran_detail["English"]["param_dok"] = "";
	$placeHolderst_pembayaran_detail["English"]["param_dok"] = "";
	$fieldLabelst_pembayaran_detail["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_pembayaran_detail["English"]["file_dok"] = "";
	$placeHolderst_pembayaran_detail["English"]["file_dok"] = "";
	$fieldLabelst_pembayaran_detail["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_pembayaran_detail["English"]["keterangan"] = "";
	$placeHolderst_pembayaran_detail["English"]["keterangan"] = "";
	$fieldLabelst_pembayaran_detail["English"]["created_date"] = "Created Date";
	$fieldToolTipst_pembayaran_detail["English"]["created_date"] = "";
	$placeHolderst_pembayaran_detail["English"]["created_date"] = "";
	$fieldLabelst_pembayaran_detail["English"]["created_by"] = "Created By";
	$fieldToolTipst_pembayaran_detail["English"]["created_by"] = "";
	$placeHolderst_pembayaran_detail["English"]["created_by"] = "";
	$fieldLabelst_pembayaran_detail["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_pembayaran_detail["English"]["updated_date"] = "";
	$placeHolderst_pembayaran_detail["English"]["updated_date"] = "";
	$fieldLabelst_pembayaran_detail["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_pembayaran_detail["English"]["updated_by"] = "";
	$placeHolderst_pembayaran_detail["English"]["updated_by"] = "";
	if (count($fieldToolTipst_pembayaran_detail["English"]))
		$tdatat_pembayaran_detail[".isUseToolTips"] = true;
}


	$tdatat_pembayaran_detail[".NCSearch"] = true;



$tdatat_pembayaran_detail[".shortTableName"] = "t_pembayaran_detail";
$tdatat_pembayaran_detail[".nSecOptions"] = 0;

$tdatat_pembayaran_detail[".mainTableOwnerID"] = "";
$tdatat_pembayaran_detail[".entityType"] = 0;
$tdatat_pembayaran_detail[".connId"] = "db_lla_at_localhost";


$tdatat_pembayaran_detail[".strOriginalTableName"] = "t_pembayaran_detail";

		 



$tdatat_pembayaran_detail[".showAddInPopup"] = false;

$tdatat_pembayaran_detail[".showEditInPopup"] = false;

$tdatat_pembayaran_detail[".showViewInPopup"] = false;

$tdatat_pembayaran_detail[".listAjax"] = false;
//	temporary
//$tdatat_pembayaran_detail[".listAjax"] = false;

	$tdatat_pembayaran_detail[".audit"] = false;

	$tdatat_pembayaran_detail[".locking"] = false;


$pages = $tdatat_pembayaran_detail[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_pembayaran_detail[".edit"] = true;
	$tdatat_pembayaran_detail[".afterEditAction"] = 1;
	$tdatat_pembayaran_detail[".closePopupAfterEdit"] = 1;
	$tdatat_pembayaran_detail[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_pembayaran_detail[".add"] = true;
$tdatat_pembayaran_detail[".afterAddAction"] = 1;
$tdatat_pembayaran_detail[".closePopupAfterAdd"] = 1;
$tdatat_pembayaran_detail[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_pembayaran_detail[".list"] = true;
}



$tdatat_pembayaran_detail[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_pembayaran_detail[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_pembayaran_detail[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_pembayaran_detail[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_pembayaran_detail[".printFriendly"] = true;
}



$tdatat_pembayaran_detail[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_pembayaran_detail[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_pembayaran_detail[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_pembayaran_detail[".isUseAjaxSuggest"] = true;

$tdatat_pembayaran_detail[".rowHighlite"] = true;



						

$tdatat_pembayaran_detail[".ajaxCodeSnippetAdded"] = false;

$tdatat_pembayaran_detail[".buttonsAdded"] = false;

$tdatat_pembayaran_detail[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_pembayaran_detail[".isUseTimeForSearch"] = false;


$tdatat_pembayaran_detail[".badgeColor"] = "edca00";


$tdatat_pembayaran_detail[".allSearchFields"] = array();
$tdatat_pembayaran_detail[".filterFields"] = array();
$tdatat_pembayaran_detail[".requiredSearchFields"] = array();

$tdatat_pembayaran_detail[".googleLikeFields"] = array();
$tdatat_pembayaran_detail[".googleLikeFields"][] = "id";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "id_p_dana";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "param_dok";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "file_dok";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "keterangan";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "created_date";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "created_by";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "updated_date";
$tdatat_pembayaran_detail[".googleLikeFields"][] = "updated_by";



$tdatat_pembayaran_detail[".tableType"] = "list";

$tdatat_pembayaran_detail[".printerPageOrientation"] = 0;
$tdatat_pembayaran_detail[".nPrinterPageScale"] = 100;

$tdatat_pembayaran_detail[".nPrinterSplitRecords"] = 40;

$tdatat_pembayaran_detail[".geocodingEnabled"] = false;










$tdatat_pembayaran_detail[".pageSize"] = 20;

$tdatat_pembayaran_detail[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_pembayaran_detail[".strOrderBy"] = $tstrOrderBy;

$tdatat_pembayaran_detail[".orderindexes"] = array();


$tdatat_pembayaran_detail[".sqlHead"] = "SELECT id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_pembayaran_detail[".sqlFrom"] = "FROM t_pembayaran_detail";
$tdatat_pembayaran_detail[".sqlWhereExpr"] = "";
$tdatat_pembayaran_detail[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_pembayaran_detail[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_pembayaran_detail[".arrGroupsPerPage"] = $arrGPP;

$tdatat_pembayaran_detail[".highlightSearchResults"] = true;

$tableKeyst_pembayaran_detail = array();
$tableKeyst_pembayaran_detail[] = "id";
$tdatat_pembayaran_detail[".Keys"] = $tableKeyst_pembayaran_detail;


$tdatat_pembayaran_detail[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["id"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "id";
//	id_p_dana
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_p_dana";
	$fdata["GoodName"] = "id_p_dana";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","id_p_dana");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_p_dana";

		$fdata["sourceSingle"] = "id_p_dana";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_p_dana";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["id_p_dana"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "id_p_dana";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["param_dok"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["file_dok"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["keterangan"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["created_date"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["created_by"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["updated_date"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_pembayaran_detail";
	$fdata["Label"] = GetFieldLabel("t_pembayaran_detail","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran_detail["updated_by"] = $fdata;
		$tdatat_pembayaran_detail[".searchableFields"][] = "updated_by";


$tables_data["t_pembayaran_detail"]=&$tdatat_pembayaran_detail;
$field_labels["t_pembayaran_detail"] = &$fieldLabelst_pembayaran_detail;
$fieldToolTips["t_pembayaran_detail"] = &$fieldToolTipst_pembayaran_detail;
$placeHolders["t_pembayaran_detail"] = &$placeHolderst_pembayaran_detail;
$page_titles["t_pembayaran_detail"] = &$pageTitlest_pembayaran_detail;


changeTextControlsToDate( "t_pembayaran_detail" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_pembayaran_detail"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_pembayaran_detail"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_perm_dana";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_perm_dana_close";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_perm_dana_close";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_pembayaran_detail"][0] = $masterParams;
				$masterTablesData["t_pembayaran_detail"][0]["masterKeys"] = array();
	$masterTablesData["t_pembayaran_detail"][0]["masterKeys"][]="id";
				$masterTablesData["t_pembayaran_detail"][0]["detailKeys"] = array();
	$masterTablesData["t_pembayaran_detail"][0]["detailKeys"][]="id_p_dana";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_pembayaran_detail()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_pembayaran_detail";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_pembayaran_detail";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_p_dana",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto8["m_sql"] = "id_p_dana";
$proto8["m_srcTableName"] = "t_pembayaran_detail";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto10["m_sql"] = "param_dok";
$proto10["m_srcTableName"] = "t_pembayaran_detail";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto12["m_sql"] = "file_dok";
$proto12["m_srcTableName"] = "t_pembayaran_detail";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto14["m_sql"] = "keterangan";
$proto14["m_srcTableName"] = "t_pembayaran_detail";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto16["m_sql"] = "created_date";
$proto16["m_srcTableName"] = "t_pembayaran_detail";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto18["m_sql"] = "created_by";
$proto18["m_srcTableName"] = "t_pembayaran_detail";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto20["m_sql"] = "updated_date";
$proto20["m_srcTableName"] = "t_pembayaran_detail";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_pembayaran_detail",
	"m_srcTableName" => "t_pembayaran_detail"
));

$proto22["m_sql"] = "updated_by";
$proto22["m_srcTableName"] = "t_pembayaran_detail";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "t_pembayaran_detail";
$proto25["m_srcTableName"] = "t_pembayaran_detail";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "id";
$proto25["m_columns"][] = "id_p_dana";
$proto25["m_columns"][] = "param_dok";
$proto25["m_columns"][] = "file_dok";
$proto25["m_columns"][] = "keterangan";
$proto25["m_columns"][] = "created_date";
$proto25["m_columns"][] = "created_by";
$proto25["m_columns"][] = "updated_date";
$proto25["m_columns"][] = "updated_by";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "t_pembayaran_detail";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "t_pembayaran_detail";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_pembayaran_detail";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_pembayaran_detail = createSqlQuery_t_pembayaran_detail();


	
					
;

									

$tdatat_pembayaran_detail[".sqlquery"] = $queryData_t_pembayaran_detail;



include_once(getabspath("include/t_pembayaran_detail_events.php"));
$tdatat_pembayaran_detail[".hasEvents"] = true;

?>