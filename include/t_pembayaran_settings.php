<?php
$tdatat_pembayaran = array();
$tdatat_pembayaran[".searchableFields"] = array();
$tdatat_pembayaran[".ShortName"] = "t_pembayaran";
$tdatat_pembayaran[".OwnerID"] = "";
$tdatat_pembayaran[".OriginalTable"] = "t_pembayaran";


$tdatat_pembayaran[".pagesByType"] = my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"]}" );
$tdatat_pembayaran[".originalPagesByType"] = $tdatat_pembayaran[".pagesByType"];
$tdatat_pembayaran[".pages"] = types2pages( my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"]}" ) );
$tdatat_pembayaran[".originalPages"] = $tdatat_pembayaran[".pages"];
$tdatat_pembayaran[".defaultPages"] = my_json_decode( "{\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\"}" );
$tdatat_pembayaran[".originalDefaultPages"] = $tdatat_pembayaran[".defaultPages"];

//	field labels
$fieldLabelst_pembayaran = array();
$fieldToolTipst_pembayaran = array();
$pageTitlest_pembayaran = array();
$placeHolderst_pembayaran = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_pembayaran["English"] = array();
	$fieldToolTipst_pembayaran["English"] = array();
	$placeHolderst_pembayaran["English"] = array();
	$pageTitlest_pembayaran["English"] = array();
	$fieldLabelst_pembayaran["English"]["id"] = "Id";
	$fieldToolTipst_pembayaran["English"]["id"] = "";
	$placeHolderst_pembayaran["English"]["id"] = "";
	$fieldLabelst_pembayaran["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_pembayaran["English"]["id_lahan"] = "";
	$placeHolderst_pembayaran["English"]["id_lahan"] = "";
	$fieldLabelst_pembayaran["English"]["pic"] = "PIC";
	$fieldToolTipst_pembayaran["English"]["pic"] = "";
	$placeHolderst_pembayaran["English"]["pic"] = "";
	$fieldLabelst_pembayaran["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_pembayaran["English"]["keterangan"] = "";
	$placeHolderst_pembayaran["English"]["keterangan"] = "";
	$fieldLabelst_pembayaran["English"]["status"] = "Status";
	$fieldToolTipst_pembayaran["English"]["status"] = "";
	$placeHolderst_pembayaran["English"]["status"] = "";
	$fieldLabelst_pembayaran["English"]["created_date"] = "Created Date";
	$fieldToolTipst_pembayaran["English"]["created_date"] = "";
	$placeHolderst_pembayaran["English"]["created_date"] = "";
	$fieldLabelst_pembayaran["English"]["created_by"] = "Created By";
	$fieldToolTipst_pembayaran["English"]["created_by"] = "";
	$placeHolderst_pembayaran["English"]["created_by"] = "";
	$fieldLabelst_pembayaran["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_pembayaran["English"]["updated_date"] = "";
	$placeHolderst_pembayaran["English"]["updated_date"] = "";
	$fieldLabelst_pembayaran["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_pembayaran["English"]["updated_by"] = "";
	$placeHolderst_pembayaran["English"]["updated_by"] = "";
	$fieldLabelst_pembayaran["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_pembayaran["English"]["nama_lahan"] = "";
	$placeHolderst_pembayaran["English"]["nama_lahan"] = "";
	$fieldLabelst_pembayaran["English"]["tipe"] = "Tipe";
	$fieldToolTipst_pembayaran["English"]["tipe"] = "";
	$placeHolderst_pembayaran["English"]["tipe"] = "";
	$fieldLabelst_pembayaran["English"]["dp"] = "Dp";
	$fieldToolTipst_pembayaran["English"]["dp"] = "";
	$placeHolderst_pembayaran["English"]["dp"] = "";
	$fieldLabelst_pembayaran["English"]["sisa_pembayaran"] = "Sisa Pembayaran";
	$fieldToolTipst_pembayaran["English"]["sisa_pembayaran"] = "";
	$placeHolderst_pembayaran["English"]["sisa_pembayaran"] = "";
	$fieldLabelst_pembayaran["English"]["biaya_admin"] = "Biaya Admin";
	$fieldToolTipst_pembayaran["English"]["biaya_admin"] = "";
	$placeHolderst_pembayaran["English"]["biaya_admin"] = "";
	$fieldLabelst_pembayaran["English"]["start_date_dp"] = "Start Date Dp";
	$fieldToolTipst_pembayaran["English"]["start_date_dp"] = "";
	$placeHolderst_pembayaran["English"]["start_date_dp"] = "";
	$fieldLabelst_pembayaran["English"]["due_date_dp"] = "Due Date Dp";
	$fieldToolTipst_pembayaran["English"]["due_date_dp"] = "";
	$placeHolderst_pembayaran["English"]["due_date_dp"] = "";
	$fieldLabelst_pembayaran["English"]["start_date_full"] = "Start Date Full";
	$fieldToolTipst_pembayaran["English"]["start_date_full"] = "";
	$placeHolderst_pembayaran["English"]["start_date_full"] = "";
	$fieldLabelst_pembayaran["English"]["due_date_full"] = "Due Date Full";
	$fieldToolTipst_pembayaran["English"]["due_date_full"] = "";
	$placeHolderst_pembayaran["English"]["due_date_full"] = "";
	$fieldLabelst_pembayaran["English"]["close_full_by"] = "Close Full By";
	$fieldToolTipst_pembayaran["English"]["close_full_by"] = "";
	$placeHolderst_pembayaran["English"]["close_full_by"] = "";
	$fieldLabelst_pembayaran["English"]["close_full_date"] = "Close Full Date";
	$fieldToolTipst_pembayaran["English"]["close_full_date"] = "";
	$placeHolderst_pembayaran["English"]["close_full_date"] = "";
	$fieldLabelst_pembayaran["English"]["close_dp_by"] = "Close Dp By";
	$fieldToolTipst_pembayaran["English"]["close_dp_by"] = "";
	$placeHolderst_pembayaran["English"]["close_dp_by"] = "";
	$fieldLabelst_pembayaran["English"]["close_dp_date"] = "Close Dp Date";
	$fieldToolTipst_pembayaran["English"]["close_dp_date"] = "";
	$placeHolderst_pembayaran["English"]["close_dp_date"] = "";
	if (count($fieldToolTipst_pembayaran["English"]))
		$tdatat_pembayaran[".isUseToolTips"] = true;
}


	$tdatat_pembayaran[".NCSearch"] = true;



$tdatat_pembayaran[".shortTableName"] = "t_pembayaran";
$tdatat_pembayaran[".nSecOptions"] = 0;

$tdatat_pembayaran[".mainTableOwnerID"] = "";
$tdatat_pembayaran[".entityType"] = 0;
$tdatat_pembayaran[".connId"] = "db_lla_at_localhost";


$tdatat_pembayaran[".strOriginalTableName"] = "t_pembayaran";

		 



$tdatat_pembayaran[".showAddInPopup"] = false;

$tdatat_pembayaran[".showEditInPopup"] = false;

$tdatat_pembayaran[".showViewInPopup"] = false;

$tdatat_pembayaran[".listAjax"] = false;
//	temporary
//$tdatat_pembayaran[".listAjax"] = false;

	$tdatat_pembayaran[".audit"] = false;

	$tdatat_pembayaran[".locking"] = false;


$pages = $tdatat_pembayaran[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_pembayaran[".edit"] = true;
	$tdatat_pembayaran[".afterEditAction"] = 1;
	$tdatat_pembayaran[".closePopupAfterEdit"] = 1;
	$tdatat_pembayaran[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_pembayaran[".add"] = true;
$tdatat_pembayaran[".afterAddAction"] = 1;
$tdatat_pembayaran[".closePopupAfterAdd"] = 1;
$tdatat_pembayaran[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_pembayaran[".list"] = true;
}



$tdatat_pembayaran[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_pembayaran[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_pembayaran[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_pembayaran[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_pembayaran[".printFriendly"] = true;
}



$tdatat_pembayaran[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_pembayaran[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_pembayaran[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_pembayaran[".isUseAjaxSuggest"] = true;

$tdatat_pembayaran[".rowHighlite"] = true;



			

$tdatat_pembayaran[".ajaxCodeSnippetAdded"] = false;

$tdatat_pembayaran[".buttonsAdded"] = false;

$tdatat_pembayaran[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_pembayaran[".isUseTimeForSearch"] = false;


$tdatat_pembayaran[".badgeColor"] = "E67349";


$tdatat_pembayaran[".allSearchFields"] = array();
$tdatat_pembayaran[".filterFields"] = array();
$tdatat_pembayaran[".requiredSearchFields"] = array();

$tdatat_pembayaran[".googleLikeFields"] = array();
$tdatat_pembayaran[".googleLikeFields"][] = "id";
$tdatat_pembayaran[".googleLikeFields"][] = "id_lahan";
$tdatat_pembayaran[".googleLikeFields"][] = "tipe";
$tdatat_pembayaran[".googleLikeFields"][] = "dp";
$tdatat_pembayaran[".googleLikeFields"][] = "sisa_pembayaran";
$tdatat_pembayaran[".googleLikeFields"][] = "biaya_admin";
$tdatat_pembayaran[".googleLikeFields"][] = "start_date_dp";
$tdatat_pembayaran[".googleLikeFields"][] = "due_date_dp";
$tdatat_pembayaran[".googleLikeFields"][] = "start_date_full";
$tdatat_pembayaran[".googleLikeFields"][] = "due_date_full";
$tdatat_pembayaran[".googleLikeFields"][] = "pic";
$tdatat_pembayaran[".googleLikeFields"][] = "keterangan";
$tdatat_pembayaran[".googleLikeFields"][] = "close_full_by";
$tdatat_pembayaran[".googleLikeFields"][] = "close_full_date";
$tdatat_pembayaran[".googleLikeFields"][] = "status";
$tdatat_pembayaran[".googleLikeFields"][] = "close_dp_by";
$tdatat_pembayaran[".googleLikeFields"][] = "close_dp_date";
$tdatat_pembayaran[".googleLikeFields"][] = "created_date";
$tdatat_pembayaran[".googleLikeFields"][] = "created_by";
$tdatat_pembayaran[".googleLikeFields"][] = "updated_date";
$tdatat_pembayaran[".googleLikeFields"][] = "updated_by";
$tdatat_pembayaran[".googleLikeFields"][] = "nama_lahan";



$tdatat_pembayaran[".tableType"] = "list";

$tdatat_pembayaran[".printerPageOrientation"] = 0;
$tdatat_pembayaran[".nPrinterPageScale"] = 100;

$tdatat_pembayaran[".nPrinterSplitRecords"] = 40;

$tdatat_pembayaran[".geocodingEnabled"] = false;










$tdatat_pembayaran[".pageSize"] = 20;

$tdatat_pembayaran[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_pembayaran[".strOrderBy"] = $tstrOrderBy;

$tdatat_pembayaran[".orderindexes"] = array();


$tdatat_pembayaran[".sqlHead"] = "SELECT t_perm_dana.id,  t_perm_dana.id_lahan,  t_perm_dana.tipe,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.biaya_admin,  t_perm_dana.start_date_dp,  t_perm_dana.due_date_dp,  t_perm_dana.start_date_full,  t_perm_dana.due_date_full,  t_perm_dana.pic,  t_perm_dana.keterangan,  t_perm_dana.close_full_by,  t_perm_dana.close_full_date,  t_perm_dana.status,  t_perm_dana.close_dp_by,  t_perm_dana.close_dp_date,  t_perm_dana.created_date,  t_perm_dana.created_by,  t_perm_dana.updated_date,  t_perm_dana.updated_by,  t_input_awal.nama_lahan";
$tdatat_pembayaran[".sqlFrom"] = "FROM t_perm_dana  INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$tdatat_pembayaran[".sqlWhereExpr"] = "";
$tdatat_pembayaran[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_pembayaran[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_pembayaran[".arrGroupsPerPage"] = $arrGPP;

$tdatat_pembayaran[".highlightSearchResults"] = true;

$tableKeyst_pembayaran = array();
$tdatat_pembayaran[".Keys"] = $tableKeyst_pembayaran;


$tdatat_pembayaran[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["id"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_perm_dana";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_lahan";

				$edata["LookupWhere"] = "t_perm_dana.status='CLOSED'";


	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["id_lahan"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "id_lahan";
//	tipe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "tipe";
	$fdata["GoodName"] = "tipe";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","tipe");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "tipe";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.tipe";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["tipe"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "tipe";
//	dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "dp";
	$fdata["GoodName"] = "dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","dp");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["dp"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "dp";
//	sisa_pembayaran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "sisa_pembayaran";
	$fdata["GoodName"] = "sisa_pembayaran";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","sisa_pembayaran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "sisa_pembayaran";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.sisa_pembayaran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["sisa_pembayaran"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "sisa_pembayaran";
//	biaya_admin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "biaya_admin";
	$fdata["GoodName"] = "biaya_admin";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","biaya_admin");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "biaya_admin";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.biaya_admin";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["biaya_admin"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "biaya_admin";
//	start_date_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "start_date_dp";
	$fdata["GoodName"] = "start_date_dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","start_date_dp");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date_dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.start_date_dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["start_date_dp"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "start_date_dp";
//	due_date_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "due_date_dp";
	$fdata["GoodName"] = "due_date_dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","due_date_dp");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date_dp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.due_date_dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["due_date_dp"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "due_date_dp";
//	start_date_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "start_date_full";
	$fdata["GoodName"] = "start_date_full";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","start_date_full");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date_full";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.start_date_full";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["start_date_full"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "start_date_full";
//	due_date_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "due_date_full";
	$fdata["GoodName"] = "due_date_full";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","due_date_full");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date_full";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.due_date_full";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["due_date_full"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "due_date_full";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["pic"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "pic";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["keterangan"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "keterangan";
//	close_full_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "close_full_by";
	$fdata["GoodName"] = "close_full_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","close_full_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_full_by";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_full_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["close_full_by"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "close_full_by";
//	close_full_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "close_full_date";
	$fdata["GoodName"] = "close_full_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","close_full_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_full_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_full_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["close_full_date"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "close_full_date";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["status"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "status";
//	close_dp_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "close_dp_by";
	$fdata["GoodName"] = "close_dp_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","close_dp_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_dp_by";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_dp_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["close_dp_by"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "close_dp_by";
//	close_dp_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "close_dp_date";
	$fdata["GoodName"] = "close_dp_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","close_dp_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_dp_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_dp_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["close_dp_date"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "close_dp_date";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["created_date"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["created_by"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["updated_date"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["updated_by"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "updated_by";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_pembayaran","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pembayaran["nama_lahan"] = $fdata;
		$tdatat_pembayaran[".searchableFields"][] = "nama_lahan";


$tables_data["t_pembayaran"]=&$tdatat_pembayaran;
$field_labels["t_pembayaran"] = &$fieldLabelst_pembayaran;
$fieldToolTips["t_pembayaran"] = &$fieldToolTipst_pembayaran;
$placeHolders["t_pembayaran"] = &$placeHolderst_pembayaran;
$page_titles["t_pembayaran"] = &$pageTitlest_pembayaran;


changeTextControlsToDate( "t_pembayaran" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_pembayaran"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_pembayaran"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_pembayaran()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_perm_dana.id,  t_perm_dana.id_lahan,  t_perm_dana.tipe,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.biaya_admin,  t_perm_dana.start_date_dp,  t_perm_dana.due_date_dp,  t_perm_dana.start_date_full,  t_perm_dana.due_date_full,  t_perm_dana.pic,  t_perm_dana.keterangan,  t_perm_dana.close_full_by,  t_perm_dana.close_full_date,  t_perm_dana.status,  t_perm_dana.close_dp_by,  t_perm_dana.close_dp_date,  t_perm_dana.created_date,  t_perm_dana.created_by,  t_perm_dana.updated_date,  t_perm_dana.updated_by,  t_input_awal.nama_lahan";
$proto0["m_strFrom"] = "FROM t_perm_dana  INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto6["m_sql"] = "t_perm_dana.id";
$proto6["m_srcTableName"] = "t_pembayaran";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto8["m_sql"] = "t_perm_dana.id_lahan";
$proto8["m_srcTableName"] = "t_pembayaran";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "tipe",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto10["m_sql"] = "t_perm_dana.tipe";
$proto10["m_srcTableName"] = "t_pembayaran";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto12["m_sql"] = "t_perm_dana.dp";
$proto12["m_srcTableName"] = "t_pembayaran";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "sisa_pembayaran",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto14["m_sql"] = "t_perm_dana.sisa_pembayaran";
$proto14["m_srcTableName"] = "t_pembayaran";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "biaya_admin",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto16["m_sql"] = "t_perm_dana.biaya_admin";
$proto16["m_srcTableName"] = "t_pembayaran";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date_dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto18["m_sql"] = "t_perm_dana.start_date_dp";
$proto18["m_srcTableName"] = "t_pembayaran";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date_dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto20["m_sql"] = "t_perm_dana.due_date_dp";
$proto20["m_srcTableName"] = "t_pembayaran";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date_full",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto22["m_sql"] = "t_perm_dana.start_date_full";
$proto22["m_srcTableName"] = "t_pembayaran";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date_full",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto24["m_sql"] = "t_perm_dana.due_date_full";
$proto24["m_srcTableName"] = "t_pembayaran";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto26["m_sql"] = "t_perm_dana.pic";
$proto26["m_srcTableName"] = "t_pembayaran";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto28["m_sql"] = "t_perm_dana.keterangan";
$proto28["m_srcTableName"] = "t_pembayaran";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "close_full_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto30["m_sql"] = "t_perm_dana.close_full_by";
$proto30["m_srcTableName"] = "t_pembayaran";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "close_full_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto32["m_sql"] = "t_perm_dana.close_full_date";
$proto32["m_srcTableName"] = "t_pembayaran";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto34["m_sql"] = "t_perm_dana.status";
$proto34["m_srcTableName"] = "t_pembayaran";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "close_dp_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto36["m_sql"] = "t_perm_dana.close_dp_by";
$proto36["m_srcTableName"] = "t_pembayaran";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "close_dp_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto38["m_sql"] = "t_perm_dana.close_dp_date";
$proto38["m_srcTableName"] = "t_pembayaran";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto40["m_sql"] = "t_perm_dana.created_date";
$proto40["m_srcTableName"] = "t_pembayaran";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto42["m_sql"] = "t_perm_dana.created_by";
$proto42["m_srcTableName"] = "t_pembayaran";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto44["m_sql"] = "t_perm_dana.updated_date";
$proto44["m_srcTableName"] = "t_pembayaran";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_pembayaran"
));

$proto46["m_sql"] = "t_perm_dana.updated_by";
$proto46["m_srcTableName"] = "t_pembayaran";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_pembayaran"
));

$proto48["m_sql"] = "t_input_awal.nama_lahan";
$proto48["m_srcTableName"] = "t_pembayaran";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto50=array();
$proto50["m_link"] = "SQLL_MAIN";
			$proto51=array();
$proto51["m_strName"] = "t_perm_dana";
$proto51["m_srcTableName"] = "t_pembayaran";
$proto51["m_columns"] = array();
$proto51["m_columns"][] = "id";
$proto51["m_columns"][] = "id_lahan";
$proto51["m_columns"][] = "start_date_dp";
$proto51["m_columns"][] = "due_date_dp";
$proto51["m_columns"][] = "start_date_full";
$proto51["m_columns"][] = "due_date_full";
$proto51["m_columns"][] = "pic";
$proto51["m_columns"][] = "keterangan";
$proto51["m_columns"][] = "keterangan_bayar";
$proto51["m_columns"][] = "close_full_by";
$proto51["m_columns"][] = "close_full_date";
$proto51["m_columns"][] = "tipe";
$proto51["m_columns"][] = "dp";
$proto51["m_columns"][] = "sisa_pembayaran";
$proto51["m_columns"][] = "biaya_admin";
$proto51["m_columns"][] = "status";
$proto51["m_columns"][] = "status_full";
$proto51["m_columns"][] = "status_badmin";
$proto51["m_columns"][] = "close_badmin_by";
$proto51["m_columns"][] = "close_badmin_date";
$proto51["m_columns"][] = "close_dp_by";
$proto51["m_columns"][] = "close_dp_date";
$proto51["m_columns"][] = "created_date";
$proto51["m_columns"][] = "created_by";
$proto51["m_columns"][] = "updated_date";
$proto51["m_columns"][] = "updated_by";
$obj = new SQLTable($proto51);

$proto50["m_table"] = $obj;
$proto50["m_sql"] = "t_perm_dana";
$proto50["m_alias"] = "";
$proto50["m_srcTableName"] = "t_pembayaran";
$proto52=array();
$proto52["m_sql"] = "";
$proto52["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto52["m_column"]=$obj;
$proto52["m_contained"] = array();
$proto52["m_strCase"] = "";
$proto52["m_havingmode"] = false;
$proto52["m_inBrackets"] = false;
$proto52["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto52);

$proto50["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto50);

$proto0["m_fromlist"][]=$obj;
												$proto54=array();
$proto54["m_link"] = "SQLL_INNERJOIN";
			$proto55=array();
$proto55["m_strName"] = "t_input_awal";
$proto55["m_srcTableName"] = "t_pembayaran";
$proto55["m_columns"] = array();
$proto55["m_columns"][] = "id";
$proto55["m_columns"][] = "id_lahan";
$proto55["m_columns"][] = "nama_lahan";
$proto55["m_columns"][] = "batas_utara";
$proto55["m_columns"][] = "batas_selatan";
$proto55["m_columns"][] = "batas_barat";
$proto55["m_columns"][] = "batas_timur";
$proto55["m_columns"][] = "blok_lokasi";
$proto55["m_columns"][] = "nama_lokasi";
$proto55["m_columns"][] = "keperluan_lahan";
$proto55["m_columns"][] = "start_date";
$proto55["m_columns"][] = "due_date";
$proto55["m_columns"][] = "pic";
$proto55["m_columns"][] = "status";
$proto55["m_columns"][] = "close_by";
$proto55["m_columns"][] = "close_date";
$proto55["m_columns"][] = "keterangan";
$proto55["m_columns"][] = "im_file";
$proto55["m_columns"][] = "created_date";
$proto55["m_columns"][] = "created_by";
$proto55["m_columns"][] = "updated_date";
$proto55["m_columns"][] = "updated_by";
$obj = new SQLTable($proto55);

$proto54["m_table"] = $obj;
$proto54["m_sql"] = "INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto54["m_alias"] = "";
$proto54["m_srcTableName"] = "t_pembayaran";
$proto56=array();
$proto56["m_sql"] = "t_input_awal.id_lahan = t_perm_dana.id_lahan";
$proto56["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_pembayaran"
));

$proto56["m_column"]=$obj;
$proto56["m_contained"] = array();
$proto56["m_strCase"] = "= t_perm_dana.id_lahan";
$proto56["m_havingmode"] = false;
$proto56["m_inBrackets"] = false;
$proto56["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto56);

$proto54["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto54);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_pembayaran";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_pembayaran = createSqlQuery_t_pembayaran();


	
					
;

																						

$tdatat_pembayaran[".sqlquery"] = $queryData_t_pembayaran;



include_once(getabspath("include/t_pembayaran_events.php"));
$tdatat_pembayaran[".hasEvents"] = true;

?>