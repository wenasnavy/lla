<?php
$tdatat_pengukuran_batas_view = array();
$tdatat_pengukuran_batas_view[".searchableFields"] = array();
$tdatat_pengukuran_batas_view[".ShortName"] = "t_pengukuran_batas_view";
$tdatat_pengukuran_batas_view[".OwnerID"] = "";
$tdatat_pengukuran_batas_view[".OriginalTable"] = "t_pengukuran_batas";


$tdatat_pengukuran_batas_view[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_pengukuran_batas_view[".originalPagesByType"] = $tdatat_pengukuran_batas_view[".pagesByType"];
$tdatat_pengukuran_batas_view[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_pengukuran_batas_view[".originalPages"] = $tdatat_pengukuran_batas_view[".pages"];
$tdatat_pengukuran_batas_view[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_pengukuran_batas_view[".originalDefaultPages"] = $tdatat_pengukuran_batas_view[".defaultPages"];

//	field labels
$fieldLabelst_pengukuran_batas_view = array();
$fieldToolTipst_pengukuran_batas_view = array();
$pageTitlest_pengukuran_batas_view = array();
$placeHolderst_pengukuran_batas_view = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_pengukuran_batas_view["English"] = array();
	$fieldToolTipst_pengukuran_batas_view["English"] = array();
	$placeHolderst_pengukuran_batas_view["English"] = array();
	$pageTitlest_pengukuran_batas_view["English"] = array();
	$fieldLabelst_pengukuran_batas_view["English"]["id"] = "Id";
	$fieldToolTipst_pengukuran_batas_view["English"]["id"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["id"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["id_ukur"] = "Id Ukur";
	$fieldToolTipst_pengukuran_batas_view["English"]["id_ukur"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["id_ukur"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_pengukuran_batas_view["English"]["id_lahan"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["id_lahan"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["jenis_batas"] = "Jenis Batas";
	$fieldToolTipst_pengukuran_batas_view["English"]["jenis_batas"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["jenis_batas"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["tipe"] = "Tipe";
	$fieldToolTipst_pengukuran_batas_view["English"]["tipe"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["tipe"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["batas"] = "ID Lahan";
	$fieldToolTipst_pengukuran_batas_view["English"]["batas"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["batas"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["batas_text"] = "Batas Text";
	$fieldToolTipst_pengukuran_batas_view["English"]["batas_text"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["batas_text"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["lat"] = "Lat";
	$fieldToolTipst_pengukuran_batas_view["English"]["lat"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["lat"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["lng"] = "Lng";
	$fieldToolTipst_pengukuran_batas_view["English"]["lng"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["lng"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_pengukuran_batas_view["English"]["keterangan"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["keterangan"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["created_date"] = "Created Date";
	$fieldToolTipst_pengukuran_batas_view["English"]["created_date"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["created_date"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["created_by"] = "Created By";
	$fieldToolTipst_pengukuran_batas_view["English"]["created_by"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["created_by"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_pengukuran_batas_view["English"]["updated_date"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["updated_date"] = "";
	$fieldLabelst_pengukuran_batas_view["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_pengukuran_batas_view["English"]["updated_by"] = "";
	$placeHolderst_pengukuran_batas_view["English"]["updated_by"] = "";
	if (count($fieldToolTipst_pengukuran_batas_view["English"]))
		$tdatat_pengukuran_batas_view[".isUseToolTips"] = true;
}


	$tdatat_pengukuran_batas_view[".NCSearch"] = true;



$tdatat_pengukuran_batas_view[".shortTableName"] = "t_pengukuran_batas_view";
$tdatat_pengukuran_batas_view[".nSecOptions"] = 0;

$tdatat_pengukuran_batas_view[".mainTableOwnerID"] = "";
$tdatat_pengukuran_batas_view[".entityType"] = 1;
$tdatat_pengukuran_batas_view[".connId"] = "db_lla_at_localhost";


$tdatat_pengukuran_batas_view[".strOriginalTableName"] = "t_pengukuran_batas";

		 



$tdatat_pengukuran_batas_view[".showAddInPopup"] = false;

$tdatat_pengukuran_batas_view[".showEditInPopup"] = false;

$tdatat_pengukuran_batas_view[".showViewInPopup"] = false;

$tdatat_pengukuran_batas_view[".listAjax"] = false;
//	temporary
//$tdatat_pengukuran_batas_view[".listAjax"] = false;

	$tdatat_pengukuran_batas_view[".audit"] = false;

	$tdatat_pengukuran_batas_view[".locking"] = false;


$pages = $tdatat_pengukuran_batas_view[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_pengukuran_batas_view[".edit"] = true;
	$tdatat_pengukuran_batas_view[".afterEditAction"] = 1;
	$tdatat_pengukuran_batas_view[".closePopupAfterEdit"] = 1;
	$tdatat_pengukuran_batas_view[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_pengukuran_batas_view[".add"] = true;
$tdatat_pengukuran_batas_view[".afterAddAction"] = 1;
$tdatat_pengukuran_batas_view[".closePopupAfterAdd"] = 1;
$tdatat_pengukuran_batas_view[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_pengukuran_batas_view[".list"] = true;
}



$tdatat_pengukuran_batas_view[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_pengukuran_batas_view[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_pengukuran_batas_view[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_pengukuran_batas_view[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_pengukuran_batas_view[".printFriendly"] = true;
}



$tdatat_pengukuran_batas_view[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_pengukuran_batas_view[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_pengukuran_batas_view[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_pengukuran_batas_view[".isUseAjaxSuggest"] = true;

$tdatat_pengukuran_batas_view[".rowHighlite"] = true;



						

$tdatat_pengukuran_batas_view[".ajaxCodeSnippetAdded"] = false;

$tdatat_pengukuran_batas_view[".buttonsAdded"] = false;

$tdatat_pengukuran_batas_view[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_pengukuran_batas_view[".isUseTimeForSearch"] = false;


$tdatat_pengukuran_batas_view[".badgeColor"] = "bc8f8f";


$tdatat_pengukuran_batas_view[".allSearchFields"] = array();
$tdatat_pengukuran_batas_view[".filterFields"] = array();
$tdatat_pengukuran_batas_view[".requiredSearchFields"] = array();

$tdatat_pengukuran_batas_view[".googleLikeFields"] = array();
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "id";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "id_ukur";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "id_lahan";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "jenis_batas";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "tipe";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "batas";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "batas_text";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "lat";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "lng";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "keterangan";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "created_date";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "created_by";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "updated_date";
$tdatat_pengukuran_batas_view[".googleLikeFields"][] = "updated_by";



$tdatat_pengukuran_batas_view[".tableType"] = "list";

$tdatat_pengukuran_batas_view[".printerPageOrientation"] = 0;
$tdatat_pengukuran_batas_view[".nPrinterPageScale"] = 100;

$tdatat_pengukuran_batas_view[".nPrinterSplitRecords"] = 40;

$tdatat_pengukuran_batas_view[".geocodingEnabled"] = false;










$tdatat_pengukuran_batas_view[".pageSize"] = 20;

$tdatat_pengukuran_batas_view[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_pengukuran_batas_view[".strOrderBy"] = $tstrOrderBy;

$tdatat_pengukuran_batas_view[".orderindexes"] = array();


$tdatat_pengukuran_batas_view[".sqlHead"] = "SELECT id,  	id_ukur,  	id_lahan,  	jenis_batas,  	tipe,  	batas,  	batas_text,  	lat,  	lng,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_pengukuran_batas_view[".sqlFrom"] = "FROM t_pengukuran_batas";
$tdatat_pengukuran_batas_view[".sqlWhereExpr"] = "";
$tdatat_pengukuran_batas_view[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_pengukuran_batas_view[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_pengukuran_batas_view[".arrGroupsPerPage"] = $arrGPP;

$tdatat_pengukuran_batas_view[".highlightSearchResults"] = true;

$tableKeyst_pengukuran_batas_view = array();
$tableKeyst_pengukuran_batas_view[] = "id";
$tdatat_pengukuran_batas_view[".Keys"] = $tableKeyst_pengukuran_batas_view;


$tdatat_pengukuran_batas_view[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["id"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "id";
//	id_ukur
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_ukur";
	$fdata["GoodName"] = "id_ukur";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","id_ukur");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_ukur";

		$fdata["sourceSingle"] = "id_ukur";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_ukur";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["id_ukur"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "id_ukur";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["id_lahan"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "id_lahan";
//	jenis_batas
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "jenis_batas";
	$fdata["GoodName"] = "jenis_batas";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","jenis_batas");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "jenis_batas";

		$fdata["sourceSingle"] = "jenis_batas";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "jenis_batas";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "m_code_list";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

				$edata["LookupWhere"] = "CatID='JBATAS'";


	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["jenis_batas"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "jenis_batas";
//	tipe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "tipe";
	$fdata["GoodName"] = "tipe";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","tipe");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "tipe";

		$fdata["sourceSingle"] = "tipe";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipe";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "m_code_list";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

				$edata["LookupWhere"] = "CatID='BATAS'";


	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["tipe"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "tipe";
//	batas
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "batas";
	$fdata["GoodName"] = "batas";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","batas");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas";

		$fdata["sourceSingle"] = "batas";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "batas";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_input_awal_new_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "nama_lahan";

	

	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_input_awal_new_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["autoCompleteFields"][] = array('masterF'=>"lat", 'lookupF'=>"Lat");
	$edata["autoCompleteFields"][] = array('masterF'=>"lng", 'lookupF'=>"Lng");
	$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "nama_lahan";

	

	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "t_input_awal_new_pengukuran";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "id_lahan";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "nama_lahan";

	

	
	$edata["LookupOrderBy"] = "created_date";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["batas"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "batas";
//	batas_text
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "batas_text";
	$fdata["GoodName"] = "batas_text";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","batas_text");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "batas_text";

		$fdata["sourceSingle"] = "batas_text";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "batas_text";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["batas_text"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "batas_text";
//	lat
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "lat";
	$fdata["GoodName"] = "lat";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","lat");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "lat";

		$fdata["sourceSingle"] = "lat";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lat";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 15;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["lat"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "lat";
//	lng
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "lng";
	$fdata["GoodName"] = "lng";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","lng");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "lng";

		$fdata["sourceSingle"] = "lng";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lng";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 15;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["lng"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "lng";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["keterangan"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["created_date"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["created_by"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["updated_date"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_pengukuran_batas";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_batas_view","updated_by");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_batas_view["updated_by"] = $fdata;
		$tdatat_pengukuran_batas_view[".searchableFields"][] = "updated_by";


$tables_data["t_pengukuran_batas_view"]=&$tdatat_pengukuran_batas_view;
$field_labels["t_pengukuran_batas_view"] = &$fieldLabelst_pengukuran_batas_view;
$fieldToolTips["t_pengukuran_batas_view"] = &$fieldToolTipst_pengukuran_batas_view;
$placeHolders["t_pengukuran_batas_view"] = &$placeHolderst_pengukuran_batas_view;
$page_titles["t_pengukuran_batas_view"] = &$pageTitlest_pengukuran_batas_view;


changeTextControlsToDate( "t_pengukuran_batas_view" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_pengukuran_batas_view"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_pengukuran_batas_view"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_pengukuran";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_pengukuran";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_pengukuran";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_pengukuran_batas_view"][0] = $masterParams;
				$masterTablesData["t_pengukuran_batas_view"][0]["masterKeys"] = array();
	$masterTablesData["t_pengukuran_batas_view"][0]["masterKeys"][]="id";
				$masterTablesData["t_pengukuran_batas_view"][0]["detailKeys"] = array();
	$masterTablesData["t_pengukuran_batas_view"][0]["detailKeys"][]="id_ukur";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_pengukuran_batas_view()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_ukur,  	id_lahan,  	jenis_batas,  	tipe,  	batas,  	batas_text,  	lat,  	lng,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_pengukuran_batas";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_pengukuran_batas_view";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto8["m_sql"] = "id_ukur";
$proto8["m_srcTableName"] = "t_pengukuran_batas_view";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto10["m_sql"] = "id_lahan";
$proto10["m_srcTableName"] = "t_pengukuran_batas_view";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "jenis_batas",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto12["m_sql"] = "jenis_batas";
$proto12["m_srcTableName"] = "t_pengukuran_batas_view";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "tipe",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto14["m_sql"] = "tipe";
$proto14["m_srcTableName"] = "t_pengukuran_batas_view";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "batas",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto16["m_sql"] = "batas";
$proto16["m_srcTableName"] = "t_pengukuran_batas_view";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "batas_text",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto18["m_sql"] = "batas_text";
$proto18["m_srcTableName"] = "t_pengukuran_batas_view";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "lat",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto20["m_sql"] = "lat";
$proto20["m_srcTableName"] = "t_pengukuran_batas_view";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "lng",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto22["m_sql"] = "lng";
$proto22["m_srcTableName"] = "t_pengukuran_batas_view";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto24["m_sql"] = "keterangan";
$proto24["m_srcTableName"] = "t_pengukuran_batas_view";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto26["m_sql"] = "created_date";
$proto26["m_srcTableName"] = "t_pengukuran_batas_view";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto28["m_sql"] = "created_by";
$proto28["m_srcTableName"] = "t_pengukuran_batas_view";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto30["m_sql"] = "updated_date";
$proto30["m_srcTableName"] = "t_pengukuran_batas_view";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_pengukuran_batas",
	"m_srcTableName" => "t_pengukuran_batas_view"
));

$proto32["m_sql"] = "updated_by";
$proto32["m_srcTableName"] = "t_pengukuran_batas_view";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "t_pengukuran_batas";
$proto35["m_srcTableName"] = "t_pengukuran_batas_view";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "id";
$proto35["m_columns"][] = "id_ukur";
$proto35["m_columns"][] = "id_lahan";
$proto35["m_columns"][] = "jenis_batas";
$proto35["m_columns"][] = "tipe";
$proto35["m_columns"][] = "batas";
$proto35["m_columns"][] = "batas_text";
$proto35["m_columns"][] = "lat";
$proto35["m_columns"][] = "lng";
$proto35["m_columns"][] = "keterangan";
$proto35["m_columns"][] = "created_date";
$proto35["m_columns"][] = "created_by";
$proto35["m_columns"][] = "updated_date";
$proto35["m_columns"][] = "updated_by";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "t_pengukuran_batas";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "t_pengukuran_batas_view";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_pengukuran_batas_view";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_pengukuran_batas_view = createSqlQuery_t_pengukuran_batas_view();


	
					
;

														

$tdatat_pengukuran_batas_view[".sqlquery"] = $queryData_t_pengukuran_batas_view;



include_once(getabspath("include/t_pengukuran_batas_view_events.php"));
$tdatat_pengukuran_batas_view[".hasEvents"] = true;

?>