<?php

/**
 * 	Dear developer!
 *	Never modify events.php file, it is autogenerated.
 *  Modify PHP/EventTemplates/events.txt instead.
 *
 */

 class eventclass_t_pengukuran  extends eventsBase
{
	function __construct()
	{
	// fill list of events
		$this->events["BeforeAdd"]=true;

		$this->events["BeforeEdit"]=true;

		$this->events["AfterAdd"]=true;



		$this->events["BeforeDelete"]=true;

		$this->events["BeforeShowEdit"]=true;


	}

//	handlers

		
		
		
		
		
		
		
		
		
		
		
		
		
				// Before record added
function BeforeAdd(&$values, &$message, $inline, $pageObject)
{

		$values['created_date']=date('Y-m-d H:i:s');
$values['created_by']=$_SESSION['username'];

$date = $values['start_date'];
$duedate = date('Y-m-d H:i:s', strtotime($date. ' + 1 week'));
$values['due_date'] = $duedate;

// Place event code here.
// Use "Add Action" button to add code snippets.

return true;
;		
} // function BeforeAdd

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				// Before record updated
function BeforeEdit(&$values, $where, &$oldvalues, &$keys, &$message, $inline, $pageObject)
{

		$values['updated_date']=date('Y-m-d H:i:s');
$values['updated_by']=$_SESSION['username'];

if($oldvalues['status']=='CLOSED' and $_SESSION["groupid"]=='2')
{
$message="Maaf, status pengukuran tanah sudah close, anda tidak bisa merubah data kembali";
return false;
}

if($values['status']=='CLOSED')
{
$values['close_by']=$_SESSION['username'];
$values['close_date']=now();
}

if($oldvalues['status']=='CLOSED' and $_SESSION["groupid"]=='2')
{
$message="Maaf, status input awal sudah close, anda tidak bisa merubah data kembali";
return false;
}

// Place event code here.
// Use "Add Action" button to add code snippets.

return true;
;		
} // function BeforeEdit

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				// After record added
function AfterAdd(&$values, &$keys, $inline, $pageObject)
{

		$date=date('Y-m-d H:i:s');
$percil = ceil($values['ukuran']/20000);
$x = 1;

$sqllahan="select id_lahan, nama_lahan from t_input_awal where id_lahan='$values[id_lahan]'";
$rslahan = CustomQuery($sqllahan);
$data = db_fetch_array($rslahan);

while($x <= $percil)
{
	$sql = "insert into t_pengukuran_percil (id, id_ukur, id_lahan, percil_name, urutan, created_date, created_by) values ('','$values[id]','$values[id_lahan]','$data[nama_lahan]_$values[id_lahan]_$x','$x','$date','$_SESSION[username]')";
	CustomQuery($sql);

	$x++;
}

// Place event code here.
// Use "Add Action" button to add code snippets.
;		
} // function AfterAdd

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				// Before record deleted
function BeforeDelete($where, &$deleted_values, &$message, $pageObject)
{

		$sql = "select * from t_negosiasi where id_lahan = '$deleted_values[id_lahan]'";
$rs = CustomQuery($sql);
$data = db_fetch_array($rs);

if($data)
{

$message="Maaf, data sudah sampai dinegosiasi, mohon tidak menghapus data agar data tidak crash ! cek kembali data anda !";

return false;
}

// Place event code here.
// Use "Add Action" button to add code snippets.

return true;
;		
} // function BeforeDelete

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				// Before display
function BeforeShowEdit(&$xt, &$templatefile, $values, $pageObject)
{

		$sql = "SELECT lla.username, lla.email, lla.fullname, lla.UserName as UN, lla.GroupID 
FROM lla_users lla
join llaugmembers llu on lla.UserName = llu.username where llu.username='$_SESSION[username]'";
$rs = db_query($sql);
$data=db_fetch_array($rs);


if($data['GroupID']=='2')

{
$attr = $xt->fetchVar("ukuran_editcontrol");
$attr = str_replace(">"," READONLY=READONLY>",$attr);
$xt->assign("ukuran_editcontrol",$attr);
}

// Place event code here.
// Use "Add Action" button to add code snippets.
;		
} // function BeforeShowEdit

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		



}
?>
