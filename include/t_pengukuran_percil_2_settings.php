<?php
$tdatat_pengukuran_percil_2 = array();
$tdatat_pengukuran_percil_2[".searchableFields"] = array();
$tdatat_pengukuran_percil_2[".ShortName"] = "t_pengukuran_percil_2";
$tdatat_pengukuran_percil_2[".OwnerID"] = "";
$tdatat_pengukuran_percil_2[".OriginalTable"] = "t_pengukuran_percil";


$tdatat_pengukuran_percil_2[".pagesByType"] = my_json_decode( "{\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_pengukuran_percil_2[".originalPagesByType"] = $tdatat_pengukuran_percil_2[".pagesByType"];
$tdatat_pengukuran_percil_2[".pages"] = types2pages( my_json_decode( "{\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_pengukuran_percil_2[".originalPages"] = $tdatat_pengukuran_percil_2[".pages"];
$tdatat_pengukuran_percil_2[".defaultPages"] = my_json_decode( "{\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_pengukuran_percil_2[".originalDefaultPages"] = $tdatat_pengukuran_percil_2[".defaultPages"];

//	field labels
$fieldLabelst_pengukuran_percil_2 = array();
$fieldToolTipst_pengukuran_percil_2 = array();
$pageTitlest_pengukuran_percil_2 = array();
$placeHolderst_pengukuran_percil_2 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_pengukuran_percil_2["English"] = array();
	$fieldToolTipst_pengukuran_percil_2["English"] = array();
	$placeHolderst_pengukuran_percil_2["English"] = array();
	$pageTitlest_pengukuran_percil_2["English"] = array();
	$fieldLabelst_pengukuran_percil_2["English"]["id"] = "Id";
	$fieldToolTipst_pengukuran_percil_2["English"]["id"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["id"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["no_register"] = "No Register";
	$fieldToolTipst_pengukuran_percil_2["English"]["no_register"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["no_register"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["percil_name"] = "Percil Name";
	$fieldToolTipst_pengukuran_percil_2["English"]["percil_name"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["percil_name"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["urutan"] = "Urutan";
	$fieldToolTipst_pengukuran_percil_2["English"]["urutan"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["urutan"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["ukuran_real"] = "Ukuran Real";
	$fieldToolTipst_pengukuran_percil_2["English"]["ukuran_real"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["ukuran_real"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_pengukuran_percil_2["English"]["param_dok"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["param_dok"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_pengukuran_percil_2["English"]["file_dok"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["file_dok"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_pengukuran_percil_2["English"]["keterangan"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["keterangan"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["created_date"] = "Created Date";
	$fieldToolTipst_pengukuran_percil_2["English"]["created_date"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["created_date"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["created_by"] = "Created By";
	$fieldToolTipst_pengukuran_percil_2["English"]["created_by"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["created_by"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_pengukuran_percil_2["English"]["updated_date"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["updated_date"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_pengukuran_percil_2["English"]["updated_by"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["updated_by"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["id_ukur"] = "Id Ukur";
	$fieldToolTipst_pengukuran_percil_2["English"]["id_ukur"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["id_ukur"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_pengukuran_percil_2["English"]["id_lahan"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["id_lahan"] = "";
	$fieldLabelst_pengukuran_percil_2["English"]["percil_date"] = "Percil Date";
	$fieldToolTipst_pengukuran_percil_2["English"]["percil_date"] = "";
	$placeHolderst_pengukuran_percil_2["English"]["percil_date"] = "";
	if (count($fieldToolTipst_pengukuran_percil_2["English"]))
		$tdatat_pengukuran_percil_2[".isUseToolTips"] = true;
}


	$tdatat_pengukuran_percil_2[".NCSearch"] = true;



$tdatat_pengukuran_percil_2[".shortTableName"] = "t_pengukuran_percil_2";
$tdatat_pengukuran_percil_2[".nSecOptions"] = 0;

$tdatat_pengukuran_percil_2[".mainTableOwnerID"] = "";
$tdatat_pengukuran_percil_2[".entityType"] = 1;
$tdatat_pengukuran_percil_2[".connId"] = "db_lla_at_localhost";


$tdatat_pengukuran_percil_2[".strOriginalTableName"] = "t_pengukuran_percil";

		 



$tdatat_pengukuran_percil_2[".showAddInPopup"] = false;

$tdatat_pengukuran_percil_2[".showEditInPopup"] = false;

$tdatat_pengukuran_percil_2[".showViewInPopup"] = false;

$tdatat_pengukuran_percil_2[".listAjax"] = false;
//	temporary
//$tdatat_pengukuran_percil_2[".listAjax"] = false;

	$tdatat_pengukuran_percil_2[".audit"] = false;

	$tdatat_pengukuran_percil_2[".locking"] = false;


$pages = $tdatat_pengukuran_percil_2[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_pengukuran_percil_2[".edit"] = true;
	$tdatat_pengukuran_percil_2[".afterEditAction"] = 1;
	$tdatat_pengukuran_percil_2[".closePopupAfterEdit"] = 1;
	$tdatat_pengukuran_percil_2[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_pengukuran_percil_2[".add"] = true;
$tdatat_pengukuran_percil_2[".afterAddAction"] = 1;
$tdatat_pengukuran_percil_2[".closePopupAfterAdd"] = 1;
$tdatat_pengukuran_percil_2[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_pengukuran_percil_2[".list"] = true;
}



$tdatat_pengukuran_percil_2[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_pengukuran_percil_2[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_pengukuran_percil_2[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_pengukuran_percil_2[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_pengukuran_percil_2[".printFriendly"] = true;
}



$tdatat_pengukuran_percil_2[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_pengukuran_percil_2[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_pengukuran_percil_2[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_pengukuran_percil_2[".isUseAjaxSuggest"] = true;

$tdatat_pengukuran_percil_2[".rowHighlite"] = true;



						

$tdatat_pengukuran_percil_2[".ajaxCodeSnippetAdded"] = false;

$tdatat_pengukuran_percil_2[".buttonsAdded"] = false;

$tdatat_pengukuran_percil_2[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_pengukuran_percil_2[".isUseTimeForSearch"] = false;


$tdatat_pengukuran_percil_2[".badgeColor"] = "d2691e";


$tdatat_pengukuran_percil_2[".allSearchFields"] = array();
$tdatat_pengukuran_percil_2[".filterFields"] = array();
$tdatat_pengukuran_percil_2[".requiredSearchFields"] = array();

$tdatat_pengukuran_percil_2[".googleLikeFields"] = array();
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "id";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "id_ukur";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "id_lahan";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "no_register";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "percil_name";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "percil_date";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "urutan";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "ukuran_real";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "param_dok";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "file_dok";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "keterangan";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "created_date";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "created_by";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "updated_date";
$tdatat_pengukuran_percil_2[".googleLikeFields"][] = "updated_by";



$tdatat_pengukuran_percil_2[".tableType"] = "list";

$tdatat_pengukuran_percil_2[".printerPageOrientation"] = 0;
$tdatat_pengukuran_percil_2[".nPrinterPageScale"] = 100;

$tdatat_pengukuran_percil_2[".nPrinterSplitRecords"] = 40;

$tdatat_pengukuran_percil_2[".geocodingEnabled"] = false;










$tdatat_pengukuran_percil_2[".pageSize"] = 20;

$tdatat_pengukuran_percil_2[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_pengukuran_percil_2[".strOrderBy"] = $tstrOrderBy;

$tdatat_pengukuran_percil_2[".orderindexes"] = array();


$tdatat_pengukuran_percil_2[".sqlHead"] = "SELECT id,  	id_ukur,  	id_lahan,  	no_register,  	percil_name,  	percil_date,  	urutan,  	ukuran_real,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_pengukuran_percil_2[".sqlFrom"] = "FROM t_pengukuran_percil";
$tdatat_pengukuran_percil_2[".sqlWhereExpr"] = "";
$tdatat_pengukuran_percil_2[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_pengukuran_percil_2[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_pengukuran_percil_2[".arrGroupsPerPage"] = $arrGPP;

$tdatat_pengukuran_percil_2[".highlightSearchResults"] = true;

$tableKeyst_pengukuran_percil_2 = array();
$tableKeyst_pengukuran_percil_2[] = "id";
$tdatat_pengukuran_percil_2[".Keys"] = $tableKeyst_pengukuran_percil_2;


$tdatat_pengukuran_percil_2[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["id"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "id";
//	id_ukur
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_ukur";
	$fdata["GoodName"] = "id_ukur";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","id_ukur");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_ukur";

		$fdata["sourceSingle"] = "id_ukur";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_ukur";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["id_ukur"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "id_ukur";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["id_lahan"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "id_lahan";
//	no_register
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "no_register";
	$fdata["GoodName"] = "no_register";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","no_register");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "no_register";

		$fdata["sourceSingle"] = "no_register";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "no_register";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["no_register"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "no_register";
//	percil_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "percil_name";
	$fdata["GoodName"] = "percil_name";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","percil_name");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "percil_name";

		$fdata["sourceSingle"] = "percil_name";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "percil_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["percil_name"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "percil_name";
//	percil_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "percil_date";
	$fdata["GoodName"] = "percil_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","percil_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "percil_date";

		$fdata["sourceSingle"] = "percil_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "percil_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["percil_date"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "percil_date";
//	urutan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "urutan";
	$fdata["GoodName"] = "urutan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","urutan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "urutan";

		$fdata["sourceSingle"] = "urutan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "urutan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["urutan"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "urutan";
//	ukuran_real
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ukuran_real";
	$fdata["GoodName"] = "ukuran_real";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","ukuran_real");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ukuran_real";

		$fdata["sourceSingle"] = "ukuran_real";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ukuran_real";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["ukuran_real"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "ukuran_real";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["param_dok"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
			$vdata["DisplayPDF"] = true;
			
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["file_dok"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["keterangan"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["created_date"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["created_by"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["updated_date"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_2","updated_by");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_2["updated_by"] = $fdata;
		$tdatat_pengukuran_percil_2[".searchableFields"][] = "updated_by";


$tables_data["t_pengukuran_percil_2"]=&$tdatat_pengukuran_percil_2;
$field_labels["t_pengukuran_percil_2"] = &$fieldLabelst_pengukuran_percil_2;
$fieldToolTips["t_pengukuran_percil_2"] = &$fieldToolTipst_pengukuran_percil_2;
$placeHolders["t_pengukuran_percil_2"] = &$placeHolderst_pengukuran_percil_2;
$page_titles["t_pengukuran_percil_2"] = &$pageTitlest_pengukuran_percil_2;


changeTextControlsToDate( "t_pengukuran_percil_2" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_pengukuran_percil_2"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_pengukuran_percil_2"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_regis";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_regis";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_regis";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_pengukuran_percil_2"][0] = $masterParams;
				$masterTablesData["t_pengukuran_percil_2"][0]["masterKeys"] = array();
	$masterTablesData["t_pengukuran_percil_2"][0]["masterKeys"][]="id_lahan";
				$masterTablesData["t_pengukuran_percil_2"][0]["detailKeys"] = array();
	$masterTablesData["t_pengukuran_percil_2"][0]["detailKeys"][]="id_lahan";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_pengukuran_percil_2()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_ukur,  	id_lahan,  	no_register,  	percil_name,  	percil_date,  	urutan,  	ukuran_real,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_pengukuran_percil";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_pengukuran_percil_2";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto8["m_sql"] = "id_ukur";
$proto8["m_srcTableName"] = "t_pengukuran_percil_2";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto10["m_sql"] = "id_lahan";
$proto10["m_srcTableName"] = "t_pengukuran_percil_2";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "no_register",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto12["m_sql"] = "no_register";
$proto12["m_srcTableName"] = "t_pengukuran_percil_2";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "percil_name",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto14["m_sql"] = "percil_name";
$proto14["m_srcTableName"] = "t_pengukuran_percil_2";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "percil_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto16["m_sql"] = "percil_date";
$proto16["m_srcTableName"] = "t_pengukuran_percil_2";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "urutan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto18["m_sql"] = "urutan";
$proto18["m_srcTableName"] = "t_pengukuran_percil_2";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ukuran_real",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto20["m_sql"] = "ukuran_real";
$proto20["m_srcTableName"] = "t_pengukuran_percil_2";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto22["m_sql"] = "param_dok";
$proto22["m_srcTableName"] = "t_pengukuran_percil_2";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto24["m_sql"] = "file_dok";
$proto24["m_srcTableName"] = "t_pengukuran_percil_2";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto26["m_sql"] = "keterangan";
$proto26["m_srcTableName"] = "t_pengukuran_percil_2";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto28["m_sql"] = "created_date";
$proto28["m_srcTableName"] = "t_pengukuran_percil_2";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto30["m_sql"] = "created_by";
$proto30["m_srcTableName"] = "t_pengukuran_percil_2";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto32["m_sql"] = "updated_date";
$proto32["m_srcTableName"] = "t_pengukuran_percil_2";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_pengukuran_percil_2"
));

$proto34["m_sql"] = "updated_by";
$proto34["m_srcTableName"] = "t_pengukuran_percil_2";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto36=array();
$proto36["m_link"] = "SQLL_MAIN";
			$proto37=array();
$proto37["m_strName"] = "t_pengukuran_percil";
$proto37["m_srcTableName"] = "t_pengukuran_percil_2";
$proto37["m_columns"] = array();
$proto37["m_columns"][] = "id";
$proto37["m_columns"][] = "id_ukur";
$proto37["m_columns"][] = "id_lahan";
$proto37["m_columns"][] = "no_register";
$proto37["m_columns"][] = "percil_name";
$proto37["m_columns"][] = "percil_date";
$proto37["m_columns"][] = "urutan";
$proto37["m_columns"][] = "ukuran_real";
$proto37["m_columns"][] = "param_dok";
$proto37["m_columns"][] = "file_dok";
$proto37["m_columns"][] = "keterangan";
$proto37["m_columns"][] = "created_date";
$proto37["m_columns"][] = "created_by";
$proto37["m_columns"][] = "updated_date";
$proto37["m_columns"][] = "updated_by";
$obj = new SQLTable($proto37);

$proto36["m_table"] = $obj;
$proto36["m_sql"] = "t_pengukuran_percil";
$proto36["m_alias"] = "";
$proto36["m_srcTableName"] = "t_pengukuran_percil_2";
$proto38=array();
$proto38["m_sql"] = "";
$proto38["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto38["m_column"]=$obj;
$proto38["m_contained"] = array();
$proto38["m_strCase"] = "";
$proto38["m_havingmode"] = false;
$proto38["m_inBrackets"] = false;
$proto38["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto38);

$proto36["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto36);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_pengukuran_percil_2";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_pengukuran_percil_2 = createSqlQuery_t_pengukuran_percil_2();


	
					
;

															

$tdatat_pengukuran_percil_2[".sqlquery"] = $queryData_t_pengukuran_percil_2;



$tdatat_pengukuran_percil_2[".hasEvents"] = false;

?>