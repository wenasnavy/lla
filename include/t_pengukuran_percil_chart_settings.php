<?php
$tdatat_pengukuran_percil_chart = array();
$tdatat_pengukuran_percil_chart[".searchableFields"] = array();
$tdatat_pengukuran_percil_chart[".ShortName"] = "t_pengukuran_percil_chart";
$tdatat_pengukuran_percil_chart[".OwnerID"] = "";
$tdatat_pengukuran_percil_chart[".OriginalTable"] = "t_pengukuran_percil";


$tdatat_pengukuran_percil_chart[".pagesByType"] = my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" );
$tdatat_pengukuran_percil_chart[".originalPagesByType"] = $tdatat_pengukuran_percil_chart[".pagesByType"];
$tdatat_pengukuran_percil_chart[".pages"] = types2pages( my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" ) );
$tdatat_pengukuran_percil_chart[".originalPages"] = $tdatat_pengukuran_percil_chart[".pages"];
$tdatat_pengukuran_percil_chart[".defaultPages"] = my_json_decode( "{\"chart\":\"chart\",\"search\":\"search\"}" );
$tdatat_pengukuran_percil_chart[".originalDefaultPages"] = $tdatat_pengukuran_percil_chart[".defaultPages"];

//	field labels
$fieldLabelst_pengukuran_percil_chart = array();
$fieldToolTipst_pengukuran_percil_chart = array();
$pageTitlest_pengukuran_percil_chart = array();
$placeHolderst_pengukuran_percil_chart = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_pengukuran_percil_chart["English"] = array();
	$fieldToolTipst_pengukuran_percil_chart["English"] = array();
	$placeHolderst_pengukuran_percil_chart["English"] = array();
	$pageTitlest_pengukuran_percil_chart["English"] = array();
	$fieldLabelst_pengukuran_percil_chart["English"]["status_percil"] = "Status Percil";
	$fieldToolTipst_pengukuran_percil_chart["English"]["status_percil"] = "";
	$placeHolderst_pengukuran_percil_chart["English"]["status_percil"] = "";
	$fieldLabelst_pengukuran_percil_chart["English"]["jml_done"] = "Jml Done";
	$fieldToolTipst_pengukuran_percil_chart["English"]["jml_done"] = "";
	$placeHolderst_pengukuran_percil_chart["English"]["jml_done"] = "";
	if (count($fieldToolTipst_pengukuran_percil_chart["English"]))
		$tdatat_pengukuran_percil_chart[".isUseToolTips"] = true;
}


	$tdatat_pengukuran_percil_chart[".NCSearch"] = true;

	$tdatat_pengukuran_percil_chart[".ChartRefreshTime"] = 0;


$tdatat_pengukuran_percil_chart[".shortTableName"] = "t_pengukuran_percil_chart";
$tdatat_pengukuran_percil_chart[".nSecOptions"] = 0;

$tdatat_pengukuran_percil_chart[".mainTableOwnerID"] = "";
$tdatat_pengukuran_percil_chart[".entityType"] = 3;
$tdatat_pengukuran_percil_chart[".connId"] = "db_lla_at_localhost";


$tdatat_pengukuran_percil_chart[".strOriginalTableName"] = "t_pengukuran_percil";

		 



$tdatat_pengukuran_percil_chart[".showAddInPopup"] = false;

$tdatat_pengukuran_percil_chart[".showEditInPopup"] = false;

$tdatat_pengukuran_percil_chart[".showViewInPopup"] = false;

$tdatat_pengukuran_percil_chart[".listAjax"] = false;
//	temporary
//$tdatat_pengukuran_percil_chart[".listAjax"] = false;

	$tdatat_pengukuran_percil_chart[".audit"] = false;

	$tdatat_pengukuran_percil_chart[".locking"] = false;


$pages = $tdatat_pengukuran_percil_chart[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_pengukuran_percil_chart[".edit"] = true;
	$tdatat_pengukuran_percil_chart[".afterEditAction"] = 1;
	$tdatat_pengukuran_percil_chart[".closePopupAfterEdit"] = 1;
	$tdatat_pengukuran_percil_chart[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_pengukuran_percil_chart[".add"] = true;
$tdatat_pengukuran_percil_chart[".afterAddAction"] = 1;
$tdatat_pengukuran_percil_chart[".closePopupAfterAdd"] = 1;
$tdatat_pengukuran_percil_chart[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_pengukuran_percil_chart[".list"] = true;
}



$tdatat_pengukuran_percil_chart[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_pengukuran_percil_chart[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_pengukuran_percil_chart[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_pengukuran_percil_chart[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_pengukuran_percil_chart[".printFriendly"] = true;
}



$tdatat_pengukuran_percil_chart[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_pengukuran_percil_chart[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_pengukuran_percil_chart[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_pengukuran_percil_chart[".isUseAjaxSuggest"] = true;




			

$tdatat_pengukuran_percil_chart[".ajaxCodeSnippetAdded"] = false;

$tdatat_pengukuran_percil_chart[".buttonsAdded"] = false;

$tdatat_pengukuran_percil_chart[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_pengukuran_percil_chart[".isUseTimeForSearch"] = false;


$tdatat_pengukuran_percil_chart[".badgeColor"] = "7B68EE";


$tdatat_pengukuran_percil_chart[".allSearchFields"] = array();
$tdatat_pengukuran_percil_chart[".filterFields"] = array();
$tdatat_pengukuran_percil_chart[".requiredSearchFields"] = array();

$tdatat_pengukuran_percil_chart[".googleLikeFields"] = array();
$tdatat_pengukuran_percil_chart[".googleLikeFields"][] = "status_percil";
$tdatat_pengukuran_percil_chart[".googleLikeFields"][] = "jml_done";



$tdatat_pengukuran_percil_chart[".tableType"] = "chart";

$tdatat_pengukuran_percil_chart[".printerPageOrientation"] = 0;
$tdatat_pengukuran_percil_chart[".nPrinterPageScale"] = 100;

$tdatat_pengukuran_percil_chart[".nPrinterSplitRecords"] = 40;

$tdatat_pengukuran_percil_chart[".geocodingEnabled"] = false;



// chart settings
$tdatat_pengukuran_percil_chart[".chartType"] = "2DPie";
// end of chart settings








$tstrOrderBy = "";
$tdatat_pengukuran_percil_chart[".strOrderBy"] = $tstrOrderBy;

$tdatat_pengukuran_percil_chart[".orderindexes"] = array();


$tdatat_pengukuran_percil_chart[".sqlHead"] = "select *";
$tdatat_pengukuran_percil_chart[".sqlFrom"] = "from  (  select   'Done' as status_percil,  count(tpp.id) as jml_done  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  union  select   'Sisa' as status_percil,  target_percil-count(tpp.id) as sisa  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  )b";
$tdatat_pengukuran_percil_chart[".sqlWhereExpr"] = "";
$tdatat_pengukuran_percil_chart[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_pengukuran_percil_chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_pengukuran_percil_chart[".arrGroupsPerPage"] = $arrGPP;

$tdatat_pengukuran_percil_chart[".highlightSearchResults"] = true;

$tableKeyst_pengukuran_percil_chart = array();
$tdatat_pengukuran_percil_chart[".Keys"] = $tableKeyst_pengukuran_percil_chart;


$tdatat_pengukuran_percil_chart[".hideMobileList"] = array();




//	status_percil
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "status_percil";
	$fdata["GoodName"] = "status_percil";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_chart","status_percil");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_percil";

	
		$fdata["FullName"] = "status_percil";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_chart["status_percil"] = $fdata;
		$tdatat_pengukuran_percil_chart[".searchableFields"][] = "status_percil";
//	jml_done
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "jml_done";
	$fdata["GoodName"] = "jml_done";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("t_pengukuran_percil_chart","jml_done");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "jml_done";

	
		$fdata["FullName"] = "jml_done";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_pengukuran_percil_chart["jml_done"] = $fdata;
		$tdatat_pengukuran_percil_chart[".searchableFields"][] = "jml_done";

$tdatat_pengukuran_percil_chart[".chartLabelField"] = "status_percil";
$tdatat_pengukuran_percil_chart[".chartSeries"] = array();
$tdatat_pengukuran_percil_chart[".chartSeries"][] = array(
	"field" => "jml_done",
	"total" => ""
);
	$tdatat_pengukuran_percil_chart[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">t_pengukuran_percil_chart</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_pie</attr>
		</attr>

		<attr value="parameters">';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '<attr value="0">
			<attr value="name">jml_done</attr>';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '</attr>';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '<attr value="1">
		<attr value="name">status_percil</attr>
	</attr>';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatat_pengukuran_percil_chart[".chartXml"] .= '<attr value="head">'.xmlencode("Progress Percil").'</attr>
<attr value="foot">'.xmlencode("").'</attr>
<attr value="y_axis_label">'.xmlencode("id").'</attr>


<attr value="slegend">true</attr>
<attr value="sgrid">false</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatat_pengukuran_percil_chart[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '<attr value="0">
		<attr value="name">status_percil</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("t_pengukuran_percil_chart","status_percil")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatat_pengukuran_percil_chart[".chartXml"] .= '<attr value="1">
		<attr value="name">jml_done</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("t_pengukuran_percil_chart","jml_done")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatat_pengukuran_percil_chart[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">t_pengukuran_percil_chart</attr>
<attr value="short_table_name">t_pengukuran_percil_chart</attr>
</attr>

</chart>';

$tables_data["t_pengukuran_percil_chart"]=&$tdatat_pengukuran_percil_chart;
$field_labels["t_pengukuran_percil_chart"] = &$fieldLabelst_pengukuran_percil_chart;
$fieldToolTips["t_pengukuran_percil_chart"] = &$fieldToolTipst_pengukuran_percil_chart;
$placeHolders["t_pengukuran_percil_chart"] = &$placeHolderst_pengukuran_percil_chart;
$page_titles["t_pengukuran_percil_chart"] = &$pageTitlest_pengukuran_percil_chart;


changeTextControlsToDate( "t_pengukuran_percil_chart" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_pengukuran_percil_chart"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_pengukuran_percil_chart"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_pengukuran_percil_chart()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "*";
$proto0["m_strFrom"] = "from  (  select   'Done' as status_percil,  count(tpp.id) as jml_done  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  union  select   'Sisa' as status_percil,  target_percil-count(tpp.id) as sisa  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  )b";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "*"
));

$proto6["m_sql"] = "*";
$proto6["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto8=array();
$proto8["m_link"] = "SQLL_MAIN";
			$proto9=array();
$proto9["m_strHead"] = "  select";
$proto9["m_strFieldList"] = "'Done' as status_percil,  count(tpp.id) as jml_done";
$proto9["m_strFrom"] = "from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')";
$proto9["m_strWhere"] = "DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null";
$proto9["m_strOrderBy"] = "";
	
					
;
						$proto9["cipherer"] = null;
$proto11=array();
$proto11["m_sql"] = "DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null";
$proto11["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null"
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
						$proto13=array();
$proto13["m_sql"] = "DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y')";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
						$proto14=array();
$proto14["m_functiontype"] = "SQLF_CUSTOM";
$proto14["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "tpp.created_date"
));

$proto14["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'%Y'"
));

$proto14["m_arguments"][]=$obj;
$proto14["m_strFunctionName"] = "DATE_FORMAT";
$obj = new SQLFunctionCall($proto14);

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "= DATE_FORMAT(now(),'%Y')";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

			$proto11["m_contained"][]=$obj;
						$proto17=array();
$proto17["m_sql"] = "no_register is not null";
$proto17["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "no_register",
	"m_strTable" => "tpp",
	"m_srcTableName" => "t_pengukuran_percil_chart"
));

$proto17["m_column"]=$obj;
$proto17["m_contained"] = array();
$proto17["m_strCase"] = "is not null";
$proto17["m_havingmode"] = false;
$proto17["m_inBrackets"] = false;
$proto17["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto17);

			$proto11["m_contained"][]=$obj;
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_where"] = $obj;
$proto19=array();
$proto19["m_sql"] = "";
$proto19["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto19["m_column"]=$obj;
$proto19["m_contained"] = array();
$proto19["m_strCase"] = "";
$proto19["m_havingmode"] = false;
$proto19["m_inBrackets"] = false;
$proto19["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto19);

$proto9["m_having"] = $obj;
$proto9["m_fieldlist"] = array();
						$proto21=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "'Done'"
));

$proto21["m_sql"] = "'Done'";
$proto21["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "status_percil";
$obj = new SQLFieldListItem($proto21);

$proto9["m_fieldlist"][]=$obj;
						$proto23=array();
			$proto24=array();
$proto24["m_functiontype"] = "SQLF_COUNT";
$proto24["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "tpp",
	"m_srcTableName" => "t_pengukuran_percil_chart"
));

$proto24["m_arguments"][]=$obj;
$proto24["m_strFunctionName"] = "count";
$obj = new SQLFunctionCall($proto24);

$proto23["m_sql"] = "count(tpp.id)";
$proto23["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "jml_done";
$obj = new SQLFieldListItem($proto23);

$proto9["m_fieldlist"][]=$obj;
$proto9["m_fromlist"] = array();
												$proto26=array();
$proto26["m_link"] = "SQLL_MAIN";
			$proto27=array();
$proto27["m_strName"] = "t_pengukuran_percil";
$proto27["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "id";
$proto27["m_columns"][] = "id_ukur";
$proto27["m_columns"][] = "id_lahan";
$proto27["m_columns"][] = "no_register";
$proto27["m_columns"][] = "percil_name";
$proto27["m_columns"][] = "percil_date";
$proto27["m_columns"][] = "urutan";
$proto27["m_columns"][] = "ukuran_real";
$proto27["m_columns"][] = "param_dok";
$proto27["m_columns"][] = "file_dok";
$proto27["m_columns"][] = "keterangan";
$proto27["m_columns"][] = "created_date";
$proto27["m_columns"][] = "created_by";
$proto27["m_columns"][] = "updated_date";
$proto27["m_columns"][] = "updated_by";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "t_pengukuran_percil tpp";
$proto26["m_alias"] = "tpp";
$proto26["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto9["m_fromlist"][]=$obj;
												$proto30=array();
$proto30["m_link"] = "SQLL_LEFTJOIN";
			$proto31=array();
$proto31["m_strName"] = "t_target_percil";
$proto31["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto31["m_columns"] = array();
$proto31["m_columns"][] = "id";
$proto31["m_columns"][] = "year";
$proto31["m_columns"][] = "target_percil";
$proto31["m_columns"][] = "created_date";
$proto31["m_columns"][] = "created_by";
$proto31["m_columns"][] = "updated_date";
$proto31["m_columns"][] = "updated_by";
$obj = new SQLTable($proto31);

$proto30["m_table"] = $obj;
$proto30["m_sql"] = "left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')";
$proto30["m_alias"] = "ttp";
$proto30["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto32=array();
$proto32["m_sql"] = "ttp.year = DATE_FORMAT(tpp.created_date,'%Y')";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "year",
	"m_strTable" => "ttp",
	"m_srcTableName" => "t_pengukuran_percil_chart"
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "= DATE_FORMAT(tpp.created_date,'%Y')";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto30["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto30);

$proto9["m_fromlist"][]=$obj;
$proto9["m_groupby"] = array();
$proto9["m_orderby"] = array();
$proto9["m_srcTableName"]="t_pengukuran_percil_chart";		
$obj = new SQLQuery($proto9);

$proto8["m_table"] = $obj;
$proto8["m_sql"] = "(  select   'Done' as status_percil,  count(tpp.id) as jml_done  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  union  select   'Sisa' as status_percil,  target_percil-count(tpp.id) as sisa  from t_pengukuran_percil tpp  left join t_target_percil ttp ON ttp.year = DATE_FORMAT(tpp.created_date,'%Y')   where DATE_FORMAT(tpp.created_date,'%Y') = DATE_FORMAT(now(),'%Y') and no_register is not null  )b";
$proto8["m_alias"] = "b";
$proto8["m_srcTableName"] = "t_pengukuran_percil_chart";
$proto34=array();
$proto34["m_sql"] = "";
$proto34["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto34["m_column"]=$obj;
$proto34["m_contained"] = array();
$proto34["m_strCase"] = "";
$proto34["m_havingmode"] = false;
$proto34["m_inBrackets"] = false;
$proto34["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto34);

$proto8["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto8);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_pengukuran_percil_chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_pengukuran_percil_chart = createSqlQuery_t_pengukuran_percil_chart();


	
					
;

		

$tdatat_pengukuran_percil_chart[".sqlquery"] = $queryData_t_pengukuran_percil_chart;



include_once(getabspath("include/t_pengukuran_percil_chart_events.php"));
$tdatat_pengukuran_percil_chart[".hasEvents"] = true;

?>