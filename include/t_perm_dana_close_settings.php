<?php
$tdatat_perm_dana_close = array();
$tdatat_perm_dana_close[".searchableFields"] = array();
$tdatat_perm_dana_close[".ShortName"] = "t_perm_dana_close";
$tdatat_perm_dana_close[".OwnerID"] = "";
$tdatat_perm_dana_close[".OriginalTable"] = "t_perm_dana";


$tdatat_perm_dana_close[".pagesByType"] = my_json_decode( "{\"edit\":[\"edit\"],\"export\":[\"export\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_perm_dana_close[".originalPagesByType"] = $tdatat_perm_dana_close[".pagesByType"];
$tdatat_perm_dana_close[".pages"] = types2pages( my_json_decode( "{\"edit\":[\"edit\"],\"export\":[\"export\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_perm_dana_close[".originalPages"] = $tdatat_perm_dana_close[".pages"];
$tdatat_perm_dana_close[".defaultPages"] = my_json_decode( "{\"edit\":\"edit\",\"export\":\"export\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_perm_dana_close[".originalDefaultPages"] = $tdatat_perm_dana_close[".defaultPages"];

//	field labels
$fieldLabelst_perm_dana_close = array();
$fieldToolTipst_perm_dana_close = array();
$pageTitlest_perm_dana_close = array();
$placeHolderst_perm_dana_close = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_perm_dana_close["English"] = array();
	$fieldToolTipst_perm_dana_close["English"] = array();
	$placeHolderst_perm_dana_close["English"] = array();
	$pageTitlest_perm_dana_close["English"] = array();
	$fieldLabelst_perm_dana_close["English"]["id"] = "Id";
	$fieldToolTipst_perm_dana_close["English"]["id"] = "";
	$placeHolderst_perm_dana_close["English"]["id"] = "";
	$fieldLabelst_perm_dana_close["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_perm_dana_close["English"]["id_lahan"] = "";
	$placeHolderst_perm_dana_close["English"]["id_lahan"] = "";
	$fieldLabelst_perm_dana_close["English"]["start_date_dp"] = "Start Date Dp";
	$fieldToolTipst_perm_dana_close["English"]["start_date_dp"] = "";
	$placeHolderst_perm_dana_close["English"]["start_date_dp"] = "";
	$fieldLabelst_perm_dana_close["English"]["due_date_dp"] = "Due Date Dp";
	$fieldToolTipst_perm_dana_close["English"]["due_date_dp"] = "";
	$placeHolderst_perm_dana_close["English"]["due_date_dp"] = "";
	$fieldLabelst_perm_dana_close["English"]["start_date_full"] = "Start Date Full";
	$fieldToolTipst_perm_dana_close["English"]["start_date_full"] = "";
	$placeHolderst_perm_dana_close["English"]["start_date_full"] = "";
	$fieldLabelst_perm_dana_close["English"]["due_date_full"] = "Due Date Full";
	$fieldToolTipst_perm_dana_close["English"]["due_date_full"] = "";
	$placeHolderst_perm_dana_close["English"]["due_date_full"] = "";
	$fieldLabelst_perm_dana_close["English"]["pic"] = "PIC";
	$fieldToolTipst_perm_dana_close["English"]["pic"] = "";
	$placeHolderst_perm_dana_close["English"]["pic"] = "";
	$fieldLabelst_perm_dana_close["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_perm_dana_close["English"]["keterangan"] = "";
	$placeHolderst_perm_dana_close["English"]["keterangan"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_full_by"] = "Close Full By";
	$fieldToolTipst_perm_dana_close["English"]["close_full_by"] = "";
	$placeHolderst_perm_dana_close["English"]["close_full_by"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_full_date"] = "Close Full Date";
	$fieldToolTipst_perm_dana_close["English"]["close_full_date"] = "";
	$placeHolderst_perm_dana_close["English"]["close_full_date"] = "";
	$fieldLabelst_perm_dana_close["English"]["status"] = "Status DP";
	$fieldToolTipst_perm_dana_close["English"]["status"] = "";
	$placeHolderst_perm_dana_close["English"]["status"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_dp_by"] = "Close Dp By";
	$fieldToolTipst_perm_dana_close["English"]["close_dp_by"] = "";
	$placeHolderst_perm_dana_close["English"]["close_dp_by"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_dp_date"] = "Close Dp Date";
	$fieldToolTipst_perm_dana_close["English"]["close_dp_date"] = "";
	$placeHolderst_perm_dana_close["English"]["close_dp_date"] = "";
	$fieldLabelst_perm_dana_close["English"]["created_date"] = "Created Date";
	$fieldToolTipst_perm_dana_close["English"]["created_date"] = "";
	$placeHolderst_perm_dana_close["English"]["created_date"] = "";
	$fieldLabelst_perm_dana_close["English"]["created_by"] = "Created By";
	$fieldToolTipst_perm_dana_close["English"]["created_by"] = "";
	$placeHolderst_perm_dana_close["English"]["created_by"] = "";
	$fieldLabelst_perm_dana_close["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_perm_dana_close["English"]["updated_date"] = "";
	$placeHolderst_perm_dana_close["English"]["updated_date"] = "";
	$fieldLabelst_perm_dana_close["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_perm_dana_close["English"]["updated_by"] = "";
	$placeHolderst_perm_dana_close["English"]["updated_by"] = "";
	$fieldLabelst_perm_dana_close["English"]["nama_lahan"] = "Nama Lahan";
	$fieldToolTipst_perm_dana_close["English"]["nama_lahan"] = "";
	$placeHolderst_perm_dana_close["English"]["nama_lahan"] = "";
	$fieldLabelst_perm_dana_close["English"]["tipe"] = "Tipe";
	$fieldToolTipst_perm_dana_close["English"]["tipe"] = "";
	$placeHolderst_perm_dana_close["English"]["tipe"] = "";
	$fieldLabelst_perm_dana_close["English"]["dp"] = "Down Payment";
	$fieldToolTipst_perm_dana_close["English"]["dp"] = "";
	$placeHolderst_perm_dana_close["English"]["dp"] = "";
	$fieldLabelst_perm_dana_close["English"]["sisa_pembayaran"] = "Sisa Pembayaran";
	$fieldToolTipst_perm_dana_close["English"]["sisa_pembayaran"] = "";
	$placeHolderst_perm_dana_close["English"]["sisa_pembayaran"] = "";
	$fieldLabelst_perm_dana_close["English"]["biaya_admin"] = "Biaya Admin";
	$fieldToolTipst_perm_dana_close["English"]["biaya_admin"] = "";
	$placeHolderst_perm_dana_close["English"]["biaya_admin"] = "";
	$fieldLabelst_perm_dana_close["English"]["status_full"] = "Status Full";
	$fieldToolTipst_perm_dana_close["English"]["status_full"] = "";
	$placeHolderst_perm_dana_close["English"]["status_full"] = "";
	$fieldLabelst_perm_dana_close["English"]["keterangan_bayar"] = "Keterangan Bayar";
	$fieldToolTipst_perm_dana_close["English"]["keterangan_bayar"] = "";
	$placeHolderst_perm_dana_close["English"]["keterangan_bayar"] = "";
	$fieldLabelst_perm_dana_close["English"]["status_badmin"] = "Status Badmin";
	$fieldToolTipst_perm_dana_close["English"]["status_badmin"] = "";
	$placeHolderst_perm_dana_close["English"]["status_badmin"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_badmin_by"] = "Close Badmin By";
	$fieldToolTipst_perm_dana_close["English"]["close_badmin_by"] = "";
	$placeHolderst_perm_dana_close["English"]["close_badmin_by"] = "";
	$fieldLabelst_perm_dana_close["English"]["close_badmin_date"] = "Close Badmin Date";
	$fieldToolTipst_perm_dana_close["English"]["close_badmin_date"] = "";
	$placeHolderst_perm_dana_close["English"]["close_badmin_date"] = "";
	$pageTitlest_perm_dana_close["English"]["edit"] = "Pembayaran Close, Edit [{%nama_lahan}]";
	$pageTitlest_perm_dana_close["English"]["view"] = "Pembayaran Close {%nama_lahan}";
	$pageTitlest_perm_dana_close["English"]["search"] = "Pembayaran Close, Advanced search";
	$pageTitlest_perm_dana_close["English"]["masterlist"] = "Pembayaran Close {%nama_lahan}";
	if (count($fieldToolTipst_perm_dana_close["English"]))
		$tdatat_perm_dana_close[".isUseToolTips"] = true;
}


	$tdatat_perm_dana_close[".NCSearch"] = true;



$tdatat_perm_dana_close[".shortTableName"] = "t_perm_dana_close";
$tdatat_perm_dana_close[".nSecOptions"] = 0;

$tdatat_perm_dana_close[".mainTableOwnerID"] = "";
$tdatat_perm_dana_close[".entityType"] = 1;
$tdatat_perm_dana_close[".connId"] = "db_lla_at_localhost";


$tdatat_perm_dana_close[".strOriginalTableName"] = "t_perm_dana";

		 



$tdatat_perm_dana_close[".showAddInPopup"] = false;

$tdatat_perm_dana_close[".showEditInPopup"] = false;

$tdatat_perm_dana_close[".showViewInPopup"] = false;

$tdatat_perm_dana_close[".listAjax"] = false;
//	temporary
//$tdatat_perm_dana_close[".listAjax"] = false;

	$tdatat_perm_dana_close[".audit"] = false;

	$tdatat_perm_dana_close[".locking"] = false;


$pages = $tdatat_perm_dana_close[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_perm_dana_close[".edit"] = true;
	$tdatat_perm_dana_close[".afterEditAction"] = 1;
	$tdatat_perm_dana_close[".closePopupAfterEdit"] = 1;
	$tdatat_perm_dana_close[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_perm_dana_close[".add"] = true;
$tdatat_perm_dana_close[".afterAddAction"] = 1;
$tdatat_perm_dana_close[".closePopupAfterAdd"] = 1;
$tdatat_perm_dana_close[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_perm_dana_close[".list"] = true;
}



$tdatat_perm_dana_close[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_perm_dana_close[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_perm_dana_close[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_perm_dana_close[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_perm_dana_close[".printFriendly"] = true;
}



$tdatat_perm_dana_close[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_perm_dana_close[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_perm_dana_close[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_perm_dana_close[".isUseAjaxSuggest"] = true;

$tdatat_perm_dana_close[".rowHighlite"] = true;



			

$tdatat_perm_dana_close[".ajaxCodeSnippetAdded"] = false;

$tdatat_perm_dana_close[".buttonsAdded"] = false;

$tdatat_perm_dana_close[".addPageEvents"] = true;

// use timepicker for search panel
$tdatat_perm_dana_close[".isUseTimeForSearch"] = false;


$tdatat_perm_dana_close[".badgeColor"] = "00C2C5";


$tdatat_perm_dana_close[".allSearchFields"] = array();
$tdatat_perm_dana_close[".filterFields"] = array();
$tdatat_perm_dana_close[".requiredSearchFields"] = array();

$tdatat_perm_dana_close[".googleLikeFields"] = array();
$tdatat_perm_dana_close[".googleLikeFields"][] = "id";
$tdatat_perm_dana_close[".googleLikeFields"][] = "id_lahan";
$tdatat_perm_dana_close[".googleLikeFields"][] = "tipe";
$tdatat_perm_dana_close[".googleLikeFields"][] = "dp";
$tdatat_perm_dana_close[".googleLikeFields"][] = "sisa_pembayaran";
$tdatat_perm_dana_close[".googleLikeFields"][] = "biaya_admin";
$tdatat_perm_dana_close[".googleLikeFields"][] = "start_date_dp";
$tdatat_perm_dana_close[".googleLikeFields"][] = "due_date_dp";
$tdatat_perm_dana_close[".googleLikeFields"][] = "start_date_full";
$tdatat_perm_dana_close[".googleLikeFields"][] = "due_date_full";
$tdatat_perm_dana_close[".googleLikeFields"][] = "pic";
$tdatat_perm_dana_close[".googleLikeFields"][] = "keterangan";
$tdatat_perm_dana_close[".googleLikeFields"][] = "keterangan_bayar";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_full_by";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_full_date";
$tdatat_perm_dana_close[".googleLikeFields"][] = "status";
$tdatat_perm_dana_close[".googleLikeFields"][] = "status_full";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_dp_by";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_dp_date";
$tdatat_perm_dana_close[".googleLikeFields"][] = "status_badmin";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_badmin_by";
$tdatat_perm_dana_close[".googleLikeFields"][] = "close_badmin_date";
$tdatat_perm_dana_close[".googleLikeFields"][] = "created_date";
$tdatat_perm_dana_close[".googleLikeFields"][] = "created_by";
$tdatat_perm_dana_close[".googleLikeFields"][] = "updated_date";
$tdatat_perm_dana_close[".googleLikeFields"][] = "updated_by";
$tdatat_perm_dana_close[".googleLikeFields"][] = "nama_lahan";



$tdatat_perm_dana_close[".tableType"] = "list";

$tdatat_perm_dana_close[".printerPageOrientation"] = 0;
$tdatat_perm_dana_close[".nPrinterPageScale"] = 100;

$tdatat_perm_dana_close[".nPrinterSplitRecords"] = 40;

$tdatat_perm_dana_close[".geocodingEnabled"] = false;










$tdatat_perm_dana_close[".pageSize"] = 20;

$tdatat_perm_dana_close[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_perm_dana_close[".strOrderBy"] = $tstrOrderBy;

$tdatat_perm_dana_close[".orderindexes"] = array();


$tdatat_perm_dana_close[".sqlHead"] = "SELECT t_perm_dana.id,  t_perm_dana.id_lahan,  t_perm_dana.tipe,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.biaya_admin,  t_perm_dana.start_date_dp,  t_perm_dana.due_date_dp,  t_perm_dana.start_date_full,  t_perm_dana.due_date_full,  t_perm_dana.pic,  t_perm_dana.keterangan,  t_perm_dana.keterangan_bayar,  t_perm_dana.close_full_by,  t_perm_dana.close_full_date,  t_perm_dana.status,  t_perm_dana.status_full,  t_perm_dana.close_dp_by,  t_perm_dana.close_dp_date,  t_perm_dana.status_badmin,  t_perm_dana.close_badmin_by,  t_perm_dana.close_badmin_date,  t_perm_dana.created_date,  t_perm_dana.created_by,  t_perm_dana.updated_date,  t_perm_dana.updated_by,  t_input_awal.nama_lahan";
$tdatat_perm_dana_close[".sqlFrom"] = "FROM t_perm_dana  INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$tdatat_perm_dana_close[".sqlWhereExpr"] = "";
$tdatat_perm_dana_close[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_perm_dana_close[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_perm_dana_close[".arrGroupsPerPage"] = $arrGPP;

$tdatat_perm_dana_close[".highlightSearchResults"] = true;

$tableKeyst_perm_dana_close = array();
$tableKeyst_perm_dana_close[] = "id";
$tdatat_perm_dana_close[".Keys"] = $tableKeyst_perm_dana_close;


$tdatat_perm_dana_close[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["id"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "id";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

		$fdata["sourceSingle"] = "id_lahan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["id_lahan"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "id_lahan";
//	tipe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "tipe";
	$fdata["GoodName"] = "tipe";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","tipe");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "tipe";

		$fdata["sourceSingle"] = "tipe";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.tipe";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "m_code_list";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Code";

				$edata["LookupWhere"] = "CatID='TYPEP'";


	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["tipe"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "tipe";
//	dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "dp";
	$fdata["GoodName"] = "dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","dp");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "dp";

		$fdata["sourceSingle"] = "dp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["dp"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "dp";
//	sisa_pembayaran
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "sisa_pembayaran";
	$fdata["GoodName"] = "sisa_pembayaran";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","sisa_pembayaran");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "sisa_pembayaran";

		$fdata["sourceSingle"] = "sisa_pembayaran";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.sisa_pembayaran";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["sisa_pembayaran"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "sisa_pembayaran";
//	biaya_admin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "biaya_admin";
	$fdata["GoodName"] = "biaya_admin";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","biaya_admin");
	$fdata["FieldType"] = 14;


	
	
										

		$fdata["strField"] = "biaya_admin";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.biaya_admin";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "Number");

	
	
	
		
		
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["biaya_admin"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "biaya_admin";
//	start_date_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "start_date_dp";
	$fdata["GoodName"] = "start_date_dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","start_date_dp");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date_dp";

		$fdata["sourceSingle"] = "start_date_dp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.start_date_dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["start_date_dp"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "start_date_dp";
//	due_date_dp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "due_date_dp";
	$fdata["GoodName"] = "due_date_dp";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","due_date_dp");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date_dp";

		$fdata["sourceSingle"] = "due_date_dp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.due_date_dp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["due_date_dp"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "due_date_dp";
//	start_date_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "start_date_full";
	$fdata["GoodName"] = "start_date_full";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","start_date_full");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "start_date_full";

		$fdata["sourceSingle"] = "start_date_full";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.start_date_full";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["start_date_full"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "start_date_full";
//	due_date_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "due_date_full";
	$fdata["GoodName"] = "due_date_full";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","due_date_full");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "due_date_full";

		$fdata["sourceSingle"] = "due_date_full";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.due_date_full";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 5;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 5;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["due_date_full"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "due_date_full";
//	pic
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "pic";
	$fdata["GoodName"] = "pic";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","pic");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "pic";

		$fdata["sourceSingle"] = "pic";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.pic";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["pic"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "pic";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["keterangan"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "keterangan";
//	keterangan_bayar
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "keterangan_bayar";
	$fdata["GoodName"] = "keterangan_bayar";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","keterangan_bayar");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan_bayar";

		$fdata["sourceSingle"] = "keterangan_bayar";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.keterangan_bayar";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["keterangan_bayar"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "keterangan_bayar";
//	close_full_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "close_full_by";
	$fdata["GoodName"] = "close_full_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_full_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_full_by";

		$fdata["sourceSingle"] = "close_full_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_full_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_full_by"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_full_by";
//	close_full_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "close_full_date";
	$fdata["GoodName"] = "close_full_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_full_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_full_date";

		$fdata["sourceSingle"] = "close_full_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_full_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_full_date"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_full_date";
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","status");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status";

		$fdata["sourceSingle"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["status"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "status";
//	status_full
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "status_full";
	$fdata["GoodName"] = "status_full";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","status_full");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_full";

		$fdata["sourceSingle"] = "status_full";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.status_full";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "Description";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["status_full"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "status_full";
//	close_dp_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "close_dp_by";
	$fdata["GoodName"] = "close_dp_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_dp_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_dp_by";

		$fdata["sourceSingle"] = "close_dp_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_dp_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_dp_by"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_dp_by";
//	close_dp_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "close_dp_date";
	$fdata["GoodName"] = "close_dp_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_dp_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_dp_date";

		$fdata["sourceSingle"] = "close_dp_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_dp_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_dp_date"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_dp_date";
//	status_badmin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "status_badmin";
	$fdata["GoodName"] = "status_badmin";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","status_badmin");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "status_badmin";

		$fdata["sourceSingle"] = "status_badmin";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.status_badmin";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_status";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "Description";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["status_badmin"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "status_badmin";
//	close_badmin_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "close_badmin_by";
	$fdata["GoodName"] = "close_badmin_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_badmin_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "close_badmin_by";

		$fdata["sourceSingle"] = "close_badmin_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_badmin_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_badmin_by"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_badmin_by";
//	close_badmin_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "close_badmin_date";
	$fdata["GoodName"] = "close_badmin_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","close_badmin_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "close_badmin_date";

		$fdata["sourceSingle"] = "close_badmin_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.close_badmin_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["close_badmin_date"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "close_badmin_date";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["created_date"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["created_by"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["updated_date"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["updated_by"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "updated_by";
//	nama_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "nama_lahan";
	$fdata["GoodName"] = "nama_lahan";
	$fdata["ownerTable"] = "t_input_awal";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_close","nama_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "nama_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_input_awal.nama_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_close["nama_lahan"] = $fdata;
		$tdatat_perm_dana_close[".searchableFields"][] = "nama_lahan";


$tables_data["t_perm_dana_close"]=&$tdatat_perm_dana_close;
$field_labels["t_perm_dana_close"] = &$fieldLabelst_perm_dana_close;
$fieldToolTips["t_perm_dana_close"] = &$fieldToolTipst_perm_dana_close;
$placeHolders["t_perm_dana_close"] = &$placeHolderst_perm_dana_close;
$page_titles["t_perm_dana_close"] = &$pageTitlest_perm_dana_close;


changeTextControlsToDate( "t_perm_dana_close" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_perm_dana_close"] = array();
//	t_pembayaran_detail
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_pembayaran_detail";
		$detailsParam["dOriginalTable"] = "t_pembayaran_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_pembayaran_detail";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_pembayaran_detail");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_perm_dana_close"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_perm_dana_close"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_perm_dana_close"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["t_perm_dana_close"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_perm_dana_close"][$dIndex]["detailKeys"][]="id_p_dana";
//	t_perm_dana_detail_bayar
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="t_perm_dana_detail_bayar";
		$detailsParam["dOriginalTable"] = "t_perm_dana_detail";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "t_perm_dana_detail_bayar";
	$detailsParam["dCaptionTable"] = GetTableCaption("t_perm_dana_detail_bayar");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["t_perm_dana_close"][$dIndex] = $detailsParam;

	
		$detailsTablesData["t_perm_dana_close"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["t_perm_dana_close"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["t_perm_dana_close"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["t_perm_dana_close"][$dIndex]["detailKeys"][]="id_p_dana";
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_perm_dana_close"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_perm_dana_close()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_perm_dana.id,  t_perm_dana.id_lahan,  t_perm_dana.tipe,  t_perm_dana.dp,  t_perm_dana.sisa_pembayaran,  t_perm_dana.biaya_admin,  t_perm_dana.start_date_dp,  t_perm_dana.due_date_dp,  t_perm_dana.start_date_full,  t_perm_dana.due_date_full,  t_perm_dana.pic,  t_perm_dana.keterangan,  t_perm_dana.keterangan_bayar,  t_perm_dana.close_full_by,  t_perm_dana.close_full_date,  t_perm_dana.status,  t_perm_dana.status_full,  t_perm_dana.close_dp_by,  t_perm_dana.close_dp_date,  t_perm_dana.status_badmin,  t_perm_dana.close_badmin_by,  t_perm_dana.close_badmin_date,  t_perm_dana.created_date,  t_perm_dana.created_by,  t_perm_dana.updated_date,  t_perm_dana.updated_by,  t_input_awal.nama_lahan";
$proto0["m_strFrom"] = "FROM t_perm_dana  INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto6["m_sql"] = "t_perm_dana.id";
$proto6["m_srcTableName"] = "t_perm_dana_close";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto8["m_sql"] = "t_perm_dana.id_lahan";
$proto8["m_srcTableName"] = "t_perm_dana_close";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "tipe",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto10["m_sql"] = "t_perm_dana.tipe";
$proto10["m_srcTableName"] = "t_perm_dana_close";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto12["m_sql"] = "t_perm_dana.dp";
$proto12["m_srcTableName"] = "t_perm_dana_close";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "sisa_pembayaran",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto14["m_sql"] = "t_perm_dana.sisa_pembayaran";
$proto14["m_srcTableName"] = "t_perm_dana_close";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "biaya_admin",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto16["m_sql"] = "t_perm_dana.biaya_admin";
$proto16["m_srcTableName"] = "t_perm_dana_close";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date_dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto18["m_sql"] = "t_perm_dana.start_date_dp";
$proto18["m_srcTableName"] = "t_perm_dana_close";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date_dp",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto20["m_sql"] = "t_perm_dana.due_date_dp";
$proto20["m_srcTableName"] = "t_perm_dana_close";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "start_date_full",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto22["m_sql"] = "t_perm_dana.start_date_full";
$proto22["m_srcTableName"] = "t_perm_dana_close";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "due_date_full",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto24["m_sql"] = "t_perm_dana.due_date_full";
$proto24["m_srcTableName"] = "t_perm_dana_close";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "pic",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto26["m_sql"] = "t_perm_dana.pic";
$proto26["m_srcTableName"] = "t_perm_dana_close";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto28["m_sql"] = "t_perm_dana.keterangan";
$proto28["m_srcTableName"] = "t_perm_dana_close";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan_bayar",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto30["m_sql"] = "t_perm_dana.keterangan_bayar";
$proto30["m_srcTableName"] = "t_perm_dana_close";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "close_full_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto32["m_sql"] = "t_perm_dana.close_full_by";
$proto32["m_srcTableName"] = "t_perm_dana_close";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "close_full_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto34["m_sql"] = "t_perm_dana.close_full_date";
$proto34["m_srcTableName"] = "t_perm_dana_close";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto36["m_sql"] = "t_perm_dana.status";
$proto36["m_srcTableName"] = "t_perm_dana_close";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "status_full",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto38["m_sql"] = "t_perm_dana.status_full";
$proto38["m_srcTableName"] = "t_perm_dana_close";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "close_dp_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto40["m_sql"] = "t_perm_dana.close_dp_by";
$proto40["m_srcTableName"] = "t_perm_dana_close";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "close_dp_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto42["m_sql"] = "t_perm_dana.close_dp_date";
$proto42["m_srcTableName"] = "t_perm_dana_close";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "status_badmin",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto44["m_sql"] = "t_perm_dana.status_badmin";
$proto44["m_srcTableName"] = "t_perm_dana_close";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "close_badmin_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto46["m_sql"] = "t_perm_dana.close_badmin_by";
$proto46["m_srcTableName"] = "t_perm_dana_close";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "close_badmin_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto48["m_sql"] = "t_perm_dana.close_badmin_date";
$proto48["m_srcTableName"] = "t_perm_dana_close";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto50["m_sql"] = "t_perm_dana.created_date";
$proto50["m_srcTableName"] = "t_perm_dana_close";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto52["m_sql"] = "t_perm_dana.created_by";
$proto52["m_srcTableName"] = "t_perm_dana_close";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto54["m_sql"] = "t_perm_dana.updated_date";
$proto54["m_srcTableName"] = "t_perm_dana_close";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto56["m_sql"] = "t_perm_dana.updated_by";
$proto56["m_srcTableName"] = "t_perm_dana_close";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "nama_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto58["m_sql"] = "t_input_awal.nama_lahan";
$proto58["m_srcTableName"] = "t_perm_dana_close";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto60=array();
$proto60["m_link"] = "SQLL_MAIN";
			$proto61=array();
$proto61["m_strName"] = "t_perm_dana";
$proto61["m_srcTableName"] = "t_perm_dana_close";
$proto61["m_columns"] = array();
$proto61["m_columns"][] = "id";
$proto61["m_columns"][] = "id_lahan";
$proto61["m_columns"][] = "start_date_dp";
$proto61["m_columns"][] = "due_date_dp";
$proto61["m_columns"][] = "start_date_full";
$proto61["m_columns"][] = "due_date_full";
$proto61["m_columns"][] = "pic";
$proto61["m_columns"][] = "keterangan";
$proto61["m_columns"][] = "keterangan_bayar";
$proto61["m_columns"][] = "close_full_by";
$proto61["m_columns"][] = "close_full_date";
$proto61["m_columns"][] = "tipe";
$proto61["m_columns"][] = "dp";
$proto61["m_columns"][] = "sisa_pembayaran";
$proto61["m_columns"][] = "biaya_admin";
$proto61["m_columns"][] = "status";
$proto61["m_columns"][] = "status_full";
$proto61["m_columns"][] = "status_badmin";
$proto61["m_columns"][] = "close_badmin_by";
$proto61["m_columns"][] = "close_badmin_date";
$proto61["m_columns"][] = "close_dp_by";
$proto61["m_columns"][] = "close_dp_date";
$proto61["m_columns"][] = "created_date";
$proto61["m_columns"][] = "created_by";
$proto61["m_columns"][] = "updated_date";
$proto61["m_columns"][] = "updated_by";
$obj = new SQLTable($proto61);

$proto60["m_table"] = $obj;
$proto60["m_sql"] = "t_perm_dana";
$proto60["m_alias"] = "";
$proto60["m_srcTableName"] = "t_perm_dana_close";
$proto62=array();
$proto62["m_sql"] = "";
$proto62["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto62["m_column"]=$obj;
$proto62["m_contained"] = array();
$proto62["m_strCase"] = "";
$proto62["m_havingmode"] = false;
$proto62["m_inBrackets"] = false;
$proto62["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto62);

$proto60["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto60);

$proto0["m_fromlist"][]=$obj;
												$proto64=array();
$proto64["m_link"] = "SQLL_INNERJOIN";
			$proto65=array();
$proto65["m_strName"] = "t_input_awal";
$proto65["m_srcTableName"] = "t_perm_dana_close";
$proto65["m_columns"] = array();
$proto65["m_columns"][] = "id";
$proto65["m_columns"][] = "id_lahan";
$proto65["m_columns"][] = "nama_lahan";
$proto65["m_columns"][] = "batas_utara";
$proto65["m_columns"][] = "batas_selatan";
$proto65["m_columns"][] = "batas_barat";
$proto65["m_columns"][] = "batas_timur";
$proto65["m_columns"][] = "blok_lokasi";
$proto65["m_columns"][] = "nama_lokasi";
$proto65["m_columns"][] = "keperluan_lahan";
$proto65["m_columns"][] = "start_date";
$proto65["m_columns"][] = "due_date";
$proto65["m_columns"][] = "pic";
$proto65["m_columns"][] = "status";
$proto65["m_columns"][] = "close_by";
$proto65["m_columns"][] = "close_date";
$proto65["m_columns"][] = "keterangan";
$proto65["m_columns"][] = "im_file";
$proto65["m_columns"][] = "created_date";
$proto65["m_columns"][] = "created_by";
$proto65["m_columns"][] = "updated_date";
$proto65["m_columns"][] = "updated_by";
$obj = new SQLTable($proto65);

$proto64["m_table"] = $obj;
$proto64["m_sql"] = "INNER JOIN t_input_awal ON t_perm_dana.id_lahan = t_input_awal.id_lahan";
$proto64["m_alias"] = "";
$proto64["m_srcTableName"] = "t_perm_dana_close";
$proto66=array();
$proto66["m_sql"] = "t_input_awal.id_lahan = t_perm_dana.id_lahan";
$proto66["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_input_awal",
	"m_srcTableName" => "t_perm_dana_close"
));

$proto66["m_column"]=$obj;
$proto66["m_contained"] = array();
$proto66["m_strCase"] = "= t_perm_dana.id_lahan";
$proto66["m_havingmode"] = false;
$proto66["m_inBrackets"] = false;
$proto66["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto66);

$proto64["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto64);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_perm_dana_close";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_perm_dana_close = createSqlQuery_t_perm_dana_close();


	
					
;

																											

$tdatat_perm_dana_close[".sqlquery"] = $queryData_t_perm_dana_close;



include_once(getabspath("include/t_perm_dana_close_events.php"));
$tdatat_perm_dana_close[".hasEvents"] = true;

?>