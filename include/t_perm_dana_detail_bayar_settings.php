<?php
$tdatat_perm_dana_detail_bayar = array();
$tdatat_perm_dana_detail_bayar[".searchableFields"] = array();
$tdatat_perm_dana_detail_bayar[".ShortName"] = "t_perm_dana_detail_bayar";
$tdatat_perm_dana_detail_bayar[".OwnerID"] = "";
$tdatat_perm_dana_detail_bayar[".OriginalTable"] = "t_perm_dana_detail";


$tdatat_perm_dana_detail_bayar[".pagesByType"] = my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print1\"],\"search\":[\"search\"]}" );
$tdatat_perm_dana_detail_bayar[".originalPagesByType"] = $tdatat_perm_dana_detail_bayar[".pagesByType"];
$tdatat_perm_dana_detail_bayar[".pages"] = types2pages( my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print1\"],\"search\":[\"search\"]}" ) );
$tdatat_perm_dana_detail_bayar[".originalPages"] = $tdatat_perm_dana_detail_bayar[".pages"];
$tdatat_perm_dana_detail_bayar[".defaultPages"] = my_json_decode( "{\"list\":\"list\",\"print\":\"print1\",\"search\":\"search\"}" );
$tdatat_perm_dana_detail_bayar[".originalDefaultPages"] = $tdatat_perm_dana_detail_bayar[".defaultPages"];

//	field labels
$fieldLabelst_perm_dana_detail_bayar = array();
$fieldToolTipst_perm_dana_detail_bayar = array();
$pageTitlest_perm_dana_detail_bayar = array();
$placeHolderst_perm_dana_detail_bayar = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_perm_dana_detail_bayar["English"] = array();
	$fieldToolTipst_perm_dana_detail_bayar["English"] = array();
	$placeHolderst_perm_dana_detail_bayar["English"] = array();
	$pageTitlest_perm_dana_detail_bayar["English"] = array();
	$fieldLabelst_perm_dana_detail_bayar["English"]["id"] = "Id";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["id"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["id"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["id_p_dana"] = "Id P Dana";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["id_p_dana"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["id_p_dana"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["param_dok"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["param_dok"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["file_dok"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["file_dok"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["keterangan"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["keterangan"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["created_date"] = "Created Date";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["created_date"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["created_date"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["created_by"] = "Created By";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["created_by"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["created_by"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["updated_date"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["updated_date"] = "";
	$fieldLabelst_perm_dana_detail_bayar["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_perm_dana_detail_bayar["English"]["updated_by"] = "";
	$placeHolderst_perm_dana_detail_bayar["English"]["updated_by"] = "";
	if (count($fieldToolTipst_perm_dana_detail_bayar["English"]))
		$tdatat_perm_dana_detail_bayar[".isUseToolTips"] = true;
}


	$tdatat_perm_dana_detail_bayar[".NCSearch"] = true;



$tdatat_perm_dana_detail_bayar[".shortTableName"] = "t_perm_dana_detail_bayar";
$tdatat_perm_dana_detail_bayar[".nSecOptions"] = 0;

$tdatat_perm_dana_detail_bayar[".mainTableOwnerID"] = "";
$tdatat_perm_dana_detail_bayar[".entityType"] = 1;
$tdatat_perm_dana_detail_bayar[".connId"] = "db_lla_at_localhost";


$tdatat_perm_dana_detail_bayar[".strOriginalTableName"] = "t_perm_dana_detail";

		 



$tdatat_perm_dana_detail_bayar[".showAddInPopup"] = false;

$tdatat_perm_dana_detail_bayar[".showEditInPopup"] = false;

$tdatat_perm_dana_detail_bayar[".showViewInPopup"] = false;

$tdatat_perm_dana_detail_bayar[".listAjax"] = false;
//	temporary
//$tdatat_perm_dana_detail_bayar[".listAjax"] = false;

	$tdatat_perm_dana_detail_bayar[".audit"] = false;

	$tdatat_perm_dana_detail_bayar[".locking"] = false;


$pages = $tdatat_perm_dana_detail_bayar[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_perm_dana_detail_bayar[".edit"] = true;
	$tdatat_perm_dana_detail_bayar[".afterEditAction"] = 1;
	$tdatat_perm_dana_detail_bayar[".closePopupAfterEdit"] = 1;
	$tdatat_perm_dana_detail_bayar[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_perm_dana_detail_bayar[".add"] = true;
$tdatat_perm_dana_detail_bayar[".afterAddAction"] = 1;
$tdatat_perm_dana_detail_bayar[".closePopupAfterAdd"] = 1;
$tdatat_perm_dana_detail_bayar[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_perm_dana_detail_bayar[".list"] = true;
}



$tdatat_perm_dana_detail_bayar[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_perm_dana_detail_bayar[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_perm_dana_detail_bayar[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_perm_dana_detail_bayar[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_perm_dana_detail_bayar[".printFriendly"] = true;
}



$tdatat_perm_dana_detail_bayar[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_perm_dana_detail_bayar[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_perm_dana_detail_bayar[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_perm_dana_detail_bayar[".isUseAjaxSuggest"] = true;

$tdatat_perm_dana_detail_bayar[".rowHighlite"] = true;



						

$tdatat_perm_dana_detail_bayar[".ajaxCodeSnippetAdded"] = false;

$tdatat_perm_dana_detail_bayar[".buttonsAdded"] = false;

$tdatat_perm_dana_detail_bayar[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_perm_dana_detail_bayar[".isUseTimeForSearch"] = false;


$tdatat_perm_dana_detail_bayar[".badgeColor"] = "9acd32";


$tdatat_perm_dana_detail_bayar[".allSearchFields"] = array();
$tdatat_perm_dana_detail_bayar[".filterFields"] = array();
$tdatat_perm_dana_detail_bayar[".requiredSearchFields"] = array();

$tdatat_perm_dana_detail_bayar[".googleLikeFields"] = array();
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "id";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "id_p_dana";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "param_dok";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "file_dok";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "keterangan";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "created_date";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "created_by";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "updated_date";
$tdatat_perm_dana_detail_bayar[".googleLikeFields"][] = "updated_by";



$tdatat_perm_dana_detail_bayar[".tableType"] = "list";

$tdatat_perm_dana_detail_bayar[".printerPageOrientation"] = 0;
$tdatat_perm_dana_detail_bayar[".nPrinterPageScale"] = 100;

$tdatat_perm_dana_detail_bayar[".nPrinterSplitRecords"] = 40;

$tdatat_perm_dana_detail_bayar[".geocodingEnabled"] = false;










$tdatat_perm_dana_detail_bayar[".pageSize"] = 20;

$tdatat_perm_dana_detail_bayar[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_perm_dana_detail_bayar[".strOrderBy"] = $tstrOrderBy;

$tdatat_perm_dana_detail_bayar[".orderindexes"] = array();


$tdatat_perm_dana_detail_bayar[".sqlHead"] = "SELECT id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_perm_dana_detail_bayar[".sqlFrom"] = "FROM t_perm_dana_detail";
$tdatat_perm_dana_detail_bayar[".sqlWhereExpr"] = "";
$tdatat_perm_dana_detail_bayar[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_perm_dana_detail_bayar[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_perm_dana_detail_bayar[".arrGroupsPerPage"] = $arrGPP;

$tdatat_perm_dana_detail_bayar[".highlightSearchResults"] = true;

$tableKeyst_perm_dana_detail_bayar = array();
$tableKeyst_perm_dana_detail_bayar[] = "id";
$tdatat_perm_dana_detail_bayar[".Keys"] = $tableKeyst_perm_dana_detail_bayar;


$tdatat_perm_dana_detail_bayar[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["id"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "id";
//	id_p_dana
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_p_dana";
	$fdata["GoodName"] = "id_p_dana";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","id_p_dana");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_p_dana";

		$fdata["sourceSingle"] = "id_p_dana";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_p_dana";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["id_p_dana"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "id_p_dana";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["param_dok"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["file_dok"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["keterangan"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["created_date"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["created_by"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["updated_date"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_bayar","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_bayar["updated_by"] = $fdata;
		$tdatat_perm_dana_detail_bayar[".searchableFields"][] = "updated_by";


$tables_data["t_perm_dana_detail_bayar"]=&$tdatat_perm_dana_detail_bayar;
$field_labels["t_perm_dana_detail_bayar"] = &$fieldLabelst_perm_dana_detail_bayar;
$fieldToolTips["t_perm_dana_detail_bayar"] = &$fieldToolTipst_perm_dana_detail_bayar;
$placeHolders["t_perm_dana_detail_bayar"] = &$placeHolderst_perm_dana_detail_bayar;
$page_titles["t_perm_dana_detail_bayar"] = &$pageTitlest_perm_dana_detail_bayar;


changeTextControlsToDate( "t_perm_dana_detail_bayar" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_perm_dana_detail_bayar"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_perm_dana_detail_bayar"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_perm_dana";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_perm_dana_close";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_perm_dana_close";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_perm_dana_detail_bayar"][0] = $masterParams;
				$masterTablesData["t_perm_dana_detail_bayar"][0]["masterKeys"] = array();
	$masterTablesData["t_perm_dana_detail_bayar"][0]["masterKeys"][]="id";
				$masterTablesData["t_perm_dana_detail_bayar"][0]["detailKeys"] = array();
	$masterTablesData["t_perm_dana_detail_bayar"][0]["detailKeys"][]="id_p_dana";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_perm_dana_detail_bayar()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_perm_dana_detail";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_p_dana",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto8["m_sql"] = "id_p_dana";
$proto8["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto10["m_sql"] = "param_dok";
$proto10["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto12["m_sql"] = "file_dok";
$proto12["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto14["m_sql"] = "keterangan";
$proto14["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto16["m_sql"] = "created_date";
$proto16["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto18["m_sql"] = "created_by";
$proto18["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto20["m_sql"] = "updated_date";
$proto20["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_bayar"
));

$proto22["m_sql"] = "updated_by";
$proto22["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "t_perm_dana_detail";
$proto25["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "id";
$proto25["m_columns"][] = "id_p_dana";
$proto25["m_columns"][] = "param_dok";
$proto25["m_columns"][] = "file_dok";
$proto25["m_columns"][] = "keterangan";
$proto25["m_columns"][] = "created_date";
$proto25["m_columns"][] = "created_by";
$proto25["m_columns"][] = "updated_date";
$proto25["m_columns"][] = "updated_by";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "t_perm_dana_detail";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "t_perm_dana_detail_bayar";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_perm_dana_detail_bayar";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_perm_dana_detail_bayar = createSqlQuery_t_perm_dana_detail_bayar();


	
					
;

									

$tdatat_perm_dana_detail_bayar[".sqlquery"] = $queryData_t_perm_dana_detail_bayar;



include_once(getabspath("include/t_perm_dana_detail_bayar_events.php"));
$tdatat_perm_dana_detail_bayar[".hasEvents"] = true;

?>