<?php
$tdatat_perm_dana_detail_dok = array();
$tdatat_perm_dana_detail_dok[".searchableFields"] = array();
$tdatat_perm_dana_detail_dok[".ShortName"] = "t_perm_dana_detail_dok";
$tdatat_perm_dana_detail_dok[".OwnerID"] = "";
$tdatat_perm_dana_detail_dok[".OriginalTable"] = "t_perm_dana_detail";


$tdatat_perm_dana_detail_dok[".pagesByType"] = my_json_decode( "{\"list\":[\"list\"],\"search\":[\"search\"]}" );
$tdatat_perm_dana_detail_dok[".originalPagesByType"] = $tdatat_perm_dana_detail_dok[".pagesByType"];
$tdatat_perm_dana_detail_dok[".pages"] = types2pages( my_json_decode( "{\"list\":[\"list\"],\"search\":[\"search\"]}" ) );
$tdatat_perm_dana_detail_dok[".originalPages"] = $tdatat_perm_dana_detail_dok[".pages"];
$tdatat_perm_dana_detail_dok[".defaultPages"] = my_json_decode( "{\"list\":\"list\",\"search\":\"search\"}" );
$tdatat_perm_dana_detail_dok[".originalDefaultPages"] = $tdatat_perm_dana_detail_dok[".defaultPages"];

//	field labels
$fieldLabelst_perm_dana_detail_dok = array();
$fieldToolTipst_perm_dana_detail_dok = array();
$pageTitlest_perm_dana_detail_dok = array();
$placeHolderst_perm_dana_detail_dok = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_perm_dana_detail_dok["English"] = array();
	$fieldToolTipst_perm_dana_detail_dok["English"] = array();
	$placeHolderst_perm_dana_detail_dok["English"] = array();
	$pageTitlest_perm_dana_detail_dok["English"] = array();
	$fieldLabelst_perm_dana_detail_dok["English"]["id"] = "Id";
	$fieldToolTipst_perm_dana_detail_dok["English"]["id"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["id"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["id_p_dana"] = "Id P Dana";
	$fieldToolTipst_perm_dana_detail_dok["English"]["id_p_dana"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["id_p_dana"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_perm_dana_detail_dok["English"]["param_dok"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["param_dok"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_perm_dana_detail_dok["English"]["file_dok"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["file_dok"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_perm_dana_detail_dok["English"]["keterangan"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["keterangan"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["created_date"] = "Created Date";
	$fieldToolTipst_perm_dana_detail_dok["English"]["created_date"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["created_date"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["created_by"] = "Created By";
	$fieldToolTipst_perm_dana_detail_dok["English"]["created_by"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["created_by"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_perm_dana_detail_dok["English"]["updated_date"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["updated_date"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_perm_dana_detail_dok["English"]["updated_by"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["updated_by"] = "";
	$fieldLabelst_perm_dana_detail_dok["English"]["id_lahan"] = "ID Lahan";
	$fieldToolTipst_perm_dana_detail_dok["English"]["id_lahan"] = "";
	$placeHolderst_perm_dana_detail_dok["English"]["id_lahan"] = "";
	$pageTitlest_perm_dana_detail_dok["English"]["list"] = "Perm Dana Dokumen";
	if (count($fieldToolTipst_perm_dana_detail_dok["English"]))
		$tdatat_perm_dana_detail_dok[".isUseToolTips"] = true;
}


	$tdatat_perm_dana_detail_dok[".NCSearch"] = true;



$tdatat_perm_dana_detail_dok[".shortTableName"] = "t_perm_dana_detail_dok";
$tdatat_perm_dana_detail_dok[".nSecOptions"] = 0;

$tdatat_perm_dana_detail_dok[".mainTableOwnerID"] = "";
$tdatat_perm_dana_detail_dok[".entityType"] = 1;
$tdatat_perm_dana_detail_dok[".connId"] = "db_lla_at_localhost";


$tdatat_perm_dana_detail_dok[".strOriginalTableName"] = "t_perm_dana_detail";

		 



$tdatat_perm_dana_detail_dok[".showAddInPopup"] = false;

$tdatat_perm_dana_detail_dok[".showEditInPopup"] = false;

$tdatat_perm_dana_detail_dok[".showViewInPopup"] = false;

$tdatat_perm_dana_detail_dok[".listAjax"] = false;
//	temporary
//$tdatat_perm_dana_detail_dok[".listAjax"] = false;

	$tdatat_perm_dana_detail_dok[".audit"] = false;

	$tdatat_perm_dana_detail_dok[".locking"] = false;


$pages = $tdatat_perm_dana_detail_dok[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_perm_dana_detail_dok[".edit"] = true;
	$tdatat_perm_dana_detail_dok[".afterEditAction"] = 1;
	$tdatat_perm_dana_detail_dok[".closePopupAfterEdit"] = 1;
	$tdatat_perm_dana_detail_dok[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_perm_dana_detail_dok[".add"] = true;
$tdatat_perm_dana_detail_dok[".afterAddAction"] = 1;
$tdatat_perm_dana_detail_dok[".closePopupAfterAdd"] = 1;
$tdatat_perm_dana_detail_dok[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_perm_dana_detail_dok[".list"] = true;
}



$tdatat_perm_dana_detail_dok[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_perm_dana_detail_dok[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_perm_dana_detail_dok[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_perm_dana_detail_dok[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_perm_dana_detail_dok[".printFriendly"] = true;
}



$tdatat_perm_dana_detail_dok[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_perm_dana_detail_dok[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_perm_dana_detail_dok[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_perm_dana_detail_dok[".isUseAjaxSuggest"] = true;

$tdatat_perm_dana_detail_dok[".rowHighlite"] = true;



						

$tdatat_perm_dana_detail_dok[".ajaxCodeSnippetAdded"] = false;

$tdatat_perm_dana_detail_dok[".buttonsAdded"] = false;

$tdatat_perm_dana_detail_dok[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_perm_dana_detail_dok[".isUseTimeForSearch"] = false;


$tdatat_perm_dana_detail_dok[".badgeColor"] = "1e90ff";


$tdatat_perm_dana_detail_dok[".allSearchFields"] = array();
$tdatat_perm_dana_detail_dok[".filterFields"] = array();
$tdatat_perm_dana_detail_dok[".requiredSearchFields"] = array();

$tdatat_perm_dana_detail_dok[".googleLikeFields"] = array();
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "id";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "id_p_dana";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "id_lahan";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "param_dok";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "file_dok";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "keterangan";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "created_date";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "created_by";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "updated_date";
$tdatat_perm_dana_detail_dok[".googleLikeFields"][] = "updated_by";



$tdatat_perm_dana_detail_dok[".tableType"] = "list";

$tdatat_perm_dana_detail_dok[".printerPageOrientation"] = 0;
$tdatat_perm_dana_detail_dok[".nPrinterPageScale"] = 100;

$tdatat_perm_dana_detail_dok[".nPrinterSplitRecords"] = 40;

$tdatat_perm_dana_detail_dok[".geocodingEnabled"] = false;










$tdatat_perm_dana_detail_dok[".pageSize"] = 20;

$tdatat_perm_dana_detail_dok[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_perm_dana_detail_dok[".strOrderBy"] = $tstrOrderBy;

$tdatat_perm_dana_detail_dok[".orderindexes"] = array();


$tdatat_perm_dana_detail_dok[".sqlHead"] = "SELECT t_perm_dana_detail.id,  t_perm_dana_detail.id_p_dana,  t_perm_dana.id_lahan,  t_perm_dana_detail.param_dok,  t_perm_dana_detail.file_dok,  t_perm_dana_detail.keterangan,  t_perm_dana_detail.created_date,  t_perm_dana_detail.created_by,  t_perm_dana_detail.updated_date,  t_perm_dana_detail.updated_by";
$tdatat_perm_dana_detail_dok[".sqlFrom"] = "FROM t_perm_dana_detail  INNER JOIN t_perm_dana ON t_perm_dana.id = t_perm_dana_detail.id_p_dana";
$tdatat_perm_dana_detail_dok[".sqlWhereExpr"] = "";
$tdatat_perm_dana_detail_dok[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_perm_dana_detail_dok[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_perm_dana_detail_dok[".arrGroupsPerPage"] = $arrGPP;

$tdatat_perm_dana_detail_dok[".highlightSearchResults"] = true;

$tableKeyst_perm_dana_detail_dok = array();
$tableKeyst_perm_dana_detail_dok[] = "id";
$tdatat_perm_dana_detail_dok[".Keys"] = $tableKeyst_perm_dana_detail_dok;


$tdatat_perm_dana_detail_dok[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["id"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "id";
//	id_p_dana
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_p_dana";
	$fdata["GoodName"] = "id_p_dana";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","id_p_dana");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_p_dana";

		$fdata["sourceSingle"] = "id_p_dana";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.id_p_dana";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["id_p_dana"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "id_p_dana";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_perm_dana";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana.id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["id_lahan"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "id_lahan";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["param_dok"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["file_dok"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["keterangan"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["created_date"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["created_by"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["updated_date"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail_dok","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "t_perm_dana_detail.updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail_dok["updated_by"] = $fdata;
		$tdatat_perm_dana_detail_dok[".searchableFields"][] = "updated_by";


$tables_data["t_perm_dana_detail_dok"]=&$tdatat_perm_dana_detail_dok;
$field_labels["t_perm_dana_detail_dok"] = &$fieldLabelst_perm_dana_detail_dok;
$fieldToolTips["t_perm_dana_detail_dok"] = &$fieldToolTipst_perm_dana_detail_dok;
$placeHolders["t_perm_dana_detail_dok"] = &$placeHolderst_perm_dana_detail_dok;
$page_titles["t_perm_dana_detail_dok"] = &$pageTitlest_perm_dana_detail_dok;


changeTextControlsToDate( "t_perm_dana_detail_dok" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_perm_dana_detail_dok"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_perm_dana_detail_dok"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_dokumentasi";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_dokumentasi";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_dokumentasi";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_perm_dana_detail_dok"][0] = $masterParams;
				$masterTablesData["t_perm_dana_detail_dok"][0]["masterKeys"] = array();
	$masterTablesData["t_perm_dana_detail_dok"][0]["masterKeys"][]="id_lahan";
				$masterTablesData["t_perm_dana_detail_dok"][0]["detailKeys"] = array();
	$masterTablesData["t_perm_dana_detail_dok"][0]["detailKeys"][]="id_lahan";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_perm_dana_detail_dok()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "t_perm_dana_detail.id,  t_perm_dana_detail.id_p_dana,  t_perm_dana.id_lahan,  t_perm_dana_detail.param_dok,  t_perm_dana_detail.file_dok,  t_perm_dana_detail.keterangan,  t_perm_dana_detail.created_date,  t_perm_dana_detail.created_by,  t_perm_dana_detail.updated_date,  t_perm_dana_detail.updated_by";
$proto0["m_strFrom"] = "FROM t_perm_dana_detail  INNER JOIN t_perm_dana ON t_perm_dana.id = t_perm_dana_detail.id_p_dana";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto6["m_sql"] = "t_perm_dana_detail.id";
$proto6["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_p_dana",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto8["m_sql"] = "t_perm_dana_detail.id_p_dana";
$proto8["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto10["m_sql"] = "t_perm_dana.id_lahan";
$proto10["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto12["m_sql"] = "t_perm_dana_detail.param_dok";
$proto12["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto14["m_sql"] = "t_perm_dana_detail.file_dok";
$proto14["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto16["m_sql"] = "t_perm_dana_detail.keterangan";
$proto16["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto18["m_sql"] = "t_perm_dana_detail.created_date";
$proto18["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto20["m_sql"] = "t_perm_dana_detail.created_by";
$proto20["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto22["m_sql"] = "t_perm_dana_detail.updated_date";
$proto22["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto24["m_sql"] = "t_perm_dana_detail.updated_by";
$proto24["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto26=array();
$proto26["m_link"] = "SQLL_MAIN";
			$proto27=array();
$proto27["m_strName"] = "t_perm_dana_detail";
$proto27["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "id";
$proto27["m_columns"][] = "id_p_dana";
$proto27["m_columns"][] = "param_dok";
$proto27["m_columns"][] = "file_dok";
$proto27["m_columns"][] = "keterangan";
$proto27["m_columns"][] = "created_date";
$proto27["m_columns"][] = "created_by";
$proto27["m_columns"][] = "updated_date";
$proto27["m_columns"][] = "updated_by";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "t_perm_dana_detail";
$proto26["m_alias"] = "";
$proto26["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
												$proto30=array();
$proto30["m_link"] = "SQLL_INNERJOIN";
			$proto31=array();
$proto31["m_strName"] = "t_perm_dana";
$proto31["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto31["m_columns"] = array();
$proto31["m_columns"][] = "id";
$proto31["m_columns"][] = "id_lahan";
$proto31["m_columns"][] = "start_date_dp";
$proto31["m_columns"][] = "due_date_dp";
$proto31["m_columns"][] = "start_date_full";
$proto31["m_columns"][] = "due_date_full";
$proto31["m_columns"][] = "pic";
$proto31["m_columns"][] = "keterangan";
$proto31["m_columns"][] = "keterangan_bayar";
$proto31["m_columns"][] = "close_full_by";
$proto31["m_columns"][] = "close_full_date";
$proto31["m_columns"][] = "tipe";
$proto31["m_columns"][] = "dp";
$proto31["m_columns"][] = "sisa_pembayaran";
$proto31["m_columns"][] = "biaya_admin";
$proto31["m_columns"][] = "status";
$proto31["m_columns"][] = "status_full";
$proto31["m_columns"][] = "status_badmin";
$proto31["m_columns"][] = "close_badmin_by";
$proto31["m_columns"][] = "close_badmin_date";
$proto31["m_columns"][] = "close_dp_by";
$proto31["m_columns"][] = "close_dp_date";
$proto31["m_columns"][] = "created_date";
$proto31["m_columns"][] = "created_by";
$proto31["m_columns"][] = "updated_date";
$proto31["m_columns"][] = "updated_by";
$obj = new SQLTable($proto31);

$proto30["m_table"] = $obj;
$proto30["m_sql"] = "INNER JOIN t_perm_dana ON t_perm_dana.id = t_perm_dana_detail.id_p_dana";
$proto30["m_alias"] = "";
$proto30["m_srcTableName"] = "t_perm_dana_detail_dok";
$proto32=array();
$proto32["m_sql"] = "t_perm_dana.id = t_perm_dana_detail.id_p_dana";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana",
	"m_srcTableName" => "t_perm_dana_detail_dok"
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "= t_perm_dana_detail.id_p_dana";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto30["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto30);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_perm_dana_detail_dok";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_perm_dana_detail_dok = createSqlQuery_t_perm_dana_detail_dok();


	
					
;

										

$tdatat_perm_dana_detail_dok[".sqlquery"] = $queryData_t_perm_dana_detail_dok;



include_once(getabspath("include/t_perm_dana_detail_dok_events.php"));
$tdatat_perm_dana_detail_dok[".hasEvents"] = true;

?>