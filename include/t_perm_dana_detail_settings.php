<?php
$tdatat_perm_dana_detail = array();
$tdatat_perm_dana_detail[".searchableFields"] = array();
$tdatat_perm_dana_detail[".ShortName"] = "t_perm_dana_detail";
$tdatat_perm_dana_detail[".OwnerID"] = "";
$tdatat_perm_dana_detail[".OriginalTable"] = "t_perm_dana_detail";


$tdatat_perm_dana_detail[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatat_perm_dana_detail[".originalPagesByType"] = $tdatat_perm_dana_detail[".pagesByType"];
$tdatat_perm_dana_detail[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatat_perm_dana_detail[".originalPages"] = $tdatat_perm_dana_detail[".pages"];
$tdatat_perm_dana_detail[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatat_perm_dana_detail[".originalDefaultPages"] = $tdatat_perm_dana_detail[".defaultPages"];

//	field labels
$fieldLabelst_perm_dana_detail = array();
$fieldToolTipst_perm_dana_detail = array();
$pageTitlest_perm_dana_detail = array();
$placeHolderst_perm_dana_detail = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_perm_dana_detail["English"] = array();
	$fieldToolTipst_perm_dana_detail["English"] = array();
	$placeHolderst_perm_dana_detail["English"] = array();
	$pageTitlest_perm_dana_detail["English"] = array();
	$fieldLabelst_perm_dana_detail["English"]["id"] = "Id";
	$fieldToolTipst_perm_dana_detail["English"]["id"] = "";
	$placeHolderst_perm_dana_detail["English"]["id"] = "";
	$fieldLabelst_perm_dana_detail["English"]["id_p_dana"] = "Id P Dana";
	$fieldToolTipst_perm_dana_detail["English"]["id_p_dana"] = "";
	$placeHolderst_perm_dana_detail["English"]["id_p_dana"] = "";
	$fieldLabelst_perm_dana_detail["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_perm_dana_detail["English"]["param_dok"] = "";
	$placeHolderst_perm_dana_detail["English"]["param_dok"] = "";
	$fieldLabelst_perm_dana_detail["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_perm_dana_detail["English"]["file_dok"] = "";
	$placeHolderst_perm_dana_detail["English"]["file_dok"] = "";
	$fieldLabelst_perm_dana_detail["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_perm_dana_detail["English"]["keterangan"] = "";
	$placeHolderst_perm_dana_detail["English"]["keterangan"] = "";
	$fieldLabelst_perm_dana_detail["English"]["created_date"] = "Created Date";
	$fieldToolTipst_perm_dana_detail["English"]["created_date"] = "";
	$placeHolderst_perm_dana_detail["English"]["created_date"] = "";
	$fieldLabelst_perm_dana_detail["English"]["created_by"] = "Created By";
	$fieldToolTipst_perm_dana_detail["English"]["created_by"] = "";
	$placeHolderst_perm_dana_detail["English"]["created_by"] = "";
	$fieldLabelst_perm_dana_detail["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_perm_dana_detail["English"]["updated_date"] = "";
	$placeHolderst_perm_dana_detail["English"]["updated_date"] = "";
	$fieldLabelst_perm_dana_detail["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_perm_dana_detail["English"]["updated_by"] = "";
	$placeHolderst_perm_dana_detail["English"]["updated_by"] = "";
	$pageTitlest_perm_dana_detail["English"]["view"] = "Permintaan Dana Dok {%id}";
	$pageTitlest_perm_dana_detail["English"]["search"] = "Permintaan Dana Dok, Advanced search";
	if (count($fieldToolTipst_perm_dana_detail["English"]))
		$tdatat_perm_dana_detail[".isUseToolTips"] = true;
}


	$tdatat_perm_dana_detail[".NCSearch"] = true;



$tdatat_perm_dana_detail[".shortTableName"] = "t_perm_dana_detail";
$tdatat_perm_dana_detail[".nSecOptions"] = 0;

$tdatat_perm_dana_detail[".mainTableOwnerID"] = "";
$tdatat_perm_dana_detail[".entityType"] = 0;
$tdatat_perm_dana_detail[".connId"] = "db_lla_at_localhost";


$tdatat_perm_dana_detail[".strOriginalTableName"] = "t_perm_dana_detail";

		 



$tdatat_perm_dana_detail[".showAddInPopup"] = false;

$tdatat_perm_dana_detail[".showEditInPopup"] = false;

$tdatat_perm_dana_detail[".showViewInPopup"] = false;

$tdatat_perm_dana_detail[".listAjax"] = false;
//	temporary
//$tdatat_perm_dana_detail[".listAjax"] = false;

	$tdatat_perm_dana_detail[".audit"] = false;

	$tdatat_perm_dana_detail[".locking"] = false;


$pages = $tdatat_perm_dana_detail[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_perm_dana_detail[".edit"] = true;
	$tdatat_perm_dana_detail[".afterEditAction"] = 1;
	$tdatat_perm_dana_detail[".closePopupAfterEdit"] = 1;
	$tdatat_perm_dana_detail[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_perm_dana_detail[".add"] = true;
$tdatat_perm_dana_detail[".afterAddAction"] = 1;
$tdatat_perm_dana_detail[".closePopupAfterAdd"] = 1;
$tdatat_perm_dana_detail[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_perm_dana_detail[".list"] = true;
}



$tdatat_perm_dana_detail[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_perm_dana_detail[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_perm_dana_detail[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_perm_dana_detail[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_perm_dana_detail[".printFriendly"] = true;
}



$tdatat_perm_dana_detail[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_perm_dana_detail[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_perm_dana_detail[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_perm_dana_detail[".isUseAjaxSuggest"] = true;

$tdatat_perm_dana_detail[".rowHighlite"] = true;



						

$tdatat_perm_dana_detail[".ajaxCodeSnippetAdded"] = false;

$tdatat_perm_dana_detail[".buttonsAdded"] = false;

$tdatat_perm_dana_detail[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_perm_dana_detail[".isUseTimeForSearch"] = false;


$tdatat_perm_dana_detail[".badgeColor"] = "daa520";


$tdatat_perm_dana_detail[".allSearchFields"] = array();
$tdatat_perm_dana_detail[".filterFields"] = array();
$tdatat_perm_dana_detail[".requiredSearchFields"] = array();

$tdatat_perm_dana_detail[".googleLikeFields"] = array();
$tdatat_perm_dana_detail[".googleLikeFields"][] = "id";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "id_p_dana";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "param_dok";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "file_dok";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "keterangan";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "created_date";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "created_by";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "updated_date";
$tdatat_perm_dana_detail[".googleLikeFields"][] = "updated_by";



$tdatat_perm_dana_detail[".tableType"] = "list";

$tdatat_perm_dana_detail[".printerPageOrientation"] = 0;
$tdatat_perm_dana_detail[".nPrinterPageScale"] = 100;

$tdatat_perm_dana_detail[".nPrinterSplitRecords"] = 40;

$tdatat_perm_dana_detail[".geocodingEnabled"] = false;










$tdatat_perm_dana_detail[".pageSize"] = 20;

$tdatat_perm_dana_detail[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_perm_dana_detail[".strOrderBy"] = $tstrOrderBy;

$tdatat_perm_dana_detail[".orderindexes"] = array();


$tdatat_perm_dana_detail[".sqlHead"] = "SELECT id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_perm_dana_detail[".sqlFrom"] = "FROM t_perm_dana_detail";
$tdatat_perm_dana_detail[".sqlWhereExpr"] = "";
$tdatat_perm_dana_detail[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_perm_dana_detail[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_perm_dana_detail[".arrGroupsPerPage"] = $arrGPP;

$tdatat_perm_dana_detail[".highlightSearchResults"] = true;

$tableKeyst_perm_dana_detail = array();
$tableKeyst_perm_dana_detail[] = "id";
$tdatat_perm_dana_detail[".Keys"] = $tableKeyst_perm_dana_detail;


$tdatat_perm_dana_detail[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["id"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "id";
//	id_p_dana
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_p_dana";
	$fdata["GoodName"] = "id_p_dana";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","id_p_dana");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_p_dana";

		$fdata["sourceSingle"] = "id_p_dana";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_p_dana";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["id_p_dana"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "id_p_dana";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["param_dok"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = array();
			$edata["acceptFileTypes"][] = strtoupper("bmp");
						$edata["acceptFileTypesHtml"] = ".bmp";
			$edata["acceptFileTypes"][] = strtoupper("doc");
						$edata["acceptFileTypesHtml"] .= ",.doc";
			$edata["acceptFileTypes"][] = strtoupper("docx");
						$edata["acceptFileTypesHtml"] .= ",.docx";
			$edata["acceptFileTypes"][] = strtoupper("gif");
						$edata["acceptFileTypesHtml"] .= ",.gif";
			$edata["acceptFileTypes"][] = strtoupper("jpeg");
						$edata["acceptFileTypesHtml"] .= ",.jpeg";
			$edata["acceptFileTypes"][] = strtoupper("jpg");
						$edata["acceptFileTypesHtml"] .= ",.jpg";
			$edata["acceptFileTypes"][] = strtoupper("pdf");
						$edata["acceptFileTypesHtml"] .= ",.pdf";
			$edata["acceptFileTypes"][] = strtoupper("png");
						$edata["acceptFileTypesHtml"] .= ",.png";
			$edata["acceptFileTypes"][] = strtoupper("xls");
						$edata["acceptFileTypesHtml"] .= ",.xls";
			$edata["acceptFileTypes"][] = strtoupper("xlsx");
						$edata["acceptFileTypesHtml"] .= ",.xlsx";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["file_dok"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["keterangan"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["created_date"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["created_by"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["updated_date"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_perm_dana_detail";
	$fdata["Label"] = GetFieldLabel("t_perm_dana_detail","updated_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_perm_dana_detail["updated_by"] = $fdata;
		$tdatat_perm_dana_detail[".searchableFields"][] = "updated_by";


$tables_data["t_perm_dana_detail"]=&$tdatat_perm_dana_detail;
$field_labels["t_perm_dana_detail"] = &$fieldLabelst_perm_dana_detail;
$fieldToolTips["t_perm_dana_detail"] = &$fieldToolTipst_perm_dana_detail;
$placeHolders["t_perm_dana_detail"] = &$placeHolderst_perm_dana_detail;
$page_titles["t_perm_dana_detail"] = &$pageTitlest_perm_dana_detail;


changeTextControlsToDate( "t_perm_dana_detail" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_perm_dana_detail"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_perm_dana_detail"] = array();



	
	//if !@t.bReportCrossTab
			$strOriginalDetailsTable="t_perm_dana";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="t_perm_dana";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "t_perm_dana";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["t_perm_dana_detail"][0] = $masterParams;
				$masterTablesData["t_perm_dana_detail"][0]["masterKeys"] = array();
	$masterTablesData["t_perm_dana_detail"][0]["masterKeys"][]="id";
				$masterTablesData["t_perm_dana_detail"][0]["detailKeys"] = array();
	$masterTablesData["t_perm_dana_detail"][0]["detailKeys"][]="id_p_dana";
		
	//endif
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_perm_dana_detail()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_p_dana,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_perm_dana_detail";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "t_perm_dana_detail";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_p_dana",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto8["m_sql"] = "id_p_dana";
$proto8["m_srcTableName"] = "t_perm_dana_detail";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto10["m_sql"] = "param_dok";
$proto10["m_srcTableName"] = "t_perm_dana_detail";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto12["m_sql"] = "file_dok";
$proto12["m_srcTableName"] = "t_perm_dana_detail";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto14["m_sql"] = "keterangan";
$proto14["m_srcTableName"] = "t_perm_dana_detail";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto16["m_sql"] = "created_date";
$proto16["m_srcTableName"] = "t_perm_dana_detail";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto18["m_sql"] = "created_by";
$proto18["m_srcTableName"] = "t_perm_dana_detail";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto20["m_sql"] = "updated_date";
$proto20["m_srcTableName"] = "t_perm_dana_detail";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_perm_dana_detail",
	"m_srcTableName" => "t_perm_dana_detail"
));

$proto22["m_sql"] = "updated_by";
$proto22["m_srcTableName"] = "t_perm_dana_detail";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "t_perm_dana_detail";
$proto25["m_srcTableName"] = "t_perm_dana_detail";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "id";
$proto25["m_columns"][] = "id_p_dana";
$proto25["m_columns"][] = "param_dok";
$proto25["m_columns"][] = "file_dok";
$proto25["m_columns"][] = "keterangan";
$proto25["m_columns"][] = "created_date";
$proto25["m_columns"][] = "created_by";
$proto25["m_columns"][] = "updated_date";
$proto25["m_columns"][] = "updated_by";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "t_perm_dana_detail";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "t_perm_dana_detail";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_perm_dana_detail";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_perm_dana_detail = createSqlQuery_t_perm_dana_detail();


	
					
;

									

$tdatat_perm_dana_detail[".sqlquery"] = $queryData_t_perm_dana_detail;



include_once(getabspath("include/t_perm_dana_detail_events.php"));
$tdatat_perm_dana_detail[".hasEvents"] = true;

?>