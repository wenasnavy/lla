<?php
$tdatat_regis_detail_backdate = array();
$tdatat_regis_detail_backdate[".searchableFields"] = array();
$tdatat_regis_detail_backdate[".ShortName"] = "t_regis_detail_backdate";
$tdatat_regis_detail_backdate[".OwnerID"] = "";
$tdatat_regis_detail_backdate[".OriginalTable"] = "t_pengukuran_percil";


$tdatat_regis_detail_backdate[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"]}" );
$tdatat_regis_detail_backdate[".originalPagesByType"] = $tdatat_regis_detail_backdate[".pagesByType"];
$tdatat_regis_detail_backdate[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"]}" ) );
$tdatat_regis_detail_backdate[".originalPages"] = $tdatat_regis_detail_backdate[".pages"];
$tdatat_regis_detail_backdate[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\"}" );
$tdatat_regis_detail_backdate[".originalDefaultPages"] = $tdatat_regis_detail_backdate[".defaultPages"];

//	field labels
$fieldLabelst_regis_detail_backdate = array();
$fieldToolTipst_regis_detail_backdate = array();
$pageTitlest_regis_detail_backdate = array();
$placeHolderst_regis_detail_backdate = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelst_regis_detail_backdate["English"] = array();
	$fieldToolTipst_regis_detail_backdate["English"] = array();
	$placeHolderst_regis_detail_backdate["English"] = array();
	$pageTitlest_regis_detail_backdate["English"] = array();
	$fieldLabelst_regis_detail_backdate["English"]["id"] = "Id";
	$fieldToolTipst_regis_detail_backdate["English"]["id"] = "";
	$placeHolderst_regis_detail_backdate["English"]["id"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["param_dok"] = "Param Dok";
	$fieldToolTipst_regis_detail_backdate["English"]["param_dok"] = "";
	$placeHolderst_regis_detail_backdate["English"]["param_dok"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["file_dok"] = "File Dok";
	$fieldToolTipst_regis_detail_backdate["English"]["file_dok"] = "";
	$placeHolderst_regis_detail_backdate["English"]["file_dok"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["keterangan"] = "Keterangan";
	$fieldToolTipst_regis_detail_backdate["English"]["keterangan"] = "";
	$placeHolderst_regis_detail_backdate["English"]["keterangan"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["created_date"] = "Created Date";
	$fieldToolTipst_regis_detail_backdate["English"]["created_date"] = "";
	$placeHolderst_regis_detail_backdate["English"]["created_date"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["created_by"] = "Created By";
	$fieldToolTipst_regis_detail_backdate["English"]["created_by"] = "";
	$placeHolderst_regis_detail_backdate["English"]["created_by"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["updated_date"] = "Updated Date";
	$fieldToolTipst_regis_detail_backdate["English"]["updated_date"] = "";
	$placeHolderst_regis_detail_backdate["English"]["updated_date"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["updated_by"] = "Updated By";
	$fieldToolTipst_regis_detail_backdate["English"]["updated_by"] = "";
	$placeHolderst_regis_detail_backdate["English"]["updated_by"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["no_register"] = "No Register";
	$fieldToolTipst_regis_detail_backdate["English"]["no_register"] = "";
	$placeHolderst_regis_detail_backdate["English"]["no_register"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["percil_name"] = "Percil Name";
	$fieldToolTipst_regis_detail_backdate["English"]["percil_name"] = "";
	$placeHolderst_regis_detail_backdate["English"]["percil_name"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["urutan"] = "Urutan";
	$fieldToolTipst_regis_detail_backdate["English"]["urutan"] = "";
	$placeHolderst_regis_detail_backdate["English"]["urutan"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["ukuran_real"] = "Ukuran Real";
	$fieldToolTipst_regis_detail_backdate["English"]["ukuran_real"] = "";
	$placeHolderst_regis_detail_backdate["English"]["ukuran_real"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["id_ukur"] = "Id Ukur";
	$fieldToolTipst_regis_detail_backdate["English"]["id_ukur"] = "";
	$placeHolderst_regis_detail_backdate["English"]["id_ukur"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["id_lahan"] = "Nama Lahan";
	$fieldToolTipst_regis_detail_backdate["English"]["id_lahan"] = "";
	$placeHolderst_regis_detail_backdate["English"]["id_lahan"] = "";
	$fieldLabelst_regis_detail_backdate["English"]["percil_date"] = "Percil Date";
	$fieldToolTipst_regis_detail_backdate["English"]["percil_date"] = "";
	$placeHolderst_regis_detail_backdate["English"]["percil_date"] = "";
	$pageTitlest_regis_detail_backdate["English"]["add"] = "Percil Backdate, Add new";
	$pageTitlest_regis_detail_backdate["English"]["print"] = "Percil Backdate";
	$pageTitlest_regis_detail_backdate["English"]["search"] = "Regis Detail Backdate, Advanced search";
	if (count($fieldToolTipst_regis_detail_backdate["English"]))
		$tdatat_regis_detail_backdate[".isUseToolTips"] = true;
}


	$tdatat_regis_detail_backdate[".NCSearch"] = true;



$tdatat_regis_detail_backdate[".shortTableName"] = "t_regis_detail_backdate";
$tdatat_regis_detail_backdate[".nSecOptions"] = 0;

$tdatat_regis_detail_backdate[".mainTableOwnerID"] = "";
$tdatat_regis_detail_backdate[".entityType"] = 1;
$tdatat_regis_detail_backdate[".connId"] = "db_lla_at_localhost";


$tdatat_regis_detail_backdate[".strOriginalTableName"] = "t_pengukuran_percil";

		 



$tdatat_regis_detail_backdate[".showAddInPopup"] = false;

$tdatat_regis_detail_backdate[".showEditInPopup"] = false;

$tdatat_regis_detail_backdate[".showViewInPopup"] = false;

$tdatat_regis_detail_backdate[".listAjax"] = false;
//	temporary
//$tdatat_regis_detail_backdate[".listAjax"] = false;

	$tdatat_regis_detail_backdate[".audit"] = false;

	$tdatat_regis_detail_backdate[".locking"] = false;


$pages = $tdatat_regis_detail_backdate[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatat_regis_detail_backdate[".edit"] = true;
	$tdatat_regis_detail_backdate[".afterEditAction"] = 1;
	$tdatat_regis_detail_backdate[".closePopupAfterEdit"] = 1;
	$tdatat_regis_detail_backdate[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatat_regis_detail_backdate[".add"] = true;
$tdatat_regis_detail_backdate[".afterAddAction"] = 1;
$tdatat_regis_detail_backdate[".closePopupAfterAdd"] = 1;
$tdatat_regis_detail_backdate[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatat_regis_detail_backdate[".list"] = true;
}



$tdatat_regis_detail_backdate[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatat_regis_detail_backdate[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatat_regis_detail_backdate[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatat_regis_detail_backdate[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatat_regis_detail_backdate[".printFriendly"] = true;
}



$tdatat_regis_detail_backdate[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatat_regis_detail_backdate[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatat_regis_detail_backdate[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatat_regis_detail_backdate[".isUseAjaxSuggest"] = true;

$tdatat_regis_detail_backdate[".rowHighlite"] = true;



			

$tdatat_regis_detail_backdate[".ajaxCodeSnippetAdded"] = false;

$tdatat_regis_detail_backdate[".buttonsAdded"] = false;

$tdatat_regis_detail_backdate[".addPageEvents"] = false;

// use timepicker for search panel
$tdatat_regis_detail_backdate[".isUseTimeForSearch"] = false;


$tdatat_regis_detail_backdate[".badgeColor"] = "778899";


$tdatat_regis_detail_backdate[".allSearchFields"] = array();
$tdatat_regis_detail_backdate[".filterFields"] = array();
$tdatat_regis_detail_backdate[".requiredSearchFields"] = array();

$tdatat_regis_detail_backdate[".googleLikeFields"] = array();
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "id";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "id_ukur";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "id_lahan";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "no_register";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "percil_name";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "percil_date";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "urutan";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "ukuran_real";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "param_dok";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "file_dok";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "keterangan";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "created_date";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "created_by";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "updated_date";
$tdatat_regis_detail_backdate[".googleLikeFields"][] = "updated_by";



$tdatat_regis_detail_backdate[".tableType"] = "list";

$tdatat_regis_detail_backdate[".printerPageOrientation"] = 0;
$tdatat_regis_detail_backdate[".nPrinterPageScale"] = 100;

$tdatat_regis_detail_backdate[".nPrinterSplitRecords"] = 40;

$tdatat_regis_detail_backdate[".geocodingEnabled"] = false;










$tdatat_regis_detail_backdate[".pageSize"] = 20;

$tdatat_regis_detail_backdate[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatat_regis_detail_backdate[".strOrderBy"] = $tstrOrderBy;

$tdatat_regis_detail_backdate[".orderindexes"] = array();


$tdatat_regis_detail_backdate[".sqlHead"] = "SELECT id,  	id_ukur,  	id_lahan,  	no_register,  	percil_name,  	percil_date,  	urutan,  	ukuran_real,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$tdatat_regis_detail_backdate[".sqlFrom"] = "FROM t_pengukuran_percil";
$tdatat_regis_detail_backdate[".sqlWhereExpr"] = "(date_format(percil_date, '%Y-%M-%D') < date_format(now(),'%Y-%M-%D'))";
$tdatat_regis_detail_backdate[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatat_regis_detail_backdate[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatat_regis_detail_backdate[".arrGroupsPerPage"] = $arrGPP;

$tdatat_regis_detail_backdate[".highlightSearchResults"] = true;

$tableKeyst_regis_detail_backdate = array();
$tdatat_regis_detail_backdate[".Keys"] = $tableKeyst_regis_detail_backdate;


$tdatat_regis_detail_backdate[".hideMobileList"] = array();




//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","id");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "id";

		$fdata["sourceSingle"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["id"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "id";
//	id_ukur
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_ukur";
	$fdata["GoodName"] = "id_ukur";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","id_ukur");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_ukur";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_ukur";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["id_ukur"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "id_ukur";
//	id_lahan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "id_lahan";
	$fdata["GoodName"] = "id_lahan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","id_lahan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "id_lahan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_lahan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["id_lahan"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "id_lahan";
//	no_register
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "no_register";
	$fdata["GoodName"] = "no_register";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","no_register");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "no_register";

		$fdata["sourceSingle"] = "no_register";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "no_register";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["no_register"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "no_register";
//	percil_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "percil_name";
	$fdata["GoodName"] = "percil_name";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","percil_name");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "percil_name";

		$fdata["sourceSingle"] = "percil_name";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "percil_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["percil_name"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "percil_name";
//	percil_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "percil_date";
	$fdata["GoodName"] = "percil_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","percil_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "percil_date";

		$fdata["sourceSingle"] = "percil_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "percil_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["percil_date"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "percil_date";
//	urutan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "urutan";
	$fdata["GoodName"] = "urutan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","urutan");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "urutan";

		$fdata["sourceSingle"] = "urutan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "urutan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["urutan"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "urutan";
//	ukuran_real
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ukuran_real";
	$fdata["GoodName"] = "ukuran_real";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","ukuran_real");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ukuran_real";

		$fdata["sourceSingle"] = "ukuran_real";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ukuran_real";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["ukuran_real"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "ukuran_real";
//	param_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "param_dok";
	$fdata["GoodName"] = "param_dok";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","param_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "param_dok";

		$fdata["sourceSingle"] = "param_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "param_dok";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "code_list_doc";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Code";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Description";

	

	
	$edata["LookupOrderBy"] = "OrderNo";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["param_dok"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "param_dok";
//	file_dok
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "file_dok";
	$fdata["GoodName"] = "file_dok";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","file_dok");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "file_dok";

		$fdata["sourceSingle"] = "file_dok";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_dok";

	
	
		$fdata["UploadCodeExpression"] = true;

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
					
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = array();
			$edata["acceptFileTypes"][] = strtoupper("bmp");
						$edata["acceptFileTypesHtml"] = ".bmp";
			$edata["acceptFileTypes"][] = strtoupper("doc");
						$edata["acceptFileTypesHtml"] .= ",.doc";
			$edata["acceptFileTypes"][] = strtoupper("docx");
						$edata["acceptFileTypesHtml"] .= ",.docx";
			$edata["acceptFileTypes"][] = strtoupper("gif");
						$edata["acceptFileTypesHtml"] .= ",.gif";
			$edata["acceptFileTypes"][] = strtoupper("jpeg");
						$edata["acceptFileTypesHtml"] .= ",.jpeg";
			$edata["acceptFileTypes"][] = strtoupper("jpg");
						$edata["acceptFileTypesHtml"] .= ",.jpg";
			$edata["acceptFileTypes"][] = strtoupper("pdf");
						$edata["acceptFileTypesHtml"] .= ",.pdf";
			$edata["acceptFileTypes"][] = strtoupper("png");
						$edata["acceptFileTypesHtml"] .= ",.png";
			$edata["acceptFileTypes"][] = strtoupper("xls");
						$edata["acceptFileTypesHtml"] .= ",.xls";
			$edata["acceptFileTypes"][] = strtoupper("xlsx");
						$edata["acceptFileTypesHtml"] .= ",.xlsx";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["file_dok"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "file_dok";
//	keterangan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "keterangan";
	$fdata["GoodName"] = "keterangan";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","keterangan");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "keterangan";

		$fdata["sourceSingle"] = "keterangan";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "keterangan";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
		$edata["UseRTE"] = true;

	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["keterangan"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "keterangan";
//	created_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "created_date";
	$fdata["GoodName"] = "created_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","created_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "created_date";

		$fdata["sourceSingle"] = "created_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["created_date"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "created_date";
//	created_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "created_by";
	$fdata["GoodName"] = "created_by";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","created_by");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "created_by";

		$fdata["sourceSingle"] = "created_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "created_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["created_by"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "created_by";
//	updated_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "updated_date";
	$fdata["GoodName"] = "updated_date";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","updated_date");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_date";

		$fdata["sourceSingle"] = "updated_date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["updated_date"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "updated_date";
//	updated_by
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "updated_by";
	$fdata["GoodName"] = "updated_by";
	$fdata["ownerTable"] = "t_pengukuran_percil";
	$fdata["Label"] = GetFieldLabel("t_regis_detail_backdate","updated_by");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "updated_by";

		$fdata["sourceSingle"] = "updated_by";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "updated_by";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
		
		
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 100;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatat_regis_detail_backdate["updated_by"] = $fdata;
		$tdatat_regis_detail_backdate[".searchableFields"][] = "updated_by";


$tables_data["t_regis_detail_backdate"]=&$tdatat_regis_detail_backdate;
$field_labels["t_regis_detail_backdate"] = &$fieldLabelst_regis_detail_backdate;
$fieldToolTips["t_regis_detail_backdate"] = &$fieldToolTipst_regis_detail_backdate;
$placeHolders["t_regis_detail_backdate"] = &$placeHolderst_regis_detail_backdate;
$page_titles["t_regis_detail_backdate"] = &$pageTitlest_regis_detail_backdate;


changeTextControlsToDate( "t_regis_detail_backdate" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["t_regis_detail_backdate"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["t_regis_detail_backdate"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_t_regis_detail_backdate()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	id_ukur,  	id_lahan,  	no_register,  	percil_name,  	percil_date,  	urutan,  	ukuran_real,  	param_dok,  	file_dok,  	keterangan,  	created_date,  	created_by,  	updated_date,  	updated_by";
$proto0["m_strFrom"] = "FROM t_pengukuran_percil";
$proto0["m_strWhere"] = "(date_format(percil_date, '%Y-%M-%D') < date_format(now(),'%Y-%M-%D'))";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "date_format(percil_date, '%Y-%M-%D') < date_format(now(),'%Y-%M-%D')";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$proto3=array();
$proto3["m_functiontype"] = "SQLF_CUSTOM";
$proto3["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "percil_date"
));

$proto3["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "'%Y-%M-%D'"
));

$proto3["m_arguments"][]=$obj;
$proto3["m_strFunctionName"] = "date_format";
$obj = new SQLFunctionCall($proto3);

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "< date_format(now(),'%Y-%M-%D')";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto6=array();
$proto6["m_sql"] = "";
$proto6["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto6["m_column"]=$obj;
$proto6["m_contained"] = array();
$proto6["m_strCase"] = "";
$proto6["m_havingmode"] = false;
$proto6["m_inBrackets"] = false;
$proto6["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto6);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto8["m_sql"] = "id";
$proto8["m_srcTableName"] = "t_regis_detail_backdate";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "id_ukur",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto10["m_sql"] = "id_ukur";
$proto10["m_srcTableName"] = "t_regis_detail_backdate";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "id_lahan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto12["m_sql"] = "id_lahan";
$proto12["m_srcTableName"] = "t_regis_detail_backdate";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "no_register",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto14["m_sql"] = "no_register";
$proto14["m_srcTableName"] = "t_regis_detail_backdate";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "percil_name",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto16["m_sql"] = "percil_name";
$proto16["m_srcTableName"] = "t_regis_detail_backdate";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "percil_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto18["m_sql"] = "percil_date";
$proto18["m_srcTableName"] = "t_regis_detail_backdate";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "urutan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto20["m_sql"] = "urutan";
$proto20["m_srcTableName"] = "t_regis_detail_backdate";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ukuran_real",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto22["m_sql"] = "ukuran_real";
$proto22["m_srcTableName"] = "t_regis_detail_backdate";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "param_dok",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto24["m_sql"] = "param_dok";
$proto24["m_srcTableName"] = "t_regis_detail_backdate";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "file_dok",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto26["m_sql"] = "file_dok";
$proto26["m_srcTableName"] = "t_regis_detail_backdate";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "keterangan",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto28["m_sql"] = "keterangan";
$proto28["m_srcTableName"] = "t_regis_detail_backdate";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "created_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto30["m_sql"] = "created_date";
$proto30["m_srcTableName"] = "t_regis_detail_backdate";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "created_by",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto32["m_sql"] = "created_by";
$proto32["m_srcTableName"] = "t_regis_detail_backdate";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_date",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto34["m_sql"] = "updated_date";
$proto34["m_srcTableName"] = "t_regis_detail_backdate";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "updated_by",
	"m_strTable" => "t_pengukuran_percil",
	"m_srcTableName" => "t_regis_detail_backdate"
));

$proto36["m_sql"] = "updated_by";
$proto36["m_srcTableName"] = "t_regis_detail_backdate";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto38=array();
$proto38["m_link"] = "SQLL_MAIN";
			$proto39=array();
$proto39["m_strName"] = "t_pengukuran_percil";
$proto39["m_srcTableName"] = "t_regis_detail_backdate";
$proto39["m_columns"] = array();
$proto39["m_columns"][] = "id";
$proto39["m_columns"][] = "id_ukur";
$proto39["m_columns"][] = "id_lahan";
$proto39["m_columns"][] = "no_register";
$proto39["m_columns"][] = "percil_name";
$proto39["m_columns"][] = "percil_date";
$proto39["m_columns"][] = "urutan";
$proto39["m_columns"][] = "ukuran_real";
$proto39["m_columns"][] = "param_dok";
$proto39["m_columns"][] = "file_dok";
$proto39["m_columns"][] = "keterangan";
$proto39["m_columns"][] = "created_date";
$proto39["m_columns"][] = "created_by";
$proto39["m_columns"][] = "updated_date";
$proto39["m_columns"][] = "updated_by";
$obj = new SQLTable($proto39);

$proto38["m_table"] = $obj;
$proto38["m_sql"] = "t_pengukuran_percil";
$proto38["m_alias"] = "";
$proto38["m_srcTableName"] = "t_regis_detail_backdate";
$proto40=array();
$proto40["m_sql"] = "";
$proto40["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto40["m_column"]=$obj;
$proto40["m_contained"] = array();
$proto40["m_strCase"] = "";
$proto40["m_havingmode"] = false;
$proto40["m_inBrackets"] = false;
$proto40["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto40);

$proto38["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto38);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="t_regis_detail_backdate";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_t_regis_detail_backdate = createSqlQuery_t_regis_detail_backdate();


	
					
;

															

$tdatat_regis_detail_backdate[".sqlquery"] = $queryData_t_regis_detail_backdate;



include_once(getabspath("include/t_regis_detail_backdate_events.php"));
$tdatat_regis_detail_backdate[".hasEvents"] = true;

?>